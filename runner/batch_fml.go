package runner

import (
	"fmt"
	"path/filepath"
	"sync"
	"time"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/util"
)

// BatchFmls
type BatchFmls struct {
	Items []BatchFml
}

// NewBatchFmls
func NewBatchFmls() BatchFmls {
	return BatchFmls{Items: make([]BatchFml, 0, 20)}
}

// AddItem add batch.fml item
func (b *BatchFmls) AddItem(periods []string, fml fmlib.IFml) {
	bf := BatchFml{
		Periods: periods,
		Fml:     fml,
	}
	b.Items = append(b.Items, bf)
}

// Run batch fmls
func (b *BatchFmls) Run(codes []string) []error {
	l := len(codes)
	count := len(b.Items)

	now := time.Now()
	cpt := util.NewComputation(vars.ProcessNum)
	cpt.IsSync = vars.IsSync
	errLocker := sync.Mutex{}
	errs := make([]error, 0, 100)
	cpt.Run(l, func(i int) {
		code := codes[i]
		for k := 0; k < count; k++ {
			if err := b.Items[k].Run(code); err != nil {
				errLocker.Lock()
				errs = append(errs, err)
				errLocker.Unlock()
				return
			}
		}
		dget.ReleaseAll(code)
		fmt.Println(i, ":", code)
	})

	fmt.Println("finished:", time.Now().Sub(now))
	return errs
}

// BatchFmls run fmls
type BatchFml struct {
	Periods []string
	Fml     fmlib.IFml
}

// Run
func (b *BatchFml) Run(code string) error {
	pl := len(b.Periods)
	for i := 0; i < pl; i++ {
		if err := b.doRunIt(code, b.Periods[i]); err != nil {
			return err
		}
	}
	return nil
}

// doRunIt
func (b *BatchFml) doRunIt(code string, period string) error {
	bar := dget.Bar(code, period)
	if bar.Len() < 10 {
		return nil
	}
	fields := b.Fml.GetFields()
	dat := series.GetA(fields)
	defer series.PutA(&dat)
	if err := b.Fml.Fml(bar, &dat); err != nil {
		return err
	}
	c := gno.GetConfig().GetConf("data")
	folder := "fmls"
	if vars.IsTest {
		folder = "test/fmls"
	}
	filename := filepath.Join(c.GetString("store_dir"), folder, period, b.Fml.String(), code+vars.ExtBin)

	return series.WriteBin(&dat, filename, fmlib.Precision)
}
