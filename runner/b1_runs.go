package runner

import (
	"fmt"
	"sync"
	"time"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/util"
)

// DoScoresBin 运行code得分的序列，并且保存为bin
func (r *Rule) DoScoresBin(codes []string, dateA, dateB int64) {
	c := gno.GetConfig().GetConf("data")
	folder := c.DefaultString("store_dir", "z:/kt") + "/scores/"
	now := time.Now()
	cpt := util.NewComputation(vars.ProcessNum)
	cpt.IsSync = vars.IsSync
	errLocker := sync.Mutex{}
	errs := make([]error, 0, 100)
	l := len(codes)
	cpt.Run(l, func(i int) {
		code := codes[i]
		fmt.Println(i, ":", code)
		dat := r.ScoresP(code, dateA, dateB)
		err := series.WriteBin(dat, folder+code+vars.ExtBin)
		if err != nil {
			errLocker.Lock()
			errs = append(errs, err)
			errLocker.Unlock()
		}

		series.PutA(dat)
		// r.Release(code)
		dget.ReleaseAll(code)
	})
	count := len(errs)
	for i := 0; i < count; i++ {
		fmt.Println(i, errs[i])
	}
	fmt.Println("\nfinished:", time.Now().Sub(now))

}

// ScoresP code得分的序列
func (r *Rule) ScoresP(code string, dateA, dateB int64) *series.A {
	return r.scoresWith(code, dateA, dateB, true)
}

// ScoresP code得分的序列
func (r *Rule) Scores(code string, dateA, dateB int64) *series.A {
	return r.scoresWith(code, dateA, dateB, false)
}

// scoresWith code得分的序列
func (r *Rule) scoresWith(code string, dateA, dateB int64, isPool bool) *series.A {
	// ds := series.GetAs()
	ds := make([]*series.A, 0, 20)
	fn := len(r.FmlsRules)
	pn := len(r.Periods)

	// 1: 获得所有fml得分data
	for i := 0; i < fn; i++ {
		fml := r.FmlsRules[i].Fml
		if fml.GetScoreType() == fmlib.ScoreTypeSub {
			continue
		}
		for k := 0; k < pn; k++ {
			period := r.Periods[k]
			filename := fmlib.BinFmlFileName(code, period, fml.String())
			dat0 := dget.GetBinDat(filename)
			dat := dat0.Range(dateA, dateB)
			// fmt.Println(code, fml.String(), dat.Len())
			ds = append(ds, &dat)
		}
	}
	var result series.A
	if isPool {
		result = series.GetA(scoreFields)
	} else {
		result = series.A{Fields: scoreFields}
	}

	series.EveryApply(func(date int64, ids []int) {
		score := r.ScoreOnP(code, date)
		if score[0]+score[1]+score[2] == 0 {
			series.PutRow(score)
			return
		}
		result.AddRow(date, []float64(score))
		series.PutRow(score)
	}, ds...)

	series.PutAs(ds)
	r.Release(code)
	return &result
}
