package runner

import (
	"gitee.com/onqee/astock/compare"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
)

var (
	scoreFields = []string{"main", "sub", "time"}
)

// FmlRule 公式规则
type FmlRule struct {
	Fml       fmlib.IFml `json:"-"`
	IsRequire bool       `json:"IsRequire"`
}

// Rule
type Rule struct {
	FmlsRules []FmlRule `json:"-"`
	Periods   []string  `json:"Periods"` // 每个公式计算的周期s

	MainPeriodScopeN int `json:"MainPeriodScopeN"` //多周期运算时，根据主周期确定时间范围
	PeriodsLogic     int `json:"PeriodsLogic"`     // 多周期运算时，得分计算的逻辑：Max, Avg, Ema, Sum
	FmlsLogic        int `json:"FmlsLogic"`        // 多公式计算时的逻辑: Max, Avg, Sum

	datFields []string
}

// ScoreOnP 计算得分
func (r *Rule) ScoreOnP(code string, date int64) fmlib.Score {
	dat := r.DatOnP(code, date) //获得数据得分矩阵
	defer series.PutA(dat)

	score := r.calcScoreByDatP(dat)
	if score[0]+score[1]+score[2] == 0 {
		return score
	}

	// if r.MinCorr != 0 {
	// 	// 找到相似的板块
	// 	// datA, err := series.ReadBinP(compare.BinFileName(code, vars.PD1), 0, 0)
	// 	datA := dget.GetBinDat(compare.BinCorrFileName(code, vars.PD1))
	// 	if datA.Len() > 0 {
	// 		row, _ := datA.RowOnP(date)
	// 		if len(row) > 0 && row[1] > r.MinCorr {
	// 			score[3] = row[0]
	// 			score[4] = row[2]
	// 		}
	// 		series.PutRow(row)
	// 	}
	// }
	return score
}

// 根据得分矩阵，计算得分
func (r *Rule) calcScoreByDatP(dat *series.A) fmlib.Score {
	l := dat.Len()
	var score fmlib.Score = series.GetRow(len(scoreFields))
	if l == 0 {
		return score
	}
	count := len(r.FmlsRules)
	if count != len(dat.Columns) {
		panic("得分矩阵fields与rule.Fmls 不匹配")
	}

	valsM := series.GetRow() // main score
	defer series.PutRow(valsM)
	valsS := series.GetRow() // sub score
	defer series.PutRow(valsS)
	valsT := series.GetRow() // time score
	defer series.PutRow(valsT)

	for i := 0; i < count; i++ {
		fr := r.FmlsRules[i]
		v := fmlib.CalcScoreVal(dat.Columns[i], r.PeriodsLogic)
		// 当前fml必须
		if fr.IsRequire && v == 0 {
			return score
		}

		switch fr.Fml.GetScoreType() {
		case fmlib.ScoreTypeMain:
			valsM = append(valsM, v)
		case fmlib.ScoreTypeSub:
			valsS = append(valsS, v)
		case fmlib.ScoreTypeTime:
			valsT = append(valsT, v)
		}
	}
	// main,sub,time,corrCode
	score.SetMain(fmlib.CalcScoreVal(valsM, r.FmlsLogic))
	score.SetSub(fmlib.CalcScoreVal(valsS, r.FmlsLogic))
	score.SetTime(fmlib.CalcScoreVal(valsT, r.FmlsLogic))

	return score
}

// DatOnP 得分矩阵
func (r *Rule) DatOnP(code string, date int64) *series.A {
	count := len(r.FmlsRules)
	if len(r.datFields) == 0 {
		r.datFields = make([]string, count)
		for i := 0; i < count; i++ {
			r.datFields[i] = r.FmlsRules[i].Fml.String()
		}
	}
	l := len(r.Periods)
	if l == 0 {
		panic("rule 没有设置周期")
	}

	bar := dget.Bar(code, r.Periods[0])
	index, stat := bar.Search(date)
	if stat == series.IndexOverRangeLeft {
		return &series.EmptyA
	}
	n := r.MainPeriodScopeN
	var dateA int64
	if n == 0 {
		dateA = bar.Dates[index-n] - 1
	} else {
		if index-n < 0 {
			dateA = bar.Dates[0]
		} else {
			dateA = bar.Dates[index-n] + 1
		}
	}

	result := series.GetA(r.datFields)

	row := series.GetRow(count)
	defer series.PutRow(row)
	for i := 0; i < l; i++ { // periods loop
		period := r.Periods[i]
		for k := 0; k < count; k++ { // fmls loop
			row[k] = r.fmlScoreOn(r.FmlsRules[k], code, period, dateA, date)
		}
		result.AddRow0(date, row)
	}
	return &result
}

// 计算fml在一段数据区间的得分
func (r *Rule) fmlScoreOn(fr FmlRule, code, period string, dateA, date int64) float64 {
	filename := fmlib.BinFmlFileName(code, period, fr.Fml.String())
	dat := dget.GetBinDat(filename)
	if dat.Len() == 0 {
		return 0
	}

	// 	return scoreBetween(fr.Fml, dat, dateA, date, fmlib.LogicScoreMax)
	// }
	// func scoreBetween(fml fmlib.IFml, dat *series.A, dateA, dateB int64, logic int) float64 {
	dat0 := dat.Range(dateA, date)
	count := dat0.Len()
	if count == 0 {
		return 0
	}

	vals := series.GetRow()
	defer series.PutRow(vals)
	row := dat0.GetRowP()
	defer series.PutRow(row)

	for i := 0; i < count; i++ {
		dat0.RowAt(i, row)
		v := fr.Fml.Score(row)
		if v == 0 {
			continue
		}
		vals = append(vals, v)
	}
	// 周期scope范围内默认Max
	return fmlib.CalcScoreVal(vals, fmlib.LogicScoreMax)
}

func (r *Rule) Release(code string) {
	count := len(r.FmlsRules)
	n := len(r.Periods)
	for i := 0; i < count; i++ {
		for k := 0; k < n; k++ {
			period := r.Periods[k]
			// 清除Fml bin
			filename := fmlib.BinFmlFileName(code, period, r.FmlsRules[i].Fml.String())
			dget.ReleaseBin(filename)
		}
	}
	dget.ReleaseAll(code)

	// r.datFields = nil
	// 清除corr 相似性
	dget.ReleaseBin(compare.BinCorrFileName(code, vars.PD1))
}
