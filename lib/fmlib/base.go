package fmlib

import (
	"fmt"
	"path/filepath"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
)

var (
	// precision
	Precision = 6
)

const (
	// LogicScoreSum 得分累加
	LogicScoreSum = 0
	// LogicScoreEMA 得分EMA累加
	LogicScoreEMA = 50
	// LogicScoreMA 得分EA累加
	LogicScoreMA = 51

	// LogicSkip 忽略
	LogicSkip = -1

	// LogicScoreFirst 逻辑存在，返回第一个
	LogicScoreFirst = 10
	// LogicScoreMin 最小的
	LogicScoreMin = 18
	// LogicScoreMax 最大的
	LogicScoreMax = 19

	// ScoreTypeMain 得分类型：主要
	ScoreTypeMain = 1
	// ScoreTypeSub 得分类型：次要
	ScoreTypeSub = 0
	// ScoreTypeTime 得分类型：时间序列
	ScoreTypeTime = 3
)

// Base base
type Base struct {
	Fields    []string `json:"Fields"`
	Name      string   `json:"Name"`
	ScoreType int      `json:"ScoreType"` // 得分的类型，main, sub, time
}

// Init f
func (r *Base) Init(name string, fields []string, scoreType int) {
	r.Name = name
	r.Fields = fields
	r.ScoreType = scoreType
}

// String name
func (r *Base) String() string {
	return r.Name
}

// GetFields []string
func (r *Base) GetFields() []string {
	return r.Fields
}

// GetScoreType int
func (r *Base) GetScoreType() int {
	return r.ScoreType
}

// GetRow []float64
func (r *Base) GetRow() []float64 {
	return series.GetRow(len(r.GetFields()))
}

// GetA series.A
func (r *Base) GetA() series.A {
	dat := series.GetA(r.GetFields())
	dat.Name = r.Name
	return dat
}

// Fml 连续结果序列
func (r *Base) Fml(bar *bars.Bar, dat *series.A) error {
	return fmt.Errorf("Not Set Fml %s %s", r.Name, bar.Period)
}

// BinFmlFileName
func BinFmlFileName(code, period, name string) string {
	c := gno.GetConfig().GetConf("data")
	folder := "fmls"
	if vars.IsTest {
		folder = "test/fmls"
	}
	return filepath.Join(c.GetString("store_dir"), folder, period, name, code+".bin")
}

// IFml 数据公式接口
type IFml interface {
	String() string
	GetFields() []string
	GetScoreType() int
	Fml(*bars.Bar, *series.A) error
	Calc(*bars.Bar, []float64) error
	GetRow() []float64
	Score([]float64) float64
	// ScoreType() int // 得分的类型，main, sub, time
}
