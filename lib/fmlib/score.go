package fmlib

import (
	"math"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/mathlib"
)

// Score 得分：main, sub, time
type Score []float64

// Plus value
func (s Score) Plus(v Score) {
	for i := 0; i < 3; i++ {
		s[i] += v[i]
	}
}

func (s Score) SetVal(v float64, itype int) {
	switch itype {
	case ScoreTypeMain:
		s.SetMain(v)
	case ScoreTypeSub:
		s.SetSub(v)
	case ScoreTypeTime:
		s.SetTime(v)
	}
}

// SetMain value
func (s Score) SetMain(v float64) {
	s[0] = v
}

// SetSub value
func (s Score) SetSub(v float64) {
	s[1] = v
}

// SetTime value
func (s Score) SetTime(v float64) {
	s[2] = v
}

// CalcScoreVal
func CalcScoreVal(vals []float64, logic int) float64 {
	var sum, max, min float64
	var arr []float64
	min = math.MaxFloat64
	if logic == LogicSkip {
		return 0
	}

	count := len(vals)
	for i := 0; i < count; i++ {
		val := vals[i]
		if val == 0 {
			continue
		}

		switch logic {
		case LogicScoreMA, LogicScoreEMA:
			if cap(arr) == 0 {
				arr = series.GetRow(0, count)
				defer series.PutRow(arr)
			}
			arr = append(arr, val)
		case LogicScoreFirst:
			if val != 0 {
				return val
			}
		}
		sum += val

		if max < math.Abs(val) {
			max = val
		}
		if min > math.Abs(val) {
			min = val
		}
	} // loop end
	if sum == 0 {
		return 0
	}

	switch logic {
	case LogicScoreMax: // 周期得分最大的数值
		return max
	case LogicScoreMin: // 周期得分最小的数值
		return min
	case LogicScoreMA:
		return mathlib.Avg(arr)
	case LogicScoreEMA:
		return mathlib.Ema(arr, len(arr))
	}
	return sum
}
