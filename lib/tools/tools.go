package tools

import (
	"bytes"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"gitee.com/onqee/astock/lib/data/vars"
)

// WinFreeMem mem
func WinFreeMem() int64 {
	buf := bytes.NewBuffer(nil)
	cmd := exec.Command("wmic", "os", "get", "FreePhysicalMemory")
	cmd.Stdout = buf
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
		return 0
	}

	s := strings.TrimSpace(buf.String())
	mem, _ := strconv.ParseInt(s[23:], 10, 64)
	return mem
}

// CodeSplit 分开合并的code
func CodeSplit(code string) (string, string) {
	if len(code) != 8 {
		return "", ""
	}
	return strings.ToUpper(code[:2]), code[2:]
}

// PeriodPlus 周期级别加法
func PeriodPlus(period string) string {
	l := len(vars.Periods)
	for i := 0; i < l; i++ {
		if vars.Periods[i] == period {
			index := i + 1
			if index == l {
				return ""
			}
			return vars.Periods[index]
		}
	}
	return ""
}
