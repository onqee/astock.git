package codetool

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/util"
)

var (
	dataA series.B
	// dataSZ  series.B
	// dataSH  series.B
	// data300 series.B
	// dataGE  series.B // 创业板 Growth Enterprise

	dataIndex  series.B
	dataAll    series.B
	codeFields = []string{vars.FieldCode, vars.FieldName}
	// isTest     = false
)

// ToCode convert int code to string code
func ToCode(v int) string {
	val := v / 1000000
	switch val {
	case 1:
		return vars.MarketSH + fmt.Sprintf("%06d", v-val*1000000)
	case 2:
		return vars.MarketSZ + fmt.Sprintf("%06d", v-val*1000000)
	default:
		return fmt.Sprint(v)
	}
}

// ToCodeI convert code to store int code
func ToCodeI(code string) int {
	b := util.GetBytes(1)
	prefix := strings.ToLower(code[:2])

	switch prefix {
	case vars.MarketSH:
		b[0] = byte('1')
	case vars.MarketSZ:
		b[0] = byte('2')
	default:
		b[0] = byte('9')
	}
	b = append(b, code[2:]...)
	v, err := strconv.ParseInt(util.Bytes2Str(b), 10, 64)
	if err != nil {
		fmt.Println("strconv.ParseInt error:", util.Bytes2Str(b))
		panic(err)
	}
	return int(v)
}

var codesAll []string

// Stock class
type Stock struct {
	CodeI int
	Code  string
	Name  string
}

// Get one by code
func Get(code string) Stock {
	all := All()
	codeI := ToCodeI(code)
	i := all.IndexOn(int64(codeI))
	if i < 0 {
		return Stock{}
	}

	return Stock{codeI, code, all.Columns[1][i]}
}

// All 所有股票、板块、指数、概念的代码 ：Tdx
func All() *series.B {
	if dataAll.Len() > 0 {
		return &dataAll
	}
	KTA()

	Indexs()
	BlockTdx()
	IndustryTdx()
	l := dataA.Len() + dataIndex.Len() + dataBlkTdx.Len() + dataIdyTdx.Len()
	dates := make([]int64, 0, l)
	cols := series.NewColumnsBN(len(codeFields), 0, l)
	dates = append(dates, dataA.Dates...)
	dates = append(dates, dataIndex.Dates...)
	dates = append(dates, dataBlkTdx.Dates...)
	dates = append(dates, dataIdyTdx.Dates...)

	cols = cols.Append(dataA.Columns)
	cols = cols.Append(dataIndex.Columns)
	cols = cols.Append(dataBlkTdx.Columns)
	cols = cols.Append(dataIdyTdx.Columns)

	dataAll = series.NewB("all", dates, cols, codeFields)
	dataAll.Sort()

	return &dataAll
}

// CodesIndexs codes
func CodesIndexs() []string {
	return Indexs().Values(vars.FieldCode)
}

// Indexs index
func Indexs() *series.B {
	if dataIndex.Len() > 0 {
		return &dataIndex
	}

	c := gno.GetConfig().GetConf(vars.FieldData)
	name := c.GetString("tdx_industry")
	if name == "" {
		return nil
	}

	filename := filepath.Join(c.GetString("tdx_home"), "T0002/export/indexs.json")

	file, err := os.Open(filename)
	// src, err := ioutil.ReadFile(filename)
	if err != nil {
		log.App.Error(err)
		return nil
	}
	src, err := io.ReadAll(file)
	file.Close()
	if err != nil {
		log.App.Error(err)
		return nil
	}

	var dat struct {
		Indexs  []map[string]string `json:"indexs"`
		Indexs2 []map[string]string `json:"indexs2"`
	}

	err = json.Unmarshal(src, &dat)
	if err != nil {
		panic(err)
	}
	l := len(dat.Indexs)
	dates := make([]int64, l)
	cols := series.NewColumnsBN(len(codeFields), l)
	for i, item := range dat.Indexs {
		code := item[vars.FieldCode]
		dates[i] = int64(ToCodeI(code))
		cols[0][i] = code
		cols[1][i] = item[vars.FieldName]
	}
	dataIndex = series.NewB("indexs", dates, cols, codeFields)
	return &dataIndex
}

// IsIndexs 属于指数
func IsIndexs(code string) bool {
	codeI := int64(ToCodeI(code))
	_, status := dataIndex.Search(codeI)
	return status == series.IndexFound
}

// WhatIndexIs 股票的指数代码是什么
func WhatIndexIs(code string) string {
	// SH
	if code[2] == '6' {
		// sh 600 207
		// 01 234 567
		switch code[2:5] {
		case "688":
			return ""
		}
		return vars.Code000001
	}

	// SZ
	switch code[2:5] {
	case "002":
		return vars.Code399005
	case "300", "301", "302":
		return vars.Code399006
	default:
		return vars.Code399001
	}
}
