package codetool

import (
	"bytes"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	iconv "github.com/djimenez/iconv-go"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/util"
)

var (
	dataBlkTdx series.B
	dataIdyTdx series.B
)

// CodesAllTdx 所有股票、板块、指数、概念的代码 ：Tdx
func CodesAllTdx() []string {
	if len(codesAll) == 0 {
		codesAll = make([]string, 0, 8000)
		codesAll = append(codesAll, CodesATdx()...)
		codesAll = append(codesAll, CodesBlockTdx()...)
		codesAll = append(codesAll, CodesIndustryTdx()...)
		codesAll = append(codesAll, CodesIndexs()...)
	}
	return codesAll
}

// CodesBlockTdx 获得板块的股票代码
func CodesBlockTdx() []string {
	return BlockTdx().Values(vars.FieldCode)
}

// BlockTdx 获得板块的股票代码
func BlockTdx() *series.B {
	if dataBlkTdx.Len() > 0 {
		return &dataBlkTdx
	}
	c := gno.GetConfig().GetConf(vars.FieldData)
	name := c.GetString("tdx_blocks")
	if name == "" {
		return nil
	}

	filename := filepath.Join(c.GetString("tdx_home"), "T0002/export", name)
	dataBlkTdx = loadTdxTxtCodes(filename)
	return &dataBlkTdx
}

// CodesIndustryTdx 获得板块的股票代码
func CodesIndustryTdx() []string {
	return IndustryTdx().Values(vars.FieldCode)
}

// IndustryTdx 获得工业板块的股票代码
func IndustryTdx() *series.B {
	if dataIdyTdx.Len() > 0 {
		return &dataIdyTdx
	}

	c := gno.GetConfig().GetConf(vars.FieldData)
	name := c.GetString("tdx_industry")
	if name == "" {
		return nil
	}

	filename := filepath.Join(c.GetString("tdx_home"), "T0002/export", name)
	dataIdyTdx = loadTdxTxtCodes(filename)
	return &dataIdyTdx
}

func loadTdxTxtCodes(filename string) series.B {
	src, err := ioutil.ReadFile(filename)
	if err != nil {
		log.App.Error(err)
		return series.B{}
	}

	dates := make([]int64, 0, 200)
	cols := series.NewColumnsBN(len(codeFields), 0, 200)

	buf := bytes.NewBuffer(src)
	var line []byte

	buf.ReadBytes('\n')
	row := util.GetStrings(2)
	defer util.PutStrings(row)
	for err == nil {
		line, err = buf.ReadBytes('\n')
		txt, _ := iconv.ConvertString(string(line), "gb2312", "utf-8")
		arr := strings.Split(txt, "\t")
		if len(arr[0]) > 6 {
			break
		}
		// codes = append(codes, market+arr[0])
		// names = append(names, arr[1])
		code := vars.MarketSH + arr[0]
		dates = append(dates, int64(ToCodeI(code)))
		// row := series.NewStrRow(2)
		row[0] = code
		row[1] = arr[1]
		cols.AddRow(row)
	}

	dat := series.NewB(filename, dates, cols, codeFields)
	dat.Sort()
	return dat
}

var (
	tdxCodes         []string
	tdxBlockCodes    []string
	tdxIndustryCodes []string
)

// CodesATdx return codes
func CodesATdx() []string {
	if len(tdxCodes) == 0 {
		loadTdxHomeCodes()
	}

	return tdxCodes
}

func loadTdxHomeCodes() error {
	tdxCodes = make([]string, 0, 8000)
	tdxBlockCodes = make([]string, 0, 350)
	tdxIndustryCodes = make([]string, 0, 300)

	c := gno.GetConfig()
	cf := c.GetConf("data")
	folder := filepath.Join(cf.GetString("tdx_home"), "vipdoc")
	err := tdxDirFiles(filepath.Join(folder, "sh/lday"), func(code string) bool {
		if len(code) != 8 {
			return false
		}
		if code[2] == '8' && code[3] == '8' {

			if 50 < code[5] && code[5] < 53 { // 8803,8804
				tdxIndustryCodes = append(tdxIndustryCodes, code)
			} else if 52 < code[5] && code[5] < 58 {
				tdxBlockCodes = append(tdxBlockCodes, code)
			}
			return false
		}

		return code[2] == '6'
	})
	if err != nil {
		return err
	}

	err = tdxDirFiles(filepath.Join(folder, "sz/lday"), func(code string) bool {
		return code[2] == '0' || (code[2] == '3' && code[3] != '9')
	})

	return err
}

func tdxDirFiles(dir string, f func(string) bool) error {
	items, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}

	l := len(items)
	for i := 0; i < l; i++ {
		info := items[i]
		if info.IsDir() {
			continue
		}

		name := info.Name()
		if name[0] != 's' || len(name) != 12 {
			continue
		}

		code := name[:8]

		if f(code) {
			tdxCodes = append(tdxCodes, code)
		}
	}
	return nil
}
