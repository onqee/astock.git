package codetool

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

// BlockKTCodes 获得板块的股票代码
func BlockKTCodes(name string) []string {
	return loadKTCodesBy(name, "blk")
}

// IndustryKTCodes 获得工业板块的股票代码
func IndustryKTCodes(name string) []string {
	return loadKTCodesBy(name, "industry")
}

func loadKTCodesBy(name, typ string) []string {
	redis := cache.GetRedis()
	keys, _ := redis.DoStrings("hkeys", "h-"+typ+":"+name)

	arr := make([]string, 0)
	// for _, key := range keys {
	// 	arr = append(arr, key)
	// }
	arr = append(arr, keys...)

	return arr
}

// InBlockKT 属于这个板块
func InBlockKT(code, blkName string) bool {
	redis := cache.GetRedis()
	isok, _ := redis.DoBool("hexists", "h-blk:"+blkName, code)
	return isok
}

// InIndustryKT 属于这个板块
func InIndustryKT(code, industryName string) bool {
	redis := cache.GetRedis()
	isok, _ := redis.DoBool("hexists", "h-industry:"+industryName, code)
	return isok
}

// CodesAKT codes
func CodesAKT() []string {
	return KTA().Values(vars.FieldCode)
}

// KTA codes
func KTA() *series.B {
	if dataA.Len() > 0 {
		return &dataA
	}

	dataA = loadKT()
	return &dataA
}

func loadKT() series.B {
	redis := cache.GetRedis()
	vals, err := redis.DoStrings("hgetall", vars.CacheKeyCodeName)
	if err != nil {
		return series.B{}
	}
	l := len(vals)

	cols := series.NewColumnsBN(2, l/2)
	dates := make([]int64, l/2)
	k := 0
	for i := 0; i < l; i++ {
		row := series.NewStrRow(2)
		row[0] = vals[i]
		i++
		row[1] = vals[i]

		cols.SetRow(row, k)
		dates[k] = int64(ToCodeI(row[0]))
		k++
	}

	dat := series.NewB("", dates, cols, codeFields)
	dat.Sort()
	return dat
}
