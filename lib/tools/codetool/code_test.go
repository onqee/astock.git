package codetool

import (
	"fmt"
	"testing"

	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../../app.conf")
}

func Test_TDX(t *testing.T) {
	codes := CodesATdx()
	if len(codes) < 3600 {
		t.Fatal("Tdx LoadCodes failed")
	}

	codes = CodesBlockTdx()
	if len(codes) < 100 {
		t.Fatal("Tdx Block LoadCodes failed")
	}

	codes = CodesATdx()
	count := len(codes)
	for i := 0; i < count; i++ {
		code := codes[i]
		v := WhatIndexIs(code)
		if v == "" {
			fmt.Println(code)
		}
	}
}
