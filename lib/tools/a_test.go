package tools

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
	"github.com/kere/gno/db"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
	db.Current().SetLogLevel("all")
}

func TestLoadDB(t *testing.T) {
	q := db.NewQuery(vars.TableBarD1).Where("code=$1 and date between $2 and $3", codetool.ToCodeI(vars.Code399001), 1190701, 1190801).Order("date")
	a, err := LoadA(q, true)
	if err != nil {
		t.Fatal(err)
	}
	v, _ := a.ValueOn(vars.FieldClose, 11907150000)
	if v != 9309.4199 {
		series.Print(&a)
		t.Fatal(v)
	}

	code := "sz000938"
	q = db.NewQuery(vars.TableShareBonus).Where("code=$1 and date between $2 and $3", codetool.ToCodeI(code), 1180101, 1190801)
	b, _ := LoadA(q, true)

	v, _ = b.ValueOn("stk_div", 11807050000)
	if v != 4 {
		series.Print(&b)
		t.Fatal(v)
	}
}
