package net

import (
	"testing"

	"github.com/kere/gno/db"
	"gitee.com/onqee/astock/lib/data/series"
)

func TestUtil(t *testing.T) {
	rows := db.MapRows{}
	rows = append(rows, db.MapRow{"a1": 0.1, "a2": 0.2, "a3": 0.3, "date": "2018-09-30"})
	rows = append(rows, db.MapRow{"a1": 0.11, "a2": 0.21, "a3": 0.31, "date": "2018-10-30"})
	rows = append(rows, db.MapRow{"a1": 0.12, "a2": 0.22, "a3": 0.32, "date": "2018-11-30"})

	timeLayout := "2006-01-02"
	dateKey := "date"
	dat, err := RowsToSeries(rows, dateKey, timeLayout)
	if err != nil {
		t.Fatal(err)
	}

	series.Print(&dat)
	v, _ := dat.ValueOn("a3", 11811300000)
	if v != 0.32 {
		t.Fatal(v)
	}
}
