package net

import (
	"github.com/kere/gno/libs/log"
)

// JSONQuery fetch class
type JSONQuery struct {
	GoQuery
	Data []byte
}

// Get func
func (a *JSONQuery) Get() error {
	log.App.Debug(a.URL)
	var err error
	a.Data, err = Get(a.URL)

	return err
}

// // BodyQuery fetch class
// type BodyQuery struct {
// 	GoQuery
// 	Body io.ReadCloser
// }
//
// // Get func
// func (a *BodyQuery) Get() error {
// 	log.App.Debug(a.URL)
// 	var err error
// 	var resp *http.Response
// 	resp, err = Get(a.URL)
// 	if err != nil {
// 		resp, err = Get(a.URL)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	a.Body = resp.Body
//
// 	return err
// }
