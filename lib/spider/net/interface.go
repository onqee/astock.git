package net

// ISpider interface
type ISpider interface {
	Run() error
	DoAction() error
	StoreData() error
	SetURL(string)
	Get() error
}
