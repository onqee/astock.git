package net

import (
	"bytes"
	"errors"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/kere/gno/libs/log"
)

// GoQuery fetch class
type GoQuery struct {
	URL      string
	Doc      *goquery.Document
	Target   ISpider
	SkipWeek bool
}

// SetURL func
func (g *GoQuery) SetURL(s string) {
	g.URL = s
}

// Run func
func (g *GoQuery) Run() error {
	week := time.Now().Weekday()
	if g.SkipWeek && (week == time.Sunday || week == time.Saturday) {
		return errors.New("today is week day")
	}

	defer func() {
		err := recover()
		if err != nil {
			log.App.Error(err).Stack()
		}
	}()

	err := g.Target.Get()
	if err != nil {
		return err
	}

	err = g.Target.DoAction()
	if err != nil {
		return err
	}

	return g.Target.StoreData()
}

// Get func
func (g *GoQuery) Get() error {
	log.App.Debug(g.URL)
	body, err := Get(g.URL)
	if err != nil {
		return err
	}

	buf := bytes.NewBuffer(body)
	g.Doc, err = goquery.NewDocumentFromReader(buf)
	if err != nil {
		return err
	}
	if g.Doc == nil {
		return errors.New("doc is nil")
	}

	return nil
}

// DoAction func
func (g *GoQuery) DoAction() error {
	return nil
}

// StoreData func
func (g *GoQuery) StoreData() error {
	return nil
}
