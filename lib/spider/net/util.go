package net

import (
	"strconv"
	"strings"

	"gitee.com/onqee/astock/lib/data/series/datescore"
	"github.com/PuerkitoBio/goquery"
	"github.com/kere/gno/libs/log"
)

// ParseTdScoreDate 解析td的值 到 日期数值
func ParseTdScoreDate(td *goquery.Selection) int64 {
	return datescore.Str2Score(td.Text()) / 10000
}

// ParseTdNum 储存数据
func ParseTdNum(td *goquery.Selection) float64 {
	txt := td.Text()
	if txt == "--" {
		return 0
	}

	txt = strings.Replace(txt, ",", "", -1)

	v, err := strconv.ParseFloat(txt, 64)
	if err != nil {
		return 0
	}

	return v
}

const (
	sempty = "--"
)

// ParseToNum 储存数据
func ParseToNum(s string) float64 {
	if s == sempty {
		return 0
	}

	var tmp string
	var n float64 = 1

	arr := []rune(s)
	l := len(arr)

	last := string(arr[l-1])
	switch last {
	case "亿":
		tmp = string(arr[:l-1])
		n = 100000000
	case "万":
		tmp = string(arr[:l-1])
		n = 10000
	case "千":
		tmp = string(arr[:l-1])
		n = 1000
	default:
		tmp = s
	}

	v, err := strconv.ParseFloat(tmp, 64)
	if err != nil {
		log.App.Error(err)
		return 0
	}
	return v * n
}
