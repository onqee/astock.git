package net

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

var (
	// DefaultTimeout timeout
	DefaultTimeout = 10
)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, time.Duration(DefaultTimeout)*time.Second)
}

// Get 抓取数据
func Get(url string) ([]byte, error) {
	transport := &http.Transport{
		Dial: dialTimeout,
	}

	client := &http.Client{
		Timeout:   time.Duration(DefaultTimeout) * time.Second,
		Transport: transport,
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("User-Agent", `Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36`)
	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		return nil, err
	} else if resp.Body == nil {
		return nil, fmt.Errorf("error: resp.Body is empty")
	}
	src, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != 200 {
		return src, fmt.Errorf("Get failed:%d", resp.StatusCode)
	}
	// var OKPJKmpr={pages:0,data:[{stats:false}]}
	return src, nil
}
