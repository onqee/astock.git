package sina

import (
	"fmt"
	"strconv"
	"strings"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/spider/net"
	"github.com/PuerkitoBio/goquery"
	iconv "github.com/djimenez/iconv-go"
	"github.com/kere/gno/db"
)

const (
	// URLSinaCapital const
	URLSinaCapital = "http://vip.stock.finance.sina.com.cn/corp/go.php/vCI_StockStructure/stockid/%s.phtml"
)

// Capital 股本信息
type Capital struct {
	Code string
	net.GoQuery

	Result series.A
	Dat    bars.Capital
}

// NewCapital func
func NewCapital(code string) *Capital {
	s := &Capital{Code: code}
	s.URL = fmt.Sprintf(URLSinaCapital, code[2:])
	s.Target = s
	return s
}

// DoAction 定义具体抓取动作，然后由net.GoQuery 运行
func (c *Capital) DoAction() error {
	tables := c.Doc.Find("#con02-1 > table")
	field1 := vars.CapitalFields[0]
	field2 := vars.CapitalFields[1]
	field3 := vars.CapitalFields[2]

	dates := make([]int64, 0, 10)
	// 3列，0长度，10cap
	cols := series.NewColumnsN(3, 0, 10)

	tables.Each(func(i int, table *goquery.Selection) {
		ds := c.parseTable(table)
		defer db.PutDataSet(&ds)
		dbRow := ds.GetDBRow()
		defer db.PutRow(dbRow.Values)
		row := series.GetRow(3)
		defer series.PutRow(row)
		// result = append(result, datArr...)
		count := ds.Len()
		for k := 0; k < count; k++ {
			ds.DBRowAt(k, dbRow)
			date := dbRow.Int64(vars.FieldDate)

			// 相同的变动日期，如果已经存在，则不以第一次为准
			isExists := false
			xl := len(dates)
			for x := 0; x < xl; x++ {
				if dates[x] == date {
					isExists = true
					break
				}
			}
			if isExists {
				continue
			}

			dates = append(dates, date)
			row[0], row[1], row[2] = dbRow.Float(field1), dbRow.Float(field2), dbRow.Float(field3)
			// row := []float64{dbRow.Float(field1), dbRow.Float(field2), dbRow.Float(field3)}
			cols.AddRow(row)
		}
	})

	c.Result = series.NewA(c.Code, dates, cols, vars.CapitalFields)

	return nil
}

// StoreData 储存数据
func (c *Capital) StoreData() error {
	l := c.Result.Len()
	if l == 0 {
		return nil
	}
	result := c.Result
	dat := bars.NewCapital(c.Code)
	err := dat.Load()
	if err != nil {
		return err
	}

	dat2 := series.FilterP(&result, func(i int) bool {
		var date int64
		if vars.IsKtDate {
			date = result.Dates[i] * 10000
		} else {
			date = result.Dates[i]*10000 + 1500
		}
		_, stat := dat.Search(date)
		return stat != series.IndexFound
	})
	defer series.PutA(&dat2)
	return series.SaveSeriesToDB(&dat2, vars.TableCapital, c.Code, nil, true)
}

// parseTable 储存数据
func (c *Capital) parseTable(sele *goquery.Selection) db.DataSet {
	trs := sele.Find("tbody tr")
	ll := trs.Length()
	if ll != 23 {
		return db.EmptyDataSet
	}

	l := trs.Eq(0).Find("td").Length()

	// var datArr db.MapRows
	ds := db.GetDataSet([]string{vars.FieldDate, vars.FieldCapital, vars.FieldTradable, vars.FieldUntradable}, l-1)

	trs.Each(func(i int, tr *goquery.Selection) {
		switch i {
		case 0: // 变动日期
			c.parseTrDate(&ds, tr)
		case 4: // 股本
			c.parseTr(&ds, 1, tr)
		case 6: // 流通股
			c.parseTr(&ds, 2, tr)
		case 8: // 非流通股
			c.parseTr(&ds, 3, tr)
		}
	})

	return ds
}

// parseTrDate 储存数据
func (c *Capital) parseTrDate(ds *db.DataSet, tr *goquery.Selection) {
	tr.Find("td").Each(func(k int, td *goquery.Selection) {
		if k == 0 {
			return
		}
		txt := strings.TrimSpace(td.Text())
		dd, _ := datescore.ScoreSplit(datescore.Str2Score(txt))

		// 设置日期 datArr[k-1]["date"] = dd
		ds.Columns[0][k-1] = dd
	})
}

// parseTr 储存数字字段
func (c *Capital) parseTr(ds *db.DataSet, index int, tr *goquery.Selection) {
	tr.Find("td").Each(func(k int, td *goquery.Selection) {
		if k == 0 {
			return
		}
		txt, _ := iconv.ConvertString(td.Text(), "gb2312", "utf-8")
		txt = strings.TrimSpace(txt)
		if txt == "--" {
			ds.Columns[index][k-1] = 0.0
			return
		}

		arr := strings.Split(txt, " ")
		switch strings.TrimSpace(arr[1]) {
		case "万股":
			v, _ := strconv.ParseFloat(arr[0], 64)
			ds.Columns[index][k-1] = int(v * 10000)
		default:
			panic(arr[1])
		}
	})
}
