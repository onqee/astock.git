package sina

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/spider/net"
	"github.com/PuerkitoBio/goquery"
	iconv "github.com/djimenez/iconv-go"
)

const (
	// URLFinSum 财务摘要
	URLFinSum = "https://vip.stock.finance.sina.com.cn/corp/go.php/vFD_FinanceSummary/stockid/%s.phtml"
)

// FinSum 财务摘要
type FinSum struct {
	Code string
	net.GoQuery

	Result series.A
	Dat    bars.FinSum
}

// NewFinSum func
func NewFinSum(code string) *FinSum {
	s := &FinSum{Code: code}
	s.URL = fmt.Sprintf(URLFinSum, code[2:])
	s.Target = s
	return s
}

// DoAction 定义具体抓取动作，然后由net.GoQuery 运行
func (c *FinSum) DoAction() error {
	trs := c.Doc.Find("#FundHoldSharesTable > tbody >tr")
	trs.Each(func(i int, tr *goquery.Selection) {
		isTitle := false
		tr.Find("td").Each(func(k int, td *goquery.Selection) {
			txt := td.Text()
			switch k {
			case 0:
				txt, _ = iconv.ConvertString(txt, "gb2312", "utf-8")
				isTitle = txt == "截止日期"
				fmt.Println(0, txt, "isTitle", isTitle)
			case 1:
				txt, _ = iconv.ConvertString(txt, "gb2312", "utf-8")
				fmt.Println(1, txt, "isEmpty", txt == "")
			}
		})
	})

	return nil
}

// StoreData 储存数据
func (c *FinSum) StoreData() error {
	return nil
}
