package sina

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/series"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../../app.conf")
}

func Test_Capital(t *testing.T) {
	code := "sh600584"
	spider := NewCapital(code)
	spider.Run()
	series.Print(&spider.Result)
}

// func Test_Fin(t *testing.T) {
// code := "sh600207"
// spider := NewFinSum(code)
// spider.Run()
// series.Print(&spider.Result)
// }
