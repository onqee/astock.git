package sina

// 送股配送数据下载

import (
	"fmt"
	"strconv"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/spider/net"
	"github.com/PuerkitoBio/goquery"
	iconv "github.com/djimenez/iconv-go"
)

const (
	// URLSinaShareBonusStrFormat const
	URLSinaShareBonusStrFormat = "http://money.finance.sina.com.cn/corp/go.php/vISSUE_ShareBonus/stockid/%s.phtml"
)

// ShareBonus 复权信息
type ShareBonus struct {
	net.GoQuery

	Code   string
	Result series.A
}

// NewShareBonus func
func NewShareBonus(code string) *ShareBonus {
	s := &ShareBonus{Code: code}
	s.URL = fmt.Sprintf(URLSinaShareBonusStrFormat, code[2:])
	s.Target = s
	// right_offerings = roff
	// "stk_div|每股转送", "cash_div|现金分红", "roff|配股", "r_price|配股价格", "r_base|基准股本"
	s.Result.Fields = []string{"stk_div", "cash_div", "roff", "r_price", "r_base"}
	return s
}

// DoAction 定义具体抓取动作，然后由net.GoQuery 运行
func (s *ShareBonus) DoAction() error {
	sele := s.Doc.Find("#center > div.centerImgBlk > div.tagmain tbody")

	ll := len(s.Result.Fields)

	iStkDiv := s.Result.FieldI("stk_div")
	iCashDiv := s.Result.FieldI("cash_div")
	iRoff := s.Result.FieldI("roff")
	iRPrice := s.Result.FieldI("r_price")
	iRBase := s.Result.FieldI("r_base")
	row := series.GetRow(ll)
	defer series.PutRow(row)

	sele.First().Find("tr").Each(func(i int, sel *goquery.Selection) {

		tds := sel.Find("td")
		if tds.Length() == 1 {
			return
		}
		var give, increase, bonus float64
		var date, dateXD, dateSale int64

		isok := true

		tds.Each(func(ii int, ss *goquery.Selection) {
			txt := ss.Text()

			switch ii {
			case 0:
				if txt == "--" {
					date = -1
				} else {
					date = datescore.Str2Score(txt)
				}
			case 1:
				// 送股
				give, _ = strconv.ParseFloat(txt, 64)
			case 2:
				// 转赠
				increase, _ = strconv.ParseFloat(txt, 64)
			case 3:
				// 分红(税前)
				bonus, _ = strconv.ParseFloat(txt, 64)
			case 4:
				isok = false
				txt, _ = iconv.ConvertString(txt, "gb2312", "utf-8")
				if txt == "实施" {
					isok = true
				}
			case 5: //除权日期
				if txt == "--" {
					dateXD = -1
				} else {
					dateXD = datescore.Str2Score(txt)
				}
			case 7:
				if txt == "--" {
					dateSale = -1
				} else {
					dateSale = datescore.Str2Score(txt)
				}
			}
		})

		if !isok {
			return
		}

		if dateXD < 1 {
			if dateSale > 0 {
				dateXD = dateSale
			} else {
				dateXD = date
			}
		}

		// row := series.NewRow(ll)
		row[iStkDiv] = give + increase
		row[iCashDiv] = bonus

		s.Result.AddRow0(dateXD, row)
	})

	// 配股
	sele.Eq(1).Find("tr").Each(func(i int, sel *goquery.Selection) {
		tds := sel.Find("td")
		if tds.Length() == 1 {
			return
		}
		var date, dateXD int64
		var roff, price, baseVol float64

		tds.Each(func(ii int, ss *goquery.Selection) {
			txt := ss.Text()
			switch ii {
			case 0:
				if txt == "--" {
					date = -1
				} else {
					date = datescore.Str2Score(txt)
				}
			case 1: // 配股方案，每10股配股数量
				roff, _ = strconv.ParseFloat(txt, 64)
			case 2: // 配股价格
				price, _ = strconv.ParseFloat(txt, 64)
			case 3:
				baseVol, _ = strconv.ParseFloat(txt, 64)
				baseVol = baseVol / 10000
			case 4: // 除权日期
				if txt == "--" {
					dateXD = -1
				} else {
					dateXD = datescore.Str2Score(txt)
				}
			}
		})
		if dateXD < 0 {
			dateXD = date
		}

		// row := series.NewRow(ll)

		row[iRoff] = roff
		row[iRPrice] = price
		row[iRBase] = baseVol

		s.Result.AddRow0(dateXD, row)
	})

	return nil
}

// StoreData 储存数据
func (s *ShareBonus) StoreData() error {
	series.SaveSeriesToDB(&s.Result, vars.TableShareBonus, s.Code, nil, true)
	// series.Print(&s.Result)
	return nil
}
