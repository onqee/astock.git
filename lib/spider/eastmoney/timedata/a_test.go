package eastmoney

import (
	"testing"

	_ "github.com/lib/pq"
	"onqee.visualstudio.com/OneAna/lib/data/vars"
)

func Test_TimeData(t *testing.T) {
	fp := NewTimeData(vars.MarketSZ, vars.Code399006)
	err := fp.Run()
	if err != nil {
		t.Fatal(err)
	}

	if fp.YestodayClose == 0 {
		t.Fail()
	}
	fp.DataSeries.Print()
}
