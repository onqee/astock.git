package timedata

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/kere/gno/libs/util"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/spider/net"
)

// ({"name":"上证指数","code":"000001","info":{"c":"2658.16","h":"2680.57","l":"2654.31","o":"2668.49","a":"79597518848","v":"8201372672","yc":"2669.48","time":"2018-09-11 14:19:40","ticks":"34200|54000|0|34200|41400|46800|54000","total":"241","pricedigit":"0.00","jys":"1","Settlement":"-","mk":1,"sp":"0.00"},"data":["2018-09-11 09:30,2668.49,0,2668.933,0","2018-09-11 09:31,2671.99,129105900,2671.956,0","2018-09-11 09:32,2670.13,86842700,2669.991,0","2018-09-11 09:33,2672.39,101485200,2669.964,0","2018-09-11 09:34,2674.48,85680800,2674.917,0"
var (
	Fields = []string{"close", "close1", "vol"}
)

const (
	// http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js?id=3990012&TYPE=r&rtntype=5&isCR=false
	urlTimeData = "http://pdfm2.eastmoney.com/EM_UBG_PDTI_Fast/api/js?id=%s%d&TYPE=r&rtntype=5&isCR=false"
)

// TimeData 分时数据
type TimeData struct {
	net.JSONQuery `json:"-"`
	Code          string
	Market        string
	DataSeries    series.Series
	YestodayClose float64
}

// NewTimeData func
func NewTimeData(market, code string) *TimeData {
	t := &TimeData{Code: code, Market: market}
	typ := 1
	if market == vars.MarketSZ {
		typ = 2
	}
	t.URL = fmt.Sprintf(urlTimeData, code, typ)
	t.Target = t

	return t
}

// DoAction 定义具体抓取动作，然后由net.GoQuery 运行
func (t *TimeData) DoAction() error {
	var obj struct {
		Name string
		Code string
		Info util.MapData
		Data []string
	}

	err := json.Unmarshal(t.Data[1:len(t.Data)-1], &obj)
	if err != nil {
		return err
	}
	l := len(obj.Data)
	if l == 0 {
		return nil
	}
	// 2018-09-11 09:30,1389.54,0,1391.709,0
	var arr []string
	splitChar := ","
	var row series.Row
	var v float64
	var d time.Time
	dates := make([]int64, l)
	rows := make([]series.Row, l)

	for i, s := range obj.Data {
		arr = strings.Split(s, splitChar)
		d, _ = time.ParseInLocation("2006-01-02 15:04", arr[0], time.Local)
		dates[i] = datescore.Date2Score(d)

		row = series.NewRow(3)
		v, _ = strconv.ParseFloat(arr[1], 64)
		row[0] = v
		v, _ = strconv.ParseFloat(arr[2], 64)
		row[2] = v
		v, _ = strconv.ParseFloat(arr[3], 64)
		row[1] = v

		rows[i] = row
	}

	t.DataSeries = series.NewSeries("", dates, rows, Fields)
	t.YestodayClose = obj.Info.Float("yc")

	return nil
}

// StoreData 储存数据
func (t *TimeData) StoreData() error {
	return nil
}
