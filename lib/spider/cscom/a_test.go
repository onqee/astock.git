package cscom

import (
	"testing"

	_ "github.com/lib/pq"
	"gitee.com/onqee/astock/lib/data/vars"
)

func Test_TimeData(t *testing.T) {
	fp := NewTimeData(vars.MarketSZ, vars.Code399001)
	err := fp.Run()
	if err != nil {
		t.Fatal(err)
	}

	fp.DataSeries.Print()
}
