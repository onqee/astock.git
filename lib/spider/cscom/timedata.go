package cscom

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"Ana/lib/spider/net"
)

// http://zzw.hsmdb.com/iwin_zzbweb-webapp/quote/v1/trend?prod_code=399006.SZ&fields=last_px,business_amount,avg_px
// 成交量为合计，需要一次相减
const (
	urlTimeData = "http://zzw.hsmdb.com/iwin_zzbweb-webapp/quote/v1/trend?prod_code=%s.%s&fields=last_px,business_amount,avg_px"
)

var (
	// Fields []string
	Fields = []string{"close", "close1", "vol"}
)

// TimeData 分时数据
type TimeData struct {
	net.JSONQuery `json:"-"`
	Market        string
	Code          string
	DataSeries    series.Series
}

// NewTimeData func
func NewTimeData(market, code string) *TimeData {
	t := &TimeData{Code: code, Market: market}
	t.URL = fmt.Sprintf(urlTimeData, code, market)
	t.Target = t

	return t
}

// DoAction 定义具体抓取动作，然后由net.GoQuery 运行
func (t *TimeData) DoAction() error {
	var obj struct {
		Data struct {
			Trend map[string]interface{}
		}
	}

	err := json.Unmarshal(t.Data, &obj)
	if err != nil {
		return err
	}
	t.DataSeries = series.NewEmpty(t.Market+t.Code, Fields)
	trend := obj.Data.Trend
	if trend == nil {
		return nil
	}

	dat, isok := trend[t.Code+"."+t.Market]
	if !isok {
		return nil
	}

	items := dat.([]interface{})
	l := len(items)
	dates := make([]int64, l)
	// rows := make([]series.Row, l)
	cols := series.NewColumnsN(len(Fields), l)

	for i := 0; i < l; i++ {
		arr := items[i].([]interface{})

		d, _ := time.ParseInLocation("200601021504", strconv.FormatInt(int64(arr[0].(float64)), 10), time.Local)
		dates[i] = datescore.Date2Score(d)

		// "close", "close1", "vol"
		row := series.NewRow(3)
		row[0] = arr[1].(float64)
		row[1] = arr[3].(float64)
		if i == 0 {
			row[2] = arr[2].(float64)
		} else {
			row[2] = arr[2].(float64) - (items[i-1].([]interface{}))[2].(float64)
		}

		// rows[i] = row
		cols.SetRow(row, i)
	}

	t.DataSeries = series.NewSeries(t.Code, dates, cols, Fields)
	return nil
}

// StoreData 储存数据
func (t *TimeData) StoreData() error {
	return nil
}
