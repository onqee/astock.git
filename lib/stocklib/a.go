package stocklib

// RangeL 获取之前的一段数据
func RangeL(vals []float64, i, n int) []float64 {
	b := i - n + 1
	if b < 0 {
		b = 0
	}
	return vals[b : i+1]
}
