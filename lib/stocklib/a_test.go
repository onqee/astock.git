package stocklib

import (
	"math"
	"testing"
)

var (
	// sz002975 博杰股份 2020-02-05 2020-03-06
	close = []float64{49.82, 54.8, 60.28, 66.31, 72.94, 80.23, 88.25, 97.08, 106.79, 117.47, 129.22, 128.98, 135.93, 131.17, 127, 115.47, 121.91, 112.3, 118.29, 118.8, 118.28, 130.11, 134.44}
	high  = []float64{49.82, 54.8, 60.28, 66.31, 72.94, 80.23, 88.25, 97.08, 106.79, 117.47, 129.22, 134.6, 137, 133.74, 130.68, 122.8, 123.5, 122.8, 119.97, 123.56, 118.39, 130.11, 140.5}
	low   = []float64{41.52, 54.8, 60.28, 66.31, 72.94, 80.23, 88.25, 97.08, 106.79, 117.47, 123.5, 125.6, 127.08, 127.63, 118.05, 115, 111.2, 111.23, 115.44, 117.27, 113.01, 120.86, 126.37}
)

func TestKD(t *testing.T) {
	k, d := KD(close, high, low, 9, 3, 3)
	if math.Abs(k[11]-97.256796) > 0.0001 || math.Abs(d[11]-99.085598) > 0.0001 {
		t.Fatal(k[11], d[11])
	}
	if math.Abs(k[15]-75.23572) > 0.0001 || math.Abs(d[15]-88.3240438) > 0.0001 {
		t.Fatal(k[15], d[15])
	}
}

func TestMacd(t *testing.T) {
	diff, dea := MACD(close, 12, 26, 9)
	if math.Abs(diff[11]-18.454640) > 0.0001 || math.Abs(dea[11]-10.962749) > 0.0001 {
		t.Fatal(diff[11], dea[11])
	}
	if math.Abs(diff[15]-20.179608) > 0.0001 || math.Abs(dea[15]-16.720589) > 0.0001 {
		t.Fatal(diff[15], dea[15])
	}
}
