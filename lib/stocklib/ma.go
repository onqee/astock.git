package stocklib

import (
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// EMA 指数平均数
// 是一个递归整个序列的计算方法
func EMA(vals []float64, params []int) [][]float64 {
	return EMAapply(vals, params, nil)
}

// EMAapply 指数平均数
// 是一个递归整个序列的计算方法: SMA(length+1, 2)
func EMAapply(vals []float64, params []int, f func(int, [][]float64)) [][]float64 {
	count := len(params)
	p2s := make([][2]int, count)
	for i := 0; i < count; i++ {
		p2s[i][0] = params[i] + 1
		p2s[i][1] = 2
	}
	// SMAapply(vals, length+1, 2, f)

	return SMAapply(vals, p2s, f)
}

// SMA 移动平均
// 是一个递归整个序列的计算方法
func SMA(vals []float64, params [][2]int) [][]float64 {
	return SMAapply(vals, params, nil)
}

func SMAapply(vals []float64, params [][2]int, f func(int, [][]float64)) [][]float64 {
	l := len(vals)
	if l < 1 {
		return nil
	}

	count := len(params)
	arr := make([][]float64, count)
	for i := 0; i < count; i++ {
		arr[i] = util.GetFloats(l)
		arr[i][0] = vals[0]
	}

	// 若Y=SMA(X,N,M) 则 Y=(M*X+(N-M)*Y')/N，其中Y'表示上一周期Y值，N必须大于M
	for i := 1; i < l; i++ {
		for k := 0; k < count; k++ {
			n := float64(params[k][0])
			m := float64(params[k][1])
			arr[k][i] = (m*vals[i] + (n-m)*arr[k][i-1]) / n
		}
		if f != nil {
			f(i, arr)
		}
	}
	return arr
}

// MA 简单平均
func MA(vals []float64, paras []int) [][]float64 {
	l := len(vals)
	if l < 1 {
		return nil
	}

	count := len(paras)
	arr := make([][]float64, count)
	for i := 0; i < count; i++ {
		arr[i] = util.GetFloats(l)
		arr[i][0] = vals[0]
	}

	for i := 1; i < l; i++ {
		for k := 0; k < count; k++ {
			n := paras[k]
			if i < n {
				arr[k][i] = mathlib.Avg(vals[0 : i+1])
			} else {
				arr[k][i] = mathlib.Avg(vals[i-n+1 : i+1])
			}
		}
	}
	return arr
}
