package stocklib

import "gitee.com/onqee/astock/lib/data/vars"

// // RangeHas 区间有
// func RangeHas(f func(index int, vals [][]float64) bool, a, b int, vals ...[]float64) bool {
//
// 	return true
// }

// 金叉、死叉
func Cross(i int, vals1, vals2 []float64) int {
	if i < 1 {
		return 0
	}
	// 金叉
	if vals1[i] > vals2[i] && vals1[i-1] < vals2[i-1] {
		return vars.PositionLow
	}
	//  死叉
	if vals1[i] < vals2[i] && vals1[i-1] > vals2[i-1] {
		return vars.PositionHigh
	}
	//V
	return 0
}

// 拐点
func Inflection(i int, val1 []float64) int {
	if i < 2 {
		return 0
	}
	if val1[i-2] < val1[i-1] && val1[i-1] > val1[i] {
		return vars.PositionHigh
	} else if val1[i-2] > val1[i-1] && val1[i-1] < val1[i] {
		return vars.PositionLow
	}
	return 0
}

// BarsLast 上一次条件成立位置
// n: 第n次出现
func BarsLast(f func(int, [][]float64) bool, index, n int, vals ...[]float64) int {
	l := len(vals[0])
	if l < 3 {
		return -1
	}
	if index == -1 {
		index = l - 1
	}

	count := 0
	for i := index; i > -1; i-- {
		if f(i, vals) {
			count++
		} else {
			continue
		}
		if count == n {
			return i
		}
	}
	return -1
}

// BarsLastCount 当前索引下条件持续成立次数
func BarsLastCount(f func(int, [][]float64) bool, index int, vals ...[]float64) int {
	l := len(vals[0])
	if l < 3 {
		return -1
	}
	if index == -1 {
		index = l - 1
	}

	count := 0
	for ; index > -1; index-- {
		isok := f(index, vals)
		if isok {
			count++
		} else {
			return count
		}
	}
	return count
}
