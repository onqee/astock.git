package stocklib

import (
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// BIAS 乖离率
func BIAS(vals []float64, n int) []float64 {
	// BIAS1 : (CLOSE-MA(CLOSE,P1))/MA(CLOSE,P1)*100;
	l := len(vals)
	// arr := make([]float64, l)
	arr := util.GetFloats(l)
	var x float64
	for i := 0; i < l; i++ {
		x = mathlib.Avg(RangeL(vals, i, n))
		arr[i] = 100 * (vals[i] - x) / x
	}

	return arr
}
