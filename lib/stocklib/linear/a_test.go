package linear

import (
	"math"
	"testing"
)

func Test_LeastSpuares(t *testing.T) {
	y := []float64{3, 2, 7, 12, 7, 12, 10, 21, 17, 22}
	data := []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	a, b := Fitting(data, y)
	if math.Abs(a-2.0909090909) > 0.000001 || math.Abs(b+0.199999999999) > 0.000001 {
		t.Fatal(a, b)
	}

	v := PointToLineY(a, b, 4, 12)
	if math.Abs(v+3.83636) > 0.00001 {
		t.Fatal(a, b, v)
	}

	stdDev := StdDev(data, y, a, b)
	if math.Abs(stdDev-2.894788) > 0.00001 {
		t.Fatal(stdDev)
	}

	a, b = Fitting(data[:7], y[:7])
	if math.Abs(a-1.46428571) > 0.000001 || math.Abs(b-1.714285714) > 0.000001 {
		t.Fatal(a, b)
	}

	data = []float64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	a, b = Fitting(data, y)
	if math.Abs(a-2.09090909) > 0.000001 || math.Abs(b-1.890909) > 0.000001 {
		t.Fatal(a, b)
	}

	y = []float64{11695, 11607.5703, 11326.2695, 9465.7998, 8420.54}
	data = []float64{float64(0), float64(51), float64(79), float64(170), float64(215)}
	a, b = Fitting(data, y)
	if math.Abs(a+16.25703567) > 0.000001 || math.Abs(b-12177.510594) > 0.000001 {
		t.Fatal(a, b)
	}

	stdDev = StdDev(data, y, a, b)
	if math.Abs(stdDev-373.738999) > 0.000001 {
		t.Fatal(stdDev)
	}

}
