package linear

import (
	"math"
)

// Fitting 完成拟合曲线参数计算
// Line: y = ax + c
// return: a, c
func Fitting(dataX, dataY []float64) (float64, float64) {
	l := len(dataX)
	var sumXY, sumY, sumX, sumSqareX float64
	for i := 0; i < l; i++ {
		sumXY += dataX[i] * dataY[i]
		sumY += dataY[i]
		sumX += dataX[i]
		sumSqareX += math.Pow(dataX[i], 2)
	}
	averageX := sumX / float64(l)
	averageY := sumY / float64(l)

	returnK := (float64(l)*sumXY - sumX*sumY) / (float64(l)*sumSqareX - sumX*sumX)
	returnB := averageY - averageX*returnK

	return returnK, returnB
}
