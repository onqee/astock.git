package linear

import (
	"gitee.com/onqee/astock/lib/mathlib"
)

// // PointToLineDistance 点到直线的距离，垂直距离
// // Line: y = ax + b
// func PointToLineDistance(a, b, x, y float64) float64 {
// 	return math.Abs(a*x+b-y) / math.Sqrt(math.Pow(a, 2)+1)
// 	math.Sqrt(a*x + b - y)
// }

// PointToLineY 点到线之间的Y距离值
// Line: y = ax + b
func PointToLineY(a, b, x, y float64) float64 {
	return a*x + b - y
}

// StdDev 距趋势线的标准差
func StdDev(x, y []float64, a, b float64) float64 {
	l := len(x)
	arr := make([]float64, l)
	for i := 0; i < l; i++ {
		arr[i] = PointToLineY(a, b, x[i], y[i])
	}
	return mathlib.StdDev(arr)
}
