package stocklib

import (
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// KDJ
func KDJ(vals, high, low []float64, n, p1, p2 int) ([]float64, []float64, []float64) {
	return kdj(vals, high, low, n, p1, p2, true)
}

// KD func
func KD(vals, high, low []float64, n, p1, p2 int) ([]float64, []float64) {
	k, d, _ := kdj(vals, high, low, n, p1, p2, false)
	return k, d
}

func kdj(vals, high, low []float64, n, p1, p2 int, isJ bool) ([]float64, []float64, []float64) {
	l := len(vals)
	if l < 1 {
		return nil, nil, nil
	}

	rsv := util.GetFloats(l)
	defer util.PutFloats(rsv)
	k := util.GetFloats(l)
	d := util.GetFloats(l)
	var j []float64
	if isJ {
		j = util.GetFloats(l)
	}
	fn := float64(n)
	fp1 := float64(p1)
	fp2 := float64(p2)

	var min float64
	var max float64
	k[0] = 100
	d[0] = 100
	for i := 1; i < l; i++ {
		min = mathlib.Min(RangeL(low, i, n))
		max = mathlib.Max(RangeL(high, i, n))
		rsv[i] = 100 * (vals[i] - min) / (max - min)
		k[i] = (fp1*rsv[i] + (fn-fp1)*k[i-1]) / fn
		d[i] = (fp2*k[i] + (fn-fp2)*d[i-1]) / fn
		if isJ {
			j[i] = 3*k[i] - 2*d[i]
		}
	}

	return k, d, j
}
