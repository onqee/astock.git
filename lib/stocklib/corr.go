package stocklib

import (
	"math"

	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// Corr 相关系数
func Corr(x, y []float64, n int) []float64 {
	l := int(math.Min(float64(len(x)), float64(len(y))))

	if l < 3 {
		return nil
	}

	var end int
	var start int
	// arr := make([]float64, l)
	arr := util.GetFloats(l)

	for i := 1; i < l; i++ {
		end = i + 1
		start = i - n
		if start < 0 {
			start = 0
		}
		arr[i] = mathlib.Corr(x[start:end], y[start:end])
		if math.IsNaN(arr[i]) {
			// 	fmt.Println(x[start:end], y[start:end], mathlib.SumSquaresDev(x[start:end]), mathlib.SumSquaresDev(y[start:end]))
			arr[i] = 0
		}
	}
	return arr
}
