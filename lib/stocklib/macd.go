package stocklib

import "github.com/kere/gno/libs/util"

// MACD func
func MACD(vals []float64, s, p, m int) ([]float64, []float64) {
	l := len(vals)
	if l < s {
		return nil, nil
	}
	diff := util.GetFloats(l)
	dea := util.GetFloats(l)
	n := float64(m + 1)

	params := []int{s, p}
	result := EMAapply(vals, params, func(i int, arr [][]float64) {
		diff[i] = arr[0][i] - arr[1][i]
		if i == 0 {
			dea[0] = diff[0]
			return
		}
		dea[i] = (2*diff[i] + (n-2)*dea[i-1]) / n
	})

	defer util.PutFloats(result[0])
	defer util.PutFloats(result[1])

	return diff, dea
}
