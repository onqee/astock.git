package stocklib

import (
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// BOLL 布林线
func BOLL(vals []float64, m, n int) (mid, upper, lower []float64) {
	// MID :  MA(CLOSE,M);
	// UPPER: MID + N*STD(CLOSE,M);
	// LOWER: MID - N*STD(CLOSE,M);
	paras := util.GetInts(1)
	defer util.PutInts(paras)
	paras[0] = n

	arr := MA(vals, paras)
	mid = arr[0]
	l := len(mid)
	nn := float64(n)
	var std float64
	for i := 0; i < l; i++ {
		std = nn * mathlib.StdDev(RangeL(vals, i, m))
		upper[i] = mid[i] + std
		lower[i] = mid[i] - std
	}
	return mid, upper, lower
}
