package stocklib

// TroughLength 返回往过去搜索第 n 次的波位置
func TroughLength(vals []float64, n int) int {
	l := len(vals)
	count := 0
	for i := l - 2; i > 0; i-- {
		// vals[i] 最小
		if vals[i+1] > vals[i] && vals[i-1] > vals[i] {
			count++
		}
		if count == n {
			return i
		}
	}

	return -1
}

// PeakLength 返回往过去搜索第 n 次的波峰位置
func PeakLength(vals []float64, n int) int {
	l := len(vals)
	count := 0
	for i := l - 2; i > 0; i-- {
		// vals[i] 最大
		if vals[i+1] < vals[i] && vals[i-1] < vals[i] {
			count++
		}
		if count == n {
			return i
		}
	}

	return -1
}
