package stocklib

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
)

// 估值

// BarSimpleValue 一段数据的估值
func BarSimpleValue(bar *bars.Bar, dateA, dateB int64) float64 {
	barDat := bar.Range(dateA, dateB)
	l := barDat.Len()
	if l < 3 {
		return 0
	}
	closeI := bar.FieldI(vars.FieldClose)
	sum := 0.0
	for i := 0; i < l; i++ {
		sum += barDat.Columns[closeI][i]
	}
	return sum / float64(l)
}

// BarSimpleValue2 一段数据的估值
func BarSimpleValue2(bar *bars.Bar, dateA, dateB int64) float64 {
	barDat := bar.Range(dateA, dateB)
	l := barDat.Len()
	if l < 3 {
		return 0
	}
	closeI := bar.FieldI(vars.FieldClose)
	highI := bar.FieldI(vars.FieldHigh)
	lowI := bar.FieldI(vars.FieldLow)
	sum := 0.0
	for i := 0; i < l; i++ {
		sum += (barDat.Columns[closeI][i] + barDat.Columns[highI][i] + barDat.Columns[lowI][i]) / 3
	}
	return sum / float64(l)
}

// BarValueWeighted 一段数据的估值，根据交易量加权
func BarValueWeighted(bar *bars.Bar, dateA, dateB int64) float64 {
	barDat := bar.Range(dateA, dateB)
	l := barDat.Len()
	if l < 3 {
		return 0
	}
	closeI := bar.FieldI(vars.FieldClose)
	volI := bar.FieldI(vars.FieldAmt)
	volSum := 0.0
	sum := 0.0

	for i := 0; i < l; i++ {
		sum += barDat.Columns[closeI][i] * barDat.Columns[volI][i]
		volSum += barDat.Columns[volI][i]
	}
	return sum / volSum
}
