package stocklib

import "github.com/kere/gno/libs/util"

// TDD9 TD马克序列
func TDD9(vals []float64, n int) []float64 {
	l := len(vals)

	// tdd := make([]float64, l)
	tdd := util.GetFloats(l)
	var v int
	var arr []float64
	for i := l - 1; i > 5; i-- {

		arr = vals[:i+1]
		v = tddUpByBar(arr, i, n)
		if v == 0 {
			v = tddDnByBar(arr, i, n)
			v = -v
		}

		tdd[i] = float64(v)
	}

	return tdd
}

// tddDnByBar td马克准备序列 下跌
// return 序号，是否为严格模式
func tddDnByBar(vals []float64, index, n int) int {
	if n <= 0 {
		n = 4
	}

	v := BarsLastCount(func(i int, arr [][]float64) bool {
		tmp := arr[0]
		if i < n {
			return false
		}
		return tmp[i] < tmp[i-n]
	}, index, vals)

	return v
}

// tddUpByBar td马克准备序列 上涨
func tddUpByBar(vals []float64, index, n int) int {
	if n <= 0 {
		n = 4
	}

	v := BarsLastCount(func(i int, arr [][]float64) bool {
		tmp := arr[0]
		if i < n {
			return false
		}
		return tmp[i] > tmp[i-n]
	}, index, vals)

	return v
}
