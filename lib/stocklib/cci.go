package stocklib

import "gitee.com/onqee/astock/lib/mathlib"

// AveDev 平均绝对差
func AveDev(vals []float64, n int) []float64 {
	l := len(vals)
	arr := make([]float64, l)
	arr[0] = 0
	for i := 1; i < l; i++ {
		if i < n {
			arr[i] = mathlib.AvgDev(vals[:i+1])
		} else {
			arr[i] = mathlib.AvgDev(RangeL(vals, i, n))
		}
	}

	return arr
}

// CCI func
func CCI(price, high, low []float64, n int) []float64 {
	// TYP := (HIGH + LOW + CLOSE)/3;
	l := len(price)
	arr := make([]float64, l)
	typs := make([]float64, l)
	for i := 0; i < l; i++ {
		typs[i] = price[i] + high[i] + low[i]
	}

	arr[0] = 0
	var vals []float64
	for i := 1; i < l; i++ {
		vals = RangeL(typs, i, n)
		// CCI1:=(TYP-MA(TYP,M))/(0.015*AVEDEV(TYP,M)), Color2;
		arr[i] = (typs[i] - mathlib.Avg(vals)) / (0.015 * mathlib.AvgDev(vals))
	}

	return arr
}
