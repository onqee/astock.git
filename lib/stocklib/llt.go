package stocklib

import "github.com/kere/gno/libs/util"

// LLT 滤波均线
func LLT(vals []float64, length int) []float64 {
	l := len(vals)
	if l < 2 {
		return nil
	}

	// llt := make([]float64, l)
	llt := util.GetFloats(l)
	llt[0] = vals[0]
	llt[1] = vals[1]
	//   llt = (a -a*a/4)*val +(a*a/2)*Ref(val,1)
	// -(a-3*a*a/4)*Ref(price,2)
	// +2*(1-a) * Ref(LLT,1)
	// -(1-a)*(1-a)*Ref(LLT,2);
	a := 2 / (float64(length) + 1)
	for i := 2; i < l; i++ {
		llt[i] = (a-a*a/4)*vals[i] + (a*a/2)*vals[i-1] - (a-3*a*a/4)*vals[i-2] + 2*(1-a)*llt[i-1] - (1-a)*(1-a)*llt[i-2]
	}

	return llt
}
