package series

import (
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// Aggregate 聚合函数
// l: rows.length ll: fields.length
func Aggregate(l int, fields []string, everyFunc func(i int, k int, total Row) float64, totalFunc func(k int, total Row) float64) Row {
	// l := s.Len()
	ll := len(fields)
	var k int
	totalRow := NewRow(ll)
	for i := 0; i < l; i++ {
		for k = 0; k < ll; k++ {
			totalRow[k] = util.Round(everyFunc(i, k, totalRow), vars.DefaultDecimal)
		}
	}

	if totalFunc != nil {
		for k = 0; k < ll; k++ {
			totalRow[k] = util.Round(totalFunc(k, totalRow), vars.DefaultDecimal)
		}
	}

	return totalRow
}

// AggSTD 聚合函数
// 每一个column 进行聚合
// func(index, filed) (method, args)
// ll: fields.length
func AggSTD(cols Columns, fields []string, everyField func(k int, field string) (string, util.MapData)) Row {
	ll := len(fields)
	var method string
	var args util.MapData
	row := NewRow(ll)
	for k := 0; k < ll; k++ {
		method, args = everyField(k, fields[k])
		if args == nil {
			args = util.MapData{}
		}
		switch method {
		case "sum":
			row[k] = mathlib.Sum(cols[k])
		case "avg":
			row[k] = mathlib.Avg(cols[k])
		case "avg2":
			row[k] = mathlib.Avg2(cols[k], args.FloatDefault("n", 0.2))
		}
	}

	return row
}
