package series

import (
	"encoding/binary"
	"io"
	"strings"

	"github.com/kere/gno/libs/util"
)

// BinHead bin文件的构建参数头
type BinHead struct {
	// Multi     int // 文件模式: 1, 单文件 >1:多文件合并，数量
	Name      string
	Mode      int // 1: A, 2:B, 3:C
	BitSize   int // 储存数值的位数
	Precision int // 数值的变化比例
	Length    int
	Fields    []string
	// Info      map[string]interface{}
}

// NewBinHead 新建文件头
func NewBinHead(name string, fields []string, l int) BinHead {
	return BinHead{Name: name, Length: l, Fields: fields, Precision: 4, BitSize: 32, Mode: 1}
}

// Write head
func (b *BinHead) Write(w io.Writer) error {
	u16 := uint16(b.Mode)
	err := binary.Write(w, binary.LittleEndian, u16)
	if err != nil {
		return err
	}

	u16 = uint16(b.BitSize)
	binary.Write(w, binary.LittleEndian, u16)

	u16 = uint16(b.Precision)
	binary.Write(w, binary.LittleEndian, u16)

	// u32 := uint32(b.Length)
	// binary.Write(w, binary.LittleEndian, u32)

	// 写入 name 长度
	bname := util.Str2Bytes(b.Name)
	u16 = uint16(len(bname))
	binary.Write(w, binary.LittleEndian, u16)

	// 写入fields长度
	sfields := strings.Join(b.Fields, ",")
	bfields := util.Str2Bytes(sfields)
	u16 = uint16(len(bfields))
	binary.Write(w, binary.LittleEndian, u16)

	// 写入 info 长度
	// uinfo := uint16(0)
	// var infoSrc []byte
	// if len(b.Info) > 0 {
	// 	infoSrc, err = json.Marshal(b.Info)
	// 	if err != nil {
	// 		return err
	// 	}
	// 	uinfo = uint16(len(infoSrc))
	// }
	// binary.Write(w, binary.LittleEndian, uinfo)

	if len(bname) > 0 {
		if _, err = w.Write(bname); err != nil {
			return err
		}
	}
	// ++ 写入 fields
	if _, err = w.Write(bfields); err != nil {
		return err
	}
	// if uinfo > 0 {
	// 	// ++ 写入 info
	// 	if _, err = w.Write(infoSrc); err != nil {
	// 		return err
	// 	}
	// }
	return nil
}

// HeadRead head
func HeadRead(r io.Reader) (BinHead, error) {
	head := BinHead{}
	var u16 uint16

	err := binary.Read(r, binary.LittleEndian, &u16)
	if err != nil {
		return head, err
	}
	head.Mode = int(u16)

	binary.Read(r, binary.LittleEndian, &u16)
	head.BitSize = int(u16)

	binary.Read(r, binary.LittleEndian, &u16)
	head.Precision = int(u16)

	// var u32 uint32
	// binary.Read(r, binary.LittleEndian, &u32)
	// head.Length = int(u32)

	// Name len
	binary.Read(r, binary.LittleEndian, &u16)
	nameSize := int(u16)
	// Fields len
	binary.Read(r, binary.LittleEndian, &u16)
	fieldSize := int(u16)
	// InfoN len
	// binary.Read(r, binary.LittleEndian, &u16)
	// infoSize := int(u16)

	// +读取 name
	bname := util.GetBytes(nameSize)
	defer util.PutBytes(bname)
	r.Read(bname)
	head.Name = string(bname)

	// +读取 fields
	bfields := util.GetBytes(fieldSize)
	defer util.PutBytes(bfields)
	r.Read(bfields)
	head.Fields = strings.Split(string(bfields), ",")

	// +读取 Info
	// if infoSize > 0 {
	// 	bSrc := make([]byte, infoSize)
	// 	r.Read(bSrc)
	// 	var info map[string]interface{}
	// 	err = json.Unmarshal(bSrc, &info)
	// 	head.Info = info
	// }
	return head, err
}
