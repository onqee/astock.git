package series

import (
	"compress/gzip"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitee.com/onqee/astock/lib/data/vars"
)

func BinNumBit(dat *A) int {
	n := dat.Len()
	if n == 0 {
		return 0
	}
	index := n - 10
	if index < 0 {
		index = 0
	}
	row := dat.GetRowP()
	defer PutRow(row)
	count := len(row)
	for i := index; i < n; i++ {
		dat.RowAt(i, row)
		for k := 0; k < count; k++ {
			if row[k] > 214748 {
				return 64
			}
		}
	}
	return 32
}

// WriteBin 写入bin 文件
// arg1 : precision: 小数精度
// arg2 : bitN: float64位数
func WriteBin(dat *A, filename string, args ...int) error {
	return writeBin(dat, filename, os.O_TRUNC, args...)
}
func AppendBin(dat *A, filename string, args ...int) error {
	if dat.Len() == 0 {
		return nil
	}
	file, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}
	var r io.Reader
	if filepath.Ext(filename) == vars.ExtZip {
		// gzip
		zr, err := gzip.NewReader(file)
		if err != nil {
			return err
		}
		defer zr.Close()
		r = zr

	} else {
		r = file
	}
	head, err := HeadRead(r)
	file.Close()
	if err != nil {
		return err
	}
	if len(head.Fields) != len(dat.Fields) {
		return fmt.Errorf("Fields 不匹配%v != %v", head.Fields, dat.Fields)
	}

	return writeBin(dat, filename, os.O_APPEND, head.Precision, head.BitSize)
}

func writeBin(dat *A, filename string, mod int, args ...int) error {
	if dat.Len() == 0 || len(dat.Columns) == 0 {
		return nil
	}
	precision := 4
	if len(args) > 0 {
		precision = args[0]
	}
	bitN := 32
	if len(args) == 2 {
		bitN = args[1]
	}
	if bitN == 0 {
		return fmt.Errorf("BitN = %d", bitN)
	}

	path := filepath.Dir(filename)
	os.MkdirAll(path, os.ModeDir)
	head := NewBinHead(dat.Name, dat.Fields, dat.Len())
	head.Precision = precision
	head.BitSize = bitN
	// head.Info = dat.Info
	if mod == os.O_APPEND {
		head.Mode = 0
	}

	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|mod, os.ModePerm)
	defer file.Close()
	if err != nil {
		return err
	}

	var w io.Writer
	if filepath.Ext(filename) == vars.ExtZip {
		zw := gzip.NewWriter(file)
		defer zw.Close()
		// Setting the Header fields is optional.
		zw.Name = strings.TrimRight(filepath.Base(filename), vars.ExtZip)
		zw.ModTime = time.Now()
		w = zw
	} else {
		w = file
	}

	return binWriteWith(w, dat.Dates, dat.Columns, head)
}

// binWriteWith 写入bin 文件
func binWriteWith(w io.Writer, dates []int64, cols Columns, head BinHead) error {
	if len(cols) == 0 {
		return nil
	}
	l := len(cols[0])
	count := len(cols)
	for i := 1; i < count; i++ {
		if len(cols[i]) != l {
			return fmt.Errorf("Columns 的数据长度不相等 %d != %d", l, len(cols[i]))
		}
	}

	if head.Mode != 0 {
		head.Write(w)
	}

	ratio := float64(1)
	if head.Precision > 0 {
		ratio = math.Pow10(head.Precision)
	}

	n := cols.Len()
	if head.BitSize == 64 {
		for i := 0; i < l; i++ {
			if err := binary.Write(w, binary.LittleEndian, dates[i]); err != nil {
				return err
			}

			for k := 0; k < n; k++ {
				v64 := int64(cols[k][i] * ratio)
				if err := binary.Write(w, binary.LittleEndian, v64); err != nil {
					return err
				}
			}
		}
	} else {
		for i := 0; i < l; i++ {
			if err := binary.Write(w, binary.LittleEndian, dates[i]); err != nil {
				return err
			}

			for k := 0; k < n; k++ {
				v32 := int32(cols[k][i] * ratio)
				if err := binary.Write(w, binary.LittleEndian, v32); err != nil {
					return err
				}
			}
		}
	}

	return nil
}
