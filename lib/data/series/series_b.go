package series

import (
	"fmt"
	"sort"
	"strings"

	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

// B 序列数据
// columns -> strColumns
type B struct {
	Name    string           `json:"name"`
	Dates   util.Int64sOrder `json:"dates"`
	Fields  []string         `json:"fields"`
	Info    util.MapData     `json:"info"`
	Columns ColumnsB         `json:"columns"`
}

// NewB func
func NewB(name string, dates []int64, colsB ColumnsB, fields []string) B {
	l := 0
	for i := range colsB {
		if i == 0 {
			l = len(colsB[0])
			continue
		}
		if l != len(colsB[i]) {
			panic(myerr.New("数据列的长度不相等", l, len(colsB[i])).Stack().Error())
		}
	}

	if len(dates) != l {
		panic(myerr.New("数据列的长度不相等", l, len(dates)).Stack().Error())
	}
	s := B{Columns: colsB}
	s.Name = name
	s.Dates = dates
	s.Fields = fields
	return s
}

// IndexOn index
func (s *B) IndexOn(date int64) int {
	return util.IndexOfInt64s(date, s.Dates)
}

// Search return (index, status)
func (s *B) Search(date int64) (int, int) {
	l := len(s.Dates)
	switch {
	case l == 0:
		return -1, IndexOverRangeLeft
	case date < s.Dates[0]:
		return 0, IndexOverRangeLeft
	case date > s.Dates[l-1]:
		return l - 1, IndexOverRangeRight
	}

	index := util.SearchInt64s(date, s.Dates)
	if s.Dates[index] != date {
		return index - 1, IndexNotFound
	}
	return index, IndexFound
}

// AddRow series without sort
func (s *B) AddRow(date int64, row StrRow) {
	if s.Columns.Len() == 0 && len(s.Fields) > 0 {
		s.Columns = NewColumnsB(len(s.Fields))
	}
	s.Dates = append(s.Dates, date)
	s.Columns.AddRow(row)
}

// Range get data range
func (s *B) Range(dateA, dateB int64) B {
	a, b := RangeI(dateA, dateB, s.Dates)
	if a >= b {
		return B{}
	}
	l := len(s.Columns)
	cols := NewColumnsB(l)
	for k := 0; k < l; k++ {
		cols[k] = s.Columns[k][a:b]
	}

	return NewB(s.Name, s.Dates[a:b], cols, s.Fields)
}

// IRange get data range
func (s *B) IRange(a, b int) B {
	cols := s.Columns.IRange(a, b)
	if len(cols) == 0 {
		return B{}
	}

	return NewB(s.Name, s.Dates[a:b+1], cols, s.Fields)
}

// Sort sort series
func (s *B) Sort() {
	sort.Sort(s)
}

// Reverse sort series
func (s *B) Reverse() {
	sort.Sort(sort.Reverse(s))
}

// Len get data length
func (s *B) Len() int {
	if s == nil {
		return 0
	}
	return len(s.Dates)
}

// Less sort series
func (s *B) Less(i, j int) bool {
	return s.Dates[i] < s.Dates[j]
}

// Swap sort series
func (s *B) Swap(i, j int) {
	l := len(s.Columns)
	for k := 0; k < l; k++ {
		s.Columns[k][i], s.Columns[k][j] = s.Columns[k][j], s.Columns[k][i]
	}
	s.Dates[i], s.Dates[j] = s.Dates[j], s.Dates[i]
}

// PrintB series
func PrintB(s *B) {
	l := s.Len()
	fmt.Println("SeriesB:", s.Name, " size:", l)
	if len(s.Info) > 0 {
		fmt.Println("info:", s.Info)
	}

	var str strings.Builder
	str.WriteString("key")
	str.WriteRune('\t')
	for k := range s.Fields {
		str.WriteRune('\t')
		str.WriteString(s.Fields[k])
	}
	fmt.Println(str.String())

	if l == 0 {
		fmt.Println(util.PrintLine, l)
		return
	}

	if l < 100 {
		for i := 0; i < l; i++ {
			fmt.Println(s.Dates[i], "\t", s.Columns.RowString(i))
		}
		fmt.Println(util.PrintLine, l)
		return
	}

	for i := 0; i < 20; i++ {
		fmt.Println(s.Dates[i], "\t", s.Columns.RowString(i))
	}

	a := l/2 - 10
	fmt.Println(util.PrintLine, a)

	for i := a; i < a+21; i++ {
		fmt.Println(s.Dates[i], "\t", s.Columns.RowString(i))
	}

	fmt.Println(util.PrintLine, a+20)

	for i := l - 21; i < l; i++ {
		fmt.Println(s.Dates[i], "\t", s.Columns.RowString(i))
	}
	fmt.Println(util.PrintLine, l)
}

// Copy data
func (s *B) Copy() B {
	dates := make([]int64, len(s.Dates))
	copy(dates, s.Dates)
	return NewB(s.Name, dates, s.Columns.Copy(), s.Fields)
}

// Values get values
func (s *B) Values(field string) []string {
	k := FieldI(field, s.Fields)
	if k < 0 {
		panic(fmt.Errorf("Series::Values %s not found in %v", field, s.Fields))
	}
	return s.Columns[k]
}

// GetRowP of date score
func (s *B) GetRowP() StrRow {
	n := len(s.Columns)
	if n == 0 {
		return nil
	}
	row := util.GetStrings(n)
	return StrRow(row)
}

// RowAt of date score
func (s *B) RowAt(i int, row StrRow) {
	s.Columns.RowAt(i, row)
}
