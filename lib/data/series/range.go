package series

import (
	"gitee.com/onqee/astock/lib/data/series/datescore"
)

// RangeI return index
func (s *A) RangeI(dateA, dateB int64) (int, int) {
	return RangeI(dateA, dateB, s.Dates)
}

// RangeI return index
func RangeI(dateA, dateB int64, dates []int64) (int, int) {
	l := len(dates)
	if l == 0 {
		return -1, -1
	}
	if dateA > dateB {
		return -1, -1
	}

	var a, b, stat int
	a, stat = Search(dateA, dates)
	switch stat {
	case IndexNotFound:
		a++
	case IndexOverRangeRight:
		return -1, -1
	}

	if dateB >= dates[l-1] {
		b = l - 1
	} else {
		b, stat = Search(dateB, dates)
		if stat == IndexOverRangeLeft {
			return -1, -1
		}
	}
	if a > b {
		return -1, -1
	}

	return a, b
}

// RangeAmI return 上午 index
func RangeAmI(day int64, dates []int64) (int, int) {
	d, _ := datescore.ScoreSplit(day)
	return RangeI(d*10000, d*10000+1159, dates)
}

// RangePmI return 下午 index
func RangePmI(day int64, dates []int64) (int, int) {
	d, _ := datescore.ScoreSplit(day)
	return RangeI(d*10000+1159, d*10000+2359, dates)
}

// RangeDayI return index
func RangeDayI(day int64, dates []int64) (int, int) {
	d, _ := datescore.ScoreSplit(day)
	return RangeI(d*10000, d*10000+2359, dates)
}

// RangeAm return 上午
func RangeAm(s *A, day int64) A {
	d, _ := datescore.ScoreSplit(day)
	return s.Range(d*10000, d*10000+1159)
}

// RangePm return 下午
func RangePm(s *A, day int64) A {
	d, _ := datescore.ScoreSplit(day)
	return s.Range(d*10000+1159, d*10000+2359)
}

// RangeDay get data range by day
func RangeDay(s *A, day int64) A {
	d, _ := datescore.ScoreSplit(day)
	return s.Range(d*10000, d*10000+2359)
}

// Range get data range
func (s *A) Range(dateA, dateB int64) A {
	if s.Len() == 0 {
		return EmptyA
	}
	a, b := s.RangeI(dateA, dateB)
	if a > b || a == -1 {
		return EmptyA
	}

	l := len(s.Columns)
	cols := NewColumns(l)
	for k := 0; k < l; k++ {
		cols[k] = s.Columns[k][a : b+1]
	}

	return NewA(s.Name, s.Dates[a:b+1], cols, s.Fields)
}

// IRange get data range
func (s *A) IRange(a, b int) A {
	l := s.Len()
	if l == 0 {
		return *s
	}

	if b == -1 || b > l {
		b = l - 1
	}
	if a == -1 || a > b {
		return EmptyA
	}

	cols := s.Columns.IRange(a, b)
	if len(cols) == 0 {
		return EmptyA
	}

	return NewA(s.Name, s.Dates[a:b+1], cols, s.Fields)
}

// OffsetI 开始index, 向右移动，共有n个长度，值返回索引
// 向左 -n 向右n
func OffsetI(index, n int, dates []int64) (int, int) {
	l := len(dates)
	k := index + n
	if k > l {
		k = l - 1
	}
	if k < 0 {
		k = 0
	}
	if n > 0 {
		return index, k
	}
	return k, index
}

// IOffset 开始index, 向右移动，共有n个长度，值返回索引
// 向左 -n 向右n
func IOffset(val int64, n int, dates []int64) (int, int) {
	index, stat := Search(val, dates)
	if stat == IndexOverRangeRight && n >= 0 {
		return -1, -1
	}
	if stat == IndexOverRangeLeft && n < 0 {
		return -1, -1
	}

	return OffsetI(index, n, dates)
}

// RangeL 向right, n or left -n 个长度
func (s *A) RangeL(date int64, n int) A {
	return s.RangeN(date, -n)
}

// RangeR 向right, n or left -n 个长度
func (s *A) RangeR(date int64, n int) A {
	return s.RangeN(date, n)
}

// RangeN 向right, n or left -n 个长度
func (s *A) RangeN(date int64, n int) A {
	start, end := IOffset(date, n, s.Dates)
	if start < 0 {
		return EmptyA
	}

	ll := s.Columns.Len()
	cc := NewColumns(ll)

	end++
	for i := 0; i < ll; i++ {
		cc[i] = s.Columns[i][start:end]
	}
	dates := s.Dates[start:end]
	return NewA(s.Name, dates, cc, s.Fields)
}

// LenBetween int
func LenBetween(dateA, dateB int64, dates []int64) int {
	a, b := RangeI(dateA, dateB, dates)
	return b - a
}
