package series

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"github.com/kere/gno/libs/myerr"
)

// Columns in series
type Columns [][]float64

// NewColumns row
func NewColumns(n int) Columns {
	return make([][]float64, n)
}

// NewColumnsN row
// params: colN, length, cap
func NewColumnsN(arr ...int) Columns {
	n := arr[0]
	l := arr[1]
	capN := l
	if len(arr) == 3 {
		capN = arr[2]
	}

	dat := make([][]float64, n)
	for i := 0; i < n; i++ {
		dat[i] = make([]float64, l, capN)
	}

	return dat
}

// ValueAt 查找数据列的数值
func (c Columns) ValueAt(i int, field string, fields []string) float64 {
	l := len(fields)
	for k := 0; k < l; k++ {
		if field == fields[k] {
			if i >= len(c[k]) {
				panic("超出了数组边界")
			}
			return c[k][i]
		}
	}

	return 0
}

// // RowAt of index
// func (c Columns) RowAt(i int) Row {
// 	l := len(c)
// 	row := NewRow(l)
// 	for k := 0; k < l; k++ {
// 		row[k] = c[k][i]
// 	}
//
// 	return row
// }

// RowAt of index
func (c Columns) RowAt(i int, row Row) {
	l := len(c)
	for k := 0; k < l; k++ {
		row[k] = c[k][i]
	}
}

// AddRow 添加数据row
func (c Columns) AddRow(row Row) {
	columnAddRow(c, row, false)
}

// AddRow0 添加数据row
func (c Columns) AddRow0(row Row) {
	columnAddRow(c, row, true)
}

// AddRow0 添加数据row，然后把row写入0
func columnAddRow(c Columns, row Row, isZero bool) {
	ll := len(row)
	if ll != len(c) {
		panic(myerr.New("不能添加Row，字段不匹配", len(row), len(c)).Error())
	}

	for i := 0; i < ll; i++ {
		c[i] = append(c[i], row[i])
		if isZero {
			row[i] = 0
		}
	}
}

// SetRow 设置数据row
func (c Columns) SetRow(i int, row Row) {
	l := c.Len()
	if len(row) != l {
		panic(fmt.Sprintf("列数量与设置的Row字段数量不相等，row:%d cols:%d", len(row), l))
	}

	for k := 0; k < l; k++ {
		c[k][i] = row[k]
	}
}

// SetRow0 设置数据row，然后清零
func (c Columns) SetRow0(i int, row Row) {
	l := c.Len()
	if len(row) != l {
		panic(fmt.Sprintf("列数量与设置的Row字段数量不相等，row:%d cols:%d", len(row), l))
	}

	for k := 0; k < l; k++ {
		c[k][i] = row[k]
		row[k] = 0
	}
}

// Len length
func (c Columns) Len() int {
	return len(c)
}

// DataLen length
func (c Columns) DataLen() int {
	if len(c) == 0 {
		return 0
	}
	return len(c[0])
}

// Append 添加数据
func (c Columns) Append(cols Columns) Columns {
	n := len(c)
	if n != len(cols) {
		panic(myerr.New("不能添加Columns，字段不匹配", n, " ", len(c)).Error())
	}
	l := len(c[0]) + len(cols[0])
	d := NewColumnsN(n, 0, l)
	for k := 0; k < n; k++ {
		d[k] = append(d[k], c[k]...)
		d[k] = append(d[k], cols[k]...)
	}
	return d
}

// SetCols set数据
func (c Columns) SetCols(fromI int, cols Columns) error {
	n := len(c)
	if n != len(cols) {
		return myerr.New("不能设置Columns，字段不匹配", n, " ", len(c))
	}
	l := len(c[0])
	ll := len(cols[0])
	if fromI+ll > l {
		return myerr.New("数据超过索引上线")
	}

	for k := 0; k < n; k++ {
		for i := 0; i < ll; i++ {
			c[k][fromI+i] = cols[k][i]
		}
	}

	return nil
}

// RowString flat string row
func (c Columns) RowString(i int) string {
	ll := len(c)
	var str strings.Builder
	for k := 0; k < ll; k++ {
		v := c[k][i]
		diff := math.Abs(v - float64(int64(v)))
		if diff < 0.00001 {
			str.WriteString(fmt.Sprint(int64(v)))
		} else if diff < 0.0001 {
			str.WriteString(strconv.FormatFloat(v, 'f', 4, 64))
		} else {
			str.WriteString(strconv.FormatFloat(v, 'f', 2, 64))
		}

		str.WriteRune('\t')
	}
	return str.String()
}

// IRange get data range
func (c Columns) IRange(a, b int) Columns {
	ll := len(c)
	if ll == 0 {
		return nil
	}
	l := len(c[0])
	if b == -1 || b > l {
		b = l - 1
	}

	if a == -1 || a > b {
		return nil
	}

	cols := NewColumns(ll)
	for k := 0; k < ll; k++ {
		cols[k] = c[k][a : b+1]
	}

	return cols
}

// RangeLeftN get data range by index and length
func (c Columns) RangeLeftN(index, n int) Columns {
	l := c.Len()
	cc := NewColumns(l)
	b := index - n + 1
	if b < 0 {
		b = 0
	}
	e := index + 1
	for i := 0; i < l; i++ {
		cc[i] = c[i][b:e]
	}
	return cc
}

// ColumnApply 返回列
func ColumnApply(c Columns, f func(i int) float64) []float64 {
	if len(c) == 0 {
		return nil
	}

	l := len(c[0])
	arr := make([]float64, l)
	for i := 0; i < l; i++ {
		arr[i] = f(i)
	}

	return arr
}

// ColumnsApply 函数
func ColumnsApply(c Columns, everyFunc func(i int) Row) Columns {
	if len(c) == 0 {
		return nil
	}
	l := len(c[0])
	if l == 0 {
		return nil
	}

	row := everyFunc(0)
	cols := NewColumnsN(row.Len(), l)
	cols.SetRow(0, row)

	for i := 1; i < l; i++ {
		cols.SetRow(i, everyFunc(i))
	}

	return cols
}

// CopyColumns of
func CopyColumns(cols Columns) Columns {
	n := cols.Len()
	if n < 0 {
		return nil
	}
	l := len(cols[0])
	arr := make([][]float64, n)
	for k := 0; k < n; k++ {
		arr[k] = GetColumn(l)
		copy(arr[k], cols[k])
	}
	return arr
}

// CopyColumnsTo of
func CopyColumnsTo(from, to Columns) {
	n := from.Len()
	if n < 0 {
		return
	}

	for k := 0; k < n; k++ {
		copy(to[k], from[k])
	}
}
