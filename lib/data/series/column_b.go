package series

import (
	"fmt"
	"strings"

	"github.com/kere/gno/libs/myerr"
)

// ColumnsB in string
type ColumnsB [][]string

// NewColumnsB row
func NewColumnsB(n int) ColumnsB {
	return make([][]string, n)
}

// NewColumnsBN row
func NewColumnsBN(arr ...int) ColumnsB {
	n := arr[0]
	l := arr[1]
	capN := l
	if len(arr) == 3 {
		capN = arr[2]
	}

	dat := make([][]string, n)
	for i := 0; i < n; i++ {
		dat[i] = make([]string, l, capN)
	}
	return dat
}

// ValueAt 查找数据列的数值
func (c ColumnsB) ValueAt(i int, field string, fields []string) string {
	l := len(fields)
	for k := 0; k < l; k++ {
		if field == fields[k] {
			if i >= len(c[k]) {
				panic("超出了数组边界")
			}
			return c[k][i]
		}
	}
	panic("没有找到")
}

// Copy of
func (c ColumnsB) Copy() ColumnsB {
	n := c.Len()
	if n < 0 {
		return nil
	}
	l := len(c[0])
	cols := make([][]string, n)
	for k := 0; k < n; k++ {
		src := make([]string, l)
		copy(src, c[k])
		cols[k] = src
	}
	return cols
}

// RowAt of date score
func (c ColumnsB) RowAt(i int, row StrRow) {
	l := len(c)
	for k := 0; k < l; k++ {
		row[k] = c[k][i]
	}
}

// AddRow 添加数据row
func (c ColumnsB) AddRow(row StrRow) {
	ll := len(row)
	if ll != len(c) {
		panic(myerr.New("不能添加Row，字段不匹配", len(row), " ", len(c)).Error())
	}

	for k := 0; k < ll; k++ {
		c[k] = append(c[k], row[k])
	}
}

// SetRow 设置数据row
func (c ColumnsB) SetRow(row StrRow, i int) {
	l := c.Len()
	if len(row) != l {
		panic("列数量与设置的Row字段数量不相等")
	}

	for k := 0; k < l; k++ {
		c[k][i] = row[k]
	}
}

// Len length
func (c ColumnsB) Len() int {
	return len(c)
}

// Append 添加数据
func (c ColumnsB) Append(cols ColumnsB) ColumnsB {
	n := len(c)
	if n != len(cols) {
		panic(myerr.New("不能添加ColumnsB，字段不匹配", n, len(cols)).Error())
	}
	l := len(c[0]) + len(cols[0])
	d := NewColumnsBN(n, 0, l)
	for k := 0; k < n; k++ {
		d[k] = append(d[k], c[k]...)
		d[k] = append(d[k], cols[k]...)
	}
	return d
}

// SetCols set数据
func (c ColumnsB) SetCols(fromI int, cols ColumnsB) error {
	n := len(c)
	if n != len(cols) {
		return myerr.New("不能设置ColumnsB，字段不匹配", n, " ", len(c))
	}
	l := len(c[0])
	ll := len(cols[0])
	if fromI+ll > l {
		return myerr.New("数据超过索引上线")
	}

	for k := 0; k < n; k++ {
		for i := 0; i < ll; i++ {
			c[k][fromI+i] = cols[k][i]
		}
	}

	return nil
}

// RowString flat string row
func (c ColumnsB) RowString(i int) string {
	ll := len(c)
	var str strings.Builder
	for k := 0; k < ll; k++ {
		str.WriteString(fmt.Sprint(c[k][i]))
		str.WriteRune('\t')
	}
	return str.String()
}

// IRange get data range
func (c ColumnsB) IRange(a, b int) ColumnsB {
	if c.Len() == 0 {
		return nil
	}

	if a < 0 {
		a = 0
	}

	if a > b {
		return nil
	}
	ll := len(c)
	cols := NewColumnsB(ll)
	for k := 0; k < ll; k++ {
		cols[k] = c[k][a : b+1]
	}

	return cols
}

// RangeLeftN get data range by index and length
func (c ColumnsB) RangeLeftN(index, n int) ColumnsB {
	l := c.Len()
	cc := NewColumnsB(l)
	b := index - n + 1
	if b < 0 {
		b = 0
	}
	e := index + 1
	for i := 0; i < l; i++ {
		cc[i] = c[i][b:e]
	}
	return cc
}
