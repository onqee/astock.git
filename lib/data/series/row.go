package series

import (
	"fmt"
	"math"
)

// Row in series
type Row []float64

// NewRow row
func NewRow(n int) Row {
	return make([]float64, n)
}

// Len int
func (r Row) Len() int {
	return len(r)
}

// PrintRow f
func PrintRow(r Row, fields []string) {
	l := len(r)
	s := ""
	for i := 0; i < l; i++ {
		s += fields[i] + "=" + fmt.Sprint(r[i]) + " "
	}
	fmt.Println(s)
}

// Set float64
func (r Row) Set(field string, v float64, fields []string) {
	i := FieldI(field, fields)
	if i < 0 {
		return
	}
	r[i] = v
}

// ValueOf float64
func (r Row) ValueOf(field string, fields []string) float64 {
	for i := range fields {
		if fields[i] == field {
			return r[i]
		}
	}
	return 0
}

// RowHasNaN 是否有NaN
func RowHasNaN(r Row) bool {
	for i := range r {
		if math.IsNaN(r[i]) {
			return true
		}
	}
	return false
}

// RowIsInvalid 是否有NaN, inf
func RowIsInvalid(r Row) bool {
	for i := range r {
		v := r[i]
		if math.IsNaN(v) || math.IsInf(v, 1) || math.IsInf(v, -1) {
			return true
		}
	}
	return false
}

// RowIsEmpty 是否empty
func RowIsEmpty(r Row) bool {
	l := len(r)
	for i := 0; i < l; i++ {
		if r[i] != 0 {
			return false
		}
	}
	return true
}

// StrRow in series
type StrRow []string

// NewStrRow row
func NewStrRow(n int) StrRow {
	return make([]string, n)
}

// Len int
func (r StrRow) Len() int {
	return len(r)
}

// Print f
func (r StrRow) Print(fields []string) {
	l := len(r)
	s := ""
	for i := 0; i < l; i++ {
		s += fields[i] + "=" + fmt.Sprint(r[i]) + " "
	}
	fmt.Println(s)
}

// Set float64
func (r StrRow) Set(field string, v string, fields []string) {
	i := FieldI(field, fields)
	if i < 0 {
		return
	}
	r[i] = v
}

// ValueOf float64
func (r StrRow) ValueOf(field string, fields []string) string {
	for i := range fields {
		if fields[i] == field {
			return r[i]
		}
	}
	return ""
}
