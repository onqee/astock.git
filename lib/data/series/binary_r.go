package series

import (
	"compress/gzip"
	"encoding/binary"
	"errors"
	"io"
	"math"
	"os"
	"path/filepath"

	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

// ReadBinP 读取Bin文件
// P:代表从pool缓存池中生成
func ReadBin(filename string, start, end int64, isPool bool) (A, error) {
	return readBinF(filename, nil, start, end, isPool)
}

// ReadBinF 读取Bin文件，可以过滤字段
// P:代表从pool缓存池中生成
func ReadBinF(filename string, fields []string, start, end int64, isPool bool) (A, error) {
	return readBinF(filename, fields, start, end, isPool)
}

func readBinF(filename string, fields []string, start, end int64, isPool bool) (A, error) {
	file, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	defer file.Close()
	if err != nil {
		return EmptyA, err
	}

	var r io.Reader
	if filepath.Ext(filename) == vars.ExtZip {
		// gzip
		zr, err := gzip.NewReader(file)
		if err != nil {
			return EmptyA, err
		}
		defer zr.Close()
		r = zr

	} else {
		r = file
	}

	param := readParam{Min: start, Max: end, IsPool: isPool}
	_, dat, err := readOne(r, fields, param)
	return dat, err
}

type readParam struct {
	Period   string
	Min, Max int64
	IsPool   bool // 是否返回pool数据
}

func readOne(r io.Reader, fields []string, param readParam) (BinHead, A, error) {
	head, err := HeadRead(r)
	if err != nil {
		return head, EmptyA, err
	}

	l := head.Length
	// prepareFunc(l)

	ll := len(head.Fields)
	if ll == 0 {
		return head, EmptyA, myerr.New("文件头没有fields")
	}

	ratio := float64(1)
	if head.Precision > 0 {
		ratio = math.Pow10(head.Precision)
	}

	// 筛选字段的索引
	lf := len(fields)
	var allows []int
	var allowFields []string
	if lf != 0 {
		// 如果fields允许的字段>0，重新设置head的fields
		// allows = make([]int, 0, lf)
		allows = util.GetInts(0, lf)
		defer util.PutInts(allows)
		allowFields = make([]string, 0, lf)
		for i := 0; i < ll; i++ {
			for k := 0; k < lf; k++ {
				if head.Fields[i] == fields[k] {
					allows = append(allows, i)
					allowFields = append(allowFields, fields[k])
					break
				}
			}
		}
		head.Fields = allowFields
	}

	var dat A
	if param.IsPool {
		dat = GetA(head.Fields, 0, l)
	} else {
		dat = A{Fields: head.Fields}
		dat.Dates = make([]int64, 0, l)
		dat.Columns = NewColumnsN(ll, 0, l)
	}
	// cols := dat.Columns

	row := GetRow(len(head.Fields))
	defer PutRow(row)
	skip64 := util.GetBytes(8 * ll)
	defer util.PutBytes(skip64)
	skip32 := util.GetBytes(4 * ll)
	defer util.PutBytes(skip32)

	for true {
		// date, isok := idxExec(i)
		var date int64
		if err := binary.Read(r, binary.LittleEndian, &date); err != nil {
			if errors.Is(err, io.ErrUnexpectedEOF) {
				return head, dat, err
			}
			if errors.Is(err, io.EOF) {
				break
			}
			return head, dat, err
		}

		isok := false
		if param.Min == 0 && param.Max == 0 {
			isok = true
		} else if param.Min == 0 {
			isok = date <= param.Max
		} else if param.Max == 0 {
			isok = date >= param.Min
		} else {
			isok = param.Min <= date && date <= param.Max
		}

		if isok {
			hh := 0
			if head.BitSize == 64 {
				// loop 每一个fields
				for k := 0; k < ll; k++ {
					var v64 int64
					err := binary.Read(r, binary.LittleEndian, &v64)
					if err != nil {
						return head, dat, err
					}
					if lf == 0 {
						// cols[k][i] = float64(v64) / ratio
						row[k] = float64(v64) / ratio

					} else {
						// 过滤字段
						for h := hh; h < lf; h++ {
							if allows[h] == k {
								// cols[h][i] = float64(v64) / ratio
								row[h] = float64(v64) / ratio
								hh = h
								break
							}
						}
					}
				}
			} else {
				for k := 0; k < ll; k++ {
					var v32 int32
					err := binary.Read(r, binary.LittleEndian, &v32)
					if err != nil {
						return head, dat, err
					}
					if lf == 0 {
						// cols[k][i] = float64(v32) / ratio
						row[k] = float64(v32) / ratio
					} else {
						// 过滤字段
						for h := hh; h < lf; h++ {
							if allows[h] == k {
								// cols[h][i] = float64(v32) / ratio
								row[h] = float64(v32) / ratio
								hh = h
								break
							}
						}
					}
				}
			} // if else end
		} else {
			if head.BitSize == 64 {
				binary.Read(r, binary.LittleEndian, skip64)
			} else {
				binary.Read(r, binary.LittleEndian, skip32)
			}
		}

		if !isok {
			continue
		}
		dat.AddRow0(date, row)

	} // loop l

	// dat.Info = head.Info
	dat.Name = head.Name

	return head, dat, nil
}
