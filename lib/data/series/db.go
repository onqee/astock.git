package series

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"

	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/db"
	"github.com/kere/gno/httpd"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

var (
	dbFieldsMap       = make(map[string][]string)
	dbFieldsMapLocker = sync.RWMutex{}
)

// LoadFields 数据表的字段
func LoadFields(table string) []string {
	dbFieldsMapLocker.RLock()
	v, isOK := dbFieldsMap[table]
	dbFieldsMapLocker.RUnlock()
	if isOK {
		return v
	}
	dbFieldsMapLocker.Lock()
	v = LoadFieldsDB(table)
	dbFieldsMap[table] = v
	dbFieldsMapLocker.Unlock()
	return v
}

// LoadDBFields 数据表的字段
func LoadFieldsDB(table string) []string {
	q := db.NewQuery(vars.TableFields)
	row, err := q.Where("table_name=$1", table).QueryOneP()
	if err != nil {
		panic(err)
	}
	var arr []string
	json.Unmarshal(row.Bytes("fields_json"), &arr)
	db.PutRow(row.Values)
	return arr
}

// SaveSeriesToDB 保存series到数据表
func SaveSeriesToDB(s *A, table, code string, exec func(i int, row []interface{}) []interface{}, isUniqe bool) error {
	l := s.Len()
	if l == 0 {
		return nil
	}

	q := db.NewQuery(vars.TableFields)
	row, err := q.Where("table_name=$1", table).QueryOneP()
	defer db.PutRow(row.Values)
	if err != nil {
		return err
	}

	if row.IsEmpty() {
		// 如果没有找到字段信息，则insert字段信息
		ins := db.NewInsert(vars.TableFields)
		fields := util.GetStrings(2)
		fields[0] = "table_name"
		fields[1] = "fields_json"
		defer util.PutStrings(fields)
		// _, err = ins.Insert(db.MapRow{"table_name": table, "fields_json": s.Fields})
		r := db.GetRow(2)
		r[0] = table
		r[1] = s.Fields
		_, err = ins.Insert(fields, r)
		db.PutRow(r)
		if err != nil {
			return err
		}
	} else {
		// 检查fields里保存的字段是否与dat.Fields相等
		var keys []string
		err = json.Unmarshal(row.Bytes("fields_json"), &keys)
		if err != nil {
			return err
		}
		if !util.SameStrings(keys, s.Fields) {
			log.App.Notice("dbkeys:", keys)
			log.App.Notice("saved:", s.Fields)
			return myerr.New("Fields不相同，不能保存")
		}
	}

	exists := db.NewExists(table)

	maxRow := 200
	if maxRow > l {
		maxRow = l
	}
	var d, t, dt int64
	var hasTime bool
	dt = s.Dates[l-1]
	if dt < 9000000 {
		hasTime = false
	} else {
		_, t = datescore.ScoreSplit(dt)
		hasTime = t == 0 || t == vars.TdxEndTime
	}
	var fields []string
	if hasTime {
		fields = []string{vars.FieldCode, vars.FieldDate, vars.FieldTime, vars.DBValuesName}
	} else {
		fields = []string{vars.FieldCode, vars.FieldDate, vars.DBValuesName}
	}
	ds := db.GetDataSet(fields)
	r := ds.GetRowP() // 临时row
	defer db.PutRow(r)

	n := len(s.Fields)
	tscode := ToCodeI(code)
	ins := db.NewInsert(table)

	for i := 0; i < l; i++ {
		dt = s.Dates[i]

		if hasTime {
			d, t = datescore.ScoreSplit(dt)
			r[2] = t
		} else {
			d = dt
		}

		if isUniqe {
			if hasTime {
				if exists.Where("code=$1 and date=$2 and time=$3", tscode, d, t).Exists() {
					continue
				}
			} else {
				if exists.Where("code=$1 and date=$2", tscode, d).Exists() {
					continue
				}
			}
		}

		datRow := make([]float64, n)
		s.RowAt(i, datRow)
		r[0], r[1] = tscode, d
		if hasTime {
			r[3] = datRow
		} else {
			r[2] = datRow
		}

		if exec == nil {
			ds.AddRow0(r)
		} else {
			ds.AddRow0(exec(i, r))
		}
		if ds.Len() >= maxRow {
			_, err = ins.InsertM(&ds)
			db.PutDataSet(&ds)
			if err != nil {
				return err
			}
			ds = db.GetDataSet(fields)
		}
	}

	if ds.Len() > 0 {
		_, err = ins.InsertM(&ds)
		db.PutDataSet(&ds)
	}

	return err
}

// LoadA 提取数据库中的数据
func LoadA(q *db.QueryBuilder, isStartDate, isPool bool) (A, error) {
	_, args := q.GetWhere()
	sqlstr := q.Parse()
	rows, err := db.Current().DB().Query(sqlstr, args...)
	if err != nil {
		return EmptyA, err
	}

	// result := A{}
	var result A

	dbFields, err := rows.Columns()
	if err != nil {
		return EmptyA, err
	}

	colsNum := len(dbFields)
	dateI := util.IndexOfStrs(dbFields, vars.FieldDate)
	if dateI < 0 {
		return result, errors.New("date field not found in db")
	}
	timeI := util.IndexOfStrs(dbFields, vars.FieldTime)
	valsI := util.IndexOfStrs(dbFields, vars.DBValuesName)
	codeI := util.IndexOfStrs(dbFields, vars.FieldCode)

	var fields []string
	if valsI == -1 {
		// bar 模式
		fields = make([]string, 0, colsNum)
		for i := 0; i < colsNum; i++ {
			if i == dateI || i == timeI || i == codeI {
				continue
			}
			fields = append(fields, dbFields[i])
		}
	} else {
		// values 模式
		fields = LoadFields(q.GetTable())
	}

	if isPool {
		result = GetA(fields)
	} else {
		result = A{}
		result.Fields = fields // set result fields
	}

	n := len(fields)

	var arrIndexs []int
	raw := make([]interface{}, colsNum)
	tem := make([]interface{}, colsNum)

	for i := 0; i < colsNum; i++ {
		tem[i] = &raw[i]
	}

	if valsI == -1 && len(arrIndexs) == 0 {
		arrIndexs = make([]int, n) // 对应的db数据索引
		for i := 0; i < n; i++ {
			index := util.IndexOfStrs(dbFields, fields[i])
			if index != -1 {
				arrIndexs[i] = index
			}
		}
	}
	row := GetRow(n)
	defer PutRow(row)

	for rows.Next() {
		if err = rows.Scan(tem...); err != nil {
			return result, err
		}

		if valsI == -1 {
			// bar 模式
			for i := 0; i < n; i++ {
				v := raw[arrIndexs[i]]
				switch v.(type) {
				case float64:
					row[i] = v.(float64)
				case int64:
					row[i] = float64(v.(int64))
				default:
					return result, fmt.Errorf("LoadA 数据scan row 时，存在没有处理的类型")
				}
			}
		} else {
			// values 模式
			err := scanValuesRow(raw[valsI], row)
			if err != nil {
				return result, err
			}
		}

		date := raw[dateI].(int64) * 10000
		if timeI != -1 {
			// 有分钟 PM120, PM90, PM60 数据
			t := raw[timeI].(int64)
			if q.GetTable() == vars.TableBarM5 {
				// tdx以结束时间为每个数据的时间date
				// 5分钟数据逻辑会出错，11408081130 没有11：30数据，而是13:00的数据
				if t == 1300 { //所以，做此修正
					t = 1130
				}
				if isStartDate {
					// 通达信5分钟，为结束时间：0935 - 1130，转换成：0930 - 1125
					// 整点1000 - 5 => 955 整点时间60进位，需要减去 (100 - 60)+5
					// 1000-40-5 = 0955; 1400-40-5=1355; 1500-40-5=1455
					if t == 1000 || t == 1400 || t == 1500 {
						t -= 45
					} else {
						t -= 5
					}
				}
			}
			date += t
		} else if !isStartDate {
			date += 1500
		}

		result.AddRow0(date, row)
	}

	return result, rows.Err()
}

func scanValuesRow(v interface{}, row []float64) error {
	src := v.([]byte)
	l := len(src)
	if l < 3 {
		// 重置row
		n := len(row)
		for i := 0; i < n; i++ {
			row[i] = 0
		}
		return nil
	}

	arr := bytes.Split(src[1:l-1], httpd.BComma)
	l = len(arr)
	for i := 0; i < l; i++ {
		val, err := strconv.ParseFloat(util.Bytes2Str(arr[i]), 64)
		if err != nil {
			return err
		}
		row[i] = val
	}
	return nil
}

// ToCodeI convert code to store int code
func ToCodeI(code string) int {
	code = strings.ToLower(code)
	var s strings.Builder
	switch code[:2] {
	case vars.MarketSH:
		s.WriteString("1")
	case vars.MarketSZ:
		s.WriteString("2")
	default:
		s.WriteString("9")
	}
	s.WriteString(code[2:])
	v, err := strconv.ParseInt(s.String(), 10, 64)
	if err != nil {
		panic(err)
	}
	return int(v)
}
