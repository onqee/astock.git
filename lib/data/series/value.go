package series

import (
	"gitee.com/onqee/astock/lib/data/series/datescore"
)

// RefDate of date
func RefDate(s *A, date int64, x int) int64 {
	var i int
	i, _ = s.Search(date)
	i = i - x
	if i < 0 {
		return 0
	}

	return s.Dates[i]
}

// RefI of date
func RefI(s *A, date int64, x int) int {
	var i int
	i, _ = s.Search(date)
	i = i - x
	if i < 0 {
		return 0
	}

	return i
}

// FilterInvalidRow 返回无效的数据列表
func FilterInvalidRow(s *A) A {
	ss := NewEmpty(s.Name, s.Fields)
	l := s.Len()
	row := (Row)(GetRow(len(s.Fields)))
	defer PutRow(row)

	for i := 0; i < l; i++ {
		s.RowAt(i, row)
		if RowIsInvalid(row) {
			ss.AddRow(s.Dates[i], row)
		}
	}
	return ss
}

// EchartsData 返回echarts 数据
func EchartsData(s *A, period, field string) [][2]interface{} {
	hasTime := period[:1] == "m"
	l := s.Len()

	arr := make([][2]interface{}, l)
	k := s.FieldI(field)

	var datestr string
	var score int64
	for i := 0; i < l; i++ {
		score = s.Dates[i]
		if hasTime {
			datestr = datescore.Score2DateTimeStr(score)
		} else {
			datestr = datescore.Score2DateStr(score)
		}
		arr[i] = [2]interface{}{datestr, s.Columns[k][i]}
	}

	return arr
}
