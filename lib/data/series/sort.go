package series

import (
	"fmt"
	"sort"

	"github.com/kere/gno/libs/util"
)

type sortedBy struct {
	A
	Indexs []int
}

// Less sort series
func (s *sortedBy) Less(i, j int) bool {
	// k := s.Indexs[0]
	l := len(s.Indexs)
	for k := 0; k < l; k++ {
		coli := s.Indexs[k]

		if s.Columns[coli][i] == s.Columns[coli][j] {
			continue
		}
		return s.Columns[coli][i] < s.Columns[coli][j]
	}
	return false
}

// Swap sort series
func (s *sortedBy) Swap(i, j int) {
	l := len(s.Columns)
	for k := 0; k < l; k++ {
		s.Columns[k][i], s.Columns[k][j] = s.Columns[k][j], s.Columns[k][i]
	}
	s.Dates[i], s.Dates[j] = s.Dates[j], s.Dates[i]
}

// SortA sort series by
func SortA(s *A, fields ...string) {
	sortA("sort", s, fields)
}

// ReverseA sort series by
func ReverseA(s *A, fields ...string) {
	sortA("reverse", s, fields)
}

// StableA sort series by
func StableA(s *A, fields ...string) {
	sortA("stable", s, fields)
}

// StableReverseA sort series by
func StableReverseA(s *A, fields ...string) {
	sortA("stable.reverse", s, fields)
}

func sortA(mod string, s *A, fields []string) {
	l := s.Len()
	if l == 0 {
		return
	}
	if l != len(s.Columns[0]) {
		panic(fmt.Sprintf("数据的索引与列数量不匹配, %d %d", l, len(s.Columns[0])))
	}

	n := len(fields)
	if n == 0 {
		s.Sort()
	}

	r := util.GetInts(n)
	defer util.PutInts(r)
	for i := 0; i < n; i++ {
		r[i] = s.FieldI(fields[i])
		if r[i] < 0 {
			panic(fields[i] + " 没有这个字段，不能进行排序")
		}
	}

	a := sortedBy{A: *s, Indexs: r}
	switch mod {
	case "reverse":
		sort.Sort(sort.Reverse(&a))
	case "stable":
		sort.Stable(&a)
	case "stable.reverse":
		sort.Stable(sort.Reverse(&a))
	default:
		sort.Sort(&a)
	}
}
