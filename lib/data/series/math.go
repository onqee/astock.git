package series

// PlusCell 加法
func PlusCell(s *A, field string, date int64, v float64) bool {
	k := s.FieldI(field)
	if k < 0 {
		return false
	}

	i, status := s.Search(date)
	if status != IndexFound {
		return false
	}

	s.Columns[k][i] += v
	return true
}

// PlusRow 加法
func PlusRow(dat *A, date int64, row Row) {
	if dat.Locker != nil {
		dat.Locker.Lock()
		defer dat.Locker.Unlock()
	}
	i, stat := dat.Search(date)
	if stat != IndexFound {
		if dat.Columns.Len() == 0 && len(dat.Fields) > 0 {
			dat.Columns = NewColumns(len(dat.Fields))
		}
		dat.Dates = append(dat.Dates, date)
		dat.Columns.AddRow(row)
		SortA(dat)
		return
	}

	l := len(dat.Fields)
	for k := 0; k < l; k++ {
		dat.Columns[k][i] += row[k]
	}
}

// PlusRow0 加法，结束后把row全部设置成0
func PlusRow0(dat *A, date int64, row Row) {
	if dat.Locker != nil {
		dat.Locker.Lock()
		defer dat.Locker.Unlock()
	}
	i, stat := dat.Search(date)
	if stat != IndexFound {
		if dat.Columns.Len() == 0 && len(dat.Fields) > 0 {
			dat.Columns = NewColumns(len(dat.Fields))
		}
		dat.Dates = append(dat.Dates, date)
		dat.Columns.AddRow0(row)
		SortA(dat)
		return
	}

	l := len(dat.Fields)
	for k := 0; k < l; k++ {
		dat.Columns[k][i] += row[k]
		row[k] = 0
	}
}

// Sum return sum row
func Sum(dat *A, src []float64) {
	l := dat.Len()
	n := len(dat.Columns)
	for i := 0; i < l; i++ {
		for k := 0; k < n; k++ {
			src[k] += dat.Columns[k][i]
		}
	}
}

// SumP return sum row
func SumP(dat *A) []float64 {
	row := dat.GetRowP()
	l := dat.Len()
	n := len(dat.Columns)
	for i := 0; i < l; i++ {
		for k := 0; k < n; k++ {
			row[k] += dat.Columns[k][i]
		}
	}
	return row
}

// MaxRowP 最大比较，返回索引
func MaxRowP(s *A, max func([]float64, []float64) bool) (int, []float64) {
	l := s.Len()

	if l == 0 {
		return -1, nil
	}
	if l == 1 {
		return 0, nil
	}
	rMax := s.GetRowP()
	r := s.GetRowP()

	s.RowAt(0, rMax)
	iMax := 0
	for i := 1; i < l; i++ {
		s.RowAt(i, r)
		if max(rMax, r) {
			iMax = i
			copy(rMax, r)
		}
	}
	PutRow(r)
	return iMax, rMax
}
