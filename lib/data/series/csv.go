package series

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/util"
)

// LoadCSV file
func LoadCSV(filename string) (A, error) {
	fields, indexs, columns, err := loadCSV(filename, 0)
	if err != nil {
		return NewEmpty(filename, fields), errors.New(err.Error() + "\n" + filename)
	}
	l := len(indexs)
	var d int64
	dates := make([]int64, l)
	for i := 0; i < l; i++ {
		d, _ = strconv.ParseInt(indexs[i], 10, 64)
		dates[i] = d
	}

	return NewA(filename, dates, columns, fields), nil
}

// LoadCSVWithR file
func LoadCSVWithR(filename string, intR float64) (A, error) {
	fields, indexs, columns, err := loadCSV(filename, intR)
	if err != nil {
		return NewEmpty(filename, fields), errors.New(err.Error() + "\n" + filename)
	}
	l := len(indexs)
	var d int64
	dates := make([]int64, l)
	for i := 0; i < l; i++ {
		d, _ = strconv.ParseInt(indexs[i], 10, 64)
		dates[i] = d
	}

	return NewA(filename, dates, columns, fields), nil
}

func loadCSV(filename string, intR float64) ([]string, []string, Columns, error) {
	log.App.Debug("[read-csv]", filename)
	f, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return nil, nil, nil, err
	}

	defer f.Close()

	sep := util.BComma
	scanner := bufio.NewScanner(f)
	scanner.Scan()
	fields := strings.Split(scanner.Text(), util.SComma)
	ll := len(fields)

	fields = fields[1:]

	var v float64
	var indexs []string
	columns := NewColumns(ll - 1)
	for scanner.Scan() {
		src := scanner.Bytes()
		if len(src) == 0 {
			continue
		}
		arr := bytes.Split(src, sep)
		indexs = append(indexs, string(arr[0]))

		if intR == 0 {
			for k := 0; k < ll-1; k++ {
				// v, _ = strconv.ParseFloat(arr[k+1], 64)
				v, _ = strconv.ParseFloat(string(arr[k+1]), 64)
				columns[k] = append(columns[k], v)
			}
		} else {
			for k := 0; k < ll-1; k++ {
				v, _ = strconv.ParseFloat(string(arr[k+1]), 64)
				columns[k] = append(columns[k], v/intR)
			}
		}
	}

	return fields, indexs, columns, nil
}

// SaveToCSV file
// intR: 转换成整数的系数
func SaveToCSV(s *A, filename string) error {
	l := s.Len()
	indexs := make([]string, l)
	for i := 0; i < l; i++ {
		indexs[i] = fmt.Sprint(s.Dates[i])
	}

	return saveToCSV(filename, s.Fields, indexs, s.Columns, 0)
}

// SaveToCSVWithR file
// intR: 转换成整数的系数
func SaveToCSVWithR(s *A, filename string, intR float64) error {
	l := s.Len()
	indexs := make([]string, l)
	for i := 0; i < l; i++ {
		indexs[i] = fmt.Sprint(s.Dates[i])
	}

	return saveToCSV(filename, s.Fields, indexs, s.Columns, intR)
}

// intR: 转换成整数的系数
func saveToCSV(filename string, fields, indexs []string, columns Columns, intR float64) error {
	if len(indexs) == 0 {
		return nil
	}

	path := filepath.Dir(filename)
	err := os.MkdirAll(path, os.ModeDir)
	if err != nil {
		return err
	}
	log.App.Debug("[save-csv]", filename)

	f, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}

	defer f.Close()

	// w := csv.NewWriter(f)

	l := len(indexs)
	if l == 0 {
		return nil
	}
	ll := len(columns)

	// src := append([]string{"key"}, fields...)
	src := util.GetStrings(1, ll+1)
	defer util.PutStrings(src)
	src[0] = "key"
	src = append(src, fields...)
	sep := util.SComma
	f.WriteString(strings.Join(src, sep))
	f.Write(util.BLineBreak)

	arr := util.GetStrings(ll)
	defer util.PutStrings(arr)
	var k int
	for i := 0; i < l; i++ {
		src[0] = indexs[i]
		if intR == 0 {
			for k = 0; k < ll; k++ {
				src[k+1] = fmt.Sprint(columns[k][i])
			}
		} else {
			for k = 0; k < ll; k++ {
				src[k+1] = fmt.Sprint(int64(columns[k][i] * intR))
			}
		}

		f.WriteString(strings.Join(src, sep))
		f.Write(util.BLineBreak)
	}

	return nil
}
