package datescore

import (
	"fmt"
	"strconv"
	"time"

	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/log"
)

var (
	dt1970, _ = time.Parse("2006-01-02", "1970-01-01")
)

// AddDay 日期增加n天
func AddDay(v int64, days int) int64 {
	d := Score2Date(v)
	d = d.AddDate(0, 0, days)
	return Date2Score(d)
}

// Days 天数
func Days(dateA, dateB int64) int {
	a := Score2Date(dateA)
	b := Score2Date(dateB)
	return int(b.Sub(a).Round(time.Hour*24) / (time.Hour * 24))
}

// DayToMn 把天转换成多少个分钟周期
func DayToMn(day int, toPeriod string) int {
	switch toPeriod {
	case vars.PM120:
		return day * 2
	case vars.PM90:
		return day * 4
	case vars.PM60:
		return day * 4
	case vars.PM30:
		return day * 8
	case vars.PM15:
		return day * 16
	}
	return 0
}

// DayKT 交易师日期
func DayKT(v int64) int64 {
	return v / 10000 * 10000
}

// DayTdx TDX日期
func DayTdx(v int64) int64 {
	return v/10000*10000 + 1500
}

// ScoreSplit 通过日期时间分数，返回KT的日期和时间整数
// 11601021503
func ScoreSplit(score int64) (int64, int64) {
	if score < 1 {
		return 0, 0
	}
	d := score / 10000
	return d, score - d*10000
}

//Score2DateStr 把交易师日期格式转换成 time string
func Score2DateStr(score int64) string {
	return Score2Date(score).Format("2006-01-02")
}

//Score2DateTimeStr 把交易师日期格式转换成 time string
func Score2DateTimeStr(score int64) string {
	return Score2Date(score).Format("2006-01-02 15:04")
}

//Score2Date 把交易师日期格式转换成 time
func Score2Date(score int64) time.Time {
	// 9001021504
	if score < 1 {
		return time.Time{}
	}
	// if score != 0 && score < 9001010101 {
	// 	panic(fmt.Errorf("score < 9001010000:%d", score))
	// }

	d, t := ScoreSplit(score)
	year := 2000 + (d / 10000) - 100
	var date time.Time
	var err error
	if t == 0 {
		date, err = time.ParseInLocation("20060102", fmt.Sprintf("%d%04d", year, d-(d/10000)*10000), time.Local)
	} else {
		date, err = time.ParseInLocation("20060102 1504", fmt.Sprintf("%d%04d %04d", year, d-(d/10000)*10000, t), time.Local)
	}

	if err != nil {
		panic(fmt.Errorf("%s, score=%d", err.Error(), score))
	}

	return date
}

// Date2Score 日期转换成ZSET的分数值
func Date2Score(d time.Time) int64 {
	if d.IsZero() {
		return 0
	}

	s, err := strconv.Atoi(d.Format("1504"))
	if err != nil {
		log.App.Error(err)
		return 0
	}
	year, month, day := d.Date()
	i := int64(year-1900)*10000 + int64(month)*100 + int64(day)

	return i*10000 + int64(s)

}

// Str2Score func
func Str2Score(s string) int64 {
	if len(s) > 10 {
		s = s[:10]
	}
	d, err := time.ParseInLocation("2006-01-02", s, time.Local)
	if err != nil {
		d, err = time.ParseInLocation("20060102", s, time.Local)
		if err != nil {
			panic(err)
		}
	}

	return Date2Score(d)
}

// // ScoreDuration 两个score 之间的天数
// func ScoreDuration(scoreA, scoreB int64) time.Duration {
// 	a := Score2Date(scoreA)
// 	b := Score2Date(scoreB)
//
// 	return b.Sub(a)
// }
//
// // Days
// func Days(dateA, dateB int64) int {
// 	v := math.Round(ScoreDuration(dateA, dateB).Hours() / 24)
// 	return int(v)
// }
