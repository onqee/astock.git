package series

import (
	"github.com/kere/gno/libs/myerr"
)

// TopAsc 需要排序
// maxV: 最大数值  length: 数量
func TopAsc(s *A, field string, maxV float64, length int) (A, error) {
	l := length
	if length < 1 || length > s.Len() {
		l = s.Len()
	}

	dates := make([]int64, l)
	cols := NewColumnsN(s.Columns.Len(), l)
	fieldI := FieldI(field, s.Fields)
	row := GetRow(len(s.Fields))
	defer PutRow(row)
	var lastV float64

	for i := 0; i < l; i++ {
		v := s.Columns[fieldI][i]
		if i > 0 && lastV > v {
			return A{}, myerr.New(field, "必须为正序:TopAsc", s.Name)
		}
		lastV = v

		if v > maxV {
			break
		}
		dates[i] = s.Dates[i]
		s.Columns.RowAt(i, row)
		cols.SetRow(i, row)
	}
	return NewA(s.Name, dates, cols, s.Fields), nil
}

// TopDesc 需要排序
// minV: 最小数值  length: 数量
func TopDesc(s *A, field string, minV float64, length int) (A, error) {
	l := length
	if length < 1 || length > s.Len() {
		l = s.Len()
	}

	dates := make([]int64, 0, l)
	cols := NewColumnsN(s.Columns.Len(), 0, l)
	fieldI := FieldI(field, s.Fields)
	row := GetRow(len(s.Fields))
	defer PutRow(row)

	var lastV float64

	for i := 0; i < l; i++ {
		v := s.Columns[fieldI][i]
		if i > 0 && lastV < v {
			return A{}, myerr.New(field, "必须为倒序 TopDesc", s.Name)
		}
		lastV = v

		if v < minV {
			break
		}
		dates = append(dates, s.Dates[i])
		s.Columns.RowAt(i, row)
		cols.AddRow(row)
	}
	return NewA(s.Name, dates, cols, s.Fields), nil
}
