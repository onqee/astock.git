package series

import (
	"fmt"
	"sort"
	"sync"

	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

const (
	// IndexOverRangeLeft not found = 8
	IndexOverRangeLeft = 0b1000
	// IndexFound found = 1
	IndexFound = 0b0001
	// IndexNotFound found = 2
	IndexNotFound = 0b0010
	// IndexOverRangeRight found = 4
	IndexOverRangeRight = 0b0100
)

var (
	// EmptyA A{}
	EmptyA = A{}
)

// A class 序列数据
type A struct {
	Name  string  `json:"name"`
	Dates []int64 `json:"dates"`
	// Rows     []Row      `json:"rows"`
	Columns Columns      `json:"columns"`
	Fields  []string     `json:"fields"`
	Info    util.MapData `json:"info"`
	Memo    string       `json:"memo"`
	Locker  *sync.Mutex
}

// NewA func
func NewA(name string, dates []int64, columns Columns, fields []string) A {
	l := 0
	for i := range columns {
		if i == 0 {
			l = len(columns[0])
			continue
		}
		if l != len(columns[i]) {
			panic(myerr.New(name, "数据列的长度不相等", l, len(columns[i])))
		}
	}

	if len(dates) != l {
		panic(myerr.New(name, "数据列的长度不相等", l, len(dates)))
	}
	return A{Name: name, Dates: dates, Columns: columns, Fields: fields}
}

// NewEmpty func
func NewEmpty(name string, fields []string, args ...int) A {
	l := 0
	capN := 0
	switch len(args) {
	case 1:
		l = args[0]
		capN = l
	case 2:
		l = args[0]
		capN = args[1]
	}
	n := len(fields)
	dat := A{Name: name, Fields: fields, Columns: NewColumns(n)}
	if l > 0 || capN > 0 {
		dat.Dates = make([]int64, l, capN)
		for i := 0; i < n; i++ {
			dat.Columns[i] = make([]float64, l, capN)
		}
	}
	return dat
}

// Release
func (s *A) Release() {
	s.Info = nil
	n := len(s.Columns)
	for i := 0; i < n; i++ {
		s.Columns[i] = nil
	}
	s.Columns = nil
	s.Dates = nil
	s.Name = ""
	s.Memo = ""
}

// IndexOn index
func (s *A) IndexOn(date int64) int {
	return util.IndexOfInt64s(date, s.Dates)
}

// EnableSync set locker
func (s *A) EnableSync() {
	s.Locker = &sync.Mutex{}
}

// Search return (index, status)
func (s *A) Search(date int64) (int, int) {
	l := len(s.Dates)
	switch {
	case l == 0:
		return -1, IndexOverRangeLeft
	case date < s.Dates[0]:
		return 0, IndexOverRangeLeft
	case date == s.Dates[0]:
		return 0, IndexFound
	case date > s.Dates[l-1]:
		return l - 1, IndexOverRangeRight
	case date == s.Dates[l-1]:
		return l - 1, IndexFound
	}

	index := util.SearchInt64s(date, s.Dates)
	if s.Dates[index] != date {
		return index - 1, IndexNotFound
	}
	return index, IndexFound
}

// Copy data
func (s *A) Copy() A {
	dat := GetA(s.Fields, len(s.Dates))
	copy(dat.Dates, s.Dates)
	CopyColumnsTo(s.Columns, dat.Columns)
	dat.Name = s.Name
	dat.Info = s.Info

	return dat
}

// SetInfo 设置其他信息
func (s *A) SetInfo(key string, v interface{}) {
	if s.Locker != nil {
		s.Locker.Lock()
		defer s.Locker.Unlock()
	}
	if s.Info == nil {
		s.Info = make(map[string]interface{})
	}
	s.Info[key] = v
}

// Len get data length
func (s *A) Len() int {
	if s == nil {
		return 0
	}
	return len(s.Dates)
}

// LastDate date
func (s *A) LastDate() int64 {
	l := s.Len()
	if l == 0 {
		return 0
	}
	return s.Dates[l-1]
}

// AppendSeries series
func AppendSeries(s *A, s2 A) A {
	if s2.Len() == 0 {
		return *s
	}
	if s.Len() == 0 && len(s.Fields) == 0 {
		s.Fields = s2.Fields
	} else if len(s.Fields) != len(s2.Fields) {
		log.App.Error("A::Append s1.Fields != s2.Fields")
		return *s
	}

	ss := NewEmpty(s.Name, s.Fields)
	ss.Dates = append(s.Dates, s2.Dates...)
	ss.Columns = s.Columns.Append(s2.Columns)

	return ss
}

// AddRow series without sort
func (s *A) AddRow(date int64, row Row) {
	if s.Locker != nil {
		s.Locker.Lock()
		defer s.Locker.Unlock()
	}
	if s.Columns.Len() == 0 && len(s.Fields) > 0 {
		s.Columns = NewColumns(len(s.Fields))
	}
	s.Dates = append(s.Dates, date)
	s.Columns.AddRow(row)
}

// AddRow0 and set row values to 0
func (s *A) AddRow0(date int64, row Row) {
	if s.Locker != nil {
		s.Locker.Lock()
		defer s.Locker.Unlock()
	}
	if s.Columns.Len() == 0 && len(s.Fields) > 0 {
		s.Columns = NewColumns(len(s.Fields))
	}
	s.Dates = append(s.Dates, date)
	s.Columns.AddRow0(row)
}

// SetRow series without sort
func (s *A) SetRow(i int, date int64, row Row) {
	if s.Locker != nil {
		s.Locker.Lock()
		defer s.Locker.Unlock()
	}
	s.Dates[i] = date
	s.Columns.SetRow(i, row)
}

// SetRow0 series without sort
func (s *A) SetRow0(i int, date int64, row Row) {
	if s.Locker != nil {
		s.Locker.Lock()
		defer s.Locker.Unlock()
	}
	s.Dates[i] = date
	s.Columns.SetRow0(i, row)
}

// AddColumn
func (s *A) AddColumn(field string, col []float64) {
	s.Fields = append(s.Fields, field)
	s.Columns = append(s.Columns, col)
}

// Merge series s2中的数据将合并到s1中，不合并已经存在的date
func Merge(s A, s2 *A) A {
	if s2.Len() == 0 {
		return EmptyA
	}
	if !util.SameStrings(s.Fields, s2.Fields) {
		log.App.Error("A::AddIfNotFound s1.Fields != s2.Fields").Stack()
		return EmptyA
	}

	l := s2.Len()
	if l == 0 {
		return EmptyA
	}

	dat := s.Copy()
	var status int
	var date int64
	row := s.GetRowP()
	defer PutRow(row)

	for i := l - 1; i > -1; i-- {
		date = s2.Dates[i]
		_, status = dat.Search(date)
		if status == IndexFound {
			continue
		}

		s2.RowAt(i, row)
		dat.AddRow(date, row)
	}

	dat.Sort()

	return dat
}

// ValueAt field
func (s *A) ValueAt(field string, i int) float64 {
	return s.Columns.ValueAt(i, field, s.Fields)
}

// ValueOn field
func (s *A) ValueOn(field string, date int64) (v float64, status int) {
	var i int
	i, status = s.Search(date)
	if i < 0 {
		return 0, status
	}
	v = s.ValueAt(field, i)
	return v, status
}

// FieldI get values
func (s *A) FieldI(field string) int {
	return FieldI(field, s.Fields)
}

// FieldI get values
func FieldI(field string, fields []string) int {
	l := len(fields)
	for i := 0; i < l; i++ {
		if fields[i] == field {
			return i
		}
	}
	return -1
}

// Values get values
func (s *A) Values(field string) []float64 {
	k := s.FieldI(field)
	if k < 0 {
		panic(fmt.Errorf("A::Values %s not found in %v", field, s.Fields))
	}
	return s.Columns[k]
}

// // RowOn of date score
// func (s *A) RowOn(date int64) (row Row, status int) {
// 	var i int
// 	i, status = s.Search(date)
//
// 	return s.Columns.RowAt(i), status
// }

// RowOn of date score
// return status
func (s *A) RowOn(date int64, row Row) int {
	i, status := s.Search(date)
	if status == IndexFound {
		s.Columns.RowAt(i, row)
	}
	return status
}

// RowOnP of date score
// return row, status
func (s *A) RowOnP(date int64) ([]float64, int) {
	row := s.GetRowP()
	i, status := s.Search(date)
	if i != -1 {
		s.Columns.RowAt(i, row)
	}
	return row, status
}

// RowAt of index
func (s *A) RowAt(i int, row Row) {
	s.Columns.RowAt(i, row)
}

// RowAtP of index
func (s *A) RowAtP(i int) []float64 {
	row := s.GetRowP()
	s.Columns.RowAt(i, row)
	return row
}

// Sort sort series
func (s *A) Sort() {
	sort.Sort(s)
}

// Reverse sort series
func (s *A) Reverse() {
	sort.Sort(sort.Reverse(s))
}

// Less sort series
func (s *A) Less(i, j int) bool {
	return s.Dates[i] < s.Dates[j]
}

// Swap sort series
func (s *A) Swap(i, j int) {
	l := len(s.Columns)
	for k := 0; k < l; k++ {
		s.Columns[k][i], s.Columns[k][j] = s.Columns[k][j], s.Columns[k][i]
	}
	s.Dates[i], s.Dates[j] = s.Dates[j], s.Dates[i]
}
