package series

import (
	"math"

	"github.com/kere/gno/libs/util"
)

// FilterP 筛选符合目标的数据
func FilterP(s *A, f func(i int) bool) A {
	result := GetA(s.Fields)
	l := s.Len()

	row := GetRow(len(s.Fields))
	defer PutRow(row)

	for i := 0; i < l; i++ {
		if f(i) {
			s.RowAt(i, row)
			result.AddRow(s.Dates[i], row)
		}
	}

	// result.Sort()
	return result
}

// FilterB 筛选符合目标的数据
func FilterB(s *B, f func(i int) bool) B {
	result := NewB(s.Name, []int64{}, NewColumnsB(len(s.Fields)), s.Fields)
	l := s.Len()
	row := s.GetRowP()
	for i := 0; i < l; i++ {
		if f(i) {
			s.RowAt(i, row)
			result.AddRow(s.Dates[i], row)
		}
	}
	util.PutStrings(row)

	result.Sort()
	return result
}

// Count 统计N周期中满足条件X的周期数, 若N=0则计算从index开始的全部数据。
func Count(s *A, f func(i int) bool, index, N int) int {
	l := s.Len()
	if index < 0 || index >= l {
		index = l - 1
	}

	n := 0
	var isok bool
	count := index - N
	if count < -1 || N == 0 {
		count = -1
	}
	for i := index; i > count; i-- {
		isok = f(i)

		if isok {
			n++
			continue
		}
	}

	return n
}

// EveryApply 顺序扫描，数据apply
// []rows: 当前数据有值时，[]rows 对应的row 下有值
func EveryApply(f func(date int64, ids []int), arr ...*A) {
	n := len(arr)
	// 把当前索引相同值时，取得的id放入toIds
	toIds := util.GetInts(n)
	defer util.PutInts(toIds)
	// 当前的索引
	ids := util.GetInts(n)
	defer util.PutInts(ids)

	// 当n=1时
	if n == 1 {
		l := arr[0].Len()
		for i := 0; i < l; i++ {
			toIds[0] = i
			f(arr[0].Dates[i], toIds)
		}
		return
	}

	breakN := 0 //breakN==n时，则break
	isOK := false
	for i := 0; i < n; i++ {
		// 初始化ids，如果dat为空，ids=-1
		if arr[i].Len() == 0 {
			ids[i] = -1
			breakN++
		} else {
			isOK = true
		}
	}

	if !isOK {
		return
	}

	// loop
	date := int64(math.MaxInt64)
	for {
		for i := 0; i < n; i++ {
			if ids[i] < 0 {
				continue
			}
			if date > arr[i].Dates[ids[i]] {
				// 找到最小的date
				date = arr[i].Dates[ids[i]]
			}
		}

		for i := 0; i < n; i++ {
			toIds[i] = -1
			index := ids[i]
			if index == -1 {
				toIds[i] = -1
				continue
			}

			// 如果 == 最小的Date，则，取值，ids++
			if date == arr[i].Dates[index] {
				toIds[i] = index
				ids[i]++
			} else {
				toIds[i] = -1
			}
			if ids[i] == arr[i].Len() {
				ids[i] = -1
				breakN++
			}
		}

		f(date, toIds)
		date = math.MaxInt64

		if breakN == n {
			// 所有arr完成
			break
		}
	}

}
