package series

import (
	"sort"

	"github.com/kere/gno/libs/util"
)

// AlignBE 首尾数据对齐，中间数据不做处理
func AlignBE(arr ...*A) []A {
	n := len(arr)
	result := make([]A, n)
	var b, e, last int64
	for i := 0; i < n; i++ {
		if arr[i].Len() == 0 {
			return result
		}

		if b < arr[i].Dates[0] {
			b = arr[i].Dates[0]
		}

		last = arr[i].LastDate()
		if i == 0 {
			e = last
			continue
		}
		if e > last {
			e = last
		}
	}

	for i := 0; i < n; i++ {
		result[i] = arr[i].Range(b, e)
	}
	return result
}

// InnerCutP 数据的交集
// 只保存时间相等的数据
func InnerCutP(arr ...*A) []A {
	n := len(arr)
	result := make([]A, n)
	minLen := arr[0].Len()

	for k := 0; k < n; k++ {
		if minLen > len(arr[k].Dates) {
			minLen = len(arr[k].Dates)
		}
	}

	for k := 0; k < n; k++ {
		result[k] = GetA(arr[k].Fields, 0, minLen)
	}
	InnerApply(func(date int64, ids []int) {
		for i := 0; i < n; i++ {
			r := GetRow(len(arr[i].Columns))
			arr[i].RowAt(ids[i], r)
			result[i].AddRow(date, r)
			PutRow(r)
		}
	}, arr...)

	return result
}

// InnerApply 数据交集过滤器
// ids: 索引相同值下的索引
func InnerApply(f func(date int64, ids []int), arr ...*A) {
	n := len(arr)
	if n == 0 {
		return
	}

	ids := util.GetInts(n)
	defer util.PutInts(ids)

	arrX := AlignBE(arr...)
	if arrX[0].Len() == 0 {
		return
	}

	for {
		// 有任何一个ids索引大于over right，则结束
		if ids[0] == arrX[0].Len() {
			break
		}

		// 对应dats.Dates[ids[i]]都相等时，是所有数据集的交集
		// 找到最大的ids，如果当前ids不相等，则ids++，直到所有ids的值都相等
		max := arrX[0].Dates[ids[0]]
		isOver := false
		for k := 1; k < n; k++ {
			// 有任何一个ids索引大于over right，则结束
			if ids[k] == arrX[k].Len() {
				isOver = true
				break
			}

			if max < arrX[k].Dates[ids[k]] {
				max = arrX[k].Dates[ids[k]]
			}
		}
		if isOver {
			break
		}

		isEq := true
		for k := 0; k < n; k++ {
			if arrX[k].Dates[ids[k]] == max {
				continue
			}
			// 小于max的日期，索引++
			if ids[k] < arrX[k].Len() {
				ids[k]++
			}

			isEq = false
		}

		if !isEq {
			continue
		}
		// 全部日期相等，则应用数据
		date := arrX[0].Dates[ids[0]]
		f(date, ids)
		for k := 0; k < n; k++ {
			ids[k]++
		}
	}
}

// UniqueAP 移除重复的记录
func UniqueAP(dat *A, f func(int) bool) A {
	l := dat.Len()
	result := GetA(dat.Fields)
	if !sort.IsSorted(dat) {
		dat.Sort()
	}

	last := dat.Dates[0]
	row := dat.GetRowP()
	defer PutRow(row)

	dat.RowAt(0, row)
	result.AddRow(last, row)
	for i := 1; i < l; i++ {
		if last == dat.Dates[i] {
			continue
		}

		if f != nil && !f(i) {
			continue
		}
		last = dat.Dates[i]
		dat.RowAt(i, row)
		result.AddRow(last, row)
	}

	return result
}
