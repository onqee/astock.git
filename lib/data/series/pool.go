package series

import (
	"sync"

	"github.com/kere/gno/libs/util"
)

var (
	datesPool sync.Pool
	rowPool   sync.Pool
	colPool   poolClas
	asPool    sync.Pool

	lockA    sync.Mutex
	lockAs   sync.Mutex
	loclClos sync.Mutex
)

type poolClas struct {
	sync.Pool
	lock sync.Mutex
}

// GetRowP from pool
func (s *A) GetRowP() Row {
	return GetRow(len(s.Fields))
}

// GetA get from pool
// l 初始化的长度
func GetA(fields []string, args ...int) A {
	lockA.Lock()

	n := len(fields)
	l, capN := util.ParsePoolArgs(args, 100)

	dates := GetDates(l, capN)
	cols := NewColumns(n)
	for i := 0; i < n; i++ {
		cols[i] = GetColumn(l, capN)
	}

	lockA.Unlock()
	return NewA("", dates, cols, fields)
}

// PutA series into pool
func PutA(dat *A) {
	if dat == nil || cap(dat.Dates) == 0 {
		dat.Release()
		return
	}
	lockA.Lock()

	PutDates(dat.Dates)
	n := len(dat.Columns)

	for i := 0; i < n; i++ {
		PutColumn(dat.Columns[i])
	}

	dat.Dates = nil
	dat.Columns = nil
	dat.Info = nil
	dat.Memo = ""
	dat.Locker = nil
	lockA.Unlock()
}

// func GetRow(args ...int) []float64 {
// 	return util.GetFloats(args...)
// }
//
// func PutRow(r []float64) {
// 	util.PutFloats(r)
// }

// GetRow from pool
func GetRow(args ...int) []float64 {
	l, capN := util.ParsePoolArgs(args, 20)
	v := rowPool.Get()
	if v == nil {
		return make([]float64, l, capN)
	}
	row := v.([]float64)

	for i := 0; i < l; i++ {
		row = append(row, 0)
	}
	return row
}

// PutRow to pool
func PutRow(row []float64) {
	if cap(row) == 0 {
		return
	}
	rowPool.Put(row[:0])
}

// GetDates from pool
func GetDates(args ...int) []int64 {
	l, capN := util.ParsePoolArgs(args, 100)
	v := datesPool.Get()
	if v == nil {
		return make([]int64, l, capN)
	}
	dates := v.([]int64)

	for i := 0; i < l; i++ {
		dates = append(dates, 0)
	}
	return dates
}

// PutDates to pool
func PutDates(r []int64) {
	if cap(r) == 0 {
		return
	}
	datesPool.Put(r[:0])
}

// GetColumn from pool
func GetColumn(args ...int) []float64 {
	// colPool.lock.Lock()
	// defer colPool.lock.Unlock()
	l, capN := util.ParsePoolArgs(args, 100)
	v := colPool.Get()
	if v == nil {
		return make([]float64, l, capN)
	}
	col := v.([]float64)

	for i := 0; i < l; i++ {
		col = append(col, 0)
	}
	return col
}

// PutColumn to pool
func PutColumn(r []float64) {
	// colPool.lock.Lock()
	// defer colPool.lock.Unlock()
	if cap(r) == 0 {
		return
	}
	colPool.Put(r[:0])
}

// GetColumns get from pool
func GetColumns(fields []string, args ...int) Columns {
	loclClos.Lock()

	n := len(fields)
	l, capN := util.ParsePoolArgs(args, 50)

	cols := NewColumns(n)
	for i := 0; i < n; i++ {
		cols[i] = GetColumn(l, capN)
	}
	loclClos.Unlock()

	return cols
}

// PutColumns into pool
func PutColumns(cols Columns) {
	loclClos.Lock()
	n := len(cols)
	for i := 0; i < n; i++ {
		if cap(cols[i]) == 0 {
			continue
		}
		PutColumn(cols[i])
	}
	loclClos.Unlock()
}

// // GetAs from pool
// func GetAs(args ...int) []*A {
// 	l, capN := util.ParsePoolArgs(args, 20)
// 	v := asPool.Get()
// 	if v == nil {
// 		return make([]*A, l, capN)
// 	}
// 	arr := v.([]*A)
//
// 	for i := 0; i < l; i++ {
// 		arr = append(arr, nil)
// 	}
// 	return arr
// }

// PutAAs 释放A
func PutAAs(arr []A) {
	lockAs.Lock()
	l := len(arr)
	for i := 0; i < l; i++ {
		PutA(&arr[i])
	}
	lockAs.Unlock()
}

// PutAs 释放A
func PutAs(arr []*A) {
	lockAs.Lock()
	l := len(arr)
	for i := 0; i < l; i++ {
		PutA(arr[i])
		arr[i] = nil
	}
	lockAs.Unlock()
}

// // PutAsAll 释放A
// func PutAsAll(arr []*A) {
// 	if cap(arr) == 0 {
// 		return
// 	}
// 	n := len(arr)
// 	for i := 0; i < n; i++ {
// 		PutA(arr[i])
// 		arr[i] = nil
// 	}
// 	asPool.Put(arr[:0])
// }

// // PutAsOnly 释放A
// func PutAsOnly(arr []*A) {
// 	if cap(arr) == 0 {
// 		return
// 	}
// 	n := len(arr)
// 	for i := 0; i < n; i++ {
// 		arr[i] = nil
// 	}
// 	asPool.Put(arr[:0])
// }

// // PutAs 释放A
// func PutAs(arr []A) {
// 	lockAs.Lock()
// 	defer lockAs.Unlock()
//
// 	l := len(arr)
// 	for i := 0; i < l; i++ {
// 		PutA(&arr[i])
// 	}
// }
