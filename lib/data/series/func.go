package series

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

func CheckA(s *A) error {
	l := len(s.Dates)
	count := len(s.Columns)
	for i := 0; i < count; i++ {
		if l != len(s.Columns[i]) {
			return fmt.Errorf("数据集，列数量不匹配 i=%d %d != %d", i, l, len(s.Columns[i]))
		}
	}
	return nil
}

// Search return (index, status)
func Search(date int64, dates []int64) (int, int) {
	l := len(dates)
	var maxDate int64
	if l > 0 {
		maxDate = dates[l-1]
	}
	switch {
	case l == 0:
		return -1, IndexOverRangeLeft
	case date < dates[0]:
		return 0, IndexOverRangeLeft
	case date == maxDate:
		return l - 1, IndexFound
	case date > maxDate:
		return l - 1, IndexOverRangeRight
	}

	// index := util.SearchInt64s(date, dates)
	index := sort.Search(l, func(i int) bool { return dates[i] >= date })
	// if index >= l {
	// 	fmt.Println(date, maxDate)
	// 	fmt.Println(dates)
	// 	fmt.Println()
	// }

	if dates[index] != date {
		return index - 1, IndexNotFound
	}
	return index, IndexFound
}

// BarLast 前N次条件成立到现在的天数
// return index and n
func BarLast(s *A, f func(i int) bool, index, N int) (int, int) {
	l := s.Len()
	if index < 1 || index >= l {
		index = l - 1
	}

	n := 0
	var isok bool
	for i := index; i > 0; i-- {
		isok = f(i)

		if !isok {
			continue
		}

		n++
		if N == n {
			return i, index - i
		}
	}

	return -1, -1
}

// BarLastCount 倒数连续满足条件的次数
// 统计从当前开始往回数，连续满足条件的次数，只要碰到不满足就终止。
// return index and n
func BarLastCount(s *A, f func(i int) bool, startI int) (int, int) {
	l := s.Len()
	if startI < 1 || startI >= l {
		startI = l - 1
	}

	var isok bool
	for i := startI; i > 0; i-- {
		isok = f(i)

		if !isok {
			return i, startI - i
		}
	}

	return -1, -1
}

func stringDate(date int64, arr []byte) string {
	s := strconv.FormatInt(date, 10)
	l := len(s)
	// 1190101 , 11901011500
	if l != 7 && l != 11 {
		return s
	}
	src := util.Str2Bytes(s)

	arr[0], arr[1], arr[2] = src[0], src[1], src[2]
	arr[3] = byte('-')
	arr[4], arr[5] = src[3], src[4]
	arr[6] = byte('-')
	arr[7], arr[8] = src[5], src[6]
	if l == 11 {
		arr[9] = byte('-')
		arr[10], arr[11], arr[12], arr[13] = src[7], src[8], src[9], src[10]
	}
	return util.Bytes2Str(arr)
}

// Print series with dat, and dateA dateB
func Print(dat *A, args ...int64) {
	var dateA, dateB int64
	if len(args) > 0 {
		dateA = args[0]
		if len(args) == 2 {
			dateB = args[1]
		}
	}

	s := dat
	if dateA+dateB != 0 {
		dat0 := dat.Range(dateA, dateB)
		s = &dat0
	}
	if s == nil {
		fmt.Println("Series: nil")
		return
	}
	l := s.Len()
	fmt.Println("Series:", s.Name, " size:", l)
	if l == 0 {
		return
	}
	if len(s.Memo) > 0 {
		fmt.Println("memo:", s.Memo)
	}
	if len(s.Info) > 0 {
		fmt.Println("info:", s.Info)
	}

	var str strings.Builder
	str.WriteString("key")
	str.WriteRune('\t')
	for k := range s.Fields {
		str.WriteRune('\t')
		str.WriteString(s.Fields[k])
	}
	fmt.Println(str.String())

	if l == 0 {
		fmt.Println(util.PrintLine, l)
		return
	}
	var tmpArr []byte
	date := s.Dates[0]
	if 1000000 < date && date < 9999999 {
		tmpArr = util.GetBytes(9)
		defer util.PutBytes(tmpArr)
	} else if 10000000000 < date && date < 99999999999 {
		tmpArr = util.GetBytes(14)
		defer util.PutBytes(tmpArr)
	}

	if l < 100 {
		for i := 0; i < l; i++ {
			fmt.Println(stringDate(s.Dates[i], tmpArr), "\t", s.Columns.RowString(i))
		}
		fmt.Println(util.PrintLine, l)
		return
	}

	for i := 0; i < 20; i++ {
		fmt.Println(stringDate(s.Dates[i], tmpArr), "\t", s.Columns.RowString(i))
	}

	a := l/2 - 10
	fmt.Println(util.PrintLine, a)

	for i := a; i < a+21; i++ {
		fmt.Println(stringDate(s.Dates[i], tmpArr), "\t", s.Columns.RowString(i))
	}

	fmt.Println(util.PrintLine, a+20)

	for i := l - 21; i < l; i++ {
		fmt.Println(stringDate(s.Dates[i], tmpArr), "\t", s.Columns.RowString(i))
	}
	fmt.Println(util.PrintLine, l)
}

// AddColumnFront column
func AddColumnFront(s *A, field string, vals []float64) error {
	l := len(vals)
	if l == 0 {
		return nil
	}

	if l != s.Len() {
		return myerr.New("add column failed, not matched length").Stack()
	}
	s.Fields = append([]string{field}, s.Fields...)

	s.Columns = append([][]float64{vals}, s.Columns...)
	return nil
}

// AddColumn column
func AddColumn(s *A, field string, vals []float64) error {
	l := len(vals)
	if l == 0 {
		return nil
	}

	if l != s.Len() {
		return myerr.New("add column failed, not matched length").Stack()
	}
	s.Fields = append(s.Fields, field)
	s.Columns = append(s.Columns, vals)
	return nil
}
