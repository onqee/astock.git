package series

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"testing"

	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
)

var (
	data     A
	storeDir = "z:/kt/test"
)

func load() error {
	src, err := ioutil.ReadFile("002049.json")
	if err != nil {
		return err
	}

	var o struct {
		Name string   `json:"name"`
		Code string   `json:"code"`
		Data []string `json:"data"`
	}

	err = json.Unmarshal(src, &o)
	if err != nil {
		return err
	}

	data = NewEmpty("", []string{"open", "close", "high", "low", "vol"})
	for _, line := range o.Data {
		arr := strings.Split(line, ",")
		row := make([]float64, 5)
		row[0], _ = strconv.ParseFloat(arr[1], 64)
		row[1], _ = strconv.ParseFloat(arr[2], 64)
		row[2], _ = strconv.ParseFloat(arr[3], 64)
		row[3], _ = strconv.ParseFloat(arr[4], 64)
		row[4], _ = strconv.ParseFloat(arr[5], 64)

		data.AddRow(datescore.Str2Score(arr[0]), row)
	}

	sort.Sort(&data)
	return nil
}

func TestA(t *testing.T) {
	err := load()
	if err != nil {
		t.Fatal(err)
	}
	filename := filepath.Join(storeDir, "SZ002049.csv")
	err = SaveToCSV(&data, filename)
	if err != nil {
		t.Fatal(err)
	}

	// search
	var score int64 = 11802090000
	index, status := data.Search(score)
	if status != IndexFound || index != 2774 {
		t.Fatal("Search failed")
	}

	score = 11802100000
	index, status = data.Search(score)
	if status != IndexNotFound || index != 2774 {
		t.Fatal("Search failed", index)
	}

	score = 10506010000
	index, status = data.Search(score)
	if status != IndexOverRangeLeft || index != 0 {
		t.Fatal("Search failed", index, status, data.Dates[0])
	}

	score = 11806140000
	index, status = data.Search(score)
	if status != IndexFound || index != 2854 {
		t.Fatal("Search failed", index, status)
	}

	score = 11806150000
	index, status = data.Search(score)
	if status != IndexOverRangeRight || index != 2854 {
		t.Fatal("Search failed", index, status)
	}

	// func
	index, _ = BarLast(&data, func(i int) bool {
		row := Row(GetRow(len(data.Fields)))
		data.RowAt(i, row)
		v := row.ValueOf(vars.FieldClose, data.Fields)
		// 2018-02-06,34.57,33.35,35.83,33.17,178533
		return v == 33.35
	}, 0, 1)

	if data.Dates[index] != 11802060000 {
		t.Fatal("BarLast:", data.Dates[index])
	}

	index = index + 12
	_, n := BarLastCount(&data, func(i int) bool {
		row := Row(GetRow(len(data.Fields)))
		data.RowAt(i, row)
		close := row.ValueOf(vars.FieldClose, data.Fields)
		open := row.ValueOf(vars.FieldOpen, data.Fields)
		return close > open
	}, index)

	if n != 6 {
		t.Fatal("BarLastCount:", n)
	}

	var dateA int64 = 11802060000
	var dateB int64 = 11803260000
	s1 := data.Range(dateA, dateB+1)

	if s1.Dates[0] != dateA || s1.LastDate() != dateB {
		t.Fatal("Range:", s1.Dates[0], s1.LastDate())
	}

	filename = filepath.Join(storeDir, "SZ002049_p1.csv")
	err = SaveToCSV(&s1, filename)
	if err != nil {
		t.Fatal(err)
	}

	// s2 := NewEmpty("", s1.Fields)
	var s2 A
	s2, err = LoadCSV(filename)
	if err != nil {
		t.Fatal(err)
	}

	if s1.Dates[0] != s2.Dates[0] || s1.LastDate() != s2.LastDate() {
		t.Fatal("读取CSV失败")
	}
	row := GetRow(len(s2.Fields))
	s2.RowAt(0, row)
	if row[0] != 34.57 {
		t.Fatal("读取CSV失败")
	}
	// s2.Print()
}

func TestRange(t *testing.T) {
	// data.IRange(a, b)
	var dateA, dateB int64
	dateA, dateB = 11805170000, 11806120000
	a, b := data.RangeI(dateA, dateB)
	if data.Dates[a] != dateA || data.Dates[b] != dateB {
		t.Fatal(data.Dates[a], data.Dates[b])
	}

	dat := data.IRange(a, b)
	if dat.Dates[0] != dateA || dat.LastDate() != dateB {
		t.Fatal(dat.Dates[0], dat.LastDate())
	}

	dat = data.Range(11806110000, 11807010000)
	if dat.LastDate() != data.LastDate() {
		t.Fatal("range failed", dat.LastDate(), data.LastDate())
	}
	dat = data.Range(11806110000, 11806110000)
	if dat.Len() != 1 {
		Print(&dat)
		t.Fatal(dat.Len())
	}

	dates := []int64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 70}
	a, b = RangeI(0, 0, dates)
	if a != 0 || b != 0 {
		t.Fatal(dates[a], dates[b])
	}
	a, b = RangeI(9, 100, dates)
	if a != 9 || b != 20 {
		t.Fatal(dates[a], dates[b], a, b)
	}

	a, b = RangeI(20, 21, dates)
	if a != -1 || b != -1 {
		t.Fatal(dates[a], dates[b])
	}
	a, b = RangeI(9, 11, dates)
	if dates[a] != 9 || dates[b] != 9 {
		t.Fatal(dates[a], dates[b])
	}
	a, b = RangeI(50, 50, dates)
	if a != 10 || b != 10 {
		t.Fatal(dates[a], dates[b])
	}
	a, b = RangeI(70, math.MaxInt32, dates)
	if dates[a] != 70 || dates[b] != 70 {
		t.Fatal(dates[a], dates[b], a, b)
	}
	a, b = RangeI(60, math.MaxInt32, dates)
	if dates[a] != 70 || dates[b] != 70 {
		t.Fatal(dates[a], dates[b], a, b)
	}

	a, b = IOffset(10, 0, dates)
	if dates[a] != 9 || dates[b] != 9 {
		t.Fatal(a, dates[a])
	}
	a, b = IOffset(10, 3, dates)
	if dates[a] != 9 || dates[b] != 52 {
		t.Fatal(a, dates[a])
	}
	a, b = IOffset(10, -3, dates)
	if dates[a] != 6 || dates[b] != 9 {
		t.Fatal(a, dates[a])
	}

}

func TestAdd(t *testing.T) {
	s := NewEmpty("addColumn", []string{"val"})
	s1 := &s
	s1.AddRow(1, Row{1})
	s1.AddRow(2, Row{2})
	row := NewRow(1)
	s1.RowAt(1, row)
	if s1.Dates[1] != 2 || row[0] != 2 {
		t.Fatal()
	}
	AddColumn(s1, "val2", []float64{10, 20})

	row = NewRow(2)
	s1.RowAt(1, row)
	if len(row) != 2 || row[1] != 20 {
		Print(s1)
		t.Fatal()
	}

	cols1 := NewColumnsN(2, 2)
	cols1[0][0] = 0
	cols1[1][0] = 1
	cols1[0][1] = 10
	cols1[1][1] = 11

	cols := NewColumnsN(2, 3)
	cols.SetCols(0, cols1)
	cols.RowAt(1, row)
	if row[0] != 10 || row[1] != 11 {
		t.Fatal(row)
	}
	cols = NewColumnsN(2, 3)
	cols.SetCols(1, cols1)
	cols.RowAt(2, row)
	if row[0] != 10 || row[1] != 11 {
		t.Fatal(row)
	}

	cols = NewColumnsN(2, 3)
	err := cols.SetCols(2, cols1)
	if err == nil {
		t.Fatal()
	}
}

func TestAlgin(t *testing.T) {
	dates := []int64{1, 2, 3, 5, 7, 9}
	cols := [][]float64{[]float64{1, 2, 3, 5, 7, 9}}
	fields := vars.FieldsVal1
	s1 := NewA("s1", dates, cols, fields)

	dates = []int64{2, 3, 4, 5, 7, 9}
	cols = [][]float64{[]float64{2, 3, 4, 5, 7, 9}}
	s2 := NewA("s2", dates, cols, fields)

	dates = []int64{2, 4, 5, 6, 7, 10}
	cols = [][]float64{[]float64{2, 4, 5, 6, 7, 10}}
	s3 := NewA("s3", dates, cols, fields)

	arr := InnerCutP(&s1, &s2, &s3)
	if arr[0].Dates[0] != 2 || arr[1].Dates[0] != 2 || arr[2].Dates[0] != 2 {
		Print(&arr[0])
		Print(&arr[1])
		Print(&arr[2])
		t.Fatal()
	}
	if arr[0].Dates[1] != 5 || arr[1].Dates[1] != 5 || arr[2].Dates[1] != 5 {
		t.Fatal()
	}

	dates = []int64{1, 2, 3, 5, 7, 9}
	cols = [][]float64{[]float64{1, 2, 3, 5, 7, 9}}
	s1 = NewA("s1", dates, cols, fields)

	dates = []int64{4, 5, 6, 7, 14, 16}
	cols = [][]float64{[]float64{2, 3, 4, 5, 7, 9}}
	s2 = NewA("s2", dates, cols, fields)

	dates = []int64{1, 5, 6, 7, 10, 20}
	cols = [][]float64{[]float64{2, 4, 5, 6, 7, 10}}
	s3 = NewA("s3", dates, cols, fields)
	arr = InnerCutP(&s1, &s2, &s3)
	if arr[0].Dates[0] != 5 || arr[1].Dates[0] != 5 || arr[2].Dates[0] != 5 {
		t.Fatal()
	}

	arr = AlignBE(&s1, &s2, &s3)
	if arr[0].Dates[0] != 5 || arr[0].LastDate() != 9 || arr[2].Dates[0] != 5 {
		for i := 0; i < 3; i++ {
			fmt.Println(arr[i].Dates[0], arr[i].LastDate())
		}
		t.Fatal()
	}
}

func TestUnique(t *testing.T) {
	dates := []int64{1, 5, 6, 7, 6, 5, 10, 20}
	cols := [][]float64{[]float64{1, 5, 6, 7, 6, 5, 10, 20}}
	s1 := NewA("s1", dates, cols, vars.FieldsVal1)
	dat := UniqueAP(&s1, nil)
	if dat.Len() != 6 || dat.Dates[3] != 7 {
		Print(&dat)
		t.Fatal()
	}
}

func TestBinary(t *testing.T) {
	filename := "z:/kt/test/test" + vars.ExtDB
	dates := []int64{1190401, 1190402, 1190403, 1190404}
	cols := [][]float64{[]float64{0, 10, 20, 30}, []float64{1, 11, 21, 31}, []float64{2, 12, 22, 32}}
	fields := []string{"val1", "val2", "val3"}
	dat := NewA("mytest", dates, cols, fields)

	err := WriteBin(&dat, filename, 4)
	if err != nil {
		fmt.Println(err)
		return
	}

	dat, err = ReadBin(filename, 0, 0, false)
	if err != nil {
		t.Fatal(err)
	}
	if dat.Name != "mytest" {
		t.Fatal(dat.Name)
	}
	if dat.Len() == 0 {
		t.Fatal()
	}
	if len(dat.Fields) != len(fields) {
		t.Fatal("Fields:", len(dat.Fields))
	}

	row := dat.GetRowP()
	dat.RowAt(1, row)
	if row[0] != 10 || row[1] != 11 {
		t.Fatal()
	}
	dat, _ = ReadBin(filename, 0, 1190403, false)
	if dat.LastDate() != 1190403 {
		Print(&dat)
		t.Fatal()
	}
	dat, _ = ReadBin(filename, 1190402, 0, false)
	if dat.Dates[0] != 1190402 {
		Print(&dat)
		t.Fatal()
	}
	dat, _ = ReadBin(filename, 1190402, 1190403, false)
	if dat.Dates[0] != 1190402 && dat.LastDate() != 1190403 {
		Print(&dat)
		t.Fatal()
	}

	dat = GetA(fields)
	row[0], row[1], row[2] = 40, 41, 42
	dat.AddRow0(1190405, row)
	row[0], row[1], row[2] = 50, 51, 52
	dat.AddRow0(1190406, row)
	AppendBin(&dat, filename)
	dat, _ = ReadBin(filename, 0, 0, false)
	if dat.LastDate() != 1190406 || dat.Columns[0][5] != 50 {
		Print(&dat)
		t.Fatal()
	}
	PutA(&dat)
}

func TestApply(t *testing.T) {
	row := GetRow(1)
	defer PutRow(row)

	dat1 := GetA(vars.FieldsVal1)
	dat1.Dates = append(dat1.Dates, []int64{3, 4, 5, 6}...)
	dat1.Columns[0] = append(dat1.Columns[0], []float64{3, 4, 5, 6}...)
	defer PutA(&dat1)

	dat2 := GetA(vars.FieldsVal1)
	dat2.Dates = append(dat2.Dates, []int64{4, 5, 7, 8}...)
	dat2.Columns[0] = append(dat2.Columns[0], []float64{4, 5, 7, 8}...)
	defer PutA(&dat2)

	dat3 := GetA(vars.FieldsVal1)
	dat3.Dates = append(dat3.Dates, []int64{5, 7, 8, 9}...)
	dat3.Columns[0] = append(dat3.Columns[0], []float64{5, 7, 8, 9}...)
	defer PutA(&dat3)

	dat4 := GetA(vars.FieldsVal1)
	dat4.Dates = append(dat4.Dates, []int64{10}...)
	dat4.Columns[0] = append(dat4.Columns[0], []float64{10}...)
	defer PutA(&dat4)

	EveryApply(func(date int64, ids []int) {
		switch date {
		case 5:
			if ids[0] != 2 || ids[1] != 1 {
				t.Fatal(date, ids)
			}
		case 8:
			if ids[1] != 3 || ids[2] != 2 {
				t.Fatal(date, ids)
			}
		}
	}, &dat1, &dat2, &dat3, &dat4)

	EveryApply(func(date int64, ids []int) {
		switch date {
		case 4:
			if ids[0] != 1 || ids[1] != -1 {
				t.Fatal(date, ids)
			}
		case 6:
			if ids[0] != 3 || ids[1] != -1 {
				t.Fatal(date, ids)
			}
		}
	}, &dat1, &EmptyA)
}
