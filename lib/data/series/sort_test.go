package series

import (
	"testing"
)

// TestSort
func TestSort(t *testing.T) {
	s := NewEmpty("", []string{"val1", "val2", "val3"})
	s.AddRow(0, []float64{0, 1, 0})
	s.AddRow(1, []float64{1, 11, 11})
	s.AddRow(2, []float64{2, 9, 22})
	s.AddRow(4, []float64{4, 3, 44})
	s.AddRow(5, []float64{5, 5, 55})
	s.AddRow(3, []float64{3, 91, 33})
	s.AddRow(6, []float64{6, 15, 66})
	s.AddRow(7, []float64{7, 7, 77})
	s.AddRow(9, []float64{9, 6, 99})
	s.AddRow(8, []float64{8, 8, 88})

	cols := s.Columns.RangeLeftN(6, 2)
	if cols[0][0] != 3 || cols[0][1] != 6 || cols[1][0] != 91 || cols[1][1] != 15 {
		t.Fatal()
	}
	s.Sort()

	index := s.Len() - 1
	row := s.GetRowP()
	s.RowAt(index, row)
	if s.Dates[index] != 9 || row[0] != 9 || row[2] != 99 {
		t.Fatal("sort")
	}
	index = 0
	s.RowAt(index, row)
	if s.Dates[index] != 0 || row[0] != 0 || row[2] != 0 {
		t.Fatal("sort")
	}
	index = 3
	s.RowAt(index, row)
	if s.Dates[index] != 3 || row[0] != 3 || row[2] != 33 {
		t.Fatal("sort")
	}
}

func Test_SortBy(t *testing.T) {
	dat := NewEmpty("", []string{"val1", "val2", "val3"})
	dat.AddRow(5, []float64{5, 4.9, 55})

	dat.AddRow(0, []float64{0, 1, 0})
	dat.AddRow(1, []float64{1, 11, 11})
	dat.AddRow(2, []float64{2, 9, 22})
	dat.AddRow(4, []float64{4, 3, 44})
	dat.AddRow(5, []float64{5, 5, 55})
	dat.AddRow(3, []float64{3, 91, 33})
	dat.AddRow(6, []float64{6, 15, 66})
	dat.AddRow(7, []float64{7, 7, 77})
	dat.AddRow(9, []float64{9, 6, 99})
	dat.AddRow(8, []float64{8, 8, 88})
	dat.AddRow(5, []float64{5, 5, 5})

	s := dat.Copy()

	SortA(&s, "val2", "val3")

	index := 4
	row := dat.GetRowP()
	s.RowAt(index, row)
	if s.Dates[index] != 5 || row[0] != 5 || row[2] != 55 {
		Print(&s)
		t.Fatal("sort", s.Dates[index], row)
	}
	index = 6
	s.RowAt(index, row)
	if s.Dates[index] != 7 || row[0] != 7 || row[2] != 77 {
		Print(&s)
		t.Fatal("sort")
	}
	index = 2
	s.RowAt(index, row)
	if s.Dates[index] != 5 || row[1] != 4.9 {
		Print(&s)
		t.Fatal("sort")
	}
	index = 3
	s.RowAt(index, row)
	if s.Dates[index] != 5 || row[2] != 5 {
		Print(&s)
		t.Fatal("sort")
	}

	ReverseA(&s, "val3")
	index = 0
	s.RowAt(index, row)
	if s.Dates[index] != 9 || row[0] != 9 || row[2] != 99 {
		t.Fatal("sort")
	}
	index = 4
	s.RowAt(index, row)
	if s.Dates[index] != 5 || row[0] != 5 || row[2] != 55 {
		t.Fatal("sort")
	}

	SortA(&dat, "val1", "val2", "val3")
	index = 6
	dat.RowAt(index, row)
	if dat.Dates[index] != 5 || row[0] != 5 || row[2] != 5 {
		Print(&dat)
		t.Fatal("sort", index, row)
	}
	index = 10
	dat.RowAt(index, row)
	if dat.Dates[index] != 8 || row[0] != 8 || row[2] != 88 {
		Print(&dat)
		t.Fatal("sort", index, row)
	}

	ReverseA(&dat, "val1", "val2", "val3")
	index = 5
	dat.RowAt(index, row)
	if dat.Dates[index] != 5 || row[0] != 5 || row[2] != 5 {
		Print(&dat)
		t.Fatal("sort")
	}

	SortA(&dat, "val2")
	index = 11
	dat.RowAt(index, row)
	if dat.Dates[index] != 3 || row[0] != 3 || row[2] != 33 {
		Print(&dat)
		t.Fatal("sort")
	}

	SortA(&dat, "val3")
	index = 11
	dat.RowAt(index, row)
	if dat.Dates[index] != 9 || row[0] != 9 || row[2] != 99 {
		Print(&dat)
		t.Fatal("sort")
	}
}
