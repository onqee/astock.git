package tdx

import (
	"path"
	"strings"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
)

// ReadTdxP 读取通达信目录下的数据
func ReadTdxP(code, period, tdxfolder string, scoreA, scoreB int64) (series.A, error) {
	code = strings.ToLower(code)
	ext := ".day"
	dtypeTDX := "lday"

	switch period {
	case vars.PM5:
		ext = ".lc5"
		dtypeTDX = "fzline"

	case vars.PM1:
		ext = ".lc1"
		dtypeTDX = "minline"

	}

	market := code[:2]

	filename := path.Join(tdxfolder, "vipdoc", strings.ToLower(market), dtypeTDX, code+ext)
	mode := 1
	if period != vars.PD1 {
		mode = 0
	}

	return ReadFileToSeriesP(code, period, filename, scoreA, scoreB, mode)
	// return ReadFileP(code, period, filename, scoreA, scoreB, mode)
}

// ReadTdxDataP read from download from tdx.com.cn
// 读取通达信下载的5分钟数据
func ReadTdxDataP(code, period, folder string, scoreA, scoreB int64) (series.A, error) {
	code = strings.ToLower(code)
	ext := ".5"
	if period == vars.PD1 {
		ext = ".day"
	}

	filename := path.Join(folder, code+ext)
	mode := 1

	// return ReadFileP(code, period, filename, scoreA, scoreB, mode)
	return ReadFileToSeriesP(code, period, filename, scoreA, scoreB, mode)
}
