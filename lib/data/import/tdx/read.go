package tdx

import (
	"bytes"
	"encoding/binary"
	"math"
	"os"
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/util"
)

// ReadFile read tdx file
// mode 1:已整数*100的模式储存，常见于m5分钟数据
func ReadFileP(code, period, filename string, scoreA, scoreB int64, valMode int) (db.DataSet, error) {
	dat, _, err := readFile(1, code, period, filename, scoreA, scoreB, valMode)
	return dat, err
}

// ReadFileToSeries read tdx file
// valMode 1:已整数*100的模式储存，常见于m5分钟数据
func ReadFileToSeriesP(code, period, filename string, scoreA, scoreB int64, valMode int) (series.A, error) {
	_, dat, err := readFile(3, code, period, filename, scoreA, scoreB, valMode)
	return dat, err
}

func readFile(typ int, code, period, filename string, scoreA, scoreB int64, valMode int) (db.DataSet, series.A, error) {
	file, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	defer file.Close()
	if err != nil {
		return db.EmptyDataSet, series.EmptyA, err
	}

	b := make([]byte, 32)
	var u32 uint32
	var u16 uint16
	var xf float32
	var i, dd, tt int
	var fields []string
	isM := period[0] == 'm'

	if typ == 3 {
		fields = []string{vars.FieldOpen, vars.FieldHigh, vars.FieldLow, vars.FieldClose, vars.FieldAmt, vars.FieldVol}
	} else {
		if isM {
			fields = []string{vars.FieldCode, vars.FieldDate, vars.FieldTime, vars.FieldOpen, vars.FieldHigh, vars.FieldLow, vars.FieldClose, vars.FieldAmt, vars.FieldVol}
		} else {
			fields = []string{vars.FieldCode, vars.FieldDate, vars.FieldOpen, vars.FieldHigh, vars.FieldLow, vars.FieldClose, vars.FieldAmt, vars.FieldVol}
		}
	}

	var dat series.A
	var ds db.DataSet

	if typ == 3 { // series
		dat = series.GetA(fields)
	} else {
		ds = db.GetDataSet(fields)
	}
	row := dat.GetRowP()
	defer series.PutRow(row)
	dsrow := ds.GetRowP()
	defer db.PutRow(dsrow)

	for true {
		_, err = file.Read(b)
		if err != nil {
			break
		}

		bbuf := bytes.NewBuffer(b)
		if typ != 3 {
			dsrow[0] = codetool.ToCodeI(code)
		}

		for i = 0; i < 7; i++ {
			switch i {
			case 0:
				if isM {
					err = binary.Read(bbuf, binary.LittleEndian, &u16)
					if err != nil {
						return db.EmptyDataSet, series.EmptyA, err
					}
					dd = parseDate(u16)
					err = binary.Read(bbuf, binary.LittleEndian, &u16)
					if err != nil {
						return db.EmptyDataSet, series.EmptyA, err
					}
					tt = parseTime(u16)
				} else {
					err = binary.Read(bbuf, binary.LittleEndian, &u32)
					if err != nil {
						return db.EmptyDataSet, series.EmptyA, err
					}
					dd = parseDateD1(u32)
				}

			case 1, 2, 3, 4: //"open", "high", "low", "close"
				if valMode == 1 {
					err = binary.Read(bbuf, binary.LittleEndian, &u32)
					if err != nil {
						return db.EmptyDataSet, series.EmptyA, err
					}
					if typ == 3 {
						row[i-1] = util.Round(float64(u32)/100, 2)
					} else {
						if isM { // code, date, time 前面有3个字段
							dsrow[i+2] = util.Round(float64(u32)/100, 2)
						} else {
							// code, date 前面有2个字段
							dsrow[i+1] = util.Round(float64(u32)/100, 2)
						}
					}

				} else {
					err = binary.Read(bbuf, binary.LittleEndian, &xf)
					if err != nil {
						return db.EmptyDataSet, series.EmptyA, err
					}
					if typ == 3 {
						row[i-1] = util.Round(float64(xf), 2)
					} else {
						if isM { // code, date, time 前面有3个字段
							dsrow[i+2] = util.Round(float64(xf), 2)
						} else {
							// code, date 前面有2个字段
							dsrow[i+1] = util.Round(float64(xf), 2)
						}
					}

				}

			case 5: // amt
				err = binary.Read(bbuf, binary.LittleEndian, &xf)
				if err != nil {
					return db.EmptyDataSet, series.EmptyA, err
				}
				if typ == 3 {
					row[i-1] = util.Round(float64(xf/10000), 0)
				} else {
					if isM { // code, date, time 前面有3个字段
						dsrow[i+2] = int(util.Round(float64(xf/10000), 0))
					} else {
						// code, date 前面有2个字段
						dsrow[i+1] = int(util.Round(float64(xf/10000), 0))
					}
				}

			case 6: // vol
				err = binary.Read(bbuf, binary.LittleEndian, &u32)
				if err != nil {
					return db.EmptyDataSet, series.EmptyA, err
				}
				if typ == 3 {
					row[i-1] = float64(u32 / 100)
				} else {
					if isM {
						// code, date, time 前面有3个字段
						dsrow[i+2] = int(u32 / 100)
					} else {
						// code, date 前面有2个字段
						dsrow[i+1] = int(u32 / 100)
					}
				}
			}

		} // set data

		datetime := int64(dd)*10000 + int64(tt)
		if !isM && !vars.IsKtDate {
			datetime += 1500
		}
		if scoreA > 0 && scoreB == 0 {
			scoreB = datescore.Date2Score(time.Now().AddDate(0, 0, 1))
		}

		if scoreA < scoreB && (datetime < scoreA || datetime > scoreB) {
			continue
		}

		if typ == 3 {
			dat.AddRow0(datetime, row)
		} else {
			dsrow[1] = dd
			if isM {
				dsrow[2] = tt
			}
			ds.AddRow0(dsrow)
		}
	} // for read 32

	if typ == 3 {
		return db.EmptyDataSet, dat, nil
	}
	return ds, series.EmptyA, nil
}

func parseDate(v uint16) int {
	num := float64(v)
	year := math.Floor(num/2048) + 2004
	month := math.Floor(math.Mod(num, 2048) / 100)
	day := math.Mod(math.Mod(num, 2048), 100)

	// 20060102
	return int((year-1900)*10000 + month*100 + day)
}

func parseTime(v uint16) int {
	num := float64(v)
	m := math.Floor(num / 60)
	return int(m*100) + int(num-m*60)
}

func parseDateD1(val uint32) int {
	v := int(val)
	year := v/10000 - 1900
	return year*10000 + (v - (v/10000)*10000)
}
