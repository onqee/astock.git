package tdx

// const (
// 	m5ext = ".lc5"
// )
//
// // ReadM5 func
// func ReadM5(code, folder string) (db.DataSet, error) {
// 	code = strings.ToLower(code)
// 	result := db.DataSet{}
// 	ext := m5ext
// 	dtype := "fzline"
// 	market := code[:2]
//
// 	filename := path.Join(folder, "vipdoc", strings.ToLower(market), dtype, code+ext)
// 	file, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
// 	if err != nil {
// 		return result, err
// 	}
//
// 	defer file.Close()
//
// 	fields := []string{"date", "open", "high", "low", "close", "amt", "vol"}
//
// 	b := make([]byte, 32)
// 	var data db.DataRow
// 	var x uint32
// 	var x2 uint16
// 	var xf float32
// 	var i int
//
// 	for true {
// 		_, err = file.Read(b)
// 		if err != nil {
// 			break
// 		}
//
// 		bbuf := bytes.NewBuffer(b)
// 		data = db.DataRow{"code": codetool.ToCodeI(code)}
// 		for i = 0; i < 7; i++ {
// 			switch i {
// 			case 0:
// 				err = binary.Read(bbuf, binary.LittleEndian, &x2)
// 				if err != nil {
// 					return nil, err
// 				}
// 				data["date"] = parseDate(x2)
//
// 				err = binary.Read(bbuf, binary.LittleEndian, &x2)
// 				if err != nil {
// 					return nil, err
// 				}
// 				data["time"] = parseTime(x2)
//
// 			case 1, 2, 3, 4:
// 				err = binary.Read(bbuf, binary.LittleEndian, &x)
// 				if err != nil {
// 					return nil, err
// 				}
// 				data[fields[i]] = float64(x) / 100
//
// 			case 5: // amt
// 				err = binary.Read(bbuf, binary.LittleEndian, &xf)
// 				if err != nil {
// 					return nil, err
// 				}
// 				data[fields[i]] = int(util.Round(float64(xf/10000), 0))
// 				// data[fields[i]] = int(util.Round(float64(xf), 0))
//
// 			case 6: // vol
// 				err = binary.Read(bbuf, binary.LittleEndian, &x)
// 				if err != nil {
// 					return nil, err
// 				}
// 				data[fields[i]] = int(x)
// 			}
//
// 		} // set data
// 		result = append(result, data)
// 	} // for read 32
//
// 	return result, nil
// }
