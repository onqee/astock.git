package tdx

import (
	"bytes"
	"io/ioutil"
	"strconv"
	"strings"

	iconv "github.com/djimenez/iconv-go"
	"github.com/kere/gno/db"
)

// ReadTxtP 读取tdx导出的xls
func ReadTxtP(filename string) (db.DataSet, error) {
	src, err := ioutil.ReadFile(filename)
	if err != nil {
		return db.EmptyDataSet, err
	}

	lines := bytes.Split(src, []byte("\n"))
	l := len(lines)

	// Fields ---------
	line := lines[0]
	sline0, _ := iconv.ConvertString(string(line), "gb2312", "utf-8")
	fields := strings.Split(sline0, "\t")
	ll := len(fields)
	sep := []byte("\t")

	// result := make([]db.MapRow, 0)
	ds := db.GetDataSet(fields)
	row := ds.GetRowP()
	defer db.PutRow(row)

	for i := 1; i < l-2; i++ {
		line = lines[i]
		arr := bytes.Split(line, sep)
		if len(arr) < 5 {
			continue
		}

		for k := 0; k < ll; k++ {
			field := fields[k]
			switch field {
			case "代码":
				row[k] = string(arr[k])
			case "名称":
				str, _ := iconv.ConvertString(string(arr[k]), "gb2312", "utf-8")
				row[k] = str
			default:
				str := string(bytes.TrimSpace(arr[k]))
				if str == "--" {
					row[k] = 0.0
					continue
				}

				row[k], err = strconv.ParseFloat(str, 64)
				if err != nil {
					row[k] = 0.0
				}
			}
		}

		ds.AddRow0(row)
	}

	return ds, nil
}
