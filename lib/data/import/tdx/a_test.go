package tdx

import (
	"math"
	"testing"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/db"
	_ "github.com/lib/pq"
)

func TestReadTdxM1(t *testing.T) {
	filename := "testdata/sh600584.lc1"
	dat, err := ReadFileToSeriesP("sh600584", vars.PM1, filename, 11907110000, 11907129999, 0)
	if err != nil {
		t.Fatal(err)
	}
	v, _ := dat.ValueOn(vars.FieldClose, 11907121450)
	if math.Abs(v-11.9) > 0.001 {
		series.Print(&dat)
		t.Fatal(v)
	}
}

func TestReadTdxM5(t *testing.T) {
	filename := "testdata/sh600584.lc5"
	dat, err := ReadFileToSeriesP("sh600584", vars.PM5, filename, 11907110000, 11907129999, 0)
	if err != nil {
		t.Fatal(err)
	}

	v, _ := dat.ValueOn(vars.FieldClose, 11907121450)
	if math.Abs(v-11.9) > 0.001 {
		series.Print(&dat)
		t.Fatal(v)
	}
	// series.Print(&dat)
	ds, err := ReadFileP("sh600584", vars.PM5, filename, 11907110000, 11907129999, 1)
	if ds.Len() != dat.Len() {
		t.Fatal()
	}

	// db.PrintDataSet(&ds)
	series.PutA(&dat)
	db.PutDataSet(&ds)
}

func TestReadTdxD1(t *testing.T) {
	vars.IsKtDate = false
	filename := "testdata/sz003010.day"
	dat, err := ReadFileToSeriesP("sz003010", vars.PD1, filename, 12105019999, 12107129999, 1)
	if err != nil {
		t.Fatal(err)
	}

	row, _ := dat.RowOnP(12106221500)
	if math.Abs(row[0]-29.31) > 0.001 || math.Abs(row[3]-29.26) > 0.001 || math.Abs(row[4]-3101) > 0.001 {
		series.Print(&dat)
		t.Fatal(row)
	}
	dat.RowOn(12107021500, row)
	if math.Abs(row[0]-27.08) > 0.001 || math.Abs(row[3]-26.81) > 0.001 || math.Abs(row[4]-1839) > 0.001 {
		series.Print(&dat)
		t.Fatal(row)
	}
	series.PutRow(row)
	series.PutA(&dat)

	ds, err := ReadFileP("sz003010", vars.PD1, filename, 12105019999, 12107129999, 1)
	if err != nil {
		t.Fatal(err)
	}
	dbrow := ds.DBRowAtP(32)
	if math.Abs(dbrow.FloatAt(2)-29.31) > 0.01 || math.Abs(dbrow.FloatAt(5)-29.26) > 0.01 || math.Abs(dbrow.FloatAt(6)-3101) > 0.01 {
		// db.PrintDataSet(&ds)
		t.Fatal(dbrow)
	}
	ds.DBRowAt(40, dbrow)
	if math.Abs(dbrow.FloatAt(2)-27.08) > 0.01 || math.Abs(dbrow.FloatAt(5)-26.81) > 0.01 || math.Abs(dbrow.FloatAt(6)-1839) > 0.00 {
		db.PrintDataSet(&ds)
		t.Fatal(dbrow)
	}
	// db.PrintDataSet(&ds)
	db.PutRow(dbrow.Values)
	db.PutDataSet(&ds)
}
