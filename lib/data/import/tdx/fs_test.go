package tdx

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
)

type HtcHead struct {
	Code    string
	DateInt uint32
	V       int
	Length  int
}

func Head(file *os.File) HtcHead {
	//  1   2  3   4  5   6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
	// [255 2 239 158 64 138 0 51 57 57 57 55 54 0 247 21 52 1 91 132  0  0 35 74 0  0  0  0  0  0]
	src := make([]byte, 30)
	file.Read(src)
	fmt.Println("===", len(src))

	var x, x1, x2 uint32
	buf := bytes.NewBuffer(src)

	b2 := make([]byte, 6)
	buf.Read(b2)

	bcode := make([]byte, 8)
	binary.Read(buf, binary.LittleEndian, &bcode)
	fmt.Println(":", string(bcode))
	binary.Read(buf, binary.LittleEndian, &x)
	binary.Read(buf, binary.LittleEndian, &x1)
	binary.Read(buf, binary.LittleEndian, &x2)
	fmt.Println(src)
	fmt.Println("---------- n:", string(b2), x, x1, x2)

	return HtcHead{Code: string(bcode[1:7]), DateInt: x, V: int(x1), Length: int(x2)}
}

func printIt(k int, b []byte) {
	var x uint32
	var f32 float32
	var x16, x16_2 uint16

	buf := bytes.NewBuffer(b)
	binary.Read(buf, binary.LittleEndian, &f32)

	buf = bytes.NewBuffer(b)
	binary.Read(buf, binary.LittleEndian, &x)

	buf = bytes.NewBuffer(b)
	binary.Read(buf, binary.LittleEndian, &x16)
	binary.Read(buf, binary.LittleEndian, &x16_2)

	fmt.Println(k, b, string(b), "|", x, "|", x16, x16_2, "|", f32)
}

func ReadTdxExport(file *os.File) {
	h := Head(file)
	var b []byte
	b = make([]byte, h.Length)
	file.Read(b)
	if h.Code == "000589" {
		printSrc(b, 100)
	}

	println()
}

func printSrc(src []byte, n int) {
	b := make([]byte, 4)
	buf := bytes.NewBuffer(src)
	k := 0
	// reg, _ := regexp.Compile("\\d+")
	// for {
	for i := 0; i < n; i++ {
		_, err := buf.Read(b)
		if err != nil {
			break
		}
		printIt(k, b)
		k++
	}
}
