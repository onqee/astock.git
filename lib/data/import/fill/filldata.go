package fill

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
)

// FillByA store result data
func FillByA(code, period string, dat series.A) (int, error) {
	l := dat.Len()
	if l == 0 {
		return 0, nil
	}

	var ds db.DataSet
	var table string
	hasTime := false

	switch period {
	case vars.PD1:
		table = vars.TableBarD1
		ds = db.GetDataSet([]string{vars.FieldCode, vars.FieldDate, vars.FieldClose, vars.FieldOpen, vars.FieldHigh, vars.FieldLow, vars.FieldVol, vars.FieldAmt})
		defer db.PutDataSet(&ds)
	case vars.PM5:
		table = vars.TableBarM5
		hasTime = true
		ds = db.GetDataSet([]string{vars.FieldCode, vars.FieldDate, vars.FieldTime, vars.FieldClose, vars.FieldOpen, vars.FieldHigh, vars.FieldLow, vars.FieldVol, vars.FieldAmt})
		defer db.PutDataSet(&ds)
	default:
		panic("can not fill with period " + period)
	}
	e := db.NewExists(table)
	e.Prepare(true)

	lastStoreDate := bars.LastDBDate(code, period)

	dat1 := dat.Range(lastStoreDate, math.MaxInt64)
	codei := codetool.ToCodeI(code)
	// db row
	dsrow := ds.GetRowP()
	defer db.PutRow(dsrow)
	dsrow[0] = codei
	// series
	row := dat.GetRowP()
	defer series.PutRow(row)

	l = dat1.Len()
	for i := 0; i < l; i++ {
		date := dat1.Dates[i]
		if date < lastStoreDate {
			break
		}

		dd, tt := datescore.ScoreSplit(date)
		if hasTime && e.Where("code=$1 and date=$2 and time=$3", codei, dd, tt).Exists() {
			continue
		} else if !hasTime && e.Where("code=$1 and date=$2", codei, dd).Exists() {
			continue
		}
		dat1.RowAt(i, row)
		dsrow[1] = dd
		if hasTime {
			dsrow[2] = tt
			//           0    1     2      3     4      5     6   7    8
			//dataset: code, date, time, close, open, high, low, vol, amt
			//                   series: open, high, low, close, amt, vol
			dsrow[3] = row[3] //close
			dsrow[4] = row[0] // open
			dsrow[5] = row[1] // high
			dsrow[6] = row[2] // low
			dsrow[7] = row[5] // vol
			dsrow[8] = row[4] // amt
		} else {
			//           0    1      2     3      4     5   6    7
			//dataset: code, date, close, open, high, low, vol, amt
			dsrow[2] = row[3] //close
			dsrow[3] = row[0] // open
			dsrow[4] = row[1] // high
			dsrow[5] = row[2] // low
			dsrow[6] = row[5] // vol
			dsrow[7] = row[4] // amt
		}
		ds.AddRow(dsrow)
	}

	count := ds.Len()
	if count == 0 {
		return 0, nil
	}

	ins := db.NewInsert(table)
	err := ins.InsertMN(&ds, 200)
	fmt.Println("insert ", code, ":", ds.Columns[1][0], "->", ds.Columns[1][count-1])

	return ds.Len(), err
}
