package fill

import (
	"fmt"
	"math"
	"os"
	"path/filepath"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/log"
)

func BarWriteTo(code, period string, startDay int64, check, isPrint bool) error {
	barSZ := dget.Bar(vars.Code399001, vars.PD1)
	dbN := 30 //试探提取天数
	if period[0] == 'm' {
		dbN = 12
	}
	testDate := barSZ.Dates[barSZ.Len()-dbN] / 10000

	// 1: 从db加载bar
	barTest := bars.NewBar(code, period)
	barTest.StartDay = testDate
	err := barTest.Load()
	if err != nil {
		return err
	}

	c := gno.GetConfig().GetConf("data")
	folder := filepath.Join(c.Get("store_dir"), "bar", period)
	if vars.IsTest {
		folder = filepath.Join(c.Get("store_dir"), "test/bar", period)
	}
	os.MkdirAll(folder, os.ModeDir)

	filename := folder + "/" + code + vars.ExtZip
	// 如果没有数据
	if barTest.Len() == 0 {
		log.App.Notice(code + " empty 没有侦探到数据，现在从DB加载数据")
		bar := bars.NewBar(code, period)
		bar.StartDay = startDay
		bar.Load()
		series.WriteBin(&bar.A, filename, 4, series.BinNumBit(&bar.A))
		// series.PutA(&bar.A)
		return nil
	}

	isChanged := false
	dateA := testDate * 10000
	if !vars.IsKtDate {
		dateA = testDate*10000 + 1500
	}
	barBin, _ := series.ReadBin(filename, dateA, math.MaxInt64, true)
	defer series.PutA(&barBin)
	if isPrint {
		series.Print(&barBin)
	}

	if barBin.Len() > 0 {
		// 2: 判断bar是否需要重新计算（因为复权）
		//    比较bar2和barBin，是否已经发生改动
		bar2 := barTest.Range(barBin.Dates[0], barBin.LastDate())
		n := bar2.Len()
		if n == 0 || n != barBin.Len() {
			isChanged = true
			log.App.Notice(code + " changed 数据长度不匹配，需要重新加载")
		} else {
			kk := n - 10
			if kk < -1 {
				kk = -1
			}
			for k := n - 1; k < kk; kk-- {
				if math.Abs(bar2.ValueAt(vars.FieldClose, k)-barBin.ValueAt(vars.FieldClose, k)) > 0.01 {
					isChanged = true
					log.App.Notice(code+" changed 数据发生了改变", bar2.Dates[k], barBin.Dates[k], bar2.ValueAt(vars.FieldClose, k), barBin.ValueAt(vars.FieldClose, k))
					break
				}
			}
		}
	} else {
		isChanged = true
		log.App.Notice(code + " changed Bin数据为空，需要重新加载")
	}

	if isChanged {
		// rewrite
		bar := bars.NewBar(code, period)
		bar.StartDay = startDay
		bar.Load()
		series.WriteBin(&bar.A, filename, 4, series.BinNumBit(&bar.A))
		series.PutA(&bar.A)
	} else if barTest.LastDate() > barBin.LastDate() {
		log.App.Notice(code + " append 增加数据")
		// append
		ai, bi := barTest.RangeI(barBin.LastDate(), math.MaxInt64)
		// dat := barTest.Range(barBin.LastDate()+1, 0).A
		dat := barTest.IRange(ai+1, bi).A
		if err := series.AppendBin(&dat, filename); err != nil {
			return err
		}

	} else {
		log.App.Notice(code + " x 数据没有发生改变")
	}

	// check
	if check {
		barA := bars.NewBar(code, period)
		barA.StartDay = testDate
		err := barA.Load()
		defer barA.Release()
		if err != nil {
			fmt.Println(code, err)
			return err
		}
		barB := bars.NewBar(code, period)
		barB.StartDay = testDate
		err = barB.LoadZip()
		defer barB.Release()
		if err != nil {
			fmt.Println(code, err)
			return err
		}
		if isPrint {
			series.Print(&barA.A)
			series.Print(&barB.A)
		}
		if barA.Len() != barB.Len() {
			fmt.Println(code, barA.Len(), "!=", barB.Len())
			return fmt.Errorf("检查：%s 长度不等 %d != %d", code, barA.Len(), barB.Len())
		}
		count := barA.Len()
		for i := 0; i < count; i++ {
			if math.Abs(barA.Columns[0][i]-barB.Columns[0][i]) > 0.01 {
				return fmt.Errorf("检查：%s 数值不等 %f != %f", code, barA.Columns[0][i], barB.Columns[0][i])
			}
		}
	}

	return nil
}
