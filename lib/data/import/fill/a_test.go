package fill

import (
	"os"
	"path/filepath"
	"testing"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	"github.com/kere/gno/db"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../../../app.conf")
	db.Current().SetLogLevel("all")
	vars.IsTest = true
}

func TestWriteTo(t *testing.T) {
	code := vars.Code399001
	period := vars.PD1

	c := gno.GetConfig().GetConf("data")
	folder := filepath.Join(c.Get("store_dir"), "test/bar", period)
	os.MkdirAll(folder, os.ModeDir)
	filename := folder + "/" + code + vars.ExtZip
	os.Remove(filename)

	// 1: 数据库提取，写入数据
	sz := dget.Bar(code, period)
	err := BarWriteTo(code, period, sz.Dates[sz.Len()-10]/10000, false, false)
	if err != nil {
		t.Fatal(err)
	}
	datR, _ := series.ReadBin(filename, 0, 0, true)
	if datR.LastDate() != sz.LastDate() {
		series.Print(&datR)
		t.Fatal()
	}

	// 2: 写入最后往前10条数据
	os.Remove(filename)
	index := sz.Len() - 10
	dat := sz.IRange(index-40, index)
	err = series.WriteBin(&dat.A, filename, 4, 64)
	if err != nil {
		t.Fatal(err)
	}
	datR, _ = series.ReadBin(filename, 0, 0, true)
	if datR.LastDate() != sz.Dates[sz.Len()-10] {
		series.Print(&datR)
		t.Fatal(sz.Dates[sz.Len()-10])
	}
	// series.Print(&datR)
	series.PutA(&datR)

	// 3: Append写入模式测试
	err = BarWriteTo(code, period, 1210101, true, false)
	if err != nil {
		// series.Print(&datR)
		t.Fatal(err)
	}
	datR, err = series.ReadBin(filename, 0, 0, true)
	if err != nil {
		series.Print(&datR)
		t.Fatal(err)
	}
	series.PutA(&datR)

	BarWriteTo("sh600005", period, 1210101, true, false)
}
