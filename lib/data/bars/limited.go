package bars

import (
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
)

var (
	// LimitedFields 解禁股数据字段
	LimitedFields = []string{"解禁数量(万股)", "解禁股流通市值(亿元)", "上市批次"}
)

// Limited 限售股解禁
type Limited struct {
	DB
}

// NewLimited struct
func NewLimited(code string) *Limited {
	a := &Limited{}
	a.Table = vars.TableLimited
	a.Q = db.NewQuery(vars.TableLimited)
	a.Q.Where("code=$1", codetool.ToCodeI(code)).Order("date")
	a.Fields = LimitedFields
	// a.StartDay = vars.DefaultStartDay
	// a.EndDay = vars.DefaultEndDay

	return a
}

// // Lift 解禁数量和金额
// // dayScore: 1180701
// // n: 在前后n天内
// func (a *Limited) Lift(dayScore int64, n int) (float64, float64) {
// 	d := datescore.Score2Date(dayScore * 10000)
// 	dA := d.AddDate(0, 0, -n)
// 	dB := d.AddDate(0, 0, n)
//
// 	dateA := datescore.Date2Score(dA)
// 	dateB := datescore.Date2Score(dB)
//
// 	dat := a.Range(dateA, dateB)
//
// 	sum := series.ColumnsSum(dat.Columns)
//
// 	v1 := sum.ValueOf("解禁数量(万股)", a.Fields)
// 	v2 := sum.ValueOf("解禁股流通市值(亿元)", a.Fields)
//
// 	return v1, v2
// }
