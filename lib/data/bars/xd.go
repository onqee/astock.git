package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

// ExcludDividend class
type ExcludDividend struct {
	IsBuiled       bool
	Code           string
	ShareBonusData *ShareBonus
	// RationedShareData *RationedShare
	xdDates []int64
	// xdTypeMap map[int64]int
	// xdPercentMap 除权比例数据表 map[score]xdRate
	xdPercentMap map[int64]float64

	A *series.A // 除权数据
	// SeriesXD    series.A //除权后的数据
	IsStartDate bool
}

// NewExcludDividend func
func NewExcludDividend(code string, seriesData *series.A, isStartDate bool) *ExcludDividend {
	// return &ExcludDividend{Code: code, A: seriesData, xdTypeMap: make(map[int64]int, 0), xdPercentMap: make(map[int64]float64, 0)}
	return &ExcludDividend{Code: code, A: seriesData, xdPercentMap: make(map[int64]float64, 0), IsStartDate: isStartDate}
}

// ExBuild XD A 除权数据
func (e *ExcludDividend) ExBuild() error {
	err := e.loadShareBonusP()
	defer func() {
		e.xdPercentMap = nil
		series.PutDates(e.xdDates)
		series.PutA(&e.ShareBonusData.A)
	}()
	if err != nil {
		return err
	}

	// if e.ShareBonusData.Len() == 0 && e.RationedShareData.Len() == 0 {
	if e.ShareBonusData.Len() == 0 {
		return nil
	}

	// 梳理除权信息
	e.build()

	var k, startI, endI, status int
	var scoreA, scoreB int64
	ll := len(vars.BarFields)
	// length := e.A.Len()

	leftScore := e.A.Dates[0]
	lastDate := e.A.LastDate()

	// // new Columns
	// var cols series.Columns
	// if isPool {
	// 	cols = series.GetColumns(vars.BarFields, ll, length)
	// } else {
	// 	cols = series.NewColumnsN(ll, length)
	// }

	l := len(e.xdDates)
	if l == 0 {
		return nil
	}

	row := e.A.GetRowP()
	defer series.PutRow(row)
	vals := series.GetRow(ll)
	defer series.PutRow(vals)

	for i := l - 1; i > -2; i-- {
		if i > -1 && lastDate < e.xdDates[i] {
			continue
		}
		var percent float64

		// 设置除权的 A、B 时间段
		if i == l-1 {
			scoreA = e.xdDates[i] // 11905271500 -> 11905270000
			scoreB = lastDate/10000*10000 + 9999
			percent = 1
		} else if i == -1 {
			scoreA = leftScore
			scoreB = e.xdDates[0]
			percent = e.xdPercentMap[scoreB]
		} else {
			scoreA = e.xdDates[i] // 11905271500 -> 11905270000
			scoreB = e.xdDates[i+1]
			percent = e.xdPercentMap[scoreB]
		}

		if leftScore >= scoreB {
			break
		}

		if leftScore > scoreA {
			scoreA = leftScore
		}

		if scoreA >= scoreB {
			continue
		}

		if percent == 1 {
			// startI, status = e.A.Search(scoreA)
			// if status == series.IndexNotFound {
			// 	startI++
			// }
			// endI = length - 1
			// for k = startI; k < endI+1; k++ {
			// 	e.A.RowAt(k, row)
			// 	cols.SetRow(k, row)
			// }
		} else {
			startI, status = e.A.Search(scoreA)
			if status == series.IndexNotFound {
				startI++
			}
			endI, _ = e.A.Search(scoreB - 1)

			// var vals, row []float64
			for k = startI; k < endI+1; k++ {
				e.A.RowAt(k, row)
				// "close", "open", "high", "low", "vol", "amt", "xd"
				// 需要除权的字段，做除权处理
				for bI := 0; bI < ll; bI++ {
					switch bI {
					case 4, 5: // "vol":
						vals[bI] = row[bI]
					// case 5: // "amt":
					// 	vals[bI] = row[bI]
					case 6: // xd 除权系数
						vals[bI] = percent

					default:
						vals[bI] = util.Round(row[bI]*percent, 2)
					}
				} // end fields
				e.A.Columns.SetRow0(k, vals)
				// cols.SetRow(k, vals)
			}
		}

	}

	return nil
}

// loadShareBonusP 加载除权数据
func (e *ExcludDividend) loadShareBonusP() error {
	lastDate := e.A.LastDate()
	s1 := NewShareBonus(e.Code, e.IsStartDate)

	err := s1.Load(true)
	if err != nil {
		return err
	}

	l := s1.Len()
	// e.xdDates = make([]int64, l)
	e.xdDates = series.GetDates(l)

	for i := 0; i < l; i++ {
		score := s1.Dates[i]
		if lastDate < score {
			continue
		}
		// e.xdDates = append(e.xdDates, score)
		e.xdDates[i] = score
		// e.xdTypeMap[score] = 1
	}

	e.ShareBonusData = s1

	tmp := (util.Int64sOrder)(e.xdDates)
	tmp.Sort()
	// sort.Ints(e.xdDates)
	return nil
}

const (
	fieldStkDiv  = "stk_div"  //每股送转
	fieldCashDiv = "cash_div" // 每股分红（税前）
	fieldRoff    = "roff"     // 配股
	fieldRPrice  = "r_price"  // 配股价格
	fieldRBase   = "r_base"   // 配股基准股本
)

// build 除权比例信息
// 以e.A 的最大日期为结束日期
func (e *ExcludDividend) build() {
	var lastPerc, pricePre float64
	e.xdPercentMap = make(map[int64]float64, 0)

	l := len(e.xdDates)
	row := e.A.GetRowP()
	defer series.PutRow(row)
	src := e.ShareBonusData.GetRowP()
	defer series.PutRow(src)

	for i := l - 1; i > -1; i-- {
		score := e.xdDates[i]
		index, stat := e.A.Search(score)
		if stat == series.IndexOverRangeLeft || index == 0 {
			continue
		}
		if stat == series.IndexNotFound {
			index++ // 如果没有找到，索引回落在前一天
		}
		e.A.RowAt(index-1, row)
		pricePre = row.ValueOf(vars.FieldClose, e.A.Fields)

		// ShareBonus
		var priceCalc float64

		e.ShareBonusData.RowOn(score, src)
		roff := src.ValueOf(fieldRoff, e.ShareBonusData.Fields)

		if roff > 0 {
			rprice := src.ValueOf(fieldRPrice, e.ShareBonusData.Fields)
			// rbase := src.ValueOf(fieldRBase, e.ShareBonusData.Fields)
			roff = roff / 10
			priceCalc = (pricePre + roff*rprice) / (1 + roff)
		} else {
			cashDiv := src.ValueOf(fieldCashDiv, e.ShareBonusData.Fields)
			stkDiv := src.ValueOf(fieldStkDiv, e.ShareBonusData.Fields)
			// priceCalc = ((pricePre - cashDiv) + nn) / (1 + stkDiv)
			priceCalc = (pricePre - cashDiv/10) / (1 + stkDiv/10)
		}

		if lastPerc > 0 {
			lastPerc = lastPerc * priceCalc / pricePre
		} else {
			lastPerc = priceCalc / pricePre
		}

		e.xdPercentMap[score] = lastPerc
	}
}
