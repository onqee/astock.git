package bars

import (
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
)

// FinSum 财务摘要信息
type FinSum struct {
	DB
}

// NewFinSum struct
func NewFinSum(code string) *FinSum {
	fin := &FinSum{}
	fin.Table = vars.TableFinSum
	fin.Q = db.NewQuery(vars.TableFinSum)
	fin.Q.Order("date").Where("code=$1", codetool.ToCodeI(code))

	return fin
}
