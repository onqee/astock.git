package bars

// // RationedShare class
// type RationedShare struct {
// 	DB
// 	Code string
// }
//
// // NewRationedShare new
// func NewRationedShare(code string) *RationedShare {
// 	d := &RationedShare{Code: code}
// 	d.StartDay = 1100101
// 	d.Fields = vars.RationedShareFields
// 	d.Table = vars.TableRationedShare
//
// 	d.Q = db.NewQuery(vars.TableRationedShare).Order("date")
// 	return d
// }
//
// // Load func
// func (b *RationedShare) Load() error {
// 	b.Q.Where("code=$1", codetool.ToCodeI(b.Code))
// 	rows, err := b.Q.Query()
// 	if err != nil {
// 		return err
// 	}
//
// 	b.Columns, b.Dates = b.ParseDateSet(rows)
// 	return nil
// }

// // ParseDateSet parse loaded rows
// func (b *RationedShare) ParseDateSet(rows db.DataSet) (series.Columns, []int64) {
// 	dates := make([]int64, 0)
// 	// arr := make([]series.Row, 0)
// 	ll := len(vars.RationedShareFields)
//
// 	var item db.DataRow
// 	var field string
// 	var k int
// 	var tmpDate int64
// 	l := len(rows)
// 	cols := series.NewColumnsN(ll, l)
// 	for i := 0; i < l; i++ {
// 		// 除权日期，如果没有值，则以 date_sale 日期为准
// 		item = rows[i]
// 		tmpDate = item.Int64("datexd")
// 		if tmpDate < 1 {
// 			tmpDate = item.Int64("date_sale")
// 		}
// 		if tmpDate < 1 {
// 			continue
// 		}
// 		tmpDate = tmpDate * 10000
// 		dates = append(dates, tmpDate)
// 		dat := make([]float64, ll)
// 		for k, field = range b.Fields {
// 			dat[k] = item.Float(field)
// 		}
// 		// arr = append(arr, dat)
// 		// cols.AddRow(dat)
// 		cols.SetRow(dat, i)
// 	}
// 	return cols, dates
// }
