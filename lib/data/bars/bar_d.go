package bars

import (
	"fmt"
	"strconv"
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
)

type weekrow struct {
	// close, open, high, low, vol, amt float64
	row      []float64
	date     int64
	year, no int
}

type monthrow struct {
	row       []float64
	date      int64
	year, mon int
}

// D1BarToN 日线转换成Dn日线
func D1BarToN(bar *Bar, period string, isStartDate, isPool, releaseOld bool) Bar {
	if period[0] != 'd' {
		panic("只能转换日线多周期，不能转换成：" + period)
	}
	n, _ := strconv.Atoi(string(period[1]))
	dat := d1ToN(bar.A, n, isStartDate, isPool, releaseOld)
	b := NewBar(bar.Code, period)
	b.A = dat
	return b
}

// d1ToN 日线转换成Dn日线
func d1ToN(dat series.A, n int, isStartDate, ispool, releaseOld bool) series.A {
	l := dat.Len()
	if l == 0 || l < n {
		return series.EmptyA
	}

	var result series.A
	if ispool {
		result = series.GetA(vars.BarFields)
		result.Name = dat.Name
		defer series.PutA(&dat)
	} else {
		result = series.NewEmpty(dat.Name, vars.BarFields, 0, 200)
		if releaseOld {
			defer series.PutA(&dat)
		}
	}

	// ll := len(dat.Fields)
	// close, open, high, low, vol, amt, xd float64
	// closeI := dat.FieldI(vars.FieldClose)
	openI := dat.FieldI(vars.FieldOpen)
	highI := dat.FieldI(vars.FieldHigh)
	lowI := dat.FieldI(vars.FieldLow)
	volI := dat.FieldI(vars.FieldVol)
	amtI := dat.FieldI(vars.FieldAmt)

	if openI < 0 || highI < 0 || lowI < 0 {
		panic(fmt.Errorf("bar field not found in:%v", dat.Fields))
	}

	var k int
	var startDate int64
	row := dat.GetRowP()
	newRow := dat.GetRowP()

	defer func() {
		series.PutRow(row)
		series.PutRow(newRow)
	}()

	// 从后往前合并
	for i := l - 1; i > -1; i-- {
		dat.RowAt(i, row)
		if k == 0 {
			copy(newRow, row)
			if !isStartDate {
				startDate = dat.Dates[i]
			}
		}

		newRow[volI] += row[volI]
		newRow[amtI] += row[amtI]
		if newRow[highI] < row[highI] {
			newRow[highI] = row[highI]
		}
		if newRow[lowI] > row[lowI] {
			newRow[lowI] = row[lowI]
		}

		k++
		if k == n {
			k = 0
			if isStartDate {
				startDate = dat.Dates[i]
			}
			newRow[openI] = row[openI]
			result.AddRow(startDate, newRow)
		}
	}

	// 从后往前，如果出现残缺的k线
	if k > 0 {
		dat.RowAt(0, row)
		newRow[openI] = row[openI]
		result.AddRow(startDate, newRow)
	}

	result.Sort()
	return result
}

// D1BarToMonth 日线转换成周线
func D1BarToMonth(bar *Bar, isStartDate, isPool, releaseOld bool) Bar {
	dat := d1ToMonth(bar.A, isStartDate, isPool, releaseOld)
	b := NewBar(bar.Code, vars.PMo1)
	b.A = dat
	return b
}

// d1ToMonth 日线转月线
func d1ToMonth(dat series.A, isStartDate, ispool, releaseOld bool) series.A {
	l := dat.Len()
	if l == 0 {
		return series.EmptyA
	}

	// result := series.NewEmpty(dat.Name, vars.BarFields)
	var result series.A
	if ispool {
		result = series.GetA(vars.BarFields)
		result.Name = dat.Name
		defer series.PutA(&dat)
	} else {
		result = series.NewEmpty(dat.Name, vars.BarFields, 0, 200)
		if releaseOld {
			defer series.PutA(&dat)
		}
	}
	// close, open, high, low, vol, amt, xd float64
	var monDat monthrow
	monDat.row = dat.GetRowP()
	defer series.PutRow(monDat.row)

	row := dat.GetRowP()
	defer series.PutRow(row)
	newRow := result.GetRowP()
	defer series.PutRow(newRow)

	for i := 0; i < l; i++ {
		d := datescore.Score2Date(dat.Dates[i])
		if monDat.year != 0 && (int(d.Month()) != monDat.mon || d.Year() != monDat.year) {
			result.AddRow0(monDat.date, monDat.row)
			monDat.date, monDat.year, monDat.mon = 0, 0, 0
		}
		dat.RowAt(i, row)
		if monDat.year == 0 {
			if isStartDate {
				monDat.date = dat.Dates[i]
			}
			monDat.row[1] = row[1] // open
			monDat.row[3] = row[3] // low
			monDat.year = d.Year()
			monDat.mon = int(d.Month())
		}
		if !isStartDate {
			monDat.date = dat.Dates[i]
		}
		monDat.row[0] = row[0] // close

		if monDat.row[2] < row[2] { // high
			monDat.row[2] = row[2]
		}
		if monDat.row[3] > row[3] { // low
			monDat.row[3] = row[3]
		}
		monDat.row[4] += row[4] // vol
		monDat.row[5] += row[5] //amt
	}

	result.AddRow(monDat.date, monDat.row)

	return result
}

// D1BarToWeek 日线转换成周线
func D1BarToWeek(bar *Bar, isStartDate, isPool, releaseOld bool) Bar {
	dat := d1ToWeek(bar.A, isStartDate, isPool, releaseOld)
	b := NewBar(bar.Code, vars.PW1)
	b.A = dat
	return b
}

// d1ToWeek 日线转周线
func d1ToWeek(dat series.A, isStartDate, ispool, releaseOld bool) series.A {
	l := dat.Len()
	if l == 0 {
		return series.EmptyA
	}

	// result := series.NewEmpty(dat.Name, vars.BarFields)
	var result series.A
	if ispool {
		result = series.GetA(vars.BarFields)
		result.Name = dat.Name
		defer series.PutA(&dat)
	} else {
		result = series.NewEmpty(dat.Name, vars.BarFields, 0, 200)
		if releaseOld {
			defer series.PutA(&dat)
		}
	}
	// close, open, high, low, vol, amt, xd float64
	var week weekrow
	var year, weekno int
	var d time.Time

	week.row = dat.GetRowP()
	defer series.PutRow(week.row)
	row := result.GetRowP()
	defer series.PutRow(row)

	for i := 0; i < l; i++ {
		d = datescore.Score2Date(dat.Dates[i])
		year, weekno = d.ISOWeek()
		// 日期跨周
		if week.year != 0 && (weekno != week.no || year != week.year) {
			// newRow[0], newRow[1], newRow[2], newRow[3], newRow[4], newRow[5], newRow[6] = week.close, week.open, week.high, week.low, week.vol, week.amt, 0
			result.AddRow0(week.date, week.row)
			week.date, week.year, week.no = 0, 0, 0
		}

		dat.RowAt(i, row)
		if week.year == 0 {
			if isStartDate {
				week.date = dat.Dates[i]
			}
			week.row[1] = row[1] // open
			week.row[3] = row[3] // low
			week.year, week.no = d.ISOWeek()
		}
		if !isStartDate {
			week.date = dat.Dates[i]
		}
		week.row[0] = row[0] // close

		if week.row[2] < row[2] { // high
			week.row[2] = row[2]
		}
		if week.row[3] > row[3] { //low
			week.row[3] = row[3]
		}
		week.row[4] += row[4] // vol
		week.row[5] += row[5] //amt
	}

	// newRow = []float64{week.close, week.open, week.high, week.low, week.vol, week.amt, 0}
	// newRow[0], newRow[1], newRow[2], newRow[3], newRow[4], newRow[5], newRow[6] = week.close, week.open, week.high, week.low, week.vol, week.amt, 0
	result.AddRow(week.date, week.row)

	return result
}
