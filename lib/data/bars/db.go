package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"github.com/kere/gno/db"
)

// DB load series base class
type DB struct {
	series.A
	Table    string          `json:"-"`
	Q        db.QueryBuilder `json:"-"`
	StartDay int64           `json:"-"`
	EndDay   int64           `json:"-"` // date score: 1150101
	IsKtDate bool            // 交易师KT的日期模式，开始时间为close
}

// Load func
func (b *DB) Load() error {
	a, err := series.LoadA(b.Q.Limit(0), b.IsKtDate, false)
	if err != nil {
		return err
	}
	b.Fields = a.Fields
	b.Columns = a.Columns
	b.Dates = a.Dates

	return nil
}
