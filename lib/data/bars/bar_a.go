package bars

import (
	"path/filepath"
	"strconv"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/util"
)

// var (
// 	// DefaultPercision expires
// 	DefaultPercision = 4
// )

// Bar price bar base data
type Bar struct {
	DB
	Code   string
	Period string
	IsXDed bool //是否已经进行了除权
}

// NewBar new func
func NewBar(code, period string) Bar {
	d := Bar{Code: code, Period: period}
	d.IsKtDate = vars.IsKtDate

	order := "date"
	var table string
	switch period[0] {
	case 'd', 'w', 'M':
		table = vars.TableBarD1

	case 'm':
		table = vars.TableBarM5
		order = "date, time"
	}

	d.Table = table
	d.Fields = vars.BarFields
	d.Name = "bar:" + code

	// QueryBuilder
	d.Q = db.NewQuery(d.Table)
	d.Q.Order(order).Select("*, 1 as xd")
	// xd: 数据库中没有xd字段，需要添加xd进行占位
	return d
}

// OffsetDate
func (b *Bar) OffsetDate(date int64, n int) (int64, int64) {
	start, end := series.IOffset(date, n, b.Dates)
	if start < 0 {
		return -1, -1
	}
	return b.Dates[start], b.Dates[end]
}

// Copy func
func (b *Bar) Copy() Bar {
	bar := NewBar(b.Code, b.Period)
	s := b.A.Copy()
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	return bar
}

// LoadZip from zip pkg
func (b *Bar) LoadZip() error {
	c := gno.GetConfig().GetConf("data")
	filename := filepath.Join(c.Get("store_dir"), "bar", b.Period, b.Code+".zip")
	if vars.IsTest {
		filename = filepath.Join(c.Get("store_dir"), "test/bar", b.Period, b.Code+".zip")
	}
	dat, err := series.ReadBin(filename, b.StartDay*10000, b.EndDay*10000, false)
	if err != nil {
		return err
	}
	b.Dates = dat.Dates
	b.Columns = dat.Columns

	return nil
}

// Load func
func (b *Bar) Load() error {
	// new bar 函数，已经设置了b.Q.Order
	if b.StartDay > 0 && b.EndDay == 0 {
		b.Q.Where("code=$1 and date>$2", codetool.ToCodeI(b.Code), b.StartDay-1)
	} else if b.StartDay > 0 && b.EndDay > 0 {
		b.Q.Where("code=$1 and date between $2 and $3", codetool.ToCodeI(b.Code), b.StartDay, b.EndDay)
	} else if b.StartDay == 0 && b.EndDay > 0 {
		b.Q.Where("code=$1 and date<$2", codetool.ToCodeI(b.Code), b.EndDay+1)
	} else {
		b.Q.Where("code=$1", codetool.ToCodeI(b.Code))
	}

	isTmp := false
	var dat series.A
	var err error
	if b.Period == vars.PM5 || b.Period == vars.PD1 {
		dat, err = series.LoadA(b.Q.Limit(0), b.IsKtDate, false)
	} else {
		// 如果是M120、M90、D2等周期，则进行Pool内存清理
		dat, err = series.LoadA(b.Q.Limit(0), b.IsKtDate, true)
		isTmp = true
	}
	if err != nil {
		if isTmp {
			series.PutA(&dat)
		}
		return err
	}

	// dat.Name = b.Code
	if dat.Len() == 0 {
		log.App.Notice("[Bar Empty]", b.Code, b.Period, b.StartDay, b.EndDay)
		if isTmp {
			series.PutA(&dat)
		}
		return nil
	}

	var result series.A
	isM2M := b.Period[0] == 'm' && b.Period != vars.PM5
	if isM2M {
		result = M2M(&dat, b.Period, b.IsKtDate, false)
		defer series.PutA(&dat)
	} else {
		result = dat
	}

	// 指数等不需要复权的数据
	if !util.InStrings(b.Code, codetool.CodesIndexs()) && result.Len() > 0 {
		ex := NewExcludDividend(b.Code, &result, b.IsKtDate)
		if err := ex.ExBuild(); err != nil {
			return err
		}
		b.IsXDed = true
	}

	switch b.Period {
	case vars.PMo1:
		// 解析复权好的月线
		result = d1ToMonth(result, b.IsKtDate, false, true)
	case vars.PW1:
		// 解析复权好的周线
		result = d1ToWeek(result, b.IsKtDate, false, true)
	case vars.PD2:
		result = d1ToN(result, 2, b.IsKtDate, false, true)
	case vars.PD3:
		result = d1ToN(result, 3, b.IsKtDate, false, true)
	}

	b.Columns = result.Columns
	b.Dates = result.Dates
	b.Fields = vars.BarFields
	b.Name = b.Code + b.Period

	log.App.Debug("[Bar Loaded]", b.Code, b.Period, b.StartDay, b.EndDay, b.Len())
	return nil
}

// StartDBDate last date
func StartDBDate(code, period string) int64 {
	return getBarLastDate(code, period, 0)
}

// LastDBDate last date
func LastDBDate(code, period string) int64 {
	return getBarLastDate(code, period, 1)
}

// getBarLastDate last date
func getBarLastDate(code, period string, typ int) int64 {
	var table string
	hasTime := false
	if period[0] == 'd' {
		table = vars.TableBarD1
	} else {
		table = vars.TableBarM5
		hasTime = true
	}

	icode := codetool.ToCodeI(code)
	q := db.NewQuery(table)
	q.Where("code=$1", icode).Limit(1)

	if hasTime {
		if typ == 1 {
			q.Order("date desc, time desc")
		} else {
			q.Order("date, time")
		}
		q.Select("date,time")
	} else {
		if typ == 1 {
			q.Order("date desc")
		} else {
			q.Order("date")
		}
		q.Select("date")
	}

	row, _ := q.QueryOne()
	if row.IsEmpty() {
		return 0
	}

	score := row.Int64At(0) * 10000
	if !hasTime && !vars.IsKtDate {
		score += 1500
	} else if hasTime {
		score += row.Int64At(1)
	}
	return score
}

// DnScope 多日线周期，的时间范围
func DnScope(barDn, barD1 *Bar, date int64) (int64, int64) {
	if barDn.Period == vars.PD1 {
		return date / 10000 * 10000, date/10000*10000 + 1500
	}

	n, _ := strconv.ParseInt(barDn.Period[1:], 10, 64)
	// barDn的重新确定日期
	index, stat := barDn.Search(date)
	if barDn.IsKtDate {
		_, dateB := barD1.OffsetDate(barDn.Dates[index], int(n-1))
		return barDn.Dates[index], dateB + 1500
	}
	if stat == series.IndexFound {
		dateA, _ := barD1.OffsetDate(barDn.Dates[index], -int(n-1))
		return dateA / 10000 * 10000, date
	}
	if stat != series.IndexNotFound {
		return -1, -1
	}
	// !isStartDate 条件下
	//如果没有找到date，则，往右一个距离
	index++
	dateA, _ := barD1.OffsetDate(barDn.Dates[index], -int(n-1))
	return dateA / 10000 * 10000, date
}
