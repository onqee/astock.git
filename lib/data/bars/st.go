package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/db"
)

type ST struct {
	Code string
	series.A
}

// NewST new func
func NewST(code string) ST {
	return ST{Code: code}
}

func (st *ST) Load() error {
	codeI := series.ToCodeI(st.Code)
	q := db.NewQuery(vars.TableSTList)
	q.Prepare(true).Select("code,date,stat").Where("code=$1", codeI)
	dat, err := series.LoadA(&q, false, false)
	if err != nil {
		return err
	}
	st.Columns = dat.Columns
	st.Dates = dat.Dates
	st.Sort()
	return nil
}

// IsSTOn 是否是ST股票
func (st *ST) IsSTOn(date int64) bool {
	if st.Len() == 0 {
		return false
	}
	index, _ := st.Search(date)
	return st.Columns[0][index] > 0
}
