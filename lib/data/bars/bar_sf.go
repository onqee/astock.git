package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/util"
)

var (
	// BarFSFields bars
	BarFSFields = []string{"price", "vol"}
)

// BarFS 分时 bar base data
type BarFS struct {
	DB
	IsPool bool
	Code   string `json:"code"`
}

// NewBarFS new func
func NewBarFS(code string) BarFS {
	d := BarFS{Code: code}
	d.StartDay = 1190101

	d.Table = vars.TableBarFS
	d.Fields = BarFSFields
	d.Name = "sf:" + code

	// QueryBuilder
	d.Q = db.NewQuery(d.Table)
	d.Q.Order("date, time")
	// xd: 数据库中没有xd字段，需要添加xd进行占位
	return d
}

// Copy func
func (b *BarFS) Copy() BarFS {
	bar := NewBarFS(b.Code)
	s := b.A.Copy()
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	return bar
}

// Load func
func (b *BarFS) Load() error {
	if b.StartDay > 0 && b.EndDay == 0 {
		b.Q.Where("code=$1 and date>$2", codetool.ToCodeI(b.Code), b.StartDay-1)
	} else if b.StartDay > 0 && b.EndDay > 0 {
		b.Q.Where("code=$1 and date between $2 and $3", codetool.ToCodeI(b.Code), b.StartDay, b.EndDay)
	} else if b.StartDay == 0 && b.EndDay > 0 {
		b.Q.Where("code=$1 and date<$2", codetool.ToCodeI(b.Code), b.EndDay+1)
	} else {
		b.Q.Where("code=$1", codetool.ToCodeI(b.Code))
	}

	dat, err := series.LoadA(b.Q.Limit(0), true, false)
	if err != nil {
		return err
	}

	if dat.Len() == 0 {
		log.App.Notice("[BarFS Empty]", b.Code, b.StartDay, b.EndDay)
		// log.App.Stack()
		return nil
	}

	// arr, dates := b.ParseDateSet(rows)
	// dat := series.NewA(b.Code, dates, arr, BarFSFields)

	// 指数等不需要复权的数据
	if !util.InStrings(b.Code, codetool.CodesIndexs()) && dat.Len() > 0 {
		ex := NewExcludDividend(b.Code, &dat, b.IsKtDate)
		ex.ExBuild()
	}

	b.Columns = dat.Columns
	b.Dates = dat.Dates
	b.Fields = dat.Fields
	b.Name = b.Code

	log.App.Info("[BarFS Loaded]", b.Code, b.StartDay, b.EndDay)
	return nil
}
