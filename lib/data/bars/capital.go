package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
)

// Capital 股本变动
type Capital struct {
	DB
	Code string
}

// NewCapital new func
func NewCapital(code string) Capital {
	d := Capital{Code: code}
	d.IsKtDate = false

	d.Table = vars.TableCapital
	d.Fields = vars.CapitalFields
	// QueryBuilder
	d.Q = db.NewQuery(d.Table)
	d.Q.Order("date").Where("code=$1", codetool.ToCodeI(code))
	return d
}

const (
	fieldTradable = "tradable"
	fieldCapital  = "capital"
)

// TradableOn 流通股
func (c *Capital) TradableOn(date int64) int64 {
	if c.Len() == 0 {
		return -1
	}
	i, stat := c.Search(date)
	if stat == series.IndexOverRangeLeft {
		return -1
	}

	v := c.ValueAt(fieldTradable, i)

	return int64(v)
}

// CapitalOn 总股本
func (c *Capital) CapitalOn(date int64) int64 {
	if c.Len() == 0 {
		return -1
	}

	i, stat := c.Search(date)
	if stat == series.IndexOverRangeLeft {
		return -1
	}

	v := c.ValueAt(fieldCapital, i)

	return int64(v)
}

// Turnover 换手率%，vol为手=100股
func (c *Capital) Turnover(date int64, vol float64) float64 {
	if c.Len() == 0 {
		return -1
	}
	i, stat := c.Search(date)
	if stat == series.IndexOverRangeLeft {
		return -1
	}

	v := c.ValueAt(fieldTradable, i)

	return 10000 * vol / v
}

// // CapitalRank 股本排名
// func (c *Capital) CapitalRank() []float64 {
// 	db.Current().Query("select code,date,values from (select code, date, values, ROW_NUMBER() over(partition by code order by date desc) as rowid from capital) as a where rowid=1;")
// }
