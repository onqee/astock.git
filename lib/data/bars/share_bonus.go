package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
)

// ShareBonus class
type ShareBonus struct {
	DB
	Code string
}

// NewShareBonus new
func NewShareBonus(code string, isStartDate bool) *ShareBonus {
	d := &ShareBonus{Code: code}
	d.IsKtDate = isStartDate
	d.Name = "sharebonus:" + code
	d.Table = vars.TableShareBonus
	// d.StartDay = vars.DefaultStartDay
	// d.EndDay = vars.DefaultEndDay

	d.Q = db.NewQuery(vars.TableShareBonus)
	d.Q.Order("date")
	return d
}

// Load func
func (b *ShareBonus) Load(isPool bool) error {
	// 不设置结束日期，因为除权日不确定
	b.Q.Where("code=$1", codetool.ToCodeI(b.Code))
	a, err := series.LoadA(&b.Q, true, isPool)
	if err != nil {
		return err
	}
	b.Dates = a.Dates
	b.Columns = a.Columns
	b.Fields = a.Fields

	return nil
}
