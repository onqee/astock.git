package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
)

// RangeAm return 上午
func (b *Bar) RangeAm(day int64) Bar {
	d, _ := datescore.ScoreSplit(day)
	return b.Range(d*10000, d*10000+1159)
}

// RangePm return 下午
func (b *Bar) RangePm(day int64) Bar {
	d, _ := datescore.ScoreSplit(day)
	return b.Range(d*10000+1159, d*10000+2359)
}

// RangeDay get data range by day
func (b *Bar) RangeDay(date int64) Bar {
	d, _ := datescore.ScoreSplit(date)
	return b.Range(d*10000, d*10000+2359)
}

// Range return index
func (b *Bar) Range(dateA, dateB int64) Bar {
	s := b.A.Range(dateA, dateB)
	bar := NewBar(b.Code, b.Period)
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	bar.Name = b.Name
	return bar
}

// IRange get data range
func (b *Bar) IRange(startI, endI int) Bar {
	s := b.A.IRange(startI, endI)
	bar := NewBar(b.Code, b.Period)
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	bar.Name = b.Name
	return bar
}

// RangeR get 右侧n个周期的数据
func (b *Bar) RangeR(date int64, n int) Bar {
	if date < 0 {
		date = b.Dates[0]
	}
	bar := NewBar(b.Code, b.Period)
	s := b.A.RangeR(date, n)
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	bar.Name = b.Name
	return bar
}

// RangeL get 左侧n个周期的数据
func (b *Bar) RangeL(date int64, n int) Bar {
	if date < 0 {
		date = b.LastDate()
	}
	bar := NewBar(b.Code, b.Period)
	s := b.A.RangeL(date, n)
	bar.Dates = s.Dates
	bar.Columns = s.Columns
	bar.Name = b.Name
	return bar
}

// AlignBE 首尾数据对齐，中间数据不做处理
func AlignBE(arr ...*Bar) []Bar {
	n := len(arr)

	var b, e, last int64

	for i := 0; i < n; i++ {
		if arr[i].Len() == 0 {
			return nil
		}

		if b < arr[i].Dates[0] {
			b = arr[i].Dates[0]
		}

		last = arr[i].LastDate()
		if arr[i].Period[0] != 'm' { // 不是分钟线
			last = arr[i].LastDate() + 2400
		}

		if i == 0 {
			e = last
			continue
		}
		if e > last {
			e = last
		}
	}

	result := make([]Bar, n)
	for i := 0; i < n; i++ {
		result[i] = arr[i].Range(b, e)
	}
	return result
}

// InnerCutP 两个数据的交集
// 只保存两个时间相等的数据
// 返回Pool的series.A
func InnerCutP(arr ...*Bar) []Bar {
	n := len(arr)
	list := make([]*series.A, n)
	result := make([]Bar, n)
	for i := 0; i < n; i++ {
		list[i] = &arr[i].A
		result[i] = NewBar(arr[i].Code, arr[i].Period)
	}
	tmp := series.InnerCutP(list...)

	for i := 0; i < n; i++ {
		result[i].A = tmp[i]
	}
	return result
}

// // AlignInner 对齐交集
// // return result, 去除数据的百分比
// func AlignInner(arr ...*Bar) ([]Bar, float64) {
// 	n := len(arr)
// 	arrS := make([]*series.A, n)
// 	for i := 0; i < n; i++ {
// 		arrS[i] = &arr[i].A
// 	}
//
// 	tmp, perc := series.AlignInner(arrS...)
// 	if len(tmp) != n {
// 		return nil, perc
// 	}
//
// 	result := make([]Bar, n)
// 	for i := 0; i < n; i++ {
// 		bar := NewBar(arr[i].Code, arr[i].Period)
// 		bar.A = tmp[i]
// 		result[i] = bar
// 	}
// 	return result, perc
// }
