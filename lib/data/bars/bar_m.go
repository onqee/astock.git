package bars

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
)

// M2M 分钟线合成更大周期数据
// n is cap
func M2M(sm *series.A, period string, isStartDate, isPool bool) series.A {
	// timeRange [][2]int64, n int
	var n int
	var timeRange [][2]int64
	switch period {
	case vars.PM120:
		n = 2
		timeRange = vars.TimeEM120
	case vars.PM90:
		n = 4
		timeRange = vars.TimeEM90
	case vars.PM60:
		n = 4
		timeRange = vars.TimeEM60
	case vars.PM30:
		n = 8
		timeRange = vars.TimeEM30
	case vars.PM15:
		n = 16
		timeRange = vars.TimeEM15
	}

	l := sm.Len()
	if l == 0 || l < n {
		return series.EmptyA
	}

	var dates []int64
	var cols series.Columns
	if isPool {
		datC := series.GetA(vars.BarFields)
		dates = datC.Dates
		cols = datC.Columns
		// defer series.PutA(sm)
	} else {
		m := l*n/48 + 20 // l*n/48 cap
		dates = make([]int64, 0, m)
		cols = series.NewColumnsN(len(vars.BarFields), 0, m)
	}

	d, t := datescore.ScoreSplit(sm.Dates[0])
	barobj := newBarComb(d, t, timeRange, isStartDate)

	row := sm.GetRowP()
	defer series.PutRow(row)

	for i := 0; i < l; i++ {
		d, t = datescore.ScoreSplit(sm.Dates[i])

		if barobj.IsThis(d, t) {
			sm.RowAt(i, row)
			barobj.Calc(row)
			continue
		}

		date := barobj.SetResult(row)
		cols.AddRow(row)
		dates = append(dates, date)
		barobj.Reset(d, t, timeRange)

		sm.RowAt(i, row)
		barobj.Calc(row)
	}

	// 最后一根
	date := barobj.SetResult(row)
	cols.AddRow(row)
	dates = append(dates, date)

	return series.NewA(sm.Name, dates, cols, vars.BarFields)
}

// barComb 5分钟数据生成其他周期数据
type barComb struct {
	Date, Time                       int64
	LeftTime, RightTime              int64
	Close, Open, High, Low, Vol, Amt float64
	isStartDate                      bool
}

func newBarComb(d, t int64, timeRange [][2]int64, isStartDate bool) *barComb {
	// s := series.NewEmpty("", vars.BarFields)
	// dat := series.GetA(vars.BarFields)
	b := &barComb{
		Date:        d,
		isStartDate: isStartDate,
	}
	b.Reset(d, t, timeRange)
	return b
}

// Reset clear
func (b *barComb) Reset(d, t int64, timeRange [][2]int64) {
	b.Date = d
	b.Open, b.Close, b.High, b.Low, b.Vol, b.Amt = 0, 0, 0, 0, 0, 0

	for _, arr := range timeRange {
		if b.isStartDate {
			if !(arr[0] <= t && t < arr[1]) {
				continue
			}
		} else {
			if !(arr[0] < t && t <= arr[1]) {
				continue
			}
		}

		b.LeftTime = arr[0]
		b.RightTime = arr[1]
		break
	}

	if b.isStartDate {
		b.Time = b.LeftTime
	} else {
		b.Time = b.RightTime
	}
}

// IsThis 是不是数据更大的周期
func (b *barComb) IsThis(d, t int64) bool {
	if b.isStartDate {
		return b.Date == d && (b.LeftTime <= t && t < b.RightTime)
	}
	return b.Date == d && (b.LeftTime < t && t <= b.RightTime)
}

// // Add 添加数据
// func (b *barComb) Add(date int64, src []float64) {
// 	b.Dat.AddRow(date, src)
// }

// SetResult 设置合并后的结果
func (b *barComb) SetResult(src []float64) int64 {
	src[0] = b.Close
	src[1] = b.Open
	src[2] = b.High
	src[3] = b.Low
	src[4] = b.Vol
	src[5] = b.Amt
	src[6] = 1
	return b.Date*10000 + b.Time
}

// Calc 计算high,low
func (b *barComb) Calc(src []float64) {
	// "close", "open", "high", "low", "vol", "amt", "xd"
	b.Close = src[0]
	if b.Open == 0 {
		b.Open = src[1]
		b.Low = src[3]
	}
	if b.High < src[2] {
		b.High = src[2]
	}
	if b.Low > src[3] {
		b.Low = src[3]
	}
	b.Vol = +src[4]
	b.Amt = +src[5]
}
