package bars

// func TestCapital(t *testing.T) {
// 	code := "sh600584"
// 	dat := NewCapital(code)
// 	err := dat.Load()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	i, stat := dat.Search(11905060000)
// 	if stat != series.IndexOverRangeRight {
// 		series.Print(&dat.A)
// 		t.Fatal(i)
// 	}
//
// 	i, stat = dat.Search(11809210000)
// 	if stat != series.IndexNotFound || dat.Dates[i] != 11808300000 {
// 		series.Print(&dat.A)
// 		t.Fatal(i)
// 	}
//
// 	i, stat = dat.Search(11807210000)
// 	if stat != series.IndexNotFound || dat.Dates[i] != 11706190000 {
// 		series.Print(&dat.A)
// 		t.Fatal(i)
// 	}
//
// 	code = "sz000938"
// 	capital := NewCapital(code)
// 	err = capital.Load()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	bar := NewBar(code, vars.PD1)
// 	bar.IsKtDate = true
// 	err = bar.Load()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	date := int64(11908020000)
// 	vol, _ := bar.ValueOn(vars.FieldVol, date)
// 	r := capital.Turnover(date, vol)
// 	if math.Abs(r-1.4095) > 0.0001 {
// 		t.Fatal(r)
// 	}
//
// 	code = "sz300748" //金力永磁
// 	capital = NewCapital(code)
// 	capital.Load()
//
// 	bar = NewBar(code, vars.PD1)
// 	bar.IsKtDate = true
// 	bar.Load()
// 	vol, _ = bar.ValueOn(vars.FieldVol, date)
// 	r = capital.Turnover(date, vol)
// 	if math.Abs(r-37.5096) > 0.0001 {
// 		t.Fatal(r)
// 	}
// }
