package bars

import (
	"sort"
)

// // Values0 close, open, high, low / 4
// func (bar *Bar) Values0() []float64 {
// 	l := bar.Len()
// 	closeI := bar.FieldI(vars.FieldClose)
// 	openI := bar.FieldI(vars.FieldOpen)
// 	highI := bar.FieldI(vars.FieldHigh)
// 	lowI := bar.FieldI(vars.FieldLow)
// 	arr := make([]float64, l)
// 	for i := 0; i < l; i++ {
// 		row := bar.Rows[i]
// 		arr[i] = (row[closeI] + row[openI] + row[highI] + row[lowI]) / 4
// 	}
// 	return arr
// }

// MinMaxScaler 归一化
func (bar *Bar) MinMaxScaler(field string) []float64 {
	l := bar.Len()
	vals := bar.Values(field)
	if l < 2 {
		return vals
	}

	arr := make([]float64, l)
	copy(arr, vals)
	sort.Float64s(arr)
	min := arr[0]
	max := arr[l-1]

	for i := 0; i < l; i++ {
		arr[i] = (vals[i] - min) / (max - min)
	}

	return arr
}
