package bars

import (
	"fmt"
	"math"
	"path/filepath"
	"testing"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../../app.conf")
	vars.IsTest = true
}

func TestShareBonus(t *testing.T) {
	dat := NewShareBonus("sz000938", true)
	if err := dat.Load(false); err != nil {
		t.Fatal(err)
	}
	if dat.Len() == 0 {
		t.Fatal()
	}
}

func TestSZ002185(t *testing.T) {
	code := "sz002185"
	period := "d1"
	bar := NewBar(code, period)
	bar.StartDay = 1190101
	bar.EndDay = 1190714
	bar.IsKtDate = true
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}
	v, _ := bar.ValueOn(vars.FieldClose, 11907010000)
	if math.Abs(v-4.7) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11907020000)
	if math.Abs(v-4.56) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11907110000)
	if math.Abs(v-4.46) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}

}
func Test_399001(t *testing.T) {
	code := "sz399001"
	period := "d1"
	bar := NewBar(code, period)
	bar.StartDay = 1180501
	bar.EndDay = 1191231
	bar.IsKtDate = true
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	row := bar.RowAtP(bar.Len() - 1)
	if math.Abs(row[4]-2826545) > 0.001 || math.Abs(row[5]-30589604) > 0.001 {
		fmt.Println(bar.LastDate(), row) // 11912311300
		t.Fatal()
	}
	series.PutRow(row)

	dateA, dateB := bar.OffsetDate(11909100000, 5)
	if dateB != 11909180000 {
		series.Print(&bar.A, 11909010000, 11909200000)
		t.Fatal(dateA, dateB)
	}
	dateA, dateB = bar.OffsetDate(11909100000, -5)
	if dateA != 11909030000 {
		t.Fatal(dateA, dateB)
	}
	dateA, dateB = bar.OffsetDate(11912120000, -1)
	if dateA != 11912110000 {
		series.Print(&bar.A)
		t.Fatal(dateA, dateB)
	}

	bar = NewBar(code, period)
	bar.StartDay = 1190101
	bar.EndDay = 1191231
	bar.Load()
	_, date := bar.OffsetDate(11909101500, 5)
	if date != 11909181500 {
		series.Print(&bar.A)
		t.Fatal(date)
	}
	date, _ = bar.OffsetDate(11909101500, -5)
	if date != 11909031500 {
		t.Fatal(date)
	}
	date, _ = bar.OffsetDate(11909101500, -1)
	if date != 11909091500 {
		t.Fatal(date)
	}

	bar = NewBar(code, vars.PM120)
	bar.StartDay = 1191101
	bar.EndDay = 1191231
	bar.IsKtDate = true
	bar.Load()

	bar0 := bar.Range(11912231300, 11912231300)
	if bar0.Dates[0] != 11912231300 {
		series.Print(&bar0.A)
		t.Fatal()
	}
	bar0 = bar.RangeL(11912311500, 1)
	if bar0.Dates[0] != 11912310930 || bar0.Dates[1] != 11912311300 {
		series.Print(&bar0.A)
		t.Fatal()
	}
	date, _ = bar.OffsetDate(11912310930, -1)
	if date != 11912301300 {
		t.Fatal(date)
	}
	date, _ = bar.OffsetDate(11912310930, -3)
	if date != 11912271300 {
		series.Print(&bar.A, 11912230000, 11912300000)
		t.Fatal(date)
	}
	_, date = bar.OffsetDate(11912271300, 3)
	if date != 11912310930 {
		t.Fatal(date)
	}
	date, _ = bar.OffsetDate(11912271300, 0)
	if date != 11912271300 {
		t.Fatal(date)
	}

}

func Test_D1(t *testing.T) {
	code := "sz000012"
	period := "d1"
	bar := NewBar(code, period)
	bar.StartDay = 1160101
	bar.EndDay = 1180805
	bar.IsKtDate = true
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	v, _ := bar.ValueOn(vars.FieldClose, 11806280000)
	if math.Abs(v-4.83) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 118062260000)
	if math.Abs(v-4.94) > 0.001 {
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11707180000)
	if math.Abs(v-7.11) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11707190000)
	if v-7.6 > 0.001 {
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11605190000)
	if v-7.89 > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11605180000)
	if v-7.7 > 0.001 {
		t.Fatal(v)
	}

	bar = NewBar(code, period)
	bar.StartDay = 1160101
	bar.EndDay = 1180805
	bar.IsKtDate = false
	bar.Load()

	v, _ = bar.ValueOn(vars.FieldClose, 11806281500)
	if math.Abs(v-4.83) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 118062261500)
	if math.Abs(v-4.94) > 0.001 {
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11707181500)
	if math.Abs(v-7.11) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11707191500)
	if v-7.6 > 0.001 {
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11605191500)
	if v-7.89 > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11605181500)
	if v-7.7 > 0.001 {
		t.Fatal(v)
	}
}

func Test_M5(t *testing.T) {
	code := "sz000012"
	period := "m5"
	bar := NewBar(code, period)
	bar.EndDay = 1180702
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	v, _ := bar.ValueOn("close", 11806271015)
	if v-5 > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn("close", 11806261450)
	if v-4.96 > 0.001 {
		// dat := bar.A.Range(11806260000, 11806261599)
		// series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn("close", 11707190950)
	if v-7.33 > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn("close", 11707171405)
	if v-7.09 > 0.001 {
		t.Fatal(v)
	}

	code = "sh600000"
	period = "m120"
	bar = NewBar(code, period)
	bar.StartDay = 1191101
	bar.EndDay = 1191224
	bar.IsKtDate = true
	err = bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	l := bar.Len()
	if bar.LastDate() != 11912241300 || bar.Dates[l-2] != 11912240930 {
		// series.Print(&bar.A)
		t.Fatal()
	}

	if bar.Dates[0] != 11911010930 || bar.Dates[1] != 11911011300 {
		// series.Print(&bar.A)
		t.Fatal()
	}

	bar = NewBar(code, period)
	bar.StartDay = 1191101
	bar.EndDay = 1191224
	bar.Load()

	l = bar.Len()
	if bar.LastDate() != 11912241500 || bar.Dates[l-2] != 11912241130 {
		series.Print(&bar.A)
		t.Fatal()
	}

	if bar.Dates[0] != 11911011130 || bar.Dates[1] != 11911011500 {
		// series.Print(&bar.A)
		t.Fatal()
	}
}

func Test_600054(t *testing.T) {
	code := "sh600054"
	period := "d1"
	bar := NewBar(code, period)
	bar.EndDay = 1180702
	bar.IsKtDate = true
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	l := bar.Len()
	row := bar.GetRowP()
	for i := 0; i < l; i++ {
		bar.RowAt(i, row)

		if len(row) == 0 {
			t.Fatal("row is empty at", bar.Dates[i])
		}
	}

	// bar.Print()
	v, _ := bar.ValueOn("close", 11806290000)
	if v-11.41 > 0.001 {
		t.Fatal("11.41")
	}
	v, _ = bar.ValueOn("close", 11706290000)
	if v-16.96 > 0.001 {
		t.Fatal("16.96")
	}
	v, _ = bar.ValueOn("close", 11706230000)
	if v-16.57 > 0.001 {
		t.Fatal("16.57")
	}
	v, _ = bar.ValueOn("close", 11606240000)
	if v-15.75 > 0.001 {
		t.Fatal("15.75")
	}
	bar = NewBar(code, period)
	bar.EndDay = 1180702
	bar.Load()
	v, _ = bar.ValueOn("close", 11806291500)
	if v-11.41 > 0.001 {
		t.Fatal("11.41")
	}
	v, _ = bar.ValueOn("close", 11706291500)
	if v-16.96 > 0.001 {
		t.Fatal("16.96")
	}
	v, _ = bar.ValueOn("close", 11706231500)
	if v-16.57 > 0.001 {
		t.Fatal("16.57")
	}
	v, _ = bar.ValueOn("close", 11606241500)
	if v-15.75 > 0.001 {
		t.Fatal("15.75")
	}
}

func Test_D2(t *testing.T) {
	code := vars.Code399001
	period := vars.PD2
	bar := NewBar(code, period)
	bar.IsKtDate = true
	bar.StartDay = 1160101
	bar.EndDay = 1180705
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	l := bar.Len()
	row := bar.GetRowP()

	bar.RowAt(l-1, row)
	if bar.LastDate() != 11807040000 || math.Abs(row[0]-8862) > 0.5 || math.Abs(row[1]-9182.96) > 0.5 {
		// series.Print(&bar.A)
		t.Fatal(bar.LastDate(), row)
	}

	bar.RowAt(l-2, row)
	if bar.Dates[l-2] != 11807020000 || math.Abs(row[0]-9221.54) > 0.5 || math.Abs(row[1]-9369.24) > 0.5 {
		t.Fatal(bar.Dates[l-2], row)
	}

	bar = NewBar(code, period)
	bar.StartDay = 1160101
	bar.EndDay = 1180705
	bar.Load()
	bar.RowAt(l-1, row)
	if bar.LastDate() != 11807051500 || math.Abs(row[0]-8862) > 0.5 || math.Abs(row[1]-9182.96) > 0.5 {
		series.Print(&bar.A)
		t.Fatal(bar.LastDate(), row)
	}

	bar.RowAt(l-2, row)
	if bar.Dates[l-2] != 11807031500 || math.Abs(row[0]-9221.54) > 0.5 || math.Abs(row[1]-9369.24) > 0.5 {
		series.Print(&bar.A)
		t.Fatal(bar.Dates[l-2], row)
	}

	bar = NewBar(code, period)
	bar.StartDay = 1160101
	bar.EndDay = 1180704
	err = bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	l = bar.Len()

	if bar.LastDate() != 11807041500 {
		t.Fatal(bar.LastDate())
	}
	if bar.Dates[l-2] != 11807021500 {
		t.Fatal(bar.Dates[l-2])
	}

	row = bar.GetRowP()
	bar.RowAt(l-1, row)
	if math.Abs(row.ValueOf(vars.FieldHigh, bar.Fields))-9233 > 0.5 {
		t.Fatal(row)
	}
	// bar.RowAt(l-1, row)
	if math.Abs(row.ValueOf(vars.FieldLow, bar.Fields))-8945 > 0.5 {
		t.Fatal(row)
	}
	// bar.Print()

	bar = NewBar(code, vars.PD1)
	bar.StartDay = 1190701
	bar.EndDay = 1190801
	bar.Load()
	v, _ := bar.ValueOn(vars.FieldClose, 11907151500)
	if math.Abs(v-9309.42) > 0.5 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}

	bar = NewBar(vars.Code399001, vars.PD2)
	bar.StartDay = 1190705
	bar.EndDay = 1191231
	bar.Load()
	// last: 12y30-12y31
	if bar.LastDate() != 11912311500 {
		t.Fatal(bar.LastDate())
	}
	if bar.Dates[bar.Len()-2] != 11912271500 {
		t.Fatal(bar.LastDate())
	}
}

func Test_000938_DWM(t *testing.T) {
	code := "sz000938"
	period := "d1"
	bar := NewBar(code, period)
	bar.StartDay = 1190409
	bar.EndDay = 1190701
	// bar.IsKtDate = true
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}
	// 1190527复权
	v, _ := bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}

	c := gno.GetConfig().GetConf("data")
	folder := filepath.Join(c.Get("store_dir"), "test/bar", period)
	filename := folder + "/" + code + ".zip"
	series.WriteBin(&bar.A, filename, 4)

	dat, _ := series.ReadBin(filename, 0, 0, false)
	v, _ = dat.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		t.Fatal(v)
	}

	// series.Print(&bar.A)

	period = "w1"
	bar = NewBar(code, period)
	bar.StartDay = 1170707
	bar.EndDay = 1180707
	err = bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	if bar.Len() == 0 {
		t.Fatal()
	}

	row := bar.GetRowP()
	bar.RowAt(bar.Len()-1, row)
	if math.Abs(row[0]-42.41) > 0.01 {
		t.Fatal(row)
	}
	index, _ := bar.Search(11802141500)
	bar.RowAt(index, row)
	if math.Abs(row[0]-40.49) > 0.01 {
		series.Print(&bar.A)
		t.Fatal(row)
	}
	index, _ = bar.Search(11804271500)
	bar.RowAt(index, row)
	if math.Abs(row[0]-51.28) > 0.01 {
		t.Fatal(row)
	}

	bar = NewBar(code, vars.PM30)
	bar.StartDay = 1190417
	bar.EndDay = 1190701
	bar.IsKtDate = true
	bar.Load()
	// 1190524 - 1190527复权

	v, _ = bar.ValueOn(vars.FieldClose, 11905271430)
	if math.Abs(v-28.03) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905271400)
	if math.Abs(v-28.09) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905270930)
	if math.Abs(v-27.00) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241300)
	if math.Abs(v-26.79) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241430)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}

	bar = NewBar(code, vars.PM30)
	bar.StartDay = 1190417
	bar.EndDay = 1190701
	bar.Load()
	// 1190524 - 1190527复权

	v, _ = bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905271430)
	if math.Abs(v-28.09) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905271000)
	if math.Abs(v-27.00) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241330)
	if math.Abs(v-26.79) > 0.001 {
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}

	dat = M2M(&bar.A, vars.PM60, false, false)
	v, _ = dat.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldClose, 11905271130)
	if math.Abs(v-27.25) > 0.001 {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldClose, 11905241400)
	if math.Abs(v-26.58) > 0.001 {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldClose, 11905241030)
	if math.Abs(v-26.86) > 0.001 {
		t.Fatal(v)
	}

	period = vars.PM120
	bar = NewBar(code, period)
	bar.StartDay = 1191111
	bar.EndDay = 1191112
	bar.Load()
	v = bar.ValueAt(vars.FieldHigh, 0)
	if v != 28.82 {
		t.Fatal(v)
	}
	v = bar.ValueAt(vars.FieldLow, 0)
	if v != 27.98 {
		t.Fatal(v)
	}
	v = bar.ValueAt(vars.FieldLow, 1)
	if v != 27.99 {
		t.Fatal(v)
	}
	bar120 := bar.RangeDay(11911110000)
	if bar120.Len() != 2 {
		series.Print(&bar120.A)
		t.Fatal(bar120.Len())
	}

	bar = NewBar(code, vars.PMo1)
	bar.StartDay = 1180101
	bar.EndDay = 1191231
	bar.IsKtDate = true
	bar.Load()
	v, _ = bar.ValueOn(vars.FieldClose, 11901310000)
	if math.Abs(v-21.8) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11913020000)
	if math.Abs(v-31.6) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}

	bar = NewBar(code, vars.PMo1)
	bar.StartDay = 1180101
	bar.EndDay = 1191231
	bar.Load()
	v, _ = bar.ValueOn(vars.FieldClose, 11901311500)
	if math.Abs(v-21.8) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11913021500)
	if math.Abs(v-31.6) > 0.001 {
		// series.Print(&bar.A)
		t.Fatal(v)
	}
}

func Test_603693(t *testing.T) {
	// 会引起除权错误
	// 除权日期比最大数据日期晚2天
	code := "sh603693"
	period := "d1"
	bar := NewBar(code, period)
	bar.EndDay = 1181024
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}
}

func Test_600136(t *testing.T) {
	code := "sh600136"
	// m5分钟线，丢失数据，没有1140808:1130造成m30合成出现两个1130的错误
	bar := NewBar(code, vars.PM30)
	bar.StartDay = 1140808
	bar.EndDay = 1140813
	bar.Load()
	row := bar.GetRowP()
	defer series.PutRow(row)

	// tdx以结束时间为每个数据的时间date
	// 如果是从5分钟数据开始合并
	// 5分钟数据规则出错，11408081130 没有11：30数据，而是13:00的数据
	bar.RowOn(11408081130, row)
	_, stat := bar.Search(11408081300)

	if row[0] != 15.6 || stat != series.IndexNotFound {
		series.Print(&bar.A)
		t.Fatal()
	}
}

func Test_Align(t *testing.T) {
	code := "sz000938"
	period := vars.PD1
	bar1 := NewBar(code, period)
	bar1.StartDay = 1181115
	bar1.EndDay = 1181215
	err := bar1.Load()
	if err != nil {
		t.Fatal(err)
	}

	code = "sz399001"
	bar2 := NewBar(code, period)
	bar2.StartDay = 1181101
	bar2.EndDay = 1181231
	err = bar2.Load()
	if err != nil {
		t.Fatal(err)
	}

	code = "sh000300"
	period = vars.PM5
	bar1 = NewBar(code, period)
	bar1.StartDay = 1190301
	bar1.EndDay = 1190318
	err = bar1.Load()
	if err != nil {
		t.Fatal(err)
	}

	code = "sz300667" // 1190311 开盘
	bar2 = NewBar(code, period)
	bar2.StartDay = 1190301
	bar2.EndDay = 1190320
	err = bar2.Load()
	if err != nil {
		t.Fatal(err)
	}

}

func Test_AlignBE(t *testing.T) {
	code := "sz000938"
	period := vars.PD1
	bar1 := NewBar(code, period)
	bar1.StartDay = 1190315
	bar1.EndDay = 1190318
	bar1.IsKtDate = true
	bar1.Load()

	period = vars.PM5
	m5 := NewBar(code, period)
	m5.StartDay = 1190301
	m5.EndDay = 1190320
	m5.IsKtDate = true
	m5.Load()

	code = "sz399001"
	m5x := NewBar(code, period)
	m5x.StartDay = 1190301
	m5x.EndDay = 1190320
	m5x.IsKtDate = true
	m5x.Load()

	arr := AlignBE(&bar1, &m5, &m5x)
	if arr[0].Dates[0] != 11903150000 || arr[0].LastDate() != 11903180000 {
		series.Print(&arr[0].A)
		t.Fatal()
	}
	if arr[1].Dates[0] != 11903150930 || arr[1].LastDate() != 11903181455 {
		t.Fatal(bar1.Dates[0], arr[1].Dates[0], arr[1].LastDate())
	}
	if arr[2].Dates[0] != 11903150930 || arr[2].LastDate() != 11903181455 {
		t.Fatal()
	}
}

func TestDnScope(t *testing.T) {
	bar := NewBar(vars.Code399001, vars.PD1)
	bar.StartDay = 1190101
	bar.EndDay = 1191231
	bar.Load()
	barD2 := NewBar(vars.Code399001, vars.PD2)
	barD2.StartDay = 1190101
	barD2.EndDay = 1191231
	barD2.Load()

	if barD2.LastDate() != 11912311500 {
		series.Print(&barD2.A)
		t.Fatal()
	}
	d1, d2 := DnScope(&barD2, &bar, 11912311500)
	if d1 != 11912300000 || d2 != 11912311500 {
		t.Fatal(d1, d2)
	}
	// 1191220-1191223组成barD2 星期五+下周一
	d1, d2 = DnScope(&barD2, &bar, 11912201500)
	if d1 != 11912200000 || d2 != 11912201500 {
		series.Print(&bar.A, 11912010000, 11912320000)
		series.Print(&barD2.A, 11912010000, 11912320000)
		t.Fatal(d1, d2)
	}
	// 1191220-1191223组成barD2 星期五+下周一
	d1, d2 = DnScope(&barD2, &bar, 11912231500)
	if d1 != 11912200000 || d2 != 11912231500 {
		t.Fatal(d1, d2)
	}

	bar3 := D1BarToN(&bar, vars.PD3, false, false, true)
	if bar3.LastDate() != 11912311500 {
		series.Print(&bar3.A)
		t.Fatal()
	}
}

func TestST(t *testing.T) {
	st := NewST("sh603353")
	err := st.Load()
	if err != nil {
		t.Fatal(err)
	}
	v := st.IsSTOn(12003051300)
	if v {
		series.Print(&st.A)
		t.Fatal(v)
	}
	st = NewST("sh600836")
	err = st.Load()
	if err != nil {
		t.Fatal(err)
	}
	v = st.IsSTOn(12005011300)
	if !v {
		series.Print(&st.A)
		t.Fatal(v)
	}
}
