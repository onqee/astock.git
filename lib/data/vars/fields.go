package vars

var (
	// BarFields bars
	// xd 复权系数; vol=单位手; amt=单位万
	BarFields = []string{"close", "open", "high", "low", "vol", "amt", "xd"}
	// RationedShareFields 除权信息表
	RationedShareFields = []string{"price", "ration", "vol"}
	// CapitalFields 股本
	CapitalFields = []string{"tradable", "untradable", "capital"}
)

const (
	// FieldName name
	FieldName = "name"
	// FieldDate date
	FieldDate = "date"
	// FieldData data
	FieldData = "data"

	// FieldClose close
	FieldClose = "close"
	// FieldHigh High
	FieldHigh = "high"
	// FieldLow low
	FieldLow = "low"
	// FieldOpen open
	FieldOpen = "open"

	// FieldVol vol
	FieldVol = "vol"
	// FieldAmt amt
	FieldAmt = "amt"
	// FieldTurnOver turn
	FieldTurnOver = "turn"
	// FieldTime time
	FieldTime = "time"

	// FieldCapital capital
	FieldCapital = "capital"
	// FieldTradable tradable
	FieldTradable = "tradable"
	// FieldUntradable untradable
	FieldUntradable = "untradable"

	// FieldPercent percent
	FieldPercent = "percent"

	// FieldVal val
	FieldVal = "val"

	// FieldVal1 val1
	FieldVal1 = "val1"

	// FieldVal2 val2
	FieldVal2 = "val2"

	// FieldCode code
	FieldCode = "code"

	// Fieldw1 w1
	Fieldw1 = "w1"
	// Fieldw2 w2
	Fieldw2 = "w2"
	// FieldW2 W2
	FieldW2 = "W2"
	// Fieldw3 w3
	Fieldw3 = "w3"
	// FieldW3 W3
	FieldW3 = "W3"
)
