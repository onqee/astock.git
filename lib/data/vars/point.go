package vars

const (
	// Win 获胜
	Win = 1
	// Loss 失败
	Loss = -1

	// PositionLow 低点
	PositionLow = -1 // position low
	// PositionHigh 高点
	PositionHigh = 1 // position High

	// ActionNone 什么都不做
	ActionNone = 0
	// ActionBuy 买点
	ActionBuy = PositionLow // position low
	// ActionSell 卖点
	ActionSell = PositionHigh // position High

	// ActionNameBuy 买点
	ActionNameBuy = "buy"
	// ActionNameSell 买点
	ActionNameSell = "sell"
)
