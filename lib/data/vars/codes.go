package vars

const (
	// MarketSH sh
	MarketSH = "sh"
	// MarketSZ sh
	MarketSZ = "sz"
	// Code000001 1
	Code000001 = "sh000001"
	// Code399001 1
	Code399001 = "sz399001"
	// Code399005 1
	Code399005 = "sz399005"
	// Code399006 1
	Code399006 = "sz399006"
	// Code000300 1
	Code000300 = "sh000300"
)
