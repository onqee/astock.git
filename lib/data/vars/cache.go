package vars

const (
	// CacheKeyCodeName 代码-名称的缓存
	CacheKeyCodeName = "h-code:name"

	// CacheKeyCodeStkBlk 代码-股票板块名称的缓存
	CacheKeyCodeStkBlk = "h-code:stkblk"

	// CacheKeyIndustryName 名称的缓存
	CacheKeyIndustryName = "h-code:industry-name"

	// CacheKeyCode 名称的缓存
	CacheKeyCode = "h-code:industry"
)
