package vars

import (
	"errors"

	"github.com/kere/gno"
)

func InitProductMode() {
	c := gno.GetConfig().GetConf("data")
	ProcessNum = c.DefaultInt("process_num", 100)
	IsTest = false
	IsSync = false
}

var (
	// DefaultDecimal 默认运算时 保留的小数位数
	DefaultDecimal = 6

	// ProcessNum 多线程数量
	ProcessNum = 100

	// IsKtDate 是否采用交易师的时间模式
	IsKtDate = false // 以起始时间作为bar的close日期

	// IsTest
	IsTest = false
	// IsSync
	IsSync = false
	// DefaultCacheExpires expires
	DefaultCacheExpires = 300

	// FieldsVal1 [val1]
	FieldsVal1 = []string{"val"}

	// FieldsVal2 [val2]
	FieldsVal2 = []string{"val1", "val2"}

	// FieldsVal3 [val3]
	FieldsVal3 = []string{"val1", "val2", "val3"}

	// ErrSkip 忽略
	ErrSkip = errors.New("skip")
	// ErrDateNotFound index
	ErrDateNotFound = errors.New("index not found")
)

const (
	// Bin file ext
	ExtBin = ".bin"
	// Ext store data
	ExtDB = ".db"
	// ZipExt zip文件
	ExtZip = ".zip"
	// WavModeW3 const
	WavModeW3 = "W3"
	// WavModeW2 const
	WavModeW2 = "W2"

	// DBValuesName const
	DBValuesName = "values"
	// NameSpiltChar var
	NameSpiltChar = ":"

	// PMo1 月线
	PMo1 = "M1"
	// PW1 周线
	PW1 = "w1"
	// PD1 日线
	PD1 = "d1"
	// PD2 日线2
	PD2 = "d2"
	// PD3 日线3
	PD3 = "d3"
	// PM120 120分钟
	PM120 = "m120"
	// PM90 90分钟
	PM90 = "m90"
	// PM60 60分钟
	PM60 = "m60"
	// PM30 30分钟
	PM30 = "m30"
	// PM15 15分钟
	PM15 = "m15"
	// PM5 5分钟
	PM5 = "m5"
	// PM1 1分钟
	PM1 = "m1"
	// PMS 分时
	PMS = "ms"

	// TdxEndTime 通达信股票的结束时间
	TdxEndTime = 1500
)

var (
	// Periods 周期排序表
	Periods = []string{PW1, PD2, PD1, PM120, PM90, PM60, PM30, PM15, PM5}

	// TimeEM120 m90分钟的时间范围
	TimeEM120 = [][2]int64{[2]int64{930, 1130}, [2]int64{1300, 1500}}
	// TimeEM90 a
	TimeEM90 = [][2]int64{[2]int64{930, 1100}, [2]int64{1100, 1400}, [2]int64{1400, 1500}}
	// TimeEM60 a
	TimeEM60 = [][2]int64{[2]int64{930, 1030}, [2]int64{1030, 1130}, [2]int64{1300, 1400}, [2]int64{1400, 1500}}
	// TimeEM30 a
	TimeEM30 = [][2]int64{[2]int64{930, 1000}, [2]int64{1000, 1030}, [2]int64{1030, 1100}, [2]int64{1100, 1130}, [2]int64{1300, 1330}, [2]int64{1330, 1400}, [2]int64{1400, 1430}, [2]int64{1430, 1500}}
	// TimeEM15 a
	TimeEM15 = [][2]int64{
		[2]int64{930, 945}, [2]int64{945, 1000},
		[2]int64{1000, 1015}, [2]int64{1015, 1030},
		[2]int64{1030, 1045}, [2]int64{1045, 1100},
		[2]int64{1100, 1115}, [2]int64{1115, 1130},
		[2]int64{1300, 1315}, [2]int64{1315, 1330},
		[2]int64{1330, 1345}, [2]int64{1345, 1400},
		[2]int64{1400, 1415}, [2]int64{1415, 1430},
		[2]int64{1430, 1445}, [2]int64{1445, 1500}}
)
