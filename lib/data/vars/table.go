package vars

const (
	// TableBarFS func
	TableBarFS = "bar_fs"

	// TableBarD1 func
	TableBarD1 = "bar_d1"

	// TableBarM5 func
	TableBarM5 = "bar_m5"

	// TableShareBonus 复权记录
	TableShareBonus = "share_bonus"

	// TableCapital 股本变动
	TableCapital = "capital"

	// TableFinSum 财务摘要指标
	TableFinSum  = "finance"
	TableFinance = "finance"

	// TableCashFlow 现金流量
	TableCashFlow = "cashflow"

	// TableIncome 利润表
	TableIncome = "income"

	// TableBalanceSheet 资产负债表
	TableBalanceSheet = "balancesheet"

	// TableLimited 限售解禁
	TableLimited = "limited"

	// TableSTList ST股票列表
	TableSTList = "stlist"

	// TableFields 字段表
	TableFields = "fields"
)
