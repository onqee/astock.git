package dget

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
	"github.com/kere/gno/libs/log"
)

var (
	// TryLoadFromZip 尝试从zip包加载数据
	TryLoadFromZip = true
	// IsPool         = true  // 是否利用pool缓存data

	// DefaultStartDay begin of bars缓存加载的数据开始时间
	DefaultStartDay = int64(1100101)
	// DefaultEndDay end of bars缓存加载的数据结束时间
	DefaultEndDay = int64(0)

	// DefaultStartDayM5 begin of bars缓存加载的数据开始时间
	DefaultStartDayM5 = int64(1100101)
	// DefaultEndDayM5 end of bars缓存加载的数据结束时间
	DefaultEndDayM5 = int64(0)
)

// IsChange 缓存是否改变
func IsChange(bar *bars.Bar) bool {
	if bar.Period[0] == 'm' {
		if bar.StartDay != DefaultStartDayM5 || bar.EndDay != DefaultEndDayM5 {
			return true
		}
	} else if bar.StartDay != DefaultStartDay || bar.EndDay != DefaultEndDay {
		return true
	}
	return false
}

// 默认bar加载器
type barCached struct {
	cache.Map
	Period           string
	StartDay, EndDay int64
}

// IsReload 检查缓存值是否正确，如果正确才保存
func (m *barCached) IsReload(args ...interface{}) bool {
	if m.Period[0] == 'm' {
		if m.StartDay != DefaultStartDayM5 || m.EndDay != DefaultEndDayM5 {
			return true
		}
	} else if m.StartDay != DefaultStartDay || m.EndDay != DefaultEndDay {
		return true
	}
	return false
}

func (m *barCached) Release(args ...interface{}) {
	code := args[0].(string)
	m.Lock.Lock()
	defer m.Lock.Unlock()

	key := m.Key(code)
	v, isok := m.Data[key]
	if !isok {
		return
	}

	bar := v.Value.(*bars.Bar)
	// if IsPool {
	// 	series.PutA(&bar.A)
	// } else {
	bar.Release()
	// }
	v.Value = nil

	// m.Data.Delete(key)
	delete(m.Data, key)
}

func (m *barCached) ClearAll() {
	m.Lock.Lock()
	defer m.Lock.Unlock()

	for k, v := range m.Data {
		bar := v.Value.(*bars.Bar)
		// if IsPool {
		// 	series.PutA(&bar.A)
		// } else {
		bar.Dates = nil
		bar.Columns = nil
		// }
		v.Value = nil
		delete(m.Data, k)
	}

	m.Data = make(map[string]cache.ExpiresVal, 0)
}

// newBarCached new
func newBarCached(period string) *barCached {
	m := &barCached{Period: period}
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *barCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	// period := args[2].(string)
	bar := bars.NewBar(code, m.Period)
	bar.IsKtDate = vars.IsKtDate

	switch m.Period[0] {
	case 'd':
		bar.StartDay = DefaultStartDay
		bar.EndDay = DefaultEndDay
		m.StartDay = DefaultStartDay
		m.EndDay = DefaultEndDay
	case 'm':
		bar.StartDay = DefaultStartDayM5
		bar.EndDay = DefaultEndDayM5
		m.StartDay = DefaultStartDayM5
		m.EndDay = DefaultEndDayM5
	}

	if TryLoadFromZip {
		if err := bar.LoadZip(); err == nil {
			log.App.Debug("[bar cache] load zip", code, m.Period, "expires:", m.GetExpires())
			return &bar, nil
		}
	}

	if err := bar.Load(); err != nil {
		return nil, err
	}

	log.App.Debug("[bar cache] load db", code, m.Period, "expires:", m.GetExpires())

	return &bar, nil
}

// 分钟线组合
// 分钟的bar: m15,m60,m90,m120
// 直接读取缓存中的m5数据，然后合成其他分钟线
type barMCached struct {
	barCached
}

func newBarMCached(period string) *barMCached {
	m := &barMCached{}
	m.Period = period
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *barMCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	var dat series.A

	switch m.Period {
	case vars.PM15:
		barM5 := barDataMapM5.Get(code).(*bars.Bar)
		dat = bars.M2M(&barM5.A, vars.PM15, vars.IsKtDate, false)
	case vars.PM30:
		barM5 := barDataMapM5.Get(code).(*bars.Bar)
		dat = bars.M2M(&barM5.A, vars.PM30, vars.IsKtDate, false)
	case vars.PM60:
		if barDataMapM30.IsCached(code) && !barDataMapM30.IsReload(code) {
			bar := barDataMapM30.Get(code).(*bars.Bar)
			dat = bars.M2M(&bar.A, vars.PM60, vars.IsKtDate, false)
		} else {
			barM5 := barDataMapM5.Get(code).(*bars.Bar)
			dat = bars.M2M(&barM5.A, vars.PM60, vars.IsKtDate, false)
		}
	case vars.PM90:
		if barDataMapM30.IsCached(code) && !barDataMapM30.IsReload(code) {
			bar := barDataMapM30.Get(code).(*bars.Bar)
			dat = bars.M2M(&bar.A, vars.PM90, vars.IsKtDate, false)
		} else {
			barM5 := barDataMapM5.Get(code).(*bars.Bar)
			dat = bars.M2M(&barM5.A, vars.PM90, vars.IsKtDate, false)
		}
	case vars.PM120:
		if barDataMapM60.IsCached(code) && !barDataMapM60.IsReload(code) {
			bar := barDataMapM60.Get(code).(*bars.Bar)
			dat = bars.M2M(&bar.A, vars.PM120, vars.IsKtDate, false)
		} else {
			barM5 := barDataMapM5.Get(code).(*bars.Bar)
			dat = bars.M2M(&barM5.A, vars.PM120, vars.IsKtDate, false)
		}
	default:
		return nil, fmt.Errorf("系统没有定义相应的周期%s", m.Period)
	}

	bar := bars.NewBar(code, m.Period)
	bar.IsKtDate = vars.IsKtDate
	bar.A = dat
	m.StartDay = DefaultStartDayM5
	m.EndDay = DefaultEndDayM5

	log.App.Debug("[barM cache] build", code, m.Period, "expires:", m.GetExpires())

	return &bar, nil
}

// 日线组合
type barDCached struct {
	barCached
}

func newBarDCached(period string) *barDCached {
	m := &barDCached{}
	m.Period = period
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build 日线组合
func (m *barDCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	barD1 := barDataMapD1.Get(code).(*bars.Bar)
	var bar bars.Bar
	switch m.Period {
	case vars.PD2, vars.PD3:
		// dat = bars.D1ToN(&barD1.A, 2)
		bar = bars.D1BarToN(barD1, m.Period, vars.IsKtDate, false, false)
	case vars.PW1:
		bar = bars.D1BarToWeek(barD1, vars.IsKtDate, false, false)
	case vars.PMo1:
		bar = bars.D1BarToMonth(barD1, vars.IsKtDate, false, false)
	default:
		return nil, fmt.Errorf("系统没有定义相应的周期%s", m.Period)
	}

	m.StartDay = DefaultStartDay
	m.EndDay = DefaultEndDay
	log.App.Debug("[barD cache] build", code, m.Period, "expires:", m.GetExpires())
	return &bar, nil
}
