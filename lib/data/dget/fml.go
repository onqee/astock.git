package dget

import (
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

var binDatCache = newBinDatCached()

type binDatCached struct {
	cache.Map
}

// NewBinDatCached new
func newBinDatCached() *binDatCached {
	m := &binDatCached{}
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *binDatCached) Build(args ...interface{}) (interface{}, error) {
	filename := args[0].(string)
	// 提取数据
	dat, _ := series.ReadBin(filename, 0, 0, false)
	return &dat, nil
}

// Release dat
func (m *binDatCached) Release(args ...interface{}) {
	name := args[0].(string)
	m.Lock.Lock()
	defer m.Lock.Unlock()

	key := m.Key(name)
	v, isok := m.Data[key]
	if !isok {
		return
	}
	dat := v.Value.(*series.A)
	dat.Release()
	// if IsPool {
	// 	series.PutA(dat)
	// }
	v.Value = nil
	delete(m.Data, key)
}

// GetBinDat 读取bindata 数据
func GetBinDat(filename string) *series.A {
	dat := binDatCache.Get(filename)
	if dat == nil {
		return &series.EmptyA
	}
	return dat.(*series.A)
}

// IsBinCached bar
func IsBinCached(name string) bool {
	return binDatCache.IsCached(name)
}

// ReleaseBin bar
func ReleaseBin(name string) {
	binDatCache.Release(name)
}

// ReleaseBinAll bar
func ReleaseBinAll() {
	binDatCache.ClearAll()
}
