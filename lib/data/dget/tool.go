package dget

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
)

// ItIndex 当前代码的指数
func ItIndex(bar *bars.Bar) *bars.Bar {
	if bar.Code[1] == 'h' {
		return Bar(vars.Code000001, bar.Period)
	}
	return Bar(vars.Code399001, bar.Period)
}

// TradeDays 两个日期，包含多少个交易周期D1
func TradeDays(dateA, dateB int64) int {
	sz := Bar(vars.Code399001, vars.PD1)
	a := dateA/10000*10000 + 1500
	b := dateB/10000*10000 + 1500
	ai, bi := sz.RangeI(a, b)
	if ai == -1 {
		return 0
	}
	return bi - ai + 1
}
