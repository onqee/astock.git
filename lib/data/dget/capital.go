package dget

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

var (
	capitalCache = newCapitalCached()
)

type capCached struct {
	Period string
	cache.Map
}

// newCapitalCached new
func newCapitalCached() *capCached {
	m := &capCached{}
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *capCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	c := bars.NewCapital(code)
	c.StartDay = DefaultStartDay
	c.EndDay = DefaultEndDay

	if err := c.Load(); err != nil {
		return nil, err
	}
	return &c, nil
}

// Capital bar
func Capital(code string) *bars.Capital {
	return capitalCache.Get(code).(*bars.Capital)
}

// IsCapitalCached bar
func IsCapitalCached(code string) bool {
	return capitalCache.IsCached(code)
}

// TurnoverRate 换手率
func TurnoverRate(code string, date int64) float64 {
	v1 := TradableOn(code, date)
	bar := Bar(code, vars.PD1)
	v2, _ := bar.ValueOn(vars.FieldVol, date)
	return 100 * v2 / float64(v1)
}

// TradableOn 流通盘
func TradableOn(code string, date int64) int64 {
	c := Capital(code)
	v := c.TradableOn(date)
	if v < 0 {
		return 0
	}
	return v
}

// TradableAmtOn 流通盘
func TradableAmtOn(code string, date int64) float64 {
	c := Capital(code)
	v := c.TradableOn(date)
	if v < 0 {
		return 0
	}
	bar := Bar(code, vars.PD1)
	price, _ := bar.ValueOn(vars.FieldClose, date)
	return price * float64(v)
}

// CapitalOn 总盘
func CapitalOn(code string, date int64) int64 {
	c := Capital(code)
	v := c.CapitalOn(date)
	if v < 0 {
		return 0
	}
	return v
}

// CapitalAmtOn 总盘
func CapitalAmtOn(code string, date int64) float64 {
	c := Capital(code)
	v := c.CapitalOn(date)
	if v < 0 {
		return 0
	}
	bar := Bar(code, vars.PD1)
	price, _ := bar.ValueOn(vars.FieldClose, date)
	return price * float64(v)
}

// IsTradableAmtInScopeOn 流通金额在范围a-b内？
// a, b 单位（亿元)
func IsTradableAmtInScopeOn(code string, date int64, scope [2]int) bool {
	v := TradableAmtOn(code, date)
	if v == 0 {
		return false
	}
	amt := int(v / 100000000)
	return scope[0] <= amt && amt <= scope[1]
}

// IsCapitalAmtInScopeOn 总股本金额在范围a-b内？
// a, b 单位（亿元)
func IsCapitalAmtInScopeOn(code string, date int64, scope [2]int) bool {
	v := CapitalAmtOn(code, date)
	if v == 0 {
		return false
	}
	amt := int(v / 100000000)
	return scope[0] <= amt && amt <= scope[1]
}
