package dget

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

var (
	barDataMapD1 = newBarCached(vars.PD1)
	barDataMapD2 = newBarDCached(vars.PD2)
	barDataMapD3 = newBarDCached(vars.PD3)
	barDataMapW1 = newBarDCached(vars.PW1)

	barDataMapM1 = newBarCached(vars.PM1)
	barDataMapM5 = newBarCached(vars.PM5)

	barDataMapM15  = newBarMCached(vars.PM15)
	barDataMapM30  = newBarMCached(vars.PM30)
	barDataMapM60  = newBarMCached(vars.PM60)
	barDataMapM90  = newBarMCached(vars.PM90)
	barDataMapM120 = newBarMCached(vars.PM120)
)

// SetExpires n
func SetExpires(n int) {
	barDataMapW1.SetExpires(n)
	barDataMapD1.SetExpires(n)
	barDataMapD2.SetExpires(n)
	barDataMapD3.SetExpires(n)
	barDataMapM1.SetExpires(n)
	barDataMapM5.SetExpires(n)
	barDataMapM15.SetExpires(n)
	barDataMapM30.SetExpires(n)
	barDataMapM60.SetExpires(n)
	barDataMapM90.SetExpires(n)
	barDataMapM120.SetExpires(n)
	capitalCache.SetExpires(n)
}

func getBarData(period string) cache.ICachedMap {
	switch period {
	case vars.PW1:
		return barDataMapW1
	case vars.PD3:
		return barDataMapD3
	case vars.PD2:
		return barDataMapD2
	case vars.PD1:
		return barDataMapD1
	case vars.PM1:
		return barDataMapM1
	case vars.PM5:
		return barDataMapM5
	case vars.PM15:
		return barDataMapM15
	case vars.PM30:
		return barDataMapM30
	case vars.PM60:
		return barDataMapM60
	case vars.PM90:
		return barDataMapM90
	case vars.PM120:
		return barDataMapM120
	}
	panic("no period " + period + " found")
}

// Bar bar
func Bar(code, period string) *bars.Bar {
	return (getBarData(period).Get(code)).(*bars.Bar)
}

// ReleaseBar series
func ReleaseBar(code, period string) {
	getBarData(period).Release(code)
	capitalCache.Release(code)
}

// IsCached bar
func IsCached(code, period string) bool {
	return getBarData(period).IsCached(code)
}

// ReleaseAll series
func ReleaseAll(code string) {
	getBarData(vars.PW1).Release(code, vars.PW1)
	getBarData(vars.PD1).Release(code, vars.PD1)
	getBarData(vars.PD2).Release(code, vars.PD2)
	getBarData(vars.PD3).Release(code, vars.PD3)
	getBarData(vars.PM5).Release(code, vars.PM5)
	getBarData(vars.PM15).Release(code, vars.PM15)
	getBarData(vars.PM30).Release(code, vars.PM30)
	getBarData(vars.PM60).Release(code, vars.PM60)
	getBarData(vars.PM90).Release(code, vars.PM90)
	getBarData(vars.PM120).Release(code, vars.PM120)

	capitalCache.Release(code)
	stCache.Release(code)
}

// Clear cache
func Clear() {
	getBarData(vars.PW1).ClearAll()
	getBarData(vars.PD3).ClearAll()
	getBarData(vars.PD2).ClearAll()
	getBarData(vars.PD1).ClearAll()
	getBarData(vars.PM5).ClearAll()
	getBarData(vars.PM15).ClearAll()
	getBarData(vars.PM30).ClearAll()
	getBarData(vars.PM60).ClearAll()
	getBarData(vars.PM90).ClearAll()
	getBarData(vars.PM120).ClearAll()
	capitalCache.ClearAll()
	stCache.ClearAll()
}
