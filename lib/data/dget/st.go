package dget

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

var stCache = newStCache()

type stCached struct {
	cache.Map
}

// NewStCache new
func newStCache() *stCached {
	m := &stCached{}
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *stCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	st := bars.NewST(code)
	err := st.Load()
	return &st, err
}

// Release dat
func (m *stCached) Release(args ...interface{}) {
	name := args[0].(string)
	m.Lock.Lock()
	defer m.Lock.Unlock()

	key := m.Key(name)
	v, isok := m.Data[key]
	if !isok {
		return
	}
	dat := v.Value.(*bars.ST)
	dat.Release()
	// if IsPool {
	// 	series.PutA(dat)
	// }
	v.Value = nil
	delete(m.Data, key)
}

// ST 读取st 数据
func ST(code string) *bars.ST {
	dat := stCache.Get(code)
	if dat == nil {
		return &bars.ST{}
	}
	return dat.(*bars.ST)
}

// IsSTCached bar
func IsSTCached(code string) bool {
	return stCache.IsCached(code)
}

// ReleaseST bar
func ReleaseST(code string) {
	stCache.Release(code)
}

// ReleaseSTAll bar
func ReleaseSTAll() {
	stCache.ClearAll()
}
