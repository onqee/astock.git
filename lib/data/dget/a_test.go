package dget

import (
	"fmt"
	"math"
	"path/filepath"
	"testing"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/log"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../../app.conf")
	db.Current().SetLogLevel(log.LogAllStr)

	vars.IsTest = true
	vars.IsSync = true
	vars.IsKtDate = false
	TryLoadFromZip = false

	DefaultStartDay = 1190101
	DefaultEndDay = 1191224
	DefaultStartDayM5 = 1190101
	DefaultEndDayM5 = 1191224
}

func TestGetBar(t *testing.T) {
	code := "sz000938"

	bar := Bar(code, vars.PD1)
	v, _ := bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		fmt.Println(v)
		t.Fatal(v)
	}
	bar = Bar(code, vars.PD1)
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		fmt.Println(v)
		t.Fatal(v)
	}

	if !getBarData(vars.PD1).IsCached(code) {
		t.Fatal(code, "is not cached")
	}

	bar = Bar(code, vars.PM120)
	v, _ = bar.ValueOn(vars.FieldClose, 11911111130)
	if v != 28.03 {
		series.Print(&bar.A, 11911100000, 11911121500)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldHigh, 11911111130)
	if v != 28.82 {
		series.Print(&bar.A, 11911100000, 11911121500)
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldLow, 11912201130)
	if math.Abs(v-30.45) > 0.001 {
		series.Print(&bar.A, 11912100000, 11912250000)
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		t.Fatal(v)
	}

	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		fmt.Println(v)
		t.Fatal(v)
	}
	if !getBarData(vars.PM120).IsCached(code) {
		t.Fatal(code, "is not cached")
	}

	bar = Bar(code, vars.PM90)
	v, _ = bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		fmt.Println(v)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	if !getBarData(vars.PM90).IsCached(code) {
		t.Fatal(code, "is not cached")
	}

	bar = Bar(code, vars.PM60)
	v, _ = bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		fmt.Println(v)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	if !getBarData(vars.PM60).IsCached(code) {
		t.Fatal(code, "is not cached")
	}

	bar = Bar(code, vars.PM30)
	v, _ = bar.ValueOn(vars.FieldClose, 11905271500)
	if math.Abs(v-28.03) > 0.001 {
		fmt.Println(v)
		t.Fatal(v)
	}
	v, _ = bar.ValueOn(vars.FieldClose, 11905241500)
	if math.Abs(v-26.61) > 0.001 {
		series.Print(&bar.A)
		t.Fatal(v)
	}
	if !getBarData(vars.PM30).IsCached(code) {
		t.Fatal(code, "is not cached")
	}

	bar = Bar(code, vars.PD2)
	if bar.Len() < 10 {
		t.Fatal(bar.Len())
	}

	bar = Bar(code, vars.PM120)
	barDay := bar.RangeDay(11911119999)
	if barDay.Len() != 2 {
		series.Print(&bar.A)
		t.Fatal()
	}

	if barDay.A.Dates[0] != 11911111130 || math.Abs(barDay.A.Columns[0][0]-28.03) > 0.001 || math.Abs(barDay.A.Columns[2][0]-28.82) > 0.0001 {
		series.Print(&barDay.A)
		t.Fatal()
	}

	if barDay.A.Dates[1] != 11911111500 || math.Abs(barDay.A.Columns[0][1]-28.19) > 0.001 || math.Abs(barDay.A.Columns[2][1]-28.24) > 0.0001 {
		series.Print(&barDay.A)
		t.Fatal()
	}

	DefaultStartDayM5 = 1200101
	DefaultEndDayM5 = 1200131
	if !getBarData(vars.PM120).IsReload(code, vars.PM120) {
		t.Fatal()
	}

	bar = Bar(code, vars.PM120)
	if getBarData(vars.PM120).IsReload(code, vars.PM120) {
		t.Fatal()
	}

	l := bar.Len()
	if bar.LastDate() != 12001231500 || bar.Columns[0][l-1] != 33.06 {
		series.Print(&bar.A)
		t.Fatal()
	}
	if bar.Dates[0] != 12001021130 {
		series.Print(&bar.A)
		t.Fatal(bar.Dates[0])
	}
}

func TestGetFmlDat(t *testing.T) {
	name := "tdd-4"
	writeFmlDat("ts001", vars.PD1, name)

	c := gno.GetConfig().GetConf("data")
	filename := filepath.Join(c.GetString("store_dir"), "test/fmls", vars.PD1, name, "ts001.bin")
	dat := GetBinDat(filename)
	if dat.Len() < 0 {
		series.Print(dat)
		t.Fatal()
	}
	if dat.LastDate() != 12004211300 {
		series.Print(dat)
		t.Fatal()
	}
	if !IsBinCached(filename) || len(binDatCache.Data) != 1 {
		t.Fatal("Dat is not cached")
	}
}

func writeFmlDat(code, period, name string) {
	type sVe struct {
		Date  int64
		Value float64
	}
	arr := []sVe{sVe{12002280930, -9}, sVe{12003041300, 9}, sVe{12003060930, -9}, sVe{12003181300, 9}, sVe{12003200930, -9}, sVe{12003201300, -10},
		sVe{12004200900, -9}, sVe{12004211300, -10},
	}
	c := gno.GetConfig().GetConf("data")
	filename := filepath.Join(c.GetString("store_dir"), "test/fmls", period, name, code+".bin")
	dat := series.GetA(vars.FieldsVal1)
	row := dat.GetRowP()
	count := len(arr)
	for i := 0; i < count; i++ {
		row[0] = arr[i].Value
		dat.AddRow0(arr[i].Date, row)
	}
	series.WriteBin(&dat, filename)
	series.PutA(&dat)
	series.PutRow(row)
}

func TestCapital(t *testing.T) {
	code := "sz000938"
	c := Capital(code)
	v := c.TradableOn(12008241500)
	if v != 2042914200 {
		t.Fatal(v)
	}
	v = c.TradableOn(12008251500)
	if v != 2860079900 {
		t.Fatal(v)
	}
	v = c.TradableOn(12009021500)
	if v != 2860079900 {
		t.Fatal(v)
	}
	v = c.TradableOn(11908191500)
	if v != 2042914200 {
		t.Fatal(v)
	}
	tr := TurnoverRate(code, 11908191500)
	if math.Abs(tr-0.011818) > 0.00001 {
		t.Fatal(tr)
	}

	code = "sh600584"
	amt := int64(TradableAmtOn(code, 11905291500))
	// 1035914800 x 15.53 = 160,8775,6844
	if amt != 16087756844 {
		t.Fatal(amt)
	}
	if !IsTradableAmtInScopeOn(code, 11905291500, [2]int{159, 160}) {
		t.Fatal(amt)
	}

}
