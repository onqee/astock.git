package mathlib

import (
	"fmt"
	"math"
	"testing"
)

func TestCombos(t *testing.T) {
	src := []int{1, 2, 3, 4, 5, 6, 7}
	selectNum := 5
	repeatable := false
	buf := 3
	arr := Combinations(IntReplacer(src), selectNum, repeatable, buf)
	for _, comb := range arr {
		fmt.Println(comb, "--")
	}
}

func TestMath(t *testing.T) {
	arr1 := []float64{20, 21, 22, 23, 32, 34, 30, 33, 29, 25, 24, 25, 21, 24, 26, 27, 33, 35, 38, 41, 39, 44, 39, 37, 36, 33, 35}
	arr2 := []float64{122, 121, 122, 123, 132, 134, 130, 133, 129, 125, 124, 125, 121, 124, 126, 127, 133, 135, 138, 141, 139, 144, 139, 137, 136, 133, 135}
	arr3 := []float64{22, 31, 42, 43, 52, 54, 50, 53, 49, 45, 44, 45, 41, 44, 46, 47, 53, 55, 58, 61, 59, 64, 59, 57, 56, 53, 55}

	v := Corr(arr1, arr2)
	if v-0.998544994 > 0.000001 {
		t.Fatal("corr", v)
	}
	v = Corr(arr1, arr3)
	if v-0.922532904 > 0.000001 {
		t.Fatal("corr", v)
	}

	vals := []float64{3, 4, 5}
	v = Ema(vals, 3)
	// if math.Abs(v-4.428571428) > 0.0001 {
	if math.Abs(v-4.333333333) > 0.0001 {
		t.Fatal("Ema", v)
	}

}

func TestNearVal(t *testing.T) {
	// 11是否在10.5 的 %10 内
	isTrue := NearVal(10, 10.5, 9.5, 11, 0.1)
	if !isTrue {
		t.Fatal("11是否在10.5 的 %10 内")
	}
	// 12是否在10.5 的 %10 内
	isTrue = NearVal(10, 10.5, 9.5, 12, 0.1)
	if isTrue {
		t.Fatal("12不在10.5 的 %10 内")
	}
	// 9是否在9.5 的 %10 内
	isTrue = NearVal(10, 10.5, 9.5, 9, 0.1)
	if !isTrue {
		t.Fatal("9不在10.5 的 %10 内")
	}
	// 8是否在9.5 的 %10 内
	isTrue = NearVal(10, 10.5, 9.5, 8, 0.1)
	if isTrue {
		t.Fatal("8不在10.5 的 %10 内")
	}
	// 9.6是否在9.5 的 %10 内
	isTrue = NearVal(10, 10.5, 9.5, 9.6, 0.1)
	if !isTrue {
		t.Fatal("9.6不在10.5 - 9.5 内")
	}
}
