package mathlib

//Combinations 排列组合
func Combinations(list Replacer, selectNum int, repeatable bool, buf int) []Replacer {
	c := make(chan Replacer, buf)
	l := list.Len()
	indexs := make([]int, l)
	for i := 0; i < l; i++ {
		indexs[i] = i
	}

	var combGenerator func([]int, int, int) chan []int
	if repeatable {
		combGenerator = repeatedCombinations
	} else {
		combGenerator = combinations
	}

	go func() {
		defer close(c)
		for comb := range combGenerator(indexs, selectNum, buf) {
			c <- list.Replace(comb)
		}
	}()

	arr := make([]Replacer, 0, l)
	for item := range c {
		arr = append(arr, item)
	}

	return arr
}

//Combination generator for int slice
func combinations(list []int, selectNum, buf int) (c chan []int) {
	c = make(chan []int, buf)
	l := len(list)
	go func() {
		defer close(c)
		switch {
		case selectNum == 0:
			return
		case selectNum == l:
			c <- list
		case l < selectNum:
			return
		default:
			for i := 0; i < l; i++ {
				for subComb := range combinations(list[i+1:], selectNum-1, buf) {
					c <- append([]int{list[i]}, subComb...)
				}
			}
		}
	}()
	return
}

//Repeated combination generator for int slice
func repeatedCombinations(list []int, selectNum, buf int) (c chan []int) {
	c = make(chan []int, buf)
	go func() {
		defer close(c)
		if selectNum == 1 {
			for v := range list {
				c <- []int{v}
			}
			return
		}
		for i := 0; i < len(list); i++ {
			for subComb := range repeatedCombinations(list[i:], selectNum-1, buf) {
				c <- append([]int{list[i]}, subComb...)
			}
		}
	}()
	return
}

// Replacer class
type Replacer interface {
	Len() int
	Replace([]int) Replacer
}

// IntReplacer 可替换，重新组合的 []int
type IntReplacer []int

// Len l
func (il IntReplacer) Len() int {
	return len(il)
}

// Replace 索引
func (il IntReplacer) Replace(indices []int) Replacer {
	result := make(IntReplacer, len(indices))
	for i, idx := range indices {
		result[i] = il[idx]
	}
	return result
}

// RuneReplacer 可替换，重新组合的 []rune
type RuneReplacer []rune

// Len int
func (rl RuneReplacer) Len() int {
	return len(rl)
}

// Replace f
func (rl RuneReplacer) Replace(indices []int) Replacer {
	result := make(RuneReplacer, len(indices), len(indices))
	for i, idx := range indices {
		result[i] = rl[idx]
	}
	return result
}

// StringReplacer 可替换，重新组合的 []string
type StringReplacer []string

// Len int
func (sl StringReplacer) Len() int {
	return len(sl)
}

// Replace f
func (sl StringReplacer) Replace(indices []int) Replacer {
	result := make(StringReplacer, len(indices), len(indices))
	for i, idx := range indices {
		result[i] = sl[idx]
	}
	return result
}

// Float64Replacer class
type Float64Replacer []string

// Len int
func (fr Float64Replacer) Len() int {
	return len(fr)
}

// Replace f
func (fr Float64Replacer) Replace(indices []int) Replacer {
	result := make(Float64Replacer, len(indices), len(indices))
	for i, idx := range indices {
		result[i] = fr[idx]
	}
	return result
}
