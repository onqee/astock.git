package mathlib

// Sma expma
func Sma(vals []float64, m int) float64 {
	// 若Y=SMA(X,N,M) 则 Y=(M*X+(N-M)*Y')/N，其中Y'表示上一周期Y值，N必须大于M
	l := len(vals)
	if l == 1 {
		return vals[0]
	}
	n := float64(l)
	a := float64(m)

	v := (a*vals[l-1] + (n-a)*Sma(vals[:l-1], m)) / n

	return v
}

// Ema expma
func Ema(vals []float64, n int) float64 {
	// EMAtoday=α * ( Pricetoday - EMAyesterday ) + EMAyesterday;
	// 若Y=EMA(X，N)，则Y=[2*X+(N-1)*Y’]/(N+1)，其中Y’表示上一周期的Y值。
	l := len(vals)
	if l == 1 {
		return vals[0]
	}
	N := float64(l)
	v := (2*vals[l-1] + (N-1)*Ema(vals[:l-1], n)) / (N + 1)
	return v
}

// MaWeigth ma with weight
func MaWeigth(vals, vols []float64) float64 {
	l := len(vals)
	var sum, sumVol float64
	for i := 0; i < l; i++ {
		sumVol += vols[i]
		sum += vals[i] * vols[i]
	}
	return sum / sumVol
}
