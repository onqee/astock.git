package mathlib

import "math"

// NearVal 是否在给的值附近，c=close h=high l=low
func NearVal(c, h, l, val, percent float64) bool {
	// val是顶部，val在high的%percent内
	if val > h {
		return val < h*(1+percent)
	}
	if val < l {
		return val > l*(1-percent)
	}
	return true
}

// Percent3P 三点的百分比计算
func Percent3P(v1, v2, v3 float64) float64 {
	return (v2 - v1) / (v2 - v3)
}

// Percent3PLog 三点的百分比计算
func Percent3PLog(v1, v2, v3 float64) float64 {
	return math.Log(v2/v1) / math.Log(v2/v3)
}
