package mathlib

import (
	"math"
	"sort"

	"github.com/kere/gno/libs/util"
)

// Float32sTo64 func
func Float32sTo64(vals []float32) []float64 {
	l := len(vals)
	arr := make([]float64, l)
	for i := 0; i < l; i++ {
		arr[i] = float64(vals[i])
	}
	return arr
}

// MaxI func
func MaxI(vals []float64) int {
	return MaxSkipI(vals, func(i int) bool {
		return false
	})
}

// MaxSkipI func
func MaxSkipI(vals []float64, f func(i int) bool) int {
	var maxI int
	maxV := vals[0]
	l := len(vals)
	for i := 1; i < l; i++ {
		if f(i) {
			continue
		}
		if maxV < vals[i] {
			maxV = vals[i]
			maxI = i
		}
	}
	return maxI
}

// MinI func
func MinI(vals []float64) int {
	return MinSkipI(vals, func(i int) bool {
		return false
	})
}

// MinSkipI func
func MinSkipI(vals []float64, f func(i int) bool) int {
	var minI int
	minV := vals[0]
	l := len(vals)
	for i := 1; i < l; i++ {
		if f(i) {
			continue
		}
		if minV > vals[i] {
			minV = vals[i]
			minI = i
		}
	}
	return minI
}

// MaxMinI func
func MaxMinI(vals []float64) (int, int) {
	var maxI, minI int
	maxV := vals[0]
	minV := vals[0]
	l := len(vals)
	for i := 1; i < l; i++ {
		if minV > vals[i] {
			minV = vals[i]
			minI = i
		}
		if maxV < vals[i] {
			maxV = vals[i]
			maxI = i
		}
	}
	return maxI, minI
}

// Max func
func Max(vals []float64) float64 {
	arr := make([]float64, len(vals))
	copy(arr, vals)
	sort.Float64s(arr)
	return arr[len(arr)-1]
}

// Min fun
func Min(vals []float64) float64 {
	arr := make([]float64, len(vals))
	copy(arr, vals)
	sort.Float64s(arr)
	return arr[0]
}

// MaxMin func
func MaxMin(vals []float64) (float64, float64) {
	arr := make([]float64, len(vals))
	copy(arr, vals)
	if len(arr) == 0 {
		return 0, 0
	}
	sort.Float64s(arr)
	return arr[len(arr)-1], arr[0]
}

// Sum func
func Sum(vals []float64) float64 {
	var sum float64
	for _, item := range vals {
		sum += item
	}
	return sum
}

// Avg 简单平均
func Avg(vals []float64) float64 {
	if len(vals) == 0 {
		return 0
	}
	return Sum(vals) / float64(len(vals))
}

// TrimMaxMinWithSorted 去掉最高最低数据
func TrimMaxMinWithSorted(vals []float64, percent float64) []float64 {
	l := len(vals)
	if l < 3 || percent == 0 {
		return vals
	}

	// arr := make([]float64, l)
	// copy(arr, vals)
	sort.Float64s(vals)

	i := int(float64(l)*percent) - 1
	if i < 1 {
		return vals
	}
	return vals[i : l-i]
}

// TrimMaxWithSorted 去掉最高数据
func TrimMaxWithSorted(vals []float64, percent float64) []float64 {
	l := len(vals)
	if l < 3 || percent == 0 {
		return vals
	}

	sort.Float64s(vals)

	i := int(float64(l)*percent) - 1
	return vals[:l-i]
}

// TrimMax 去掉最高数据
func TrimMax(vals []float64, percent float64) []float64 {
	l := len(vals)
	if l < 3 || percent == 0 {
		return vals
	}
	arr := make([]float64, l)
	copy(arr, vals)
	return TrimMaxWithSorted(arr, percent)
}

// TrimMinWithSorted 去掉最低数据
func TrimMinWithSorted(vals []float64, percent float64) []float64 {
	l := len(vals)
	if l < 3 || percent == 0 {
		return vals
	}

	// arr := make([]float64, l)
	// copy(arr, vals)
	sort.Float64s(vals)

	i := int(float64(l)*percent) - 1
	return vals[i:]
}

// TrimMin 去掉最低数据
func TrimMin(vals []float64, percent float64) []float64 {
	l := len(vals)
	if l < 3 || percent == 0 {
		return vals
	}
	arr := make([]float64, l)
	copy(arr, vals)
	return TrimMinWithSorted(arr, percent)
}

// Avg2WithSorted 去掉最高最低数据后的简单平均
func Avg2WithSorted(vals []float64, percent float64) float64 {
	arr := TrimMaxMinWithSorted(vals, percent)
	return Avg(arr)
}

// AvgTrimMaxWithSorted 去掉最高最据后的简单平均
func AvgTrimMaxWithSorted(vals []float64, percent float64) float64 {
	arr := TrimMaxWithSorted(vals, percent)
	return Avg(arr)
}

// Avg2 去掉最高最低数据后的简单平均
func Avg2(vals []float64, percent float64) float64 {
	arr := make([]float64, len(vals))
	copy(arr, vals)
	return Avg(TrimMaxMinWithSorted(arr, percent))
}

// Square 平方
func Square(val float64) float64 {
	return val * val
}

// Var Variance 方差
func Var(vals []float64) float64 {
	return SumSquaresDev(vals) / float64(len(vals)-1)
}

// SumSquaresDev Sum of Squares of Deviations 离差平方和
// Sum((v-x)^2)
func SumSquaresDev(vals []float64) float64 {
	x := Avg(vals)
	var sum float64
	l := len(vals)
	for i := 0; i < l; i++ {
		sum += Square(vals[i] - x)
	}
	return sum
}

// AvgDev Sum of Deviations 平均离差
// (v-x)/length
func AvgDev(vals []float64) float64 {
	l := len(vals)
	if l == 0 {
		return 0
	}
	x := Avg(vals)
	var sum float64
	for i := 0; i < l; i++ {
		sum += math.Abs(vals[i] - x)
	}
	return sum / float64(l)
}

// StdDev 标准差
// Sqrt(Sum((v-x)^2)/l)
func StdDev(vals []float64) float64 {
	// ssd := SumSquaresDeviations(vals)
	// return math.Sqrt(ssd / float64(len(vals)))
	return math.Sqrt(Var(vals))
}

// Sin func
func Sin(x, y float64) float64 {
	return y / math.Sqrt(x*x+y*y)
}

// Cos func
func Cos(x, y float64) float64 {
	return x / math.Sqrt(x*x+y*y)
}

// Cov Covariance 协方差
func Cov(x, y []float64) float64 {
	l := len(x)
	if l != len(y) {
		panic("data length not matched")
	}
	avgX := Avg(x)
	avgY := Avg(y)

	var sum float64
	for i := 0; i < l; i++ {
		sum += (x[i] - avgX) * (y[i] - avgY)
	}

	return sum / float64(l-1)
}

// Corr Correlation 相关系数
func Corr(x, y []float64) float64 {
	return CorrWithF(x, y, nil)
}

// CorrWithF Correlation 相关系数
func CorrWithF(x, y []float64, f func(int)) float64 {
	l := len(x)
	if l != len(y) {
		panic("data length not matched")
	}
	if l == 0 {
		return 0
	}

	var sumX, sumY float64
	for i := 0; i < l; i++ {
		sumX += x[i]
		sumY += y[i]
	}
	vLen := float64(l)
	avgX := sumX / vLen
	avgY := sumY / vLen

	var cov, varX, varY float64
	for i := 0; i < l; i++ {
		if f != nil {
			f(i)
		}
		v1 := x[i] - avgX
		v2 := y[i] - avgY
		cov += v1 * v2
		varX += Square(v1)
		varY += Square(v2)
	}

	return cov / math.Sqrt(varX*varY)
	// return Cov(x, y) / math.Sqrt(Var(x)*Var(y))
}

// ReversalFloatP
func ReversalFloatP(vals []float64) []float64 {
	l := len(vals)
	if l == 0 {
		return nil
	}
	arr := util.GetFloats(l)
	n := l - 1
	for i := 0; i < l; i++ {
		arr[n-i] = vals[i]
	}
	return arr
}
