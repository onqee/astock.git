package tushare

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

var (
	tuShareToken string
)

const (
	fieldEndDate = "end_date"
	fieldTsCode  = "ts_code"
)

// SetToken string
func SetToken(s string) {
	tuShareToken = s
}

// GetToken string
func GetToken() string {
	if tuShareToken == "" {
		c := gno.GetConfig().GetConf("tushare")
		tuShareToken = c.Get("token")
	}
	return tuShareToken
}

const (
	// urlstr = "http://api.tushare.pro"
	urlstr = "http://api.waditu.com"
	// DateFormart format
	DateFormart = "20060102"

	codeField = "ts_code"
)

// ScoreToDateStr 转换成tushare 的日期
func ScoreToDateStr(score int64) string {
	d := datescore.Score2Date(score)
	return d.Format(DateFormart)
}

// tuShareCode 转换成tushare 的code
func tuShareCode(market, code string) string {
	return fmt.Sprintf("%s.%s", code, strings.ToUpper(market))
}

// Send api
// api_name：接口名称，比如stock_basic
// token ：用户唯一标识，可通过登录pro网站获取
// params：接口参数，如daily接口中start_date和end_date
// fields：字段列表，用于接口获取指定的字段，以逗号分隔，如"open,high,low,close"
// delay: 延迟的时间s
func Send(token, method string, args util.MapData, fields []string, delay time.Duration) (Recive, error) {
	vals := util.MapData{}
	vals["api_name"] = method
	if len(fields) == 0 {
		vals["fields"] = ""
	} else {
		vals["fields"] = strings.Join(fields, ",")
	}
	vals["token"] = token
	vals["params"] = args
	params, _ := json.Marshal(vals)

	var obj Recive
	req, err := http.NewRequest(http.MethodPost, urlstr, bytes.NewReader(params))
	if err != nil {
		return obj, err
	}

	client := &http.Client{}

	resq, err := client.Do(req)
	time.Sleep(delay)
	if resq != nil {
		defer resq.Body.Close()
	}
	if err != nil {
		return obj, err
	}

	body, err := ioutil.ReadAll(resq.Body)
	if err != nil {
		return obj, err
	}

	if resq.StatusCode != http.StatusOK {
		return obj, myerr.New(string(body))
	}

	err = json.Unmarshal(body, &obj)
	if err != nil {
		return obj, err
	}

	if obj.Msg != "" {
		return obj, myerr.New(obj.Code, obj.Msg)
	}

	return obj, nil
}

func getStartDateStr(table, market, code string) string {
	q := db.NewQuery(table)
	row, err := q.Where("code=$1", codetool.ToCodeI(code)).Order("date desc").QueryOne()
	if err != nil {
		return ""
	}
	var date int64
	var ddate time.Time
	if row.IsEmpty() {
		date = bars.StartDBDate(code, vars.PD1)
		ddate = datescore.Score2Date(date)
	} else {
		date = row.Int64(vars.FieldDate)
		ddate = datescore.Score2Date(date * 10000)
		ddate = ddate.AddDate(0, 0, 1)
	}

	now := time.Now()
	if ddate.After(now) {
		return ""
	}
	return ddate.Format(DateFormart)
}
