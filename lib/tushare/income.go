package tushare

import (
	"github.com/kere/gno/libs/util"
	"gitee.com/onqee/astock/lib/data/vars"
)

// Income 利润表
type Income struct {
	BaseDownload
}

// NewIncome class
func NewIncome() *Income {
	o := &Income{}
	o.Init(vars.TableIncome, fieldEndDate, o)
	o.Skips = []string{"ann_date", "f_ann_date"}
	return o
}

// Params 指标API的参数
func (b *Income) Params(tsCode string) util.MapData {
	return financeParams(tsCode, b.Table)
}
