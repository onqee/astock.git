package tushare

import (
	"strconv"
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

// IDownload interface{}
type IDownload interface {
	Params(code string) util.MapData
	ValueExec(key string, v interface{}) float64
}

// BaseDownload class
type BaseDownload struct {
	target     IDownload
	Table      string
	Method     string
	DateField  string
	Fields     []string
	Skips      []string
	DataSeries series.A
	TimeDelay  time.Duration

	ValueCheck CheckExec
}

// Init f
func (b *BaseDownload) Init(table, dateField string, a IDownload) {
	b.target = a
	b.Table = table
	b.DateField = dateField
	b.TimeDelay = time.Millisecond * 780

	switch table {
	case vars.TableBalanceSheet:
		b.Method = "balancesheet"
	case vars.TableFinance:
		b.Method = "fina_indicator"
	case vars.TableCashFlow:
		b.Method = "cashflow"
	case vars.TableIncome:
		b.Method = "income"
	case vars.TableShareBonus:
		b.Method = "dividend"
	default:
		b.Method = table
	}
}

// Sync 下载利润表
func (b *BaseDownload) Sync(tsCodes []string) map[string]error {
	l := len(tsCodes)
	errs := make(map[string]error)
	for i := 0; i < l; i++ {
		err := b.DownloadAndSave(tsCodes[i])
		if err != nil {
			errs[tsCodes[i]] = err
		}
	}
	return errs
}

// Download 下载指标
func (b *BaseDownload) Download(tsCode string) (series.A, error) {
	args := b.target.Params(tsCode)
	if len(args) == 0 {
		return series.A{}, myerr.New("params is empty")
	}

	// args := util.MapData{ts_code: tscode, "period": period}
	if _, isok := args[fieldTsCode]; isok {
		code := args.String(fieldTsCode)
		args[fieldTsCode] = tuShareCode(code[:2], code[2:])
	}

	// 每秒100次
	rec, err := Send(GetToken(), b.Method, args, b.Fields, b.TimeDelay)
	if err != nil {
		return series.A{}, err
	}

	l := len(rec.Data.Items)
	if l == 0 {
		log.App.Info("[empty download]", b.Table, tsCode, args)
		return series.A{}, nil
	}
	log.App.Info("downloaded:", l, b.Table, tsCode, args)

	return rec.Data.ToSeriesWithFunc(b.DateField, b.Skips, b.ValueCheck, b.target.ValueExec)
}

// ValueExec 每条数据的执行func
func (b *BaseDownload) ValueExec(key string, v interface{}) float64 {
	switch v.(type) {
	case float64:
		return v.(float64)
	case string:
		val, _ := strconv.ParseFloat(v.(string), 64)
		return float64(val)
	}
	return 0
}

// DownloadAndSave 下载并且保存
func (b *BaseDownload) DownloadAndSave(tsCode string) error {
	s, err := b.Download(tsCode)
	if err != nil {
		return err
	}

	// "comp_type", "report_type"
	return series.SaveSeriesToDB(&s, b.Table, tsCode, nil, true)
}
