package tushare

import (
	"fmt"
	"strings"
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

// NameChange 股票曾用名
type NameChange struct {
	BaseDownload
}

// NewNameChange class
func NewNameChange() *NameChange {
	o := &NameChange{}
	o.Init("namechange", fieldEndDate, o)
	o.Fields = []string{"name", "start_date", "change_reason"}
	// o.Skips = []string{"ann_date", "f_ann_date"}
	return o
}

// Params 指标API的参数
func (b *NameChange) Params(tsCode string) util.MapData {
	return util.MapData{"ts_code": tsCode}
}

// Download 下载指标
func (b *NameChange) Download(tsCode string) (db.DataSet, error) {
	args := b.target.Params(tsCode)
	if len(args) == 0 {
		return db.EmptyDataSet, myerr.New("params is empty")
	}

	// args := util.MapData{ts_code: tscode, "period": period}
	if _, isok := args[fieldTsCode]; isok {
		code := args.String(fieldTsCode)
		args[fieldTsCode] = tuShareCode(code[:2], code[2:])
	}

	// 每秒100次
	rec, err := Send(GetToken(), b.Method, args, b.Fields, b.TimeDelay)
	if err != nil {
		return db.EmptyDataSet, err
	}

	l := len(rec.Data.Items)
	if l == 0 {
		log.App.Info("[empty download]", b.Table, tsCode, args)
		return db.EmptyDataSet, err
	}
	log.App.Info("downloaded:", l, b.Table, tsCode, args)

	items := rec.Data.Items
	l = len(items)
	if l == 0 {
		return db.EmptyDataSet, nil
	}
	for i := 0; i < l; i++ {
		fmt.Println(items[i])
	}

	ds := db.GetDataSet([]string{"code", "date", "stat", "name"})
	row := db.GetRow(4)
	defer db.PutRow(row)
	row[0] = codetool.ToCodeI(tsCode)

	// 最早的记录，起始点
	dd, err := time.Parse(DateFormart, (items[l-1][1]).(string))
	if err != nil {
		return db.EmptyDataSet, err
	}
	name := fixKTLabelName(items[l-1][0].(string))
	last := datescore.Date2Score(dd) / 10000
	prevStat := nameToStat(name) //上一次的状态。如果上一次是ST，当前没有ST，则关键点
	row[1], row[2], row[3] = last, prevStat, name
	ds.AddRow(row)

	for i := l - 2; i > -1; i-- {
		item := items[i]
		dd, _ := time.Parse(DateFormart, (item[1]).(string))
		date := datescore.Date2Score(dd)
		name = fixKTLabelName(item[0].(string))
		if date == last {
			continue
		}
		// 1: 取得日期
		row[1] = date / 10000
		// 2: 取得状态stat
		stat := nameToStat(name)
		row[2] = stat
		// 3: 取得名字name
		row[3] = item[0].(string)

		// 4: 是否插入记录
		// 如果前一prevStat>0，当前stat==0，则当前是关键点
		if stat != prevStat {
			ds.AddRow(row)
			prevStat = stat
		}

		last = date
	}
	count := ds.Len()
	// 最后一条记录更名为最新名称
	if count > 0 {
		ds.Columns[3][count-1] = name
	}

	return ds, err
}

// DownloadAndSave 下载并且保存
func (b *NameChange) DownloadAndSave(code string) error {
	ds, err := b.Download(code)
	defer db.PutDataSet(&ds)
	if err != nil {
		return err
	}
	n := ds.Len()
	if n == 0 {
		return nil
	}
	q := db.NewQuery(vars.TableSTList)
	q.Prepare(true).Select("stat,name")
	upd := db.NewUpdate(vars.TableSTList)
	q.Prepare(true)
	ufields := []string{"stat", vars.FieldName}

	row := ds.GetDBRow()
	defer db.PutRow(row.Values)
	dat := db.GetDataSet(ds.Fields)
	defer db.PutDataSet(&dat)
	urow := db.GetRow(2)
	defer db.PutRow(urow)

	codeI := series.ToCodeI(code)
	for i := 0; i < n; i++ {
		ds.DBRowAt(i, row)

		r, err := q.Where("code=$1 and date=$2", codeI, row.Values[1]).QueryOneP()
		if err != nil {
			db.PutRow(r.Values)
			return err
		}
		// a: 如果Empty, Add insert
		if r.IsEmpty() {
			dat.AddRow(row.Values)
			db.PutRow(r.Values)
			continue
		}
		name := row.StringAt(3)

		// 如果下载的名称，状态与数据库中储存的不同，则更新名称
		// row: "code", "date", "stat", "name"
		if r.IntAt(0) != row.IntAt(2) || r.StringAt(1) != row.StringAt(3) {
			// b: Add insert
			urow[0], urow[1] = row.IntAt(2), name
			_, err := upd.Where("code=$1 and date=$2", codeI, row.Values[1]).Update(ufields, urow)
			if err != nil {
				db.PutRow(r.Values)
				return err
			}
		}
		db.PutRow(r.Values)
	}

	if dat.Len() > 0 {
		ins := db.NewInsert(vars.TableSTList)
		_, err = ins.InsertM(&dat)
	}

	if err != nil {
		return fmt.Errorf("%s:%s", code, err.Error())
	}
	return nil
}

func fixKTLabelName(name string) string {
	substr := "Ａ"
	if strings.Index(name, substr) != -1 {
		name = strings.ReplaceAll(name, substr, "A")
	}
	return name
}

// sz000719
// [中原传媒 20180115 改名]
// [大地传媒 20111202 其他]
// [*ST鑫安 20110627 完成股改]
// [S*ST鑫安 20070508 *ST]
// [SST鑫安 20061009 未股改加S]
// [ST鑫安 20051114 ST]
// [焦作鑫安 20010611 改名]
// [焦作碱业 19970331 其他]
// sz000918
// [嘉凯城 20091020 摘星改名]
// [*ST亚华 20090430 完成股改]
// [S*ST亚华 20090429 *ST]
// [SST亚华 20080310 摘星]
// [S*ST亚华 20061009 未股改加S]
// [*ST亚华 20060301 *ST]
// [亚华控股 20060209 改名]
// [亚华种业 19990720 其他]
func nameToStat(name string) int {
	index := strings.Index(name, "ST")
	stat := 0
	if index != -1 {
		if index > 0 && name[index-1] == '*' {
			stat = 2
		} else {
			stat = 1
		}
	}
	return stat
}
