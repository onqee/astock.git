package tushare

import (
	"time"

	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

// Finance 财务指标
type Finance struct {
	BaseDownload
}

// NewFinance class
func NewFinance() *Finance {
	o := &Finance{}
	o.Init(vars.TableFinance, fieldEndDate, o)
	o.Skips = []string{"ann_date"}
	return o
}

func financeParams(tsCode, table string) util.MapData {
	market := tsCode[:2]
	code := tsCode[2:]
	now := time.Now()

	startDate := getStartDateStr(table, market, code)
	if startDate == "" {
		return nil
	}

	return util.MapData{"ts_code": tsCode, "start_date": startDate, fieldEndDate: now.Format(DateFormart)}
}

// Params 指标API的参数
func (b *Finance) Params(tsCode string) util.MapData {
	return financeParams(tsCode, b.Table)
}

// // ValueExec 每条数据的执行func
// func (b *Finance) ValueExec(key string, v interface{}) float64 {
//
// }
