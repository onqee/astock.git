package tushare

import (
	"github.com/kere/gno/libs/util"
	"gitee.com/onqee/astock/lib/data/vars"
)

// ShareBonus 分红送股
type ShareBonus struct {
	BaseDownload
}

// NewShareBonus class
func NewShareBonus() *ShareBonus {
	o := &ShareBonus{}
	o.Skips = []string{"div_proc"}
	// ex_date:除权日期		div_proc:实施进度		stk_div:每股转送	stk_bo_rate:每股转送比例	stk_co_rate:每股转赠比例
	// cash_div:每股分红	cash_div_tax:每股分红（税前）
	o.Fields = []string{"ex_date", "div_proc", "stk_div", "cash_div"}
	o.Init(vars.TableShareBonus, "ex_date", o)

	fieldDivProc := "div_proc"
	vDivProc := "实施"
	o.ValueCheck = func(fields []string, dat []interface{}) bool {
		l := len(fields)
		for i := 0; i < l; i++ {
			if fields[i] == fieldDivProc {
				if dat[i].(string) == vDivProc {
					return true
				}
			}
		}
		return false
	}
	return o
}

// Params 指标API的参数
func (b *ShareBonus) Params(tsCode string) util.MapData {
	return util.MapData{"ts_code": tsCode}
}

// ValueExec 每条数据的执行func
// func (b *ShareBonus) ValueExec(key string, v interface{}) float64 {
// 	// switch v.(type) {
// 	// case float64:
// 	// 	return v.(float64)
// 	// case string:
// 	// 	val, _ := strconv.ParseFloat(v.(string), 64)
// 	// 	return float64(val)
// 	// }
// 	fmt.Println(key, v)
// 	return 0
// }
