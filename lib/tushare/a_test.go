package tushare

import (
	"testing"

	"github.com/kere/gno"
	"github.com/kere/gno/db"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
}

func TestA(t *testing.T) {
	// a := NewShareBonus()
	// dat, err := a.Download("sz000012")
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if dat.Len() == 0 {
	// 	t.Fatal()
	// }
	//
	// a = NewShareBonus()
	// a.Fields = nil
	// dat, err = a.Download("sh600828")
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if dat.Len() == 0 {
	// 	t.Fatal()
	// }

	// f := NewFinance()
	// dat, err := f.Download("sz000938") // 紫光股份
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if dat.Len() == 0 {
	// 	t.Fatal("sz000938")
	// }
	//
	// f = NewFinance()
	// dat, err = f.Download("sz000655") //金岭矿业
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if dat.Len() == 0 {
	// 	t.Fatal("sz000655")
	// }
	// dat.Print()
	code := "sz000005"
	n := NewNameChange()
	ds, err := n.Download(code)
	if err != nil {
		t.Fatal(err)
	}
	if ds.Len() == 0 {
		t.Fatal()
	}

	row := ds.RowAtP(ds.Len() - 1)
	if row[1].(int64) != 1210506 || row[2].(int) != 1 {
		db.PrintDataSet(&ds)
		t.Fatal()
	}
	ds.RowAt(0, row)
	if row[1].(int64) != 901210 || row[2].(int) != 0 {
		db.PrintDataSet(&ds)
		t.Fatal()
	}

	// err = n.DownloadAndSave(code)
	// if err != nil {
	// 	db.PrintDataSet(&ds)
	// 	t.Fatal(err)
	// }
}
