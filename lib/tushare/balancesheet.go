package tushare

import (
	"github.com/kere/gno/libs/util"
	"gitee.com/onqee/astock/lib/data/vars"
)

// BalanceSheet 利润表
type BalanceSheet struct {
	BaseDownload
}

// NewBalanceSheet class
func NewBalanceSheet() *BalanceSheet {
	o := &BalanceSheet{}
	o.Init(vars.TableBalanceSheet, fieldEndDate, o)
	o.Skips = []string{"ann_date", "f_ann_date"}
	return o
}

// Params 指标API的参数
func (b *BalanceSheet) Params(tsCode string) util.MapData {
	return financeParams(tsCode, b.Table)
}
