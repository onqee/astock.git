package tushare

import (
	"github.com/kere/gno/libs/util"
	"gitee.com/onqee/astock/lib/data/vars"
)

// Cashflow 现金流量表
type Cashflow struct {
	BaseDownload
}

// NewCashflow class
func NewCashflow() *Cashflow {
	o := &Cashflow{}
	o.Init(vars.TableCashFlow, fieldEndDate, o)
	o.Skips = []string{"ann_date", "f_ann_date"}
	return o
}

// Params 指标API的参数
func (b *Cashflow) Params(tsCode string) util.MapData {
	return financeParams(tsCode, b.Table)
}
