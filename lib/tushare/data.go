package tushare

import (
	"time"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"github.com/kere/gno/db"
	"github.com/kere/gno/libs/log"
	"github.com/kere/gno/libs/myerr"
	"github.com/kere/gno/libs/util"
)

// Recive class
type Recive struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data Data   `json:"data"`
}

// Data class
type Data struct {
	Fields []string        `json:"fields"`
	Items  [][]interface{} `json:"items"`
}

// FieldExec func
type FieldExec func(key string, v interface{}) float64

// CheckExec func
type CheckExec func(keys []string, item []interface{}) bool

// ToSeriesWithFunc f
func (d *Data) ToSeriesWithFunc(dateField string, skips []string, checkValFunc CheckExec, fieldValFunc FieldExec) (series.A, error) {
	keyIdxs := []int{}
	keys := []string{}

	// prepare fields
	l := len(d.Fields)
	k := 0
	dateIdx := -1
	for i := 0; i < l; i++ {
		field := d.Fields[i]
		switch field {
		case codeField:
			continue
		case dateField:
			dateIdx = i
			continue
		}
		if util.InStrings(field, skips) {
			continue
		}

		keys = append(keys, field)
		keyIdxs = append(keyIdxs, i)
	}

	ll := len(keys)
	if dateIdx < 0 {
		return series.A{}, myerr.New("没有相应的 date 时间字段")
	}

	l = len(d.Items)
	// vals := make([]series.Row, l)
	cols := series.NewColumnsN(ll, 0, l)
	dates := make([]int64, 0, l)
	for i := 0; i < l; i++ {
		item := d.Items[i]
		if checkValFunc != nil && !checkValFunc(d.Fields, item) {
			continue
		}

		row := series.NewRow(ll)
		if item[dateIdx] == nil {
			log.App.Info("date value is nil", item)
			continue
		}
		date, err := time.Parse(DateFormart, (item[dateIdx]).(string))
		if err != nil {
			return series.A{}, err
		}
		dates = append(dates, datescore.Date2Score(date))

		for k = 0; k < ll; k++ {
			index := keyIdxs[k]
			if item[index] == nil {
				row[k] = 0
				continue
			}
			row[k] = fieldValFunc(keys[k], item[index])
		}

		// vals[i] = row
		cols.AddRow(row)
	}

	return series.NewA("", dates, cols, keys), nil
}

func (d *Data) ToDataSetP() db.DataSet {
	ds := db.GetDataSet(d.Fields)
	count := len(d.Items)
	for i := 0; i < count; i++ {
		ds.AddRow(d.Items[i])
	}
	return ds
}
