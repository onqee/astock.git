package compare

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno/libs/util"
)

// // PowerP bar作为比较对象，和所有codes进行比较
// func PowerP(codes []string, date int64) series.A {
// 	l := len(codes)
// 	for i := 0; i < l; i++ {
// 		code := codes[i]
// 		bar := dget.Bar(code, vars.PD1)
// 		wparam := wave.DefaultWParam
// 		wave.SmartBarOn(bar, wparam, date, n)
// 	}
// }

// PowerToOnP bar作为比较对象，和所有codes进行比较
func PowerToOnP(bar *bars.Bar, codes []string, date int64) series.A {
	closeI := bar.FieldI(vars.FieldClose)
	ints := util.GetInts(1)
	defer util.PutInts(ints)
	ints[0] = 15
	dat := one2m(bar, codes, bar.Period, date, []string{"d1"}, ints, func(itype int, row []float64, dat1, dat2 *series.A) {
		row[0] = calcPower(dat1.Columns[closeI], dat2.Columns[closeI])
	})

	series.ReverseA(&dat, "d1")
	return dat
}

func calcPower(valsA, valsB []float64) float64 {
	count := len(valsA)
	vals := util.GetFloats(count)
	for i := 1; i < count; i++ {
		a := 100 * (valsA[i] - valsA[i-1]) / valsA[i-1]
		b := 100 * (valsB[i] - valsB[i-1]) / valsB[i-1]
		// vals[i] = a - b
		vals[count-i-1] = b - a
	}
	v := mathlib.Ema(vals, count)
	util.PutFloats(vals)
	return v
}

// func BinPowerFileName(code string) string {
// 	c := gno.GetConfig().GetConf("data")
// 	folder := "power"
// 	if vars.IsTest {
// 		folder = "test/power"
// 	}
// 	return filepath.Join(c.GetString("store_dir"), folder, code+vars.ExtBin)
// }
