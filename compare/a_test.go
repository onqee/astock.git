package compare

import (
	"math"
	"testing"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	vars.IsTest = true
	vars.IsSync = true
	gno.Init("../app.conf")

	dget.DefaultStartDay = 1181001
	dget.DefaultEndDay = 1201201
	dget.DefaultStartDayM5 = 1181001
	dget.DefaultEndDayM5 = 1201201
}

func TestCorr(t *testing.T) {
	bar := dget.Bar("sh000001", vars.PD1)
	//sz399001, 海通证券 安彩高科 北大荒 紫光股份
	codes := []string{"sz399001", "sh600837", "sh600207", "sh600598", "sz000938"}
	date := datescore.DayTdx(11904091500)
	dat := CorrOnP(bar, codes, vars.PD1, date)
	defer series.PutA(&dat)
	row := dat.RowAtP(0)
	if math.Abs(row[1])-0.95850422 > 0.000001 {
		series.Print(&dat)
		t.Fatal()
	}

	codes = []string{"sh600837", "sh600207", "sh600598", "sz000938"}
	toCodes := codetool.CodesBlockTdx()
	doStoreCorr(codes, toCodes, vars.PD1)
}

func TestPower(t *testing.T) {
	codes := []string{"sh600207", "sh600598", "sz000938"}
	bar := dget.Bar(vars.Code399001, vars.PD1)
	dat := PowerToOnP(bar, codes, 12004301500)
	if dat.Len() != 3 || dat.Dates[0] != 2000938 {
		series.Print(&dat)
		t.Fatal()
	}
}
