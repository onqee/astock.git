package compare

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/tools/codetool"
)

var (
// IsTest = false
// IsSync 是否同步单线程
// IsSync = true
)

var (
	compFields = []string{"val", "n30", "n60", "n100"}
	powerAbout series.A
	powerList  []*series.A
)

// 1对多
// ns: 对比时的配置信息
func one2m(bar *bars.Bar, codes []string, period string, date int64, fields []string, ns []int, f func(int, []float64, *series.A, *series.A)) series.A {
	if bar.Len() < 5 {
		return series.EmptyA
	}
	l := len(codes)
	count := len(ns)
	barArr := make([]bars.Bar, count)
	for i := 0; i < count; i++ {
		barArr[i] = bar.RangeL(date, ns[i]-1)
	}
	if barArr[0].Len() < 10 {
		return series.EmptyA
	}

	fn := len(fields)
	result := series.GetA(fields)
	// result.EnableSync()

	row := series.GetRow(fn)
	defer series.PutRow(row)

	for i := 0; i < l; i++ {
		code := codes[i]
		bar0 := dget.Bar(code, period)
		for k := 0; k < count; k++ {
			barX := &barArr[k]
			bar1 := bar0.Range(barX.Dates[0], date)
			arr := series.InnerCutP(&barX.A, &bar1.A)
			if arr[0].Len() > 10 {
				f(ns[k], row, &arr[0], &arr[1])
			}
			series.PutAAs(arr)
		}

		result.AddRow(int64(codetool.ToCodeI(code)), row)
	}

	return result
}
