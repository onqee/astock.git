package compare

import (
	"fmt"
	"path/filepath"
	"sync"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/util"
)

var corrFields = []string{"code1", "v1", "code2", "v2", "code3", "v3"}

// CorrOnP 相似性
func CorrOnP(bar *bars.Bar, codes []string, period string, date int64) series.A {
	closeI := bar.FieldI(vars.FieldClose)
	ints := util.GetInts(3)
	defer util.PutInts(ints)
	ints[0], ints[1], ints[1] = 30, 60, 100
	return one2m(bar, codes, period, date, compFields, ints, func(itype int, row []float64, dat1, dat2 *series.A) {
		switch itype {
		case 30:
			row[1] = mathlib.Corr(dat1.Columns[closeI], dat2.Columns[closeI])
		case 60:
			row[2] = mathlib.Corr(dat1.Columns[closeI], dat2.Columns[closeI])
		case 100:
			row[3] = mathlib.Corr(dat1.Columns[closeI], dat2.Columns[closeI])
			// case 0:
			// 	row[4] = mathlib.Corr(dat1.Columns[closeI], dat2.Columns[closeI])
			row[0] = (row[1] + row[2] + row[3]) / 3
		}
	})
}

// StoreCorr 运行并保存相似性比较
func StoreCorr(bar *bars.Bar, codes []string) error {
	result := series.GetA(corrFields)
	defer series.PutA(&result)
	row := result.GetRowP()
	defer series.PutRow(row)

	l := bar.Len()
	// 每十天运行corrp
	for i := l - 1; i > 0; i -= 10 {
		dat := CorrOnP(bar, codes, bar.Period, bar.Dates[i])
		if dat.Len() < 1 {
			series.PutA(&dat)
			continue
		}
		series.ReverseA(&dat, vars.FieldVal)
		// series.SortA(&dat, vars.FieldVal)

		row[0] = float64(dat.Dates[0])
		row[1] = dat.Columns[0][0]
		row[2] = float64(dat.Dates[1])
		row[3] = dat.Columns[0][1]
		row[4] = float64(dat.Dates[2])
		row[5] = dat.Columns[0][2]
		// row[0] = 18000
		result.AddRow0(bar.Dates[i], row)
		series.PutA(&dat)
	}

	result.Sort()
	return series.WriteBin(&result, BinCorrFileName(bar.Code, bar.Period), 6, 64)
}

func BinCorrFileName(code, period string) string {
	c := gno.GetConfig().GetConf("data")
	folder := "corr"
	if vars.IsTest {
		folder = "test/corr"
	}
	return filepath.Join(c.GetString("store_dir"), folder, period, code+vars.ExtBin)
}

// doStoreCorr
func doStoreCorr(codes, toCodes []string, period string) []error {
	l := len(codes)

	cpt := util.NewComputation(vars.ProcessNum)
	cpt.IsSync = vars.IsSync
	errLocker := sync.Mutex{}
	errs := make([]error, 0, 100)
	cpt.Run(l, func(i int) {
		bar := dget.Bar(codes[i], period)
		defer dget.ReleaseBar(bar.Code, period)
		if err := StoreCorr(bar, toCodes); err != nil {
			errLocker.Lock()
			errs = append(errs, err)
			errLocker.Unlock()
			return
		}
		fmt.Println(i, bar.Code)
	})
	return errs
}
