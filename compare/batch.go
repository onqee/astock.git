package compare

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
)

func BatchCorr() {
	codes := codetool.CodesATdx()
	toCodes := codetool.CodesBlockTdx()
	errs := doStoreCorr(codes, toCodes, vars.PD1)
	l := len(errs)
	for i := 0; i < l; i++ {
		fmt.Println(errs[i])
	}
	toCodes = codetool.CodesIndustryTdx()
	errs = doStoreCorr(codes, toCodes, vars.PD1)
	l = len(errs)
	for i := 0; i < l; i++ {
		fmt.Println(errs[i])
	}
}
