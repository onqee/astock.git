package mval

import (
	"fmt"

	"gitee.com/onqee/astock/fmls"
	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"

	"github.com/kere/gno/libs/util"
)

//  0     1      2     3     4     5      6       7       8
// "w1", "w2", "W2", "w3", "W3", "m30", "m60", "m120", "m240"

// MVal 平均值
type MVal struct {
	fmls.Base
	PressNum  int // 压力线数量
	MinN      int // 最小周期数
	DateRange map[string][2]int64
}

var (
	fieldMVal = []string{"bitstr", "count"}
)

// NewMVal build
func NewMVal() *MVal {
	m := &MVal{MinN: 5, PressNum: 9}
	fields := fieldMVal
	name := "mval-" + fmt.Sprint(m.MinN)
	m.Init(name, fields)
	return m
}

func calcMVal(bar *bars.Bar, wavs []wave.Wav, wi int, modeStr string) (float64, int, bool, int64, int64) {
	mdat, wI := wave.ModeAt(wavs, wi)
	if mdat.IsEmpty() {
		return 0, wi, false, 0, 0
	}

	start := mdat.StartDate()
	end := mdat.EndDate()
	barC := bar.Range(start, end)
	vals := barC.Values(vars.FieldClose)
	return mathlib.Avg(vals), wI, wI > 0, start, end
}

func calcIt(bar *bars.Bar, start, end int64) float64 {
	barC := bar.Range(start, end)
	vals := barC.Values(vars.FieldClose)
	return mathlib.Avg(vals)
}

// Fml 连续结果序列
func (m *MVal) Fml(bar *bars.Bar, dat *series.A) error {
	// return runner.Calc2Fml(bar, dat, m.Calc)
	return nil
}

// Calc 单值公式
func (m *MVal) Calc(bar *bars.Bar, row []float64) error {
	wParam := wave.NewWParam(10, m.MinN)
	// wavs := wave.FmlWavs(bar, wParam)
	wavs := wave.WavsOnP(bar.Code, bar.Period, wParam, bar.LastDate())
	defer wave.PutWavs(wavs)

	dat := series.GetRow(m.PressNum)
	defer series.PutRow(dat)
	var start int64
	var wI int
	var isok bool
	if len(wavs) > 0 {

		dat[0], wI, isok, _, _ = calcMVal(bar, wavs, 0, "w1")
		if !isok {
			dat[0] = 0
		}

		dat[1], wI, isok, start, _ = calcMVal(bar, wavs, wI-1, "w2")
		if !isok {
			dat[1] = 0
		}
	}

	index := bar.Len() - 1
	if index < 0 {
		return nil
	}

	date := bar.Dates[index]

	// darr := [2]int64{dataRange["w2"][0], date}
	dat[2] = calcIt(bar, start, date)

	dat[3], _, isok, start, _ = calcMVal(bar, wavs, wI-1, "w3")
	if !isok {
		dat[3] = 0
	}

	// darr = [2]int64{dataRange["w3"][0], date}
	dat[4] = calcIt(bar, start, date)
	//  0     1      2     3     4     5      6       7       8
	// "w1", "w2", "W2", "w3", "W3", "m30", "m60", "m120", "m240"
	vals := bar.Values(vars.FieldClose)
	l := len(vals)
	dat[5] = mathlib.Avg(util.RangeFloats(vals, l-29, 0))
	dat[6] = mathlib.Avg(util.RangeFloats(vals, l-59, 0))
	dat[7] = mathlib.Avg(util.RangeFloats(vals, l-119, 0))
	dat[8] = mathlib.Avg(util.RangeFloats(vals, l-239, 0))

	c := bar.ValueAt(vars.FieldClose, index)
	high := bar.ValueAt(vars.FieldHigh, index)
	low := bar.ValueAt(vars.FieldLow, index)

	strb := util.GetBytes(9) // len = 9
	defer util.PutBytes(strb)

	// 在均值附近percent%内
	percent := 0.015
	count := 0
	for i := 0; i < 9; i++ {
		if dat[i] == 0 {
			continue
		}
		if !mathlib.NearVal(c, high, low, dat[i], percent) {
			continue
		}
		util.SetBytesMaskTrue(strb, i)
		count++
	}

	row[0] = float64(util.MaskBytes2Int(strb))
	row[1] = float64(count)

	return nil
}

// Score 计算得分 return 类型，得分
func (m *MVal) Score(row []float64) float64 {
	n := row[1]
	if n < 2 {
		return n
	}
	if n < 3 {
		return 1.2
	}

	return 1.5
}
