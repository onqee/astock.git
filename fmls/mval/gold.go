package mval

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"

	"github.com/kere/gno/libs/util"
)

var (
	pressListLog = []float64{0, 0.382, 0.5, 0.618, 1, 1.382, 1.5, 1.618, 2}
	pressList    = []float64{0, 0.333, 0.5, 0.667, 1, 1.333, 1.5, 1.667, 2}
	fieldsGold   = []string{"W", "Wlog", "w", "wlog", "count"}
)

// Gold 黄金比例
// 计算周期 W, w下，普通坐标，和log坐标的压力位，压力位值参看pressList pressListLog
// 是否为压力位，以二进制BitStr 1001010 形式表示
type Gold struct {
	fmlib.Base
	MinN int // 最小周期数
}

// NewGold build
func NewGold() *Gold {
	m := &Gold{MinN: 5}
	name := "gold-" + fmt.Sprint(m.MinN)
	m.Init(name, fieldsGold)
	return m
}

// Fml 连续结果序列
func (m *Gold) Fml(bar *bars.Bar, dat *series.A) error {
	// return fmls.Calc2Fml(bar, dat, m.Calc)
	return nil
}

// Calc 计算黄金比例压力位
func (m *Gold) Calc(bar *bars.Bar, row []float64) error {
	// wavs := wave.FmlWavs(bar, wave.DefaultWParam)
	// fmt.Println(bar.Code, bar.Period, bar.LastDate(), bar.Dates[0])
	wavs := wave.WavsOnP(bar.Code, bar.Period, wave.DefaultWParam, bar.LastDate())
	defer wave.PutWavs(wavs)
	// wave.PrintWavs(wavs)
	if len(wavs) < 5 {
		return nil
	}

	mdat, _ := wave.SelectAtByMode(wavs, 0, vars.WavModeW2)
	if len(mdat.Modes) < 2 {
		return nil
	}
	// mdat.Print()

	m1 := mdat.Modes[0]
	if m1.Mode == "" {
		return nil
	}

	if len(m1.Wavs) < 2 {
		return nil
	}

	count := 0 // 压力位计数
	lastW := m1.Wavs[len(m1.Wavs)-1]
	// gap 为前一波段振幅的1/10
	gap := calgap(lastW.Left.AMP(false) * 0.1)
	val := valPress(false, gap, m1.StartVal(), lastW.Start.Val(), m1.EndVal())
	row[0] = float64(val)
	if val != 0 {
		count++
	}

	gap = calgap(lastW.Left.AMP(true) * 0.1)
	val = valPress(true, gap, m1.StartVal(), lastW.Start.Val(), m1.EndVal())
	row[1] = float64(val)
	if val != 0 {
		count++
	}

	m2 := mdat.Modes[1]

	if m1.Mode == "" || m2.Mode == "" {
		return nil
	}

	gap = calgap(m2.AMP(false) * 0.1)
	if gap < 0.02 {
		gap = 0.02
	}
	val = valPress(false, gap, m2.StartVal(), m2.EndVal(), m1.EndVal())
	row[2] = float64(val)
	if val != 0 {
		count++
	}

	gap = calgap(m2.AMP(true) * 0.1)
	val = valPress(true, gap, m2.StartVal(), m2.EndVal(), m1.EndVal())
	row[3] = float64(val)
	if val != 0 {
		count++
	}

	return nil
}

// Score 计算得分 return 类型，得分
func (m *Gold) Score(row []float64) float64 {
	l := len(row)
	if l == 0 {
		return 0
	}

	n := row[l-1]
	if n < 2 {
		return n
	}
	if n < 3 {
		return 1.2
	}

	return 1.5
}

func calgap(amp float64) float64 {
	if amp < 0.02 {
		return 0.02
	}
	if amp > 0.06 {
		return 0.06
	}
	return amp
}

// 是否在压力位
// gap 偏差范围最大百分比%
// 按照pressList 进行 BitStr 值设置
func valPress(islog bool, gap, vl, vmid, vr float64) int {
	var items []float64
	if islog {
		items = pressListLog
	} else {
		items = pressList
	}

	v := math.Abs(calPress(islog, vl, vmid, vr))

	// gap := 0.02 // %2

	l := len(items)

	b := util.GetBytes(l)

	for i := 0; i < l; i++ {
		per := items[i]

		if per-gap < v && v < per+gap {
			util.SetBytesMaskTrue(b, i)
			break
		}
	}

	uv := util.MaskBytes2Int(b)
	util.PutBytes(b)

	return uv
}

func calPress(islog bool, vl, vmid, vr float64) float64 {
	if islog {
		return math.Log(vr/vmid) / math.Log(vmid/vl)
	}

	return (vr - vmid) / (vmid - vl)
}
