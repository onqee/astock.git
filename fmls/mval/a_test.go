package mval

import (
	"fmt"
	"math"
	"testing"

	"gitee.com/onqee/astock/lib/data/series"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/util"
)

func init() {
	gno.Init("../../app.conf")
	codetool.DataFolder = "../../"

	dget.DefaultStartDay = 1150101
	dget.DefaultEndDay = 1190802
}

func TestMVal(t *testing.T) {
	date := int64(11906050000)
	period := vars.PD1
	barDat := dget.Bar(vars.Code399001, period)
	bar := barDat.Range(0, date)

	mv := NewMVal()
	row := series.GetRow(len(mv.Fields()))
	defer series.PutRow(row)
	err := mv.Calc(&bar, row)
	if err != nil {
		t.Fatal(err)
	}

	v := int(row[0])
	// 110000001
	if !util.IsMaskTrueAt(v, 8) || !util.IsMaskTrueAt(v, 7) {
		for k, d := range mv.DateRange {
			fmt.Println(k, d)
		}
		//  0     1     2      3     4     5      6       7       8
		// "w1", "w2", "W2", "w3", "W3", "m30", "m60", "m120", "m240"
		t.Fatal(row[0])
	}

}

func TestGold(t *testing.T) {
	v := calPress(false, 10, 100, 55)
	if v != -0.5 {
		t.Fatal(v)
	}
	v = calPress(true, 10, 100, 31.6227)
	if math.Abs(v)-0.5 > 0.001 {
		t.Fatal(v)
	}

	u := valPress(false, 0.02, 10, 100, 55)
	if !util.IsMaskTrueAt(u, 2) {
		t.Fatal()
	}
	u = valPress(true, 0.02, 10, 100, 31.8227)
	if !util.IsMaskTrueAt(u, 2) {
		t.Fatal()
	}

	code := vars.Code399001
	date := int64(11906050000)
	period := vars.PD1
	barDat := dget.Bar(code, period)
	bar := barDat.Range(0, date)

	mv := NewGold()
	row := series.GetRow(len(mv.Fields()))
	defer series.PutRow(row)
	// row, err := runner.CalcOn(mv, bar, date)
	err := mv.Calc(&bar, row)
	if err != nil {
		t.Fatal(err)
	}

	if !util.IsMaskTrueAt(int(row[0]), 2) {
		t.Fatal(row)
	}

	date = int64(11906060000)
	// row, err = runner.CalcOn(mv, bar, date)
	bar = barDat.Range(0, date)
	err = mv.Calc(&bar, row)
	if err != nil {
		t.Fatal(err)
	}
	if !util.IsMaskTrueAt(int(row[1]), 2) {
		t.Fatal(util.MaskStr(int(row[1])))
	}
	if !util.IsMaskTrueAt(int(row[2]), 1) {
		t.Fatal(util.MaskStr(int(row[2])))
	}
}
