package tdd

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
)

func init() {
	gno.Init("../../app.conf")

	dget.DefaultStartDay = 1150101
	dget.DefaultEndDay = 1191102
}

func TestTDD(t *testing.T) {
	date := int64(11909061500)
	period := vars.PD1
	bar := dget.Bar(vars.Code399001, period)

	barDat := bar.Range(0, date)

	tdd := NewTDD()
	row := series.GetRow(1)
	err := tdd.Calc(&barDat, row)
	if err != nil {
		t.Fatal(err)
	}
	if row[0] != 9 {
		t.Fatal(row)
	}

	date = int64(11909091500)
	barDat = bar.Range(0, date)
	tdd.Calc(&barDat, row)
	if row[0] != 10 {
		t.Fatal(row)
	}

	date = int64(11905091500)
	barDat = bar.Range(0, date)
	tdd.Calc(&barDat, row)
	if row[0] != -11 {
		t.Fatal(row)
	}

	date = int64(11905071500)
	barDat = bar.Range(0, date)
	tdd.Calc(&barDat, row)
	if row[0] != -9 {
		t.Fatal(row)
	}
}
