package tdd

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"github.com/kere/gno/libs/util"
)

// TDD 迪马克序列
type TDD struct {
	fmlib.Base
	N int
}

// NewTDD build
func NewTDD() *TDD {
	m := &TDD{N: 4}
	name := "tdd-" + fmt.Sprint(m.N)
	m.Init(name, vars.FieldsVal1, fmlib.ScoreTypeTime)
	return m
}

// Fml 连续结果序列
func (m *TDD) Fml(bar *bars.Bar, dat *series.A) error {
	l := bar.Len()
	row := m.GetRow()
	defer series.PutRow(row)
	for i := 6; i < l; i++ {
		v := CalcTdd(bar, i, m.N)
		if v != 0 {
			row[0] = float64(v)
			dat.AddRow(bar.Dates[i], row)
		}
	}
	return nil
}

// Calc 单值公式
func (m *TDD) Calc(bar *bars.Bar, row []float64) error {
	l := bar.Len()
	if l < 6 {
		return nil
	}

	v := CalcTdd(bar, l-1, m.N)
	row[0] = float64(v)

	return nil
}

// Score 计算得分 return 类型，得分
func (m *TDD) Score(row []float64) float64 {
	val := row[0]

	score := 0.0
	switch {
	case val > 8:
		score = 1.0
	case val < -8:
		score = -1.0
	}

	return score
}

// CalcTdd 计算TDD序列
func CalcTdd(bar *bars.Bar, index, N int) int {
	closeI := bar.FieldI(vars.FieldClose)
	vals := bar.Columns[closeI]
	l := len(vals)
	if index-N < 0 || index > l-1 {
		return 0
	}

	count := 0
	if vals[index-N] <= vals[index] {
		count++
	} else if vals[index-N] >= vals[index] {
		count--
	}

	for i := index - 1; i > 0; i-- {
		if i-N < 0 {
			break
		}
		// 上升
		if count > 0 {
			if vals[i-N] <= vals[i] {
				count++ // 连续大于，计数++
				continue
			} else {
				// 连续被打破，连续上涨破坏
				break
			}
		} else {
			// 下跌
			if vals[i-N] >= vals[i] {
				count--
				continue
			} else {
				// 连续被打破
				break
			}
		}
	}

	if util.Abs(count) < 9 {
		return 0
	}

	return count
}
