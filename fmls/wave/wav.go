package wave

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
)

// Point 趋势线的关键点
// [3]{date, x, y}
type Point [3]float64

// NewPoint func
func NewPoint(date int64, x int, y float64) Point {
	return [3]float64{float64(date), float64(x), y}
}

// Date 返回点的日期
func (a Point) Date() int64 {
	return int64(a[0])
}

// Index value
func (a Point) Index() int {
	return int(a[1])
}

// Val value
func (a Point) Val() float64 {
	return a[2]
}

// Wav class
type Wav struct {
	IType     int   // 1 顶点  -1 底点
	Start     Point // 波浪起始点
	End       Point
	ValueMode int // 0: 默认Close  1:CloseHighLow，进行波浪定型

	Seq int // 点连续新低或新高波段计数

	// Left  *Wav // 前一波
	// Right *Wav
	Left *Wav
}

// NewWav new
func NewWav(bar *bars.Bar, itype, endI int, last *Wav, valueMode int) Wav {
	w := Wav{
		IType:     itype,
		ValueMode: valueMode,
		Left:      last,
	}
	w.SetEndI(bar, endI)
	if last == nil {
		w.SetStartI(bar, 0)
	} else {
		w.Start = last.End
	}
	w.Calculate(last)
	return w
}

func valueModeField(itype, mode int) string {
	if mode == 0 {
		return vars.FieldClose
	}
	if itype == 1 {
		return vars.FieldHigh
	}
	return vars.FieldLow
}

// SetStartI 设置开始索引
func (w *Wav) SetStartI(bar *bars.Bar, i int) {
	row := bar.GetRowP()
	bar.RowAt(i, row)
	w.Start = NewPoint(bar.Dates[i], i, row.ValueOf(valueModeField(w.IType, w.ValueMode), bar.Fields))
	series.PutRow(row)
}

// SetEndI 设置开始索引
func (w *Wav) SetEndI(bar *bars.Bar, i int) {
	row := bar.GetRowP()
	bar.RowAt(i, row)
	w.End = NewPoint(bar.Dates[i], i, row.ValueOf(valueModeField(w.IType, w.ValueMode), bar.Fields))
	series.PutRow(row)
}

// Calculate 计算波浪的数据，例如：波浪连续计数点
func (w *Wav) Calculate(left *Wav) {
	// w.Amp = util.Round(100*(w.End.Val()-w.Start.Val())/w.Start.Val(), series.DefaultDecimal)

	if left == nil {
		return
	}
	w.Left = left

	if left.Left == nil {
		return
	}

	ppVal := left.Left.End.Val()
	// w.SameR = util.Round(100*(w.End.Val()-ppVal)/ppVal, series.DefaultDecimal)

	// 连续新高
	if ppVal < w.End.Val() {
		if left.Left.Seq > 0 {
			w.Seq = left.Left.Seq + 1
		} else {
			w.Seq = 1
		}
	} else { // 连续新低
		if left.Left.Seq < 0 {
			w.Seq = left.Left.Seq - 1
		} else {
			w.Seq = -1
		}
	}
}

// AMP 振幅
func (w *Wav) AMP(islog bool) float64 {
	bv := w.Start.Val()
	if islog {
		return math.Log(w.End.Val() / bv)
	}
	return (w.End.Val() - bv) / bv
}

// Print some
func (w *Wav) Print() {
	fmt.Println("Wav:", w.IType)
	fmt.Println(w.Start.Date(), " - ", w.End.Date())
	fmt.Println()
}

// PrintPoints print
func PrintPoints(items []Point) {
	arr := make([]string, len(items))
	for i, item := range items {
		arr[i] = fmt.Sprint(item.Date())
	}
	fmt.Println(arr)
}

// PrintWavs print
func PrintWavs(wavs []Wav) {
	n := len(wavs)
	fmt.Println("i\tdate\titype\tseq")
	for i := 0; i < n; i++ {
		wav := wavs[i]
		fmt.Printf("%d\t%d\t%d\t%d\n", i, wav.End.Date(), wav.IType, wav.Seq)
	}
	fmt.Println()
}
