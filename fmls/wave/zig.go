package wave

// FmlZig return zig
func FmlZig(vals, high, low []float64, points []Wav) []float64 {
	l := len(points)
	var p Wav
	var endI, startI int

	arr := make([]float64, len(vals))

	arr[0] = vals[0]

	// 通过波峰波谷点，补足数据
	for i := 0; i < l; i++ {
		p = points[i]
		startI = p.Start.Index()
		endI = p.End.Index()
		typ := p.IType
		if typ == 0 {
			typ = p.Left.IType * -1
		}
		if typ > 0 { //上涨波
			arr[endI] = high[endI]    // 高点赋值
			arr[startI] = low[startI] // 低点赋值
		} else { //下跌波
			arr[endI] = low[endI]      // 低点赋值
			arr[startI] = high[startI] // 低点赋值
		}

		count := endI - startI
		avg := (arr[endI] - arr[startI]) / float64(count)

		for k := 1; k < count; k++ {
			arr[startI+k] = arr[startI] + avg*float64(k)
		}

	}

	return arr
}

// // BuildZig z字转向
// func BuildZig(bar *bars.Bar, vals []float64, percent float64, minN int) []float64 {
// 	l := len(vals)
// 	result := make([]float64, l)
// 	points := FmlWavePoints(bar, minN, percent)
// 	ll := len(points)
// 	if ll == 0 {
// 		return result
// 	}
// 	result[0] = vals[0]
//
// 	var p WavPoint
// 	var k int
// 	var step float64
// 	// 填充中间的数值
// 	for i := 0; i < ll; i++ {
// 		p = points[i]
// 		result[p.End.Index()] = p.EndVal
// 		l = p.End.Index() - p.Start.Index()
// 		// step = (vals[p.Index] - vals[p.Start.Index()]) / float64(p.Index-p.Start.Index())
// 		step = (p.EndVal - p.StartVal) / float64(p.End.Index()-p.Start.Index())
// 		for k = 1; k < l; k++ {
// 			result[p.End.Index()-k] = p.EndVal - float64(k)*step
// 		}
// 	}
// 	return result
// }
