package wave

// 从后往前计算波浪
// 因为缓存方案，导致该方法作废

// func TestWave2(t *testing.T) {
// 	code := "sz399001"
// 	period := "d1"
// 	barDat := bars.NewBar(code, period)
// 	barDat.StartDay = 1160101
// 	barDat.EndDay = 1191101
// 	err := barDat.Load()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	bar := barDat.Range(0, 11811010000)
//
// 	params := NewWParam(5, 5)
//
// 	wavs := FmlWavs2(&bar, params)
// 	l := len(wavs)
// 	// for i := 0; i < l; i++ {
// 	// 	p := &wavs[i]
// 	// 	fmt.Println(i, p.Start.Date(), p.End.Date(), p.End[1]-p.Start[1], ":", p.Seq, p.IType)
// 	// }
//
// 	p := wavs[l-3]
// 	if bar.Dates[p.End.Index()] != 11809260000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	if p.Right.End.Date() != 11810180000 || p.Left.End.Date() != 11809170000 {
// 		t.Fatal(p.Left.End.Date(), p.Right.End.Date())
// 	}
//
// 	p = wavs[l-4]
// 	if bar.Dates[p.End.Index()] != 11809170000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-5]
// 	if bar.Dates[p.End.Index()] != 11807240000 || p.IType != 1 || p.Seq != -3 {
// 		t.Fatal(p.End.Date(), p.IType, p.Seq)
// 	}
// 	p = wavs[l-6]
// 	if bar.Dates[p.End.Index()] != 11807050000 || p.IType != -1 || p.Seq != -2 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-11]
// 	if p.End.Date() != 11711130000 || p.Seq != 1 {
// 		t.Fatal(p.End.Date(), p.Seq)
// 	}
//
// 	fml := NewWave(5)
// 	dat, err := fml.Run(&bar)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	if dat.Dates[dat.Len()-2] != 11810180000 {
// 		series.Print(&dat)
// 		t.Fatal()
// 	}
// 	if dat.Dates[dat.Len()-3] != 11809260000 {
// 		series.Print(&dat)
// 		t.Fatal()
// 	}
//
// 	bar = barDat.Range(0, 11909170000)
// 	params.Percent = 3
// 	wavs = FmlWavs2(&bar, params)
// 	l = len(wavs)
// 	// for i := 0; i < l; i++ {
// 	// 	fmt.Println(i, wavs[i].Start.Date(), wavs[i].End.Date(), wavs[i].IType)
// 	// }
// 	p = wavs[l-2]
// 	if p.End.Date() != 11908090000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-3]
// 	if p.End.Date() != 11907300000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-4]
// 	if p.End.Date() != 11907220000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-5]
// 	if p.End.Date() != 11907020000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-6]
// 	if p.End.Date() != 11906060000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-7]
// 	if p.End.Date() != 11904090000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-8]
// 	if p.End.Date() != 11901030000 {
// 		t.Fatal(p.End.Date())
// 	}
// 	p = wavs[l-9]
// 	if p.End.Date() != 11811190000 {
// 		t.Fatal(p.End.Date())
// 	}
// }
