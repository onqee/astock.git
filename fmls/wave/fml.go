package wave

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
)

// Fields                0,       1,     2,      3
var Fields = []string{"type", "val", "seq", "length"}

// Wave formula
type Wave struct {
	MinN    int     // 最小周期数
	Percent float64 // 波浪百分比
}

// NewWave fml
func NewWave(percent float64) Wave {
	w := Wave{MinN: 4, Percent: percent}
	return w
}

// String build
func (w Wave) String() string {
	return "wav-" + fmt.Sprint(w.Percent) + "-" + fmt.Sprint(w.MinN)
}

// Run build
func (w Wave) Run(bar *bars.Bar) (series.A, error) {
	param := NewWParam(w.Percent, w.MinN)
	wavs := WavsOnP(bar.Code, bar.Period, param, bar.LastDate())
	defer PutWavs(wavs)

	s := series.NewEmpty(w.String(), Fields)
	l := len(wavs)
	n := len(Fields)
	row := series.GetRow(n)
	for i := 0; i < l; i++ {
		err := parseData(wavs, i, n, bar, row)
		if err != nil {
			continue
		}
		s.AddRow(bar.Dates[wavs[i].End.Index()], row)
	}
	series.PutRow(row)

	return s, nil
}

// parseData 解析波形
func parseData(wavs []Wav, i, n int, bar *bars.Bar, row []float64) error {
	p := wavs[i]
	// row := series.NewRow(len(Fields))
	row[0] = float64(p.IType)
	row[1] = p.End.Val()
	row[2] = float64(p.Seq)
	row[3] = float64(p.End.Index() - p.Start.Index())

	return nil
}
