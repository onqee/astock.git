package wave

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
)

// SmartStart n个大波浪周期
func SmartStart(code, period string, n int) (int64, int64) {
	// bar := bars.NewBar(code, period)
	// err := bar.Load()
	// if err != nil {
	// 	panic(err)
	// }
	bar := dget.Bar(code, period)
	index := SmartBarStartIndex(bar, NewWParam(7, 5), 0, n)
	score := bar.Dates[index]
	dd, _ := datescore.ScoreSplit(score)
	return dd, score
}

// SmartBarOn 返回一个通过波浪计算后，最佳计算量的bar
// n: 几个波浪
func SmartBarOn(bar *bars.Bar, wparam WParam, date int64, n int) bars.Bar {
	var index int
	if date == 0 {
		index = 0
	} else {
		var stat int
		index, stat = bar.Search(date)
		if stat == series.IndexOverRangeLeft || stat == series.IndexOverRangeRight {
			return *bar
		}
	}
	return SmartBarAt(bar, wparam, index, n)
}

// SmartBarAt 返回一个通过波浪计算后，最佳计算量的bar
// n: 几个波浪
func SmartBarAt(bar *bars.Bar, wparam WParam, index, n int) bars.Bar {
	if index < 1 {
		index = bar.Len() - 1
	}
	start := SmartBarStartIndex(bar, wparam, index, n)
	if start == 0 {
		return *bar
	}
	return bar.IRange(start, index)
}

// SmartBarStartIndex 开始的索引
func SmartBarStartIndex(bar *bars.Bar, wparam WParam, index, n int) int {
	if n == 0 {
		n = 3
	}
	if index < 1 {
		index = bar.Len() - 1
	}
	mode := fmt.Sprintf("W%d", n)

	step := 300

	for i := 1; i < 6; i++ {
		if index <= i*step {
			return 0
		}
		// 首先取300个周期
		// barA := bar.RangeL(index, i*step)
		a, b := series.OffsetI(index, n, bar.Dates)
		if a < 0 {
			continue
		}
		barA := bar.IRange(a, b)

		wavs := WavsOnP(barA.Code, barA.Period, wparam, barA.LastDate())

		mdat, _ := SelectAtByMode(wavs, 0, mode)

		if len(mdat.Wavs) > 0 {
			if mdat.Wavs[0].Start.Index() < 10 {
				continue
			}
			k, _ := bar.Search(mdat.Wavs[0].Start.Date())
			return k
		}
	}

	return 0
}
