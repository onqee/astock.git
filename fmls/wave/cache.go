package wave

import (
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/cache"
)

var (
	wavsCachedMap *wavsCached
)

// wavs加载器
type wavsCached struct {
	cache.Map
	StartDate, EndDate int64
}

// newWavsCached new
func newWavsCached() *wavsCached {
	m := &wavsCached{}
	m.Init(m, vars.DefaultCacheExpires)
	return m
}

// Build data
func (m *wavsCached) Build(args ...interface{}) (interface{}, error) {
	code := args[0].(string)
	period := args[1].(string)
	bar := dget.Bar(code, period)
	if dget.IsChange(bar) {
		key := m.Key(args...)
		delete(m.Data, key)
	}

	percent := args[2].(float64)
	minN := args[3].(int)
	mode := args[4].(int) // 0: 默认Close  1:CloseHighLow，进行波浪定型

	param := NewWParam(percent, minN)
	param.Mode = mode

	wavs := FmlWavs(bar, param)
	return wavs, nil
}

// WavsOnP 通过缓存+额外计算，获得波浪
func WavsOnP(code, period string, param WParam, date int64) []Wav {
	if wavsCachedMap == nil {
		wavsCachedMap = newWavsCached()
	}

	tmpVal := wavsCachedMap.Get(code, period, param.Percent, param.MinN, param.Mode)
	if tmpVal == nil {
		return nil
	}

	wavs := tmpVal.([]Wav)
	n := len(wavs)

	dates := series.GetDates(n)
	defer series.PutDates(dates)

	for i := 0; i < n; i++ {
		dates[i] = wavs[i].End.Date()
	}

	if date < 1 {
		date = 22212319999
	}
	// 找到当前时间索引
	index, _ := series.Search(date, dates)

	if index < 2 {
		bar := dget.Bar(code, period)
		bar1 := bar.Range(0, date)
		return FmlWavsP(&bar1, param)
	}

	b := index - 1
	if b < 0 {
		b = 0
	}
	dateA := wavs[b].Start.Date()

	// 重新计算B段
	bar := dget.Bar(code, period)
	bar1 := bar.Range(dateA, date)
	if bar1.Len() < 5 {
		return nil
	}
	wavsB := FmlWavsP(&bar1, param)
	defer PutWavs(wavsB)

	// 找到A段
	wavs2 := wavs[:index-1]
	n = len(wavs2)
	// wavsA := make([]Wav, n, n+len(wavsB))
	wavsA := GetWavs(n, n+len(wavsB))
	copy(wavsA, wavs2)

	var last Wav
	if n > 1 {
		last = wavsA[n-1]
	}

	n = len(wavsB)

	// 拼接A，B两端
	isFound := false
	isOK := false
	for i := 0; i < n; i++ {
		if !isOK {
			if last.Start.Date() == wavsB[i].Start.Date() && last.End.Date() == wavsB[i].End.Date() {
				isFound = true
				continue
			}
			if isFound {
				isOK = true
			} else {
				continue
			}
		}
		wavsA = append(wavsA, wavsB[i])
	}

	if !isOK {
		bar := dget.Bar(code, period)
		bar1 := bar.Range(0, date)
		return FmlWavsP(&bar1, param)
	}

	l := len(wavsA)
	index = l - 1 - n
	if index < 1 {
		index = 1
	}

	for i := index; i < l; i++ {
		wavsA[i].Calculate(&wavsA[i-1])
	}

	return wavsA
}
