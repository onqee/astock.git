package wave

import (
	"fmt"
	"math"
	"testing"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
	dget.DefaultStartDay = 1140101
	dget.DefaultEndDay = 1201231
}

func TestB(t *testing.T) {
	code := "sz399001"
	period := "d1"
	bar := bars.NewBar(code, period)
	bar.IsKtDate = true
	bar.StartDay = 1150101
	bar.EndDay = 1190409
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}
	params := NewWParam(10, 5)

	// vals := bar.Values(vars.FieldClose)
	wavs := FmlWavs(&bar, params)
	mdat, _ := ModeAt(wavs, 0)
	if mdat.StartDate() != 11901030000 || mdat.EndDate() != 11904090000 {
		mdat.Print()
		t.Fatal()
	}

	mdat, isok := SelectAtByMode(wavs, 0, "W3")
	if !isok {
		mdat.Print()
		t.Fatal("对399001进行W3模式处理失败")
	}

	if len(mdat.Modes) != 3 {
		mdat.Print()
		t.Fatal("对399001的W3模式modes len=", len(mdat.Modes))
	}

	m := mdat.Modes[0]
	if m.StartDate() != 11901030000 || m.EndDate() != 11904090000 {
		mdat.Print()
		t.Fatal()
	}
	if math.Abs(m.AMP(false)-0.472136) > 0.00001 || math.Abs(m.AMP(true)-0.3867144) > 0.00001 {
		t.Fatal(m.AMP(false), m.AMP(true))
	}

	m = mdat.Modes[2]
	if m.StartDate() != 11601280000 || m.EndDate() != 11711130000 {
		mdat.Print()
		t.Fatal()
	}

	bar = bars.NewBar(code, vars.PM120)
	bar.StartDay = 1150101
	bar.EndDay = 1190509
	bar.Load()
	bar2 := SmartBarOn(&bar, DefaultWParam, 11904091500, 3)
	if bar2.Dates[0] != 11701171130 {
		t.Fatal(bar2.Dates[0])
	}
}

func Test_Wave300577(t *testing.T) {
	// 开润股份 开盘：1171221
	code := "sz300577"
	period := "d1"
	bar := bars.NewBar(code, period)
	bar.IsKtDate = true
	bar.StartDay = 1140101
	bar.EndDay = 1181101
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	param := NewWParam(15, 4)
	wavs := FmlWavs(&bar, param)
	l := len(wavs)
	if wavs[l-2].End.Date() != 11810110000 || wavs[l-3].End.Date() != 11806060000 || wavs[l-4].End.Date() != 11802090000 {
		for i := 0; i < l; i++ {
			w := wavs[i]
			fmt.Println(w.Start.Date(), w.End.Date())
		}
		t.Fatal()
	}

	waveFml := NewWave(10)
	waveFml.MinN = 5
	dat, err := waveFml.Run(&bar)
	if err != nil {
		t.Fatal(err)
	}

	l = dat.Len()
	if l < 10 {
		t.Fatal(l)
	}

	if dat.Dates[l-2] != 11810110000 || dat.Dates[l-3] != 11808270000 {
		series.Print(&dat)
		t.Fatal()
	}

	row := dat.GetRowP()
	dat.RowAt(l-2, row)
	if math.Abs(row[1]-27.52) > 0.01 || row[2] != -3 || row[3] != 4 {
		series.Print(&dat)
		t.Fatal(row)
	}

}

func Test_Wave002926(t *testing.T) {
	code := "sz002926"
	period := "d1"
	bar := bars.NewBar(code, period)
	bar.IsKtDate = true
	bar.StartDay = 1170101
	bar.EndDay = 1181204
	err := bar.Load()
	if err != nil {
		t.Fatal(err)
	}

	params := NewWParam(5, 5)
	points := FmlWavs(&bar, params)
	l := len(points)
	// for i := 0; i < l; i++ {
	// 	fmt.Println(points[i].StartDate, points[i].EndDate)
	// }
	p := points[l-2]
	if p.End.Date() != 11811290000 {
		t.Fatal()
	}

	valsClose := bar.Values("close")
	vals := FmlZig(valsClose, valsClose, valsClose, points)
	l = len(vals)
	for i := 0; i < l; i++ {
		if vals[i] == 0 {
			fmt.Println(bar.Dates[i])
			t.Fatal()
		}
	}
}

func TestGetWavs(t *testing.T) {
	dget.DefaultStartDay = 1181231
	dget.DefaultEndDay = 1191231
	code := "sz399001"
	param := NewWParam(5, 5)
	date := int64(11911210000)
	wavs := WavsOnP(code, vars.PD1, param, date)
	if wavs[7].End.Date() != 11911051500 {
		PrintWavs(wavs)
		t.Fatal()
	}
	if wavs[6].End.Date() != 11909301500 {
		PrintWavs(wavs)
		t.Fatal()
	}
	if wavs[5].End.Date() != 11909091500 {
		PrintWavs(wavs)
		t.Fatal()
	}
	if wavs[4].End.Date() != 11908091500 {
		PrintWavs(wavs)
		t.Fatal()
	}
	PutWavs(wavs)

	dget.DefaultStartDay = 1150101
	dget.DefaultEndDay = 1190802
	date = int64(11906060000)
	wavs = WavsOnP(code, vars.PD1, DefaultWParam, date)

	bar0 := dget.Bar(code, vars.PD1)
	bar := bar0.Range(0, 11906060000)
	wavs2 := FmlWavs(&bar, DefaultWParam)
	n := len(wavs)
	for i := 0; i < n; i++ {
		wav := wavs[i]
		wav2 := wavs2[i]
		if wav.End.Date() != wav2.End.Date() || wav.Seq != wav2.Seq {
			PrintWavs(wavs)
			PrintWavs(wavs2)
			t.Fatal(i)
		}
	}
	PutWavs(wavs)

	date = int64(11803120000)
	wavs = WavsOnP(code, vars.PD1, DefaultWParam, date)
	if wavs[16].End.Date() != 11802091500 || wavs[16].Seq != 4 {
		PrintWavs(wavs)
		t.Fatal()
	}
	PutWavs(wavs)

	dget.DefaultStartDay = 1181231
	dget.DefaultEndDay = 1191231
	code = "sz399001"
	param = NewWParam(5, 5)
	date = int64(11912201500)
	wavs = WavsOnP(code, vars.PD1, param, date)

	l := len(wavs)
	if wavs[l-1].End.Date() != date || wavs[l-2].End.Date() != 11909301500 || wavs[l-3].End.Date() != 11909091500 {
		t.Fatal()
	}
	PutWavs(wavs)
}
