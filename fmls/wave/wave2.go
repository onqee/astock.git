package wave

// 从后往前计算波浪
// 因为缓存方案，导致该方法作废

// // FmlWavs2 计算波浪的关键点
// func FmlWavs2(bar *bars.Bar, param WParam) []Wav {
// 	vals := bar.Values(vars.FieldClose)
// 	n := len(vals)
// 	if n < 3 {
// 		return nil
// 	}
//
// 	warr := make([]Wav, 1, n/22)
// 	index := n - 1
// 	topV := vals[index]
// 	botV := topV
// 	topI, botI := -1, -1
//
// 	warr[0] = Wav{End: NewPoint(bar.Dates[index], index, topV)}
// 	current := &warr[0]
//
// 	for i := n - 2; i > 0; i-- {
// 		val := vals[i]
// 		if topV < val {
// 			topV = val
// 			topI = i
// 		}
// 		if botV > val {
// 			botV = val
// 			botI = i
// 		}
//
// 		if n-1-i <= param.MinN {
// 			continue
// 		}
//
// 		// (最高值-当前值)/val
// 		if 100*(current.End.Val()-val)/val >= param.Percent {
// 			current.IType = 1
// 			current.Start = NewPoint(bar.Dates[topI], topI, topV)
// 			index = i
// 			break
// 		} else if 100*(current.End.Val()-val)/val >= param.Percent {
// 			current.IType = -1
// 			current.Start = NewPoint(bar.Dates[botI], botI, botV)
// 			index = i
// 			break
// 		}
// 	}
//
// 	topV = vals[index]
// 	topI = index
// 	botV = vals[index]
// 	botI = index
//
// 	for i := index; i > -1; i-- {
// 		val := vals[i]
// 		if topV < val {
// 			topV = val
// 			topI = i
// 		}
// 		if botV > val {
// 			botV = val
// 			botI = i
// 		}
//
// 		if current.End.Index()-i <= param.MinN {
// 			continue
// 		}
//
// 		// 找到高低点，确认current,只设置point：start, end
// 		if current.IType == 1 {
// 			// 寻找低点
// 			if botI-i > param.MinN && 100*(val-botV)/val >= param.Percent {
// 				// p波段内，是否还有其他最低点，并且，最低点到高点的距离<MinN
// 				// 399001 1190506 - 1190516 还有最低点 1190509
// 				if isSkipPoint(current, vals, botI, param.MinN) {
// 					i = botI
// 					topI = botI
// 					topV = botV
// 					warr = warr[:len(warr)-1]
// 					current = &warr[len(warr)-1]
// 					continue
// 				}
//
// 				p := NewPoint(bar.Dates[botI], botI, botV)
// 				current.Start = p
// 				warr = append(warr, Wav{IType: -1, End: p})
//
// 				current = &warr[len(warr)-1]
// 				topI = i
// 				topV = val
// 			}
// 		} else {
// 			// 寻找高点
// 			if topI-i > param.MinN && 100*(topV-val)/val >= param.Percent {
// 				if isSkipPoint(current, vals, botI, param.MinN) {
// 					i = topI
// 					botI = topI
// 					botV = topV
// 					warr = warr[:len(warr)-1]
// 					current = &warr[len(warr)-1]
// 					continue
// 				}
//
// 				p := NewPoint(bar.Dates[topI], topI, topV)
// 				current.Start = p
// 				warr = append(warr, Wav{IType: 1, End: p})
//
// 				current = &warr[len(warr)-1]
// 				botI = i
// 				botV = val
// 			}
// 		}
// 	}
//
// 	// revers
// 	l := len(warr)
// 	wavs := make([]Wav, l)
// 	for i := 0; i < l; i++ {
// 		wavs[i] = warr[l-1-i]
// 	}
// 	// 起始点，赋值
// 	wav := &wavs[0]
// 	wav.Start = NewPoint(bar.Dates[0], 0, vals[0])
// 	if l < 3 {
// 		return wavs
// 	}
//
// 	// 设置 wavs 的left,right
// 	wav.Right = &wavs[1]
//
// 	wavs[1].Left = &wavs[0]
// 	wavs[1].Right = &wavs[2]
//
// 	wavs[l-1].Left = &wavs[l-2]
// 	wavs[l-2].Right = &wavs[l-1]
// 	wavs[l-2].Left = &wavs[l-3]
//
// 	for i := 2; i < l-2; i++ {
// 		wav = &wavs[i]
// 		wav.Left = &wavs[i-1]
// 		wav.Right = &wavs[i+1]
// 		prepareSeq(wavs, wav, i)
// 	}
//
// 	prepareSeq(wavs, &wavs[l-2], l-2)
// 	prepareSeq(wavs, &wavs[l-1], l-1)
//
// 	return wavs
// }
//
// func isSkipPoint(wav *Wav, vals []float64, a, minN int) bool {
// 	_, maxI, _, minI := maxminI(vals, a, wav.End.Index())
// 	if wav.IType > 0 { //寻找低点
// 		// 已经确定的波浪，还有低点，并且低点 < minN
// 		if minI > a && wav.End.Index()-minI <= minN {
// 			return true
// 		}
// 	} else {
// 		if maxI > a && wav.End.Index()-maxI <= minN {
// 			return true
// 		}
// 	}
// 	return false
// }
//
// func prepareSeq(wavs []Wav, wav *Wav, i int) {
// 	prev := wavs[i-2]
// 	if prev.End.Val() == wav.End.Val() {
// 		// 如果==，Seq继续
// 		if prev.Seq < 0 {
// 			wav.Seq = prev.Seq - 1
// 		} else {
// 			wav.Seq = prev.Seq + 1
// 		}
// 	} else if prev.End.Val() < wav.End.Val() {
// 		// 上涨，连续高点
// 		if prev.Seq < 0 { // 如果方向相反，则重置计数
// 			wav.Seq = 1
// 		} else {
// 			wav.Seq = prev.Seq + 1
// 		}
// 	} else {
// 		// 下跌 连续低点
// 		if prev.Seq > 0 { // 如果方向相反，则重置计数
// 			wav.Seq = -1
// 		} else {
// 			wav.Seq = prev.Seq - 1
// 		}
// 	}
// }
//
// func maxminI(vals []float64, a, b int) (float64, int, float64, int) {
// 	n := b + 1
// 	maxV := vals[a]
// 	maxI := a
// 	minV := maxV
// 	minI := a
// 	for i := a + 1; i < n; i++ {
// 		val := vals[i]
// 		if maxV < val {
// 			maxV = val
// 			maxI = i
// 		}
// 		if minV > val {
// 			minV = val
// 			minI = i
// 		}
// 	}
// 	return maxV, maxI, minV, minI
// }
