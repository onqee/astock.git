package wave

import (
	"sync"

	"github.com/kere/gno/libs/util"
)

var (
	wavsPool wavsPoolClas
)

type wavsPoolClas struct {
	sync.Pool
	lock sync.Mutex
}

// GetWavs from pool
func GetWavs(args ...int) []Wav {
	l, capN := util.ParsePoolArgs(args, 50)

	wavsPool.lock.Lock()
	defer wavsPool.lock.Unlock()

	v := wavsPool.Get()
	if v == nil {
		return make([]Wav, l, capN)
	}
	row := v.([]Wav)

	for i := 0; i < l; i++ {
		row = append(row, Wav{})
	}
	return row
}

// PutWavs for bit setup
func PutWavs(wavs []Wav) {
	if cap(wavs) == 0 {
		return
	}
	l := len(wavs)
	for i := 0; i < l; i++ {
		wavs[i].Left = nil
	}
	wavsPool.Put(wavs[:0])
}
