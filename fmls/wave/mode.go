package wave

import (
	"errors"
	"fmt"
	"math"
	"sort"
	"strconv"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
	"gitee.com/onqee/astock/lib/stocklib/linear"

	"github.com/kere/gno/libs/util"
)

const (
	// WavU 上涨
	WavU = "U"
	// WavD 下跌
	WavD = "D"
	// WavX 震荡
	WavX = "X"
)

// ModeData 波浪的模型数据
type ModeData struct {
	Wavs  []Wav
	Mode  string
	name  string
	Modes []ModeData // 大波浪下，波浪类型列表
}

// setWavsCopy 添加一个Copy Wavs
func (m *ModeData) setWavsCopy(wavs []Wav) {
	m.Wavs = make([]Wav, len(wavs))
	copy(m.Wavs, wavs)
}

// IsEmpty 是否为空
func (m *ModeData) IsEmpty() bool {
	return len(m.Wavs) == 0
}

// AMP 振幅
func (m *ModeData) AMP(islog bool) float64 {
	l := len(m.Wavs)
	if l == 0 {
		return 0
	}
	if l == 1 {
		return m.Wavs[0].AMP(islog)
	}

	b := m.Wavs[0]
	e := m.Wavs[l-1]

	ev := e.End.Val()
	bv := b.Start.Val()

	if islog {
		return math.Log(ev / bv)
	}
	return (ev - bv) / bv
}

// wavReplacer 可替换，重新组合的
type wavReplacer []Point

// Len l
func (wp wavReplacer) Len() int {
	return len(wp)
}

// Replace 索引
func (wp wavReplacer) Replace(indices []int) mathlib.Replacer {
	result := make(wavReplacer, len(indices))
	for i, idx := range indices {
		result[i] = wp[idx]
	}
	return result
}

// SortedLines 排序，用于确定最近的趋势线
type SortedLines struct {
	Lines  []TrendLine
	Values []float64
}

// Len int
func (t *SortedLines) Len() int {
	return len(t.Lines)
}

// Less sort series
func (t *SortedLines) Less(i, j int) bool {
	// return t.Values[i] < t.Values[j]
	return math.Abs(t.Values[i]) < math.Abs(t.Values[j])
}

// Swap sort series
func (t *SortedLines) Swap(i, j int) {
	t.Values[i], t.Values[j] = t.Values[j], t.Values[i]
	t.Lines[i], t.Lines[j] = t.Lines[j], t.Lines[i]
}

// CalcLastPoint2Lines 结束点到所有趋势线，小于min%的趋势线
func (m *ModeData) CalcLastPoint2Lines(bar *bars.Bar, min float64) SortedLines {
	vals := make([]float64, 0, 10)
	lines := make([]TrendLine, 0, 10)

	// all up dn
	arrAll, arrUP, arrDN := m.CalcLines()
	l := len(arrAll)

	for i := 0; i < l; i++ {
		// 计算Bar最后一点到趋势线的距离
		_, per := calcLast2Line(bar, arrAll[i])
		if math.Abs(per) < min {
			lines = append(lines, arrAll[i])
			vals = append(vals, per)
		}
	}
	l = len(arrUP)
	for i := 0; i < l; i++ {
		_, per := calcLast2Line(bar, arrUP[i])
		if math.Abs(per) < min {
			lines = append(lines, arrUP[i])
			vals = append(vals, per)
		}
	}
	l = len(arrDN)
	for i := 0; i < l; i++ {
		_, per := calcLast2Line(bar, arrDN[i])
		if math.Abs(per) < min {
			lines = append(lines, arrDN[i])
			vals = append(vals, per)
		}
	}
	s := SortedLines{lines, vals}
	sort.Sort(&s)

	return s
}

// 计算Bar最后一点到趋势线的距离
func calcLast2Line(bar *bars.Bar, line TrendLine) (float64, float64) {
	l := bar.Len()
	val := bar.ValueAt(vars.FieldClose, l-1)
	if line.IsLog {
		val = math.Log10(val)
	}
	v := linear.PointToLineY(line.A, line.B, float64(l-line.Points[0].Index()-1), val)
	perc := 100 * v / val

	tmp := make([]string, len(line.Points))
	for i, p := range line.Points {
		dd, _ := datescore.ScoreSplit(p.Date())
		tmp[i] = fmt.Sprint(dd)
	}

	return v, perc
}

// CalcLines 计算所有趋势线
// 趋势线1：all, up, dn
// up：上缘趋势线  dn：下缘趋势线   all：综合趋势线
// 如果高点>2，减1个点的高点排列组合
// 如果低点>2，减1个点的低点排列组合
func (m *ModeData) CalcLines() ([3]TrendLine, []TrendLine, []TrendLine) {
	all, up, dn := splitUpDn(m.Wavs)
	arr := [3]TrendLine{}
	arr[0] = CalcTrendLine(all, false)
	arr[1] = CalcTrendLine(up, false)
	arr[2] = CalcTrendLine(dn, false)

	var arr1, arr2 []TrendLine
	l := len(up)
	if l > 2 {
		items := mathlib.Combinations(wavReplacer(up), l-1, false, 5)
		count := len(items)
		arr1 = make([]TrendLine, count)
		for i := 0; i < count; i++ {
			arr1[i] = CalcTrendLine(([]Point)(items[i].(wavReplacer)), false)
		}
	} else {
		arr1 = make([]TrendLine, 1)
		arr1[0] = CalcTrendLine(up, false)
	}

	l = len(dn)
	if l > 2 {
		items := mathlib.Combinations(wavReplacer(dn), l-1, false, 5)
		count := len(items)
		arr2 = make([]TrendLine, count)
		for i := 0; i < count; i++ {
			arr2[i] = CalcTrendLine(([]Point)(items[i].(wavReplacer)), false)
			// arr2[i*2+1] = CalcTrendLine(([]Point)(items[i].(wavReplacer)), true)
		}
	} else {
		arr2 = make([]TrendLine, 1)
		arr2[0] = CalcTrendLine(dn, false)
		// arr2[1] = CalcTrendLine(dn, true)
	}

	return arr, arr1, arr2
}

// Split 分成上边 和 下边
// return all, up, dn
func splitUpDn(wavs []Wav) ([]Point, []Point, []Point) {
	l := len(wavs)
	if l == 1 {
		w := wavs[0]
		all := []Point{w.End}
		if w.IType > 0 {
			return all, all, nil
		}
		return all, nil, all
	}

	up := make([]Point, 0, l)
	dn := make([]Point, 0, l)
	all := make([]Point, 0, l+1)
	for i := 0; i < l; i++ {
		itype := wavs[i].IType

		if itype == 0 {
			if wavs[i-1].IType > 0 {
				dn = append(dn, wavs[i].End)
			} else {
				up = append(up, wavs[i].End)
			}

		} else if itype > 0 {
			up = append(up, wavs[i].End)
		} else {
			dn = append(dn, wavs[i].End)
		}
		all = append(all, wavs[i].End)

		if i == 0 {
			// 如果 i==0，添加波浪开始点
			if itype > 0 {
				dn = append(dn, wavs[i].Start)
			} else {
				up = append(up, wavs[i].Start)
			}
			all = append(all, wavs[i].Start)
		}
	}
	return all, up, dn
}

// StartDate 开始时间
func (m *ModeData) StartDate() int64 {
	l := len(m.Wavs)
	if l < 1 {
		return 0
	}
	return m.Wavs[0].Start.Date()
}

// StartVal 开始收盘Close
func (m *ModeData) StartVal() float64 {
	l := len(m.Wavs)
	if l < 1 {
		return 0
	}
	return m.Wavs[0].Start.Val()
}

// EndVal 结束
func (m *ModeData) EndVal() float64 {
	l := len(m.Wavs)
	if l < 1 {
		return 0
	}
	return m.Wavs[l-1].End.Val()
}

// EndDate 结束时间
func (m *ModeData) EndDate() int64 {
	l := len(m.Wavs)
	if l < 1 {
		return 0
	}
	return m.Wavs[l-1].End.Date()
}

// BarLen 波浪的bar.len
func (m *ModeData) BarLen() int {
	l := len(m.Wavs)
	if l == 0 {
		return 0
	}
	switch m.Mode[0] {
	case 'w':
		if l == 1 {
			return m.Wavs[0].End.Index() - m.Wavs[0].Start.Index()
		}
		return m.Wavs[l-1].End.Index() - m.Wavs[0].Start.Index()
	default:
		if l > 1 {
			return m.Wavs[l-1].End.Index() - m.Wavs[0].Start.Index()
		} else if l == 1 {
			return m.Wavs[0].End.Index() - m.Wavs[0].Start.Index()
		}
		return 0
	}
}

// IsUP 是上涨
func (m *ModeData) IsUP() bool {
	if m.Mode == "" {
		return false
	}
	name := m.Name()
	return name[0] == 'U'
}

// Name 模式类型
func (m *ModeData) Name() string {
	if m.name != "" {
		return m.name
	}

	wt, _, _ := m.CalcLines()
	var name string
	if wt[0].A > 0 {
		name = WavU
	} else if wt[0].A < 0 {
		name = WavD
	}
	if wt[1].A > 0 {
		name += WavU
	} else if wt[1].A < 0 {
		name += WavD
	}
	if wt[2].A > 0 {
		name += WavU
	} else if wt[2].A < 1 {
		name += WavD
	}
	m.name = name

	return m.name
}

// Print stdin
func (m *ModeData) Print() {
	fmt.Println("Name:", m.Name(), " Mode:", m.Mode)
	l := len(m.Wavs)
	fmt.Println("-- wavs", l)
	for i := 0; i < l; i++ {
		fmt.Println(m.Wavs[i].Start.Date(), m.Wavs[i].End.Date(), m.Wavs[i].End.Val())
	}

	count := len(m.Modes)
	fmt.Println("-- modes", count)
	for i := 0; i < count; i++ {
		mdat := m.Modes[i]
		fmt.Println(mdat.StartDate(), mdat.EndDate())
	}
	fmt.Println()
}

// ModeOn date 选取某个模式的波浪
func ModeOn(wavs []Wav, date int64) (ModeData, error) {
	l := len(wavs)
	for i := l - 1; i > 0; i-- {
		d := wavs[i].End.Date()
		if d == date {
			mdat, _ := ModeAt(wavs, i)
			return mdat, nil
		}
	}
	return ModeData{}, errors.New("date not found in wavs")
}

// ModeAt 选取某个模式的波浪
// 规则：
// 1. 先看当前高低量点的seq连续
// 2. 单边连续，另一边采用趋势线确定方向
// return ModeData, start index of wave
func ModeAt(wavs []Wav, index int) (ModeData, int) {
	var md ModeData
	l := len(wavs)

	if index < 1 {
		index = l - 1
	}
	if l < 4 || index < 3 {
		return md, -1
	}

	var wavH, wavL *Wav
	if wavs[index].IType > 0 || (wavs[index].IType == 0 && wavs[index-1].IType < 0) {
		wavH = &wavs[index]
		wavL = &wavs[index-1]
	} else {
		wavL = &wavs[index]
		wavH = &wavs[index-1]
	}

	md.Mode = "W"
	var startI int

	if wavH.Seq == 0 || wavL.Seq == 0 {
		startI = 0
	} else if (wavH.Seq > 1 && wavL.Seq > 1) || (wavH.Seq == 1 && wavL.Seq > 1) || (wavH.Seq > 1 && wavL.Seq == 1) {
		// 上涨波，寻找低点，以低点为准，右移1
		startI = startAtBySeq(wavL, index, wavs, 1)
	} else if (wavH.Seq < -1 && wavL.Seq < -1) || (wavH.Seq == -1 && wavL.Seq < -1) || (wavH.Seq < -1 && wavL.Seq == -1) {
		// 下跌波，寻找高点，以高点为准
		startI = startAtBySeq(wavH, index, wavs, 1)
	} else if util.Abs(wavH.Seq) == 1 && util.Abs(wavL.Seq) == 1 {
		// 锯齿形状
		md.Mode = "m"
		// 寻找seq ！= 1的索引
		for k := index - 2; k > -1; k-- {
			if util.Abs(wavs[k].Seq) != 1 {
				startI = k + 1
				break
			}
		}
	} else if wavH.Seq == 1 && wavL.Seq < -1 {
		// W形状，高点=1，低点连续Seq< -1
		// 如果大于前面波浪的高点，则Seq1波浪有效
		if wavH.End.Val() > wavH.Left.Left.End.Val() {
			startI = index
		} else {
			startI = startAtBySeq(wavL, index, wavs, 0)
		}

	} else if wavL.Seq == -1 && wavH.Seq > 1 {
		// M形状，其中有一个波Seq=1
		if wavL.End.Val() < wavL.Left.Left.End.Val() {
			startI = index
		} else {
			startI = startAtBySeq(wavH, index, wavs, 0)
		}

	} else {
		// < 渐大波，寻找最短seq，如果相等，以低点为准
		// > 渐小波，寻找最短seq
		if util.Abs(wavH.Seq) >= util.Abs(wavL.Seq) {
			startI = startAtBySeq(wavL, index, wavs, 0)
		} else {
			startI = startAtBySeq(wavH, index, wavs, 0)
		}
	}

	md.setWavsCopy(wavs[startI : index+1])

	return md, startI
}

// SelectOnByMode 当前的波浪模型长度
// w:波浪单边；S:连续单边计数波浪；W:双边波浪模式
func SelectOnByMode(wavs []Wav, date int64, mode string) (ModeData, bool) {
	l := len(wavs)
	for i := l - 1; i > 0; i-- {
		w := &wavs[i]
		if w.End.Date() == date || (wavs[i-1].End.Date() < date && date < w.End.Date()) {
			return SelectAtByMode(wavs, i, mode)
		}
	}
	return ModeData{}, false
}

// SelectAtByMode 当前的波浪模型长度
// w:波浪单边；S:连续单边计数波浪；W:双边波浪模式
// return bool: 波浪是否完整
func SelectAtByMode(wavs []Wav, index int, mode string) (ModeData, bool) {
	l := len(wavs)
	if l < 2 {
		return ModeData{}, false
	}

	if index < 1 {
		index = len(wavs) - 1
	}

	n := 1
	if len(mode) > 1 {
		tmpN, _ := strconv.ParseInt(mode[1:], 10, 64)
		n = int(tmpN)
	}

	switch mode[0] {
	case 'w': // 小波浪
		k := index - n + 1
		if wavs[l-1].IType == 0 { //如果最后wav.IType=0，则需要在右移一格，去掉wav.IType=0的影响
			k--
		}
		if k < 0 {
			return ModeData{Mode: mode, Wavs: wavs[0 : index+1]}, false
		}
		return ModeData{Mode: mode, Wavs: wavs[k : index+1]}, true

	case 'S':
		f := func(index int) (ModeData, int) {
			for k := index; k > -1; k-- {
				seq := util.Abs(wavs[k].Seq)
				if seq == 1 {
					continue
				}

				k -= seq * 2

				// 上涨波，高点 // 下跌波，低点 // 需要再左挪1格
				if (wavs[k].Seq > 0 && wavs[k].IType > 0) || (wavs[k].Seq < 0 && wavs[k].IType < 0) {
					k--
				}
				return ModeData{Mode: mode, Wavs: wavs[k : index+1]}, k
			}

			return ModeData{}, -1
		}

		k := index
		result := ModeData{Mode: mode}
		for i := 0; i < n; i++ {
			mdat, h := f(k)
			if h < 0 {
				// result.Wavs = wavs[0 : index+1]
				result.setWavsCopy(wavs[0 : index+1])
				return result, false
			}
			k = h
			result.Modes = append(result.Modes, mdat)
		}
		// result.Wavs = wavs[k : index+1]
		result.setWavsCopy(wavs[k : index+1])
		return result, true

	case 'W':
		result := ModeData{Mode: mode}
		wI := index
		for i := 0; i < n; i++ {
			if wI < 1 {
				return result, false
			}
			if i > 0 {
				// 往左移动一，新开始关键点
				wI--
			}
			mdat, h := ModeAt(wavs, wI)
			result.Modes = append(result.Modes, mdat)
			if h < 2 {
				// result.Wavs = wavs[0 : index+1]
				result.setWavsCopy(wavs[0 : index+1])
				return result, false
			}

			wI = h
			// result.Wavs = wavs[wI : index+1]
			result.setWavsCopy(wavs[wI : index+1])
		}

		return result, true

	default:
		return ModeData{}, false
	} // switch end
}

// private function -----------------

// w:目标点；index：当前索引
func startAtBySeq(w *Wav, index int, wavs []Wav, offset int) int {
	i := index
	// 目标点，不在当前索引，当前索引 -1
	if w.End.Date() == wavs[index-1].End.Date() {
		i = index - 1
	}
	a := i - 2*util.Abs(w.Seq) + offset
	if a < 0 {
		a = 0
	}
	return a
}
