package wave

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/lib/stocklib/linear"
)

// TrendLine 趋势线
type TrendLine struct {
	A, B   float64 // y = ax + b
	Length int
	StdDev float64 // 距离趋势线的标准差
	Points []Point // [3]{date, x, y}
	IsLog  bool
}

// PrintTrendLine abc
func PrintTrendLine(line *TrendLine) {
	fmt.Println("TrendLine  A:", line.A, "  B:", line.B, "  Length:", line.Length)
	if line.IsLog {
		fmt.Println("IsLog")
	}
	l := len(line.Points)
	for i := 0; i < l; i++ {
		fmt.Println(line.Points[i].Date(), line.Points[i].Val())
	}
	if l > 0 {
		fmt.Println("end", line.A*float64(line.Points[l-1].Index()-line.Points[0].Index())+line.B)
	}
	fmt.Println()
}

// CalcTrendLine 计算趋势线
func CalcTrendLine(points []Point, isLog bool) TrendLine {
	l := len(points)
	if l < 2 {
		return TrendLine{}
	}

	if l == 2 {
		p := points[0]
		t := points[l-1]

		x := float64(p.Index() - t.Index())
		y := p.Val() - t.Val()
		b := t.Val()
		if isLog {
			y = math.Log10(p.Val()) / math.Log10(t.Val())
			b = math.Log10(b)
		}

		return TrendLine{A: y / x, B: b, Length: int(x), StdDev: 0, Points: points, IsLog: isLog}
	}

	x := make([]float64, l)
	y := make([]float64, l)

	for i := 0; i < l; i++ {
		t := points[i]
		if isLog {
			y[i] = math.Log10(t.Val())
		} else {
			y[i] = t.Val()
		}
		x[i] = float64(t.Index() - points[0].Index())
	}

	a, b := linear.Fitting(x, y)
	dev := linear.StdDev(x, y, a, b)

	return TrendLine{A: a, B: b, Length: points[l-1].Index() - points[0].Index(), StdDev: dev, Points: points, IsLog: isLog}
}
