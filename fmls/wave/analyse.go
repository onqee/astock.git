package wave

import (
	"gitee.com/onqee/astock/lib/data/bars"
)

// Every 遍历每一个时间点
func Every(bar *bars.Bar, wavs []Wav, leftN, rightN int, f func(int, int64, int64)) {
	l := len(wavs)
	for i := l - 1; i > -1; i-- {
		date := wavs[i].End.Date()
		var start, end int64
		if leftN > 0 {
			start, _ = bar.OffsetDate(date, -leftN)
		}
		if rightN > 0 {
			_, end = bar.OffsetDate(date, rightN)
		}

		f(i, start, end)
	}
}
