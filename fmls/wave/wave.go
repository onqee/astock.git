package wave

import (
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
)

// DefaultWParam 默认波浪参数
var DefaultWParam = NewWParam(10, 4)

// WParam 波浪的参数
type WParam struct {
	Percent float64 // 波浪最小幅度
	MinN    int     // 波浪最小间隔周期
	Mode    int     // 0: 默认Close  1:CloseHighLow，进行波浪定型
	// MinWavN int     //自动计算最小bar运算周期时，最少Wav数量
}

// NewWParam 新建参数
func NewWParam(percent float64, minN int) WParam {
	return WParam{percent, minN, 0}
}

// FmlWavs 计算波浪的关键点
func FmlWavs(bar *bars.Bar, param WParam) []Wav {
	return fmlWavs(bar, param, false)
}

// FmlWavsP 计算波浪的关键点
func FmlWavsP(bar *bars.Bar, param WParam) []Wav {
	return fmlWavs(bar, param, true)
}

func fmlWavs(bar *bars.Bar, param WParam, isPool bool) []Wav {
	l := bar.Len()
	if l < 3 {
		return nil
	}
	vals := bar.Values(vars.FieldClose)

	var wavs []Wav
	if isPool {
		wavs = GetWavs()
	} else {
		wavs = make([]Wav, 0, 50)
	}

	pi := 0
	pV := vals[0]

	var last *Wav
	for i := 1; i < l; i++ {
		topI, topV, botI, botV := maxmin(vals, pi, i+1)
		n := len(wavs)
		if n > 0 {
			last = &wavs[n-1]
			if n > 1 {
				last.Left = &wavs[n-2]
			}
		}

		// 向上三角^
		if topV > pV && 100*(topV-vals[i])/vals[i] >= param.Percent {
			if n > 1 && topI-last.End.Index() <= param.MinN {
				if last.Left.End.Val() > topV {
					// 如果，周期小于minN，并且前一高点是最高，则wavs-1，然后跳过
					// 小于最小周期数，
					pi = last.Start.Index()
					pV = last.Start.Val()
					wavs = wavs[:n-1]
					continue
				} else {
					// 如果，周期小于minN，并且当前高点是最高点，则wavs-2，然后append当前wav
					wavs = wavs[:n-2]
				}
			}
			// 最低最高 大于最小值
			p := NewWav(bar, 1, topI, last, 0)
			// p.Calculate(last) //NewWav 已经在NewWav里运算了

			//判断是否需要合并关键点
			if last != nil && p.IType == last.IType {
				p.SetStartI(bar, last.Start.Index())
				// wavs = wavs[:n-1]
			}

			wavs = append(wavs, p)
			// 前进一个点
			pi = topI
			pV = topV

		} else if botV < pV && 100*(vals[i]-botV)/botV >= param.Percent {
			if n > 1 && botI-last.End.Index() <= param.MinN {
				// 如果，周期小于minN
				if last.Left.End.Val() < botV {
					// 并且前一低点是最低，则wavs-1，然后跳过
					pi = last.Start.Index()
					pV = last.Start.Val()
					wavs = wavs[:n-1]
					continue
				} else {
					// 如果，周期小于minN，并且当前低点是最低点，则wavs-2，然后append当前wav
					wavs = wavs[:n-2]
				}
			}
			// 向下三角
			p := NewWav(bar, -1, botI, last, 0)
			// p.StartVal = vals[pi]
			// p.EndVal = botV
			// p.Calculate(last) //NewWav 已经在NewWav里运算了

			//判断是否需要合并关键点
			if last != nil && p.IType == last.IType {
				p.SetStartI(bar, last.Start.Index())
				// wavs = wavs[:n-1]
			}
			wavs = append(wavs, p)
			// 前进一个点
			pi = botI
			pV = botV
		}

	}

	// 添加最后关键点
	ll := len(wavs)
	if ll < 2 {
		return wavs
	}

	last = &wavs[ll-1]

	// 末尾阶段
	if last.End.Index() != l-1 {
		var index int
		var p Wav

		if last.IType > 0 {
			// 插入一个最低点, IType == -2
			index = mathlib.MinI(vals[last.End.Index():l])
			p = NewWav(bar, -2, last.End.Index()+index, last, 0)
			// p.Calculate(bar, last)

		} else {
			// 插入一个最高点, IType == 2
			index = mathlib.MaxI(vals[last.End.Index():l])
			p = NewWav(bar, 2, last.End.Index()+index, last, 0)
			// p.Calculate(bar, last)
		}

		// 必须大于最小周期minN
		if p.End.Index()-last.End.Index() > param.MinN && p.End.Index() < l-param.MinN {
			wavs = append(wavs, p)
		}

		//补足最后一根
		if index != l-1 {
			ll := len(wavs)
			last = &wavs[ll-1]
			// IType == 0
			p := NewWav(bar, 0, l-1, last, 0)
			p.Calculate(last)
			wavs = append(wavs, p)
		}

	}

	return wavs
}

func maxmin(arr []float64, start, end int) (topI int, topV float64, botI int, botV float64) {
	topV = arr[start]
	topI = start
	botV = arr[start]
	botI = start
	for i := start; i < end; i++ {
		if topV < arr[i] {
			topV = arr[i]
			topI = i
		}
		if botV > arr[i] {
			botV = arr[i]
			botI = i
		}
	}
	return topI, topV, botI, botV
}
