package fmls

import (
	"strings"

	"gitee.com/onqee/astock/fmls/macd"
	"gitee.com/onqee/astock/fmls/tdd"
	"gitee.com/onqee/astock/lib/fmlib"
)

func FmlsFactory(params []FmlParam) []fmlib.IFml {
	l := len(params)
	arr := make([]fmlib.IFml, l)
	for i := 0; i < l; i++ {
		arr[i] = FmlFactory(params[i])
	}
	return arr
}

type FmlParam struct {
	Name      string
	IsRequire bool
	Data      map[string]int
}

func FmlFactory(param FmlParam) fmlib.IFml {
	var fml fmlib.IFml
	arr := strings.Split(param.Name, "-")
	switch arr[0] {
	case "tdd":
		tdd := tdd.NewTDD()
		tdd.N = param.Data["N"]
		fml = tdd
	case "macdy":
		macdy := macd.NewMacdy()
		macdy.MinN = param.Data["MinN"]
		macdy.LLT = param.Data["LLT"]
		fml = macdy
	}

	return fml
}
