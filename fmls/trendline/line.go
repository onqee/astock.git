package trendline

import (
	"fmt"
	"sort"

	"gitee.com/onqee/astock/fmls/macd"
	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/fmlib"
)

// TrendLine 趋势线
type TrendLine struct {
	fmlib.Base
	MinN int // 最小周期数
}

// a, b 最优趋势线ax+b
// n 几根趋势线压线
var trendlineFields = []string{"n", "a", "b"}

// NewTrendLine build
func NewTrendLine() *TrendLine {
	m := &TrendLine{MinN: 5}
	name := "trendline-" + fmt.Sprint(m.MinN)
	m.Init(name, trendlineFields, fmlib.ScoreTypeSub)

	return m
}

// Calc 单值公式
func (m *TrendLine) Calc(bar *bars.Bar, row []float64) error {
	param := macd.NewWParam(m.MinN)
	wavs := macd.FmlWavs(bar, param, 0)

	if len(wavs) < 3 {
		return nil
	}

	l := len(wavs)
	if l < 4 {
		return nil
	}

	mdat, _ := wave.SelectAtByMode(wavs[:len(wavs)-1], -1, "W3")
	// mdat.Print()
	n := len(mdat.Modes)

	vals := series.GetRow(0, 3)
	defer series.PutRow(vals)

	sl := &wave.SortedLines{Lines: make([]wave.TrendLine, 0, 3), Values: vals}

	for i := 0; i < n; i++ {
		m := mdat.Modes[i]
		// 小于2%的趋势线
		s := m.CalcLastPoint2Lines(bar, 2)
		if len(s.Lines) == 0 {
			break
		}
		sl.Lines = append(sl.Lines, s.Lines[0])
		sl.Values = append(sl.Values, s.Values[0])
	}

	if len(sl.Lines) == 0 {
		return nil
	}
	sort.Sort(sl)
	line := sl.Lines[0]

	row[0] = float64(len(sl.Lines))
	row[1] = line.A
	row[2] = line.B
	// wave.PrintTrendLine(&line)

	return nil
}

// Score 计算得分 return 类型，得分
func (m *TrendLine) Score(row []float64) float64 {
	score := 0.0
	if row[0] > 0 {
		score = 1 + row[0]/10
	}

	return score
}
