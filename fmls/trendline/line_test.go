package trendline

import (
	"math"
	"testing"

	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"

	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
}

func TestA(t *testing.T) {
	bar := bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1170101
	bar.EndDay = 1190420
	bar.Load()

	// wavs := wave.FmlWavs(&bar, wave.NewWParam(4, 4))
	// l := len(wavs)
	// for i := 0; i < l; i++ {
	// 	fmt.Println(wavs[i].End.Date(), wavs[i].End.Val(), wavs[i].End.Index())
	// }
	// l := bar.Len()
	// for i := 0; i < l; i++ {
	// 	fmt.Println(bar.Dates[i], bar.Columns[0][i])
	// }

	points := []wave.Point{
		wave.Point{1180522, 335, 10765.81},
		wave.Point{1180724, 379, 9465.8},
		wave.Point{1180828, 404, 8733.75},
		wave.Point{1180926, 424, 8420.54},
	}

	tline := wave.CalcTrendLine(points, false)
	if math.Abs(tline.A+27.04005886) > 0.00001 {
		t.Fatal(tline)
	}
	if math.Abs(tline.B-10711.9979726) > 0.00001 {
		t.Fatal(tline)
	}

	barDat := bar.Range(11704011500, 11809261500)

	c := NewTrendLine()
	row := series.GetRow(len(c.Fields))
	defer series.PutRow(row)
	c.Calc(&barDat, row)
	if math.Abs(row[1]+15.21530679) > 0.00001 {
		t.Fatal(row)
	}

	if row[0] != 1 {
		t.Fatal(row)
	}

}
