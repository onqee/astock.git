package macd

import (
	"testing"

	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
}

func Test399001(t *testing.T) {
	bar := bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1160101
	bar.EndDay = 1190409
	bar.Load()

	// vals := bar.Values(vars.FieldClose)
	// diff, dea := stocklib.MACD(vals, 12, 26, 9)
	param := NewWParam(5)
	wavs := FmlWavs(&bar, param, 0)
	l := len(wavs)
	// for i := 0; i < l; i++ {
	// 	w := wavs[i]
	// 	fmt.Println(w.IType, bar.Dates[w.Start.Index()], bar.Dates[w.End.Index()])
	// }

	if wavs[l-2].End.Date() != 11903271500 || wavs[l-3].End.Date() != 11903121500 {
		t.Fatal()
	}
	if wavs[l-4].End.Date() != 11901041500 || wavs[l-5].End.Date() != 11811161500 {
		t.Fatal()
	}
	if wavs[l-6].End.Date() != 11810191500 || wavs[l-7].End.Date() != 11809261500 || wavs[l-8].End.Date() != 11809181500 {
		t.Fatal()
	}
	if wavs[l-9].End.Date() != 11809041500 {
		//重新fix前半区域高点1180828，移到后半部1180904
		t.Fatal()
	}

	md, _ := wave.ModeAt(wavs, 0)
	if md.StartDate() != 11901041500 || md.EndDate() != 11904091500 {
		md.Print()
		t.Fatal()
	}

	date := md.Wavs[0].End.Date()
	md, _ = wave.ModeOn(wavs, date)
	if md.StartDate() != 11901041500 || md.EndDate() != 11903121500 {
		// wave.PrintWavs(wavs)
		// fmt.Println(date)
		md.Print()
		t.Fatal()
	}

	mdat, _ := wave.SelectAtByMode(wavs, 0, "W3")
	if len(mdat.Modes) != 3 || mdat.Modes[1].EndDate() != 11901041500 || mdat.Modes[1].StartDate() != 11711131500 {
		mdat.Print()
		t.Fatal()
	}

	bar = bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1160101
	bar.EndDay = 1191224
	bar.Load()

	wavs = FmlWavs(&bar, DefaultWParam, 10)
	l = len(wavs)
	if wavs[0].End.Date() != 11904221500 {
		t.Fatal()
	}
	if wavs[l-1].Start.Date() != 11911291500 || wavs[l-1].End.Date() != 11912241500 {
		t.Fatal()
	}

	// for i := 0; i < l; i++ {
	// 	w := wavs[i]
	// 	fmt.Println(w.Start.Date(), w.End.Date())
	// }
}

func TestMode(t *testing.T) {
	bar := bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1180130
	bar.EndDay = 1190409
	bar.Load()

	param := NewWParam(5)
	wavs := FmlWavs(&bar, param, 0)
	mdat, isok := wave.SelectAtByMode(wavs, 0, "W")
	if !isok || mdat.StartDate() != 11901041500 || mdat.EndDate() != 11904091500 {
		mdat.Print()
		t.Fatal(isok)
	}

	mdat, isok = wave.SelectAtByMode(wavs, 0, "W2")
	if !isok || mdat.StartDate() != 11803131500 {
		mdat.Print()
		t.Fatal(isok)
	}

	if len(mdat.Modes) != 2 || mdat.Modes[1].EndDate() != 11901041500 {
		t.Fatal()
	}

	mdat, isok = wave.SelectAtByMode(wavs, 0, "w1")
	if !isok || mdat.StartDate() != 11903121500 {
		mdat.Print()
		t.Fatal()
	}

	mdat, isok = wave.SelectAtByMode(wavs, 0, "w2")
	if !isok || mdat.StartDate() != 11901041500 {
		t.Fatal(mdat.StartDate())
	}

	// 周期比较少，只有一个W1 波段
	bar = bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1190103
	bar.EndDay = 1190409
	bar.Load()
	wavs = FmlWavs(&bar, param, 0)
	mdat, isok = wave.SelectAtByMode(wavs, 0, "W2")
	if isok || mdat.StartDate() != 11901031500 {
		mdat.Print()
		t.Fatal(isok)
	}

	bar = bars.NewBar("sh000300", vars.PD1)
	bar.StartDay = 1181030
	bar.EndDay = 1190419
	bar.Load()

	wavs = FmlWavs(&bar, param, 0)
	mdat, isok = wave.SelectAtByMode(wavs, 0, "w2")
	if !isok || mdat.StartDate() != 11903041500 || mdat.EndDate() != 11904191500 {
		mdat.Print()
		t.Fatal()
	}

	// sz399003
	bar = bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1130101
	bar.EndDay = 1190409
	bar.Load()
	wavs = FmlWavs(&bar, param, 0)
	mdat, isok = wave.SelectAtByMode(wavs, 0, "W3")
	if !isok || len(mdat.Modes) != 3 {
		mdat.Print()
		t.Fatal()
	}

}
