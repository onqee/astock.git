package macd

import (
	"strconv"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"gitee.com/onqee/astock/lib/stocklib"
)

// Macdx 交叉
type Macdx struct {
	fmlib.Base
	N1, N2, N3 int // 12,26,9
	LLT        int // 滤波周期参数，0 不进行滤波
}

// NewMacdx fml
func NewMacdx() *Macdx {
	m := &Macdx{LLT: 6, N1: 12, N2: 26, N3: 9}
	name := "macdx-llt" + strconv.FormatInt(int64(m.LLT), 10)

	m.Init(name, vars.FieldsVal2, fmlib.ScoreTypeSub)
	return m
}

// Fml 连续结果序列
func (m *Macdx) Fml(bar *bars.Bar, dat *series.A) error {
	vals := bar.Values(vars.FieldClose)
	if m.LLT > 0 {
		vals = stocklib.LLT(vals, m.LLT)
		defer series.PutColumn(vals)
	}

	diff, dea := stocklib.MACD(vals, m.N1, m.N2, m.N3)
	defer series.PutColumn(diff)
	defer series.PutColumn(dea)
	row := m.GetRow()
	defer series.PutRow(row)

	l := len(vals)
	for i := 0; i < l; i++ {
		v1 := stocklib.Cross(i, diff, dea)
		v2 := stocklib.Inflection(i, diff)
		if v1 == 0 && v2 == 0 {
			continue
		}
		row[0], row[1] = float64(v1), float64(v2)
		dat.AddRow0(bar.Dates[i], row)
	}

	return nil
}

// Calc 单值公式
func (m *Macdx) Calc(bar *bars.Bar, row []float64) error {
	dat := bar.RangeL(-1, m.N1+m.N2+10)
	l := dat.Len()
	if l < 5 {
		return nil
	}

	vals := dat.Values(vars.FieldClose)
	diff, dea := stocklib.MACD(vals, m.N1, m.N2, m.N3)
	defer series.PutColumn(diff)
	defer series.PutColumn(dea)

	i := l - 1
	row[0] = float64(stocklib.Cross(i, diff, dea))
	row[1] = float64(stocklib.Inflection(i, diff))

	return nil
}

// Score 计算得分 return 类型，得分
func (m *Macdx) Score(row []float64) float64 {
	score := row[0]
	if score == 0 {
		score = row[1] / 3
	}
	return score
}
