package macd

import (
	"fmt"

	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
)

// SmartBarOn 返回一个通过波浪计算后，最佳计算量的bar
// n: 几个波浪
func SmartBarOn(bar *bars.Bar, date int64, n int) bars.Bar {
	if date > bar.LastDate() {
		return SmartBarAt(bar, bar.Len()-1, n)
	}
	index, stat := bar.Search(date)
	if stat == series.IndexOverRangeLeft || stat == series.IndexOverRangeRight {
		return *bar
	}
	return SmartBarAt(bar, index, n)
}

// SmartBarAt 返回一个通过波浪计算后，最佳计算量的bar
// n: 几个波浪
func SmartBarAt(bar *bars.Bar, index, n int) bars.Bar {
	if index < 1 {
		index = bar.Len() - 1
	}
	start := SmartBarStartIndex(bar, index, n)
	if start == 0 {
		return *bar
	}
	return bar.IRange(start, index)
}

// SmartBarStartIndex 开始的索引
func SmartBarStartIndex(barDat *bars.Bar, index, n int) int {
	if n == 0 {
		n = 3
	}
	if index < 1 {
		index = barDat.Len() - 1
	}

	mode := fmt.Sprintf("W%d", n)
	bar := barDat.IRange(0, index)

	for i := 1; i < 4; i++ {
		wavs := FmlWavs(&bar, DefaultWParam, i*10)
		mdat, _ := wave.SelectAtByMode(wavs, 0, mode)
		if len(mdat.Wavs) > 0 {
			if mdat.Wavs[0].Start.Index() < 10 {
				continue
			}
			k, _ := bar.Search(mdat.Wavs[0].Start.Date())
			return k
		}

	}

	return 0
}
