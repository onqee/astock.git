package macd

import (
	"gitee.com/onqee/astock/fmls/wave"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/mathlib"
	"gitee.com/onqee/astock/lib/stocklib"
)

// DefaultWParam 默认波浪参数
var DefaultWParam = NewWParam(4)

// 区间的高低点数据
// P1,P2 为macd 红蓝、蓝红 交替区间，有“蓝”区域交集
type mRange struct {
	Stat    int
	MPoints [2]mPoint
	P1      Section
	P2      Section
}

// mPoint
type mPoint struct {
	IType int // -1: 低点； 1：高点
	Val   float64
	Index int
}

// NewWParam 新建波浪参数
func NewWParam(minN int) wave.WParam {
	wp := wave.NewWParam(0, minN)
	wp.Mode = 1
	return wp
}

// FmlWavs 计算波浪的关键点
// MACD当前红，找高点; MACD当前绿，找低点
// 如果，index 在左半部分，则以最后diff拐弯为wav拐点
func FmlWavs(bar *bars.Bar, param wave.WParam, maxPointN int) []wave.Wav {
	valsH := bar.Values(vars.FieldHigh)
	valsL := bar.Values(vars.FieldLow)
	valsC := bar.Values(vars.FieldClose)
	ttl := stocklib.LLT(valsC, 6)
	defer series.PutColumn(ttl)
	diff, dea := stocklib.MACD(ttl, 12, 26, 9)
	defer series.PutColumn(diff)
	defer series.PutColumn(dea)

	// macd 按照macd红角线和蓝角线分段的数据点
	items := CalcPoints(diff, dea, param.MinN, maxPointN)
	count := len(items)
	if count < 3 {
		return nil
	}

	wavs := make([]wave.Wav, 0, count+5)
	var last *wave.Wav
	minLengh := param.MinN
	if minLengh < 3 {
		minLengh = 3
	}

	// items 为时间倒序排列
	for i := count - 1; i > -1; i-- { // 时间从小到大
		p := items[i]

		var index int
		length := p.End - p.Start - 1

		if p.IType > 0 { //macd红，找高点
			index = mathlib.MaxI(valsH[p.Start : p.End+1])

			// 区间长度> minLengh && i>1 ：保证有两个以上的wavs && index不在前半周，因为修正点也会出现在前半周
			if last != nil && length > minLengh && i > 1 && index > length/2 {
				// 修正1，前半周期，有新低点出现
				e := p.End - length/2 + 1
				minV := mathlib.Min(valsL[p.Start:e])
				if minV < last.End.Val() {
					minI := mathlib.MinI(valsL[p.Start:e])
					wavs = wavs[:len(wavs)-1]
					if len(wavs) == 0 {
						last = nil
					} else {
						last = &wavs[len(wavs)-1]
					}
					w := wave.NewWav(bar, p.IType, p.Start+minI, last, param.Mode)
					w.Calculate(last)
					wavs = append(wavs, w)
					last = &w
				}
			}

		} else { // macd蓝，找低点
			index = mathlib.MinI(valsL[p.Start : p.End+1])

			if last != nil && length > minLengh && i > 1 && index > length/2 {
				e := p.End - length/2 + 1
				maxV := mathlib.Max(valsH[p.Start:e])
				// 修正1，前半周期，有新高点出现
				if maxV > last.End.Val() {
					maxI := mathlib.MaxI(valsH[p.Start:e])
					wavs = wavs[:len(wavs)-1]
					if len(wavs) == 0 {
						last = nil
					} else {
						last = &wavs[len(wavs)-1]
					}
					w := wave.NewWav(bar, p.IType, p.Start+maxI, last, param.Mode)
					w.Calculate(last)
					wavs = append(wavs, w)
					last = &w
				}
			}
		}

		// 修正2
		// 之前for循环，已经计算过index 和 length
		// 如果，index 在左半部分，则以后半部分的高低点为准
		if index < length/2-1 {
			a := p.Start + length/2
			if p.IType > 0 { //macd红，找高点
				index = mathlib.MaxI(valsH[a : p.End+1])
			} else { // macd蓝，找低点
				index = mathlib.MinI(valsL[a : p.End+1])
			}
			index += length / 2
		}

		w := wave.NewWav(bar, p.IType, p.Start+index, last, param.Mode)
		w.Calculate(last)
		wavs = append(wavs, w)
		last = &w
	} // for end

	//补足最后一根
	l := bar.Len()
	lastI := last.End.Index()
	if lastI != l-1 {
		if l-1-lastI <= param.MinN {
			// 去掉最后一个波浪点，合并到最后一个
			wavs = wavs[:len(wavs)-1]
		}

		last = &wavs[len(wavs)-1]
		w := wave.NewWav(bar, 0, l-1, last, 0) // IType == 0
		w.Calculate(last)
		wavs = append(wavs, w)
	}

	return wavs
}

// // fmlWaveSeq 计算波浪的连续计数
// func fmlWaveSeq(vals []float64, points []wave.Wav) []float64 {
// 	l := len(points)
// 	var p wave.Wav
// 	// arr := make([]float64, len(vals))
// 	arr := util.GetColumn(len(vals))
// 	for i := 0; i < l; i++ {
// 		p = points[i]
// 		arr[p.End.Index()] = float64(p.Seq)
// 	}
//
// 	return arr
// }
