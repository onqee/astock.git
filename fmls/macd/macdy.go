package macd

const (
	// ValTurn 转向
	ValTurn = 9

	// ValReady 准备背离阶段
	ValReady = 5
	// ValReady2 准备背离阶段-弱
	ValReady2 = 4

	// ValPrepare prepare
	ValPrepare = 1
)

// Section macdy 计算时波浪的状态点
type Section struct {
	IType         int // >0 up <0 down
	Start         int
	End           int
	IsDivergence  bool
	DivergenceNth int // 第几次背离
	DataLength    int
}

func newSection(itype, start, end, l int) Section {
	if start < 0 {
		start = 0
	}
	if l > 0 && end >= l {
		end = l - 1
	}
	return Section{Start: start, End: end, IType: itype, DataLength: l}
}

// CalcPoints 平滑过滤后，导出关键点
// macd 改变后，diff 必须连续 > minN 才能确认当前的趋势状态
// minN: 小于这个周期，则认为是干扰，不影响当前状态
// maxSize: 最大point数量
func CalcPoints(diff, dea []float64, minN, maxSize int) []Section {
	l := len(diff)
	if l < 3 {
		return nil
	}
	n := l / 15
	if n < 3 {
		n = 3
	}
	points := make([]Section, 1, n)
	points[0] = newSection(0, 0, l-1, 0)

	continueN := 0 //连续计数

	step := minN
	for i := l - 1; i > -1; i -= step {
		pn := len(points)
		p := &points[pn-1]

		if i == 0 {
			p.Start = 0
			p.IType = 0
			break
		}

		macd := diff[i] - dea[i]
		prevCount := continueN

		// begin do
		if continueN == 0 {
			if macd > 0 {
				continueN++
			} else {
				continueN--
			}
			continue
		}

		isTurn := false
		if (macd > 0 && continueN < 0) || (macd < 0 && continueN > 0) { // 转向
			continueN = 0
			isTurn = true
		}

		// macd > 0 转向时，代表金叉，当前波段为下跌波段
		if macd > 0 {
			continueN++
		} else {
			continueN--
		}

		if !isTurn {
			continue
		}

		var prev *Section
		if pn > 1 {
			prev = &points[pn-2]
		}

		n := checkTurn(p, prev, l, i, diff, dea, minN, prevCount)
		if isTurn && n != -1 {
			i = n
			if len(points) == maxSize {
				points = append(points, newSection(0, 0, i, 0))
				break
			} else {
				points = append(points, newSection(-1*p.IType, 0, i, 0))
			}
		}
	}

	return points
}

func checkTurn(p, prev *Section, l, index int, diff, dea []float64, minN, continueN int) int {
	for i := index; i < l; i++ {
		macd1 := diff[i] - dea[i]
		macd2 := diff[i-1] - dea[i-1]
		if macd1*macd2 < 0 {
			// 转向
			if p.End-i >= minN {
				p.Start = i
				if continueN > 0 {
					p.IType = 1
				} else {
					p.IType = -1
				}

				// 399001 -> 11906110000
				// 小于minN，导致跳过，得到两个相同的point.IType
				if prev != nil && p.IType == prev.IType {
					prev.Start = i
					p.End = i
					return -1
				}

				return i
			}

			return -1
		}
	}
	return -1
}
