package macd

import (
	"fmt"
	"math"
	"strconv"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"gitee.com/onqee/astock/lib/mathlib"
	"gitee.com/onqee/astock/lib/stocklib"
)

// Macdy formula
type Macdy struct {
	fmlib.Base
	MinN int // 最小周期数
	// ScoreScope int // 计算score时，考虑数值的范围:前n个周期
	LLT int // 滤波周期参数，0 不进行滤波
}

// NewMacdy fml
func NewMacdy() *Macdy {
	m := &Macdy{MinN: 3, LLT: 6}
	name := "macdy-" + fmt.Sprint(m.MinN) + "-llt" + strconv.FormatInt(int64(m.LLT), 10)
	fields := []string{"val"}
	m.Init(name, fields, fmlib.ScoreTypeMain)
	return m
}

// Fml 单值公式
func (m *Macdy) Fml(bar *bars.Bar, dat *series.A) error {
	macdy(bar, dat, m.MinN, m.LLT, 0)
	return nil
}

// Calc 单值公式
func (m *Macdy) Calc(bar *bars.Bar, row []float64) error {
	dat := series.GetA(vars.FieldsVal1)
	defer series.PutA(&dat)

	macdy(bar, &dat, m.MinN, m.LLT, 8)
	if dat.Len() == 0 {
		return nil
	}
	v, stat := dat.ValueOn(vars.FieldVal, bar.LastDate())
	if stat == series.IndexFound {
		row[0] = v
	}

	return nil
}

// Score 计算得分 return 类型，得分
func (m *Macdy) Score(row []float64) float64 {
	score := row[0]

	switch score {
	case ValTurn:
		return 1.5
	case -ValTurn:
		return -1.5
	case ValReady, ValReady2:
		return 1
	case -ValReady, -ValReady2:
		return -1
	default:
		return 0
	}
}

// macdy 计算背离
// llt 参数
// maxPointN: 从后往前计算，大于N则退出不做计算，节省计算量
func macdy(bar *bars.Bar, dat *series.A, minN, llt, maxPointN int) {
	vals := bar.Values(vars.FieldClose)
	if llt > 0 {
		vals = stocklib.LLT(vals, llt)
		defer series.PutColumn(vals)
	}
	diff, dea := stocklib.MACD(vals, 12, 26, 9)
	defer series.PutColumn(diff)
	defer series.PutColumn(dea)

	points := CalcPoints(diff, dea, minN, maxPointN)
	l := len(points)

	for i := 0; i < l-2; i++ {
		// 第一次背离
		pB := points[i] // PB，PA相同方向的区间
		pA := newSection(0, points[i+2].Start, points[i+1].End, 0)
		if calcMacdyBy(bar, pA, pB, vals, diff, dea, minN, 0, dat) {
			continue
		}

		if i > l-5 {
			break
		}
		// 第二次背离
		pA = newSection(0, points[i+4].Start, points[i+1].End, 0)
		calcMacdyBy(bar, pA, pB, vals, diff, dea, minN, 1, dat)
	} // for end

	dat.Sort()
}

func valsByP(l int, p Section) (s, e int) {
	s = p.Start - 2
	if s < 0 {
		s = 0
	}
	e = p.End + 3
	if e > l {
		e = l
	}
	return s, e
}

// calcMacdyBy 计算背离
func calcMacdyBy(bar *bars.Bar, pA, pB Section, vals, diff, dea []float64, minN, nth int, dat *series.A) bool {
	l := len(vals)
	s, e := valsByP(l, pA)
	arrVA := vals[s:e]
	arrA := diff[s:e]

	s, e = valsByP(l, pB)
	arrVB := vals[s:e]
	arrB := diff[s:e]

	beginB := -1 //开始背离的index

	// 顶背离
	if pB.IType > 0 {
		maxVA := mathlib.Max(arrVA)
		maxA := mathlib.Max(arrA)
		maxVB := mathlib.Max(arrVB)
		// maxB := mathlib.Max(arrB)
		maxI := mathlib.MaxI(arrB)
		maxB := arrB[maxI]

		// 没有背离
		if !(maxVB > maxVA && maxB < maxA) {
			return false
		}

		// 在pB区间，查找具体背离值
		for i := s + minN; i < e; i++ {
			isB := diff[i] < maxA && vals[i] > maxVA
			if beginB < 0 && isB {
				//开始背离
				beginB = i
			}

			if beginB < 0 { //没有开始背离
				continue
			}

			// 死叉向下
			if diff[i-1] > dea[i-1] && diff[i] < dea[i] {
				dat.Dates = append(dat.Dates, bar.Dates[i])
				dat.Columns[0] = append(dat.Columns[0], ValTurn)
				//背离消失
				beginB = -1
				continue

			} else if diff[i-2] < diff[i-1] && diff[i-1] > diff[i] {
				// 背离 50%
				dat.Dates = append(dat.Dates, bar.Dates[i])
				dat.Columns[0] = append(dat.Columns[0], diffMacdyVal(i, diff))
				continue

			} else {
				// 背离准备
				dat.Dates = append(dat.Dates, bar.Dates[i])
				dat.Columns[0] = append(dat.Columns[0], ValPrepare)
				continue

			}
		} // end for
		return true
	}

	// 底背离
	minVA := mathlib.Min(arrVA)
	minA := mathlib.Min(arrA)
	minVB := mathlib.Min(arrVB)
	// minB := mathlib.Min(arrB)
	minI := mathlib.MinI(arrB)
	minB := arrB[minI]

	// 没有背离
	if !(minVB < minVA && minB > minA) {
		return false
	}

	// 在pB区间，查找具体背离值
	for i := s + minN; i < e; i++ {
		isB := diff[i] > minA && vals[i] < minVA
		if beginB < 0 && isB {
			//开始背离，当出现交叉点后，结束背离
			beginB = i
		}

		if beginB < 0 { //没有开始背离
			continue
		}

		// 金叉向上，确认背离
		if diff[i-1] < dea[i-1] && diff[i] > dea[i] {
			dat.Dates = append(dat.Dates, bar.Dates[i])
			dat.Columns[0] = append(dat.Columns[0], -ValTurn)
			//背离消失
			beginB = -1
			continue

		} else if diff[i-2] > diff[i-1] && diff[i-1] < diff[i] {
			// 背离 50%
			dat.Dates = append(dat.Dates, bar.Dates[i])
			dat.Columns[0] = append(dat.Columns[0], -diffMacdyVal(i, diff))
			continue

		} else {
			// 背离准备
			dat.Dates = append(dat.Dates, bar.Dates[i])
			dat.Columns[0] = append(dat.Columns[0], -ValPrepare)
			continue
		}

	} // end for

	return true
}

func diffMacdyVal(i int, diff []float64) float64 {
	v := math.Abs(diff[i])
	vdiff := math.Abs(diff[i-1] - diff[i])

	if v > 100 {
		if vdiff > 9.3 {
			return ValReady
		}
		return ValReady2
	} else if v > 10 {
		if vdiff > 1 {
			return ValReady
		}
		return ValReady2
	} else if v > 1 {
		if vdiff > 0.1 {
			return ValReady
		}
		return ValReady2
	}
	return ValReady2
}
