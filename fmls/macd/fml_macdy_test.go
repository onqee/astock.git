package macd

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/stocklib"
)

func TestMacdy(t *testing.T) {
	bar := bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1160101
	bar.EndDay = 1191031
	bar.Load()

	vals := bar.Values(vars.FieldClose)
	diff, dea := stocklib.MACD(vals, 12, 26, 9)
	points := CalcPoints(diff, dea, 3, 10)
	// l := len(points)
	// for i := 0; i < l; i++ {
	// 	fmt.Println(i, bar.Dates[points[i].Start], bar.Dates[points[i].End], points[i].IType)
	// }
	if bar.Dates[points[7].End] != 11905291500 {
		t.Fatal(bar.Dates[points[7].End])
	}
	if bar.Dates[points[6].End] != 11907111500 {
		t.Fatal(bar.Dates[points[6].End])
	}

	points = CalcPoints(diff, dea, 6, 10)
	if bar.Dates[points[3].End] != 11907111500 && bar.Dates[points[4].End] != 11905291500 {
		t.Fatal(bar.Dates[points[3].End], bar.Dates[points[4].End])
	}
	if bar.Dates[points[5].End] != 11903141500 && bar.Dates[points[6].End] != 11901081500 {
		t.Fatal(bar.Dates[points[5].End], bar.Dates[points[6].End])
	}

	dat := series.GetA(vars.FieldsVal1)
	macdy(&bar, &dat, 3, 0, 0)
	v, _ := dat.ValueOn(vars.FieldVal, 11904091500)
	if v != ValPrepare {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11904101500)
	if v != ValReady2 {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11904111500)
	if v != ValTurn {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11901081500)
	if v != -ValTurn {
		series.Print(&dat)
		t.Fatal(v)
	}
	_, stat := dat.ValueOn(vars.FieldVal, 11812131500)
	if stat != series.IndexNotFound {
		series.Print(&dat)
		t.Fatal(stat)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11809191500)
	if v != -ValTurn {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11808231500)
	if v != -ValTurn {
		series.Print(&dat)
		t.Fatal(v)
	}
	series.PutA(&dat)

	dat = series.GetA(vars.FieldsVal1)
	m := NewMacdy()
	m.MinN = 3
	m.LLT = 0
	m.Fml(&bar, &dat)
	defer series.PutA(&dat)

	bar = bar.Range(0, 11904111500)
	vals = bar.Values(vars.FieldClose)
	diff, dea = stocklib.MACD(vals, 12, 26, 9)
	// points := CalcPoints(diff, dea, 3, 10)
	points = CalcPoints(diff, dea, 3, 15)
	// l := len(points)
	// for i := 0; i < l; i++ {
	// 	fmt.Println(i, bar.Dates[points[i].Start], bar.Dates[points[i].End], points[i].IType)
	// }

	if bar.Dates[points[1].End] != 11904031500 && bar.Dates[points[2].End] != 11903141500 {
		t.Fatal(bar.Dates[points[1].End], bar.Dates[points[2].End])
	}
	if bar.Dates[points[5].End] != 11812041500 && bar.Dates[points[6].End] != 11811261500 {
		t.Fatal(bar.Dates[points[5].End], bar.Dates[points[6].End])
	}
	if bar.Dates[points[9].End] != 11809191500 && bar.Dates[points[9].Start] != 11809101500 {
		t.Fatal(bar.Dates[points[9].Start], bar.Dates[points[9].End])
	}

	m = NewMacdy()
	// m.CalcOnScope = 5
	m.LLT = 0
	row := series.GetRow(len(m.GetFields()))
	barDat := bar.Range(0, 11904111500)
	m.Calc(&barDat, row)
	if row[0] != ValTurn {
		t.Fatal(row)
	}

	barDat = bar.Range(0, 11904181500)
	m.Calc(&barDat, row)
	if row[0] != ValTurn {
		t.Fatal(row)
	}

	dat = series.GetA(m.GetFields())
	err := m.Fml(&barDat, &dat)
	if err != nil {
		t.Fatal(err)
	}

	v, _ = dat.ValueOn(vars.FieldVal, 11901081500)
	if v != -ValTurn {
		series.Print(&dat)
		t.Fatal()
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11904101500)
	if v != ValReady2 {
		series.Print(&dat)
		t.Fatal()
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11904111500)
	if v != ValTurn {
		series.Print(&dat)
		t.Fatal()
	}
}

func TestMacdy600073(t *testing.T) {
	bar := bars.NewBar("sh600073", vars.PD1)
	bar.StartDay = 1160101
	bar.EndDay = 1191031
	bar.Load()

	m := NewMacdy()
	m.LLT = 0

	barDat := bar.Range(0, 11910151500)
	row := series.GetRow(1)
	m.Calc(&barDat, row)

	if row[0] != -ValTurn {
		t.Fatal(row)
	}
}

func TestMacdx(t *testing.T) {
	bar := bars.NewBar(vars.Code399001, vars.PD1)
	bar.StartDay = 1190101
	bar.EndDay = 1191231
	bar.Load()

	fml := NewMacdx()
	fml.LLT = 0
	dat := series.GetA(fml.GetFields())
	err := fml.Fml(&bar, &dat)
	if err != nil {
		t.Fatal(err)
	}
	v, _ := dat.ValueOn(vars.FieldVal1, 11912051500)
	if int(v) != vars.PositionLow {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal1, 11911121500)
	if int(v) != vars.PositionHigh {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal1, 11910281500)
	if int(v) != vars.PositionLow {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal1, 11909191500)
	if int(v) != vars.PositionHigh {
		t.Fatal(v)
	}
}
