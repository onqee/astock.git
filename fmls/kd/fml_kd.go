package macd

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/stocklib"
	"gitee.com/onqee/astock/runner"
	"github.com/kere/gno/libs/util"
)

// KDx
type KDx struct {
	runner.Base
	N1, N2, N3 int // 9,3,3
}

// NewKDx fml
func NewKDx() *KDx {
	m := &KDx{N1: 9, N2: 3, N3: 3}
	name := "kdx" + fmt.Sprintf("n-%d-%d-%d", m.N1, m.N2, m.N3)
	fields := vars.FieldsVal1
	m.Init(name, fields)
	return m
}

// Fml 连续结果序列
// RSV:=(CLOSE-LLV(LOW,N))/(HHV(HIGH,N)-LLV(LOW,N))*100;
// K:SMA(RSV,P1,1);
// D:SMA(K,P2,1)
func (m *KDx) Fml(bar *bars.Bar, dat *series.A) error {
	vals := bar.Values(vars.FieldClose)
	l := len(vals)
	if l < m.N1 {
		return nil
	}
	high := bar.Values(vars.FieldHigh)
	low := bar.Values(vars.FieldLow)

	k, d := stocklib.KD(vals, high, low, m.N1, m.N2, m.N3)
	row := m.GetRow()

	for i := 0; i < l; i++ {
		v := stocklib.Cross(i, k, d)
		if v == 0 {
			continue
		}
		row[0] = float64(v)
		dat.AddRow0(bar.Dates[i], row)
	}

	util.PutFloats(k)
	util.PutFloats(d)
	series.PutRow(row)
	return nil
}

// Calc 单值公式
func (m *KDx) Calc(bar *bars.Bar, row []float64) error {
	dat := bar.RangeL(-1, m.N1+m.N2+10)
	l := dat.Len()
	if l < 3 {
		return nil
	}
	vals := bar.Values(vars.FieldClose)
	high := bar.Values(vars.FieldHigh)
	low := bar.Values(vars.FieldLow)

	k, d := stocklib.KD(vals, high, low, m.N1, m.N2, m.N3)
	defer series.PutColumn(k)
	defer series.PutColumn(d)
	row[0] = float64(stocklib.Cross(l-1, k, d))

	return nil
}

// Score 计算得分 return 类型，得分
func (m *KDx) Score(row []float64) float64 {
	score := row[0]
	if score == 0 {
		score = row[1] / 3
	}
	return score
}
