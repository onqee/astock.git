package macd

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"github.com/kere/gno"
)

func init() {
	gno.Init("../../app.conf")
	codetool.DataFolder = "../../"
}

func TestMacdy(t *testing.T) {
	bar := bars.NewBar("sz399001", vars.PD1)
	bar.StartDay = 1190101
	bar.EndDay = 1191231
	bar.Load()

	fml := NewKDx()
	dat := series.GetA(fml.Fields())
	err := fml.Fml(&bar, &dat)
	if err != nil {
		t.Fatal(err)
	}

	v, _ := dat.ValueOn(vars.FieldVal, 11912030000)
	if int(v) != vars.PositionLow {
		series.Print(&dat)
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11912200000)
	if int(v) != vars.PositionHigh {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11909110000)
	if int(v) != vars.PositionHigh {
		t.Fatal(v)
	}
	v, _ = dat.ValueOn(vars.FieldVal, 11908130000)
	if int(v) != vars.PositionLow {
		t.Fatal(v)
	}

}
