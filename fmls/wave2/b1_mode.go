package wave2

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
)

// LineMode of wave
type LineMode struct {
	Name         string
	DateA, DateB int64
	Ai, Bi       int
}

// WMode of wave
type WMode struct {
	Name string
	U, L LineMode
}

// SelectWModeOnBySeq
// n: LineMode数量
// seq 连续长度，越长权重越高
func SelectWModeOnBySeq(wavs *series.A, seqs []float64, date int64, n int) []WMode {
	l := wavs.Len()
	var iLb, iUb, iLa, iUa int
	var isLTurn, isUTurn bool // 高低点是否转向
	isLFirst, isUFirst := true, true
	var modeU, modeL, modeU2, modeL2 LineMode
	items := make([]WMode, 0, 10)

	for i := 0; i < l; i++ {
		if wavs.Dates[i] > date {
			continue
		}
		// []string{price, itype, index}
		switch wavs.Columns[1][i] {
		case 1:
			iUa = i
			if isUFirst { //第一次循环赋值
				isUFirst = false
				iUb = i
				continue
			}
			if isUTurn { //寻找最长的序列，如果转向，则不再计算
				continue
			}
			sval := seqs[i] * seqs[i-2]
			modeU2 = LineMode{DateA: wavs.Dates[iUa], Ai: iUa, DateB: wavs.Dates[iUb], Bi: iUb}
			if sval < 0 { // 转向
				modeU = modeU2
				iUb = i
				isUTurn = true
				modeL = modeL2
			}

		case -1:
			iLa = i
			if isLFirst { //第一次循环赋值
				isLFirst = false
				iLb = i
				continue
			}
			if isLTurn { //寻找最长的序列，如果转向，则不再计算
				continue
			}
			sval := seqs[i] * seqs[i-2]
			modeL2 = LineMode{DateA: wavs.Dates[iLa], Ai: iLa, DateB: wavs.Dates[iLb], Bi: iLb}
			// 转向
			if sval < 0 {
				if modeU.DateA > modeL.DateA {

				}
				modeL = modeL2
				iLb = i
				isLTurn = true
				modeU = modeU2
			}

		}

		if isUTurn && isLTurn {
			// 上涨浪：左低点开始，右高点结束
			// 下跌浪：左高点开始，右低点结束

			items = append(items, WMode{U: modeU, L: modeL})
			isUTurn = false
			isLTurn = false
		}
	}

	return items
}

func PrintWModes(items []WMode) {
	l := len(items)
	for i := 0; i < l; i++ {
		item := items[i]
		fmt.Printf("i:%d \n  U DateA:%s	DateB:%s\n  L DateA:%s	DateB:%s\n", i+1,
			datescore.Score2DateStr(item.U.DateA), datescore.Score2DateStr(item.U.DateB),
			datescore.Score2DateStr(item.L.DateA), datescore.Score2DateStr(item.L.DateB))
	}
}
