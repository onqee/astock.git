package wave2

import (
	"math"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

// DefaultWParam 默认波浪参数
var (
	// DefaultWParam params
	DefaultWParam = NewWParam(10, 4)

	// itype : 1 顶点  -1 底点
	waveFields = []string{"price", "itype", "index"}
)

// WParam 波浪的参数
type WParam struct {
	Percent float64 // 波浪最小幅度
	MinN    int     // 波浪最小间隔周期
	Mode    int     // 0: 默认Close  1:CloseHighLow，进行波浪定型
	// MinWavN int     //自动计算最小bar运算周期时，最少Wav数量
}

// NewWParam 新建参数
func NewWParam(percent float64, minN int) WParam {
	return WParam{percent, minN, 0}
}

// CalcWavsP 计算波浪关键点
// series.A: "price", "itype"
func CalcWavsP(bar *bars.Bar, param WParam) series.A {
	count := bar.Len()
	if count < param.MinN*2 {
		return series.EmptyA
	}

	var closeI, highI, lowI int
	var high, low float64

	wavs := series.GetA(waveFields)

	closeI = util.StringsI(vars.FieldClose, bar.Fields)
	if param.Mode == 1 { // 高低价，作为波浪的高低点
		highI = util.StringsI(vars.FieldHigh, bar.Fields)
		lowI = util.StringsI(vars.FieldLow, bar.Fields)
	}

	type wPoint struct {
		Index int
		Value float64
		IType int
	}
	var maxI, minI int
	max := -math.MaxFloat32
	min := math.MaxFloat32

	row := wavs.GetRowP()
	defer series.PutRow(row)

	// 起始点，最后一个点
	i := count - 1
	p := wPoint{Index: i, Value: bar.Columns[closeI][i]}
	percent := param.Percent / 100
	isAdd := false

	for ; i > -1; i-- {
		if param.Mode == 1 {
			high, low = bar.Columns[highI][i], bar.Columns[lowI][i]
		} else {
			high = bar.Columns[closeI][i]
			low = high
		}

		if max < high {
			max, maxI = high, i
		}
		if min > low {
			min, minI = low, i
		}

		switch p.IType {
		case 0: // 确定开始P点
			if math.Abs(p.Value-min)/min > percent {
				// 高点:
				p = wPoint{Index: maxI, Value: max, IType: 1}
				isAdd = true
			} else if math.Abs(max-p.Value)/p.Value > percent {
				// 低点
				p = wPoint{Index: minI, Value: min, IType: -1}
				isAdd = true
			}
			if isAdd && p.Index != count-1 {
				//如果起始点，没有和高低点重合，则，添加起始点
				row[0], row[2] = bar.Columns[closeI][count-1], float64(count-1)
				wavs.AddRow0(bar.Dates[count-1], row)
			}
		case 1: //当前是高点，往左，寻找低点
			if math.Abs(high-min)/min > percent {
				// 低点
				p = wPoint{Index: minI, Value: min, IType: -1}
				isAdd = true
			}

		case -1: //当前是低点，往左，寻找高点
			if math.Abs(max-low)/low > percent {
				// 高点:
				p = wPoint{Index: maxI, Value: max, IType: 1}
				isAdd = true
			}
		}
		if isAdd {
			// "price", "itype", "index"
			row[0], row[1], row[2] = p.Value, float64(p.IType), float64(p.Index)
			wavs.AddRow0(bar.Dates[p.Index], row)
			max, min = high, low
			maxI, minI = i, i
			isAdd = false
		}
	}

	l := wavs.Len()
	// 检查wavs起始点，如果没有，则添加
	if l > 0 && wavs.LastDate() != bar.Dates[0] {
		wavs.RowAt(l-1, row)
		row[0], row[1], row[2] = bar.Columns[closeI][0], -row[1], 0
		wavs.AddRow0(bar.Dates[0], row)
	}

	if l < 4 {
		return wavs
	}

	var wavs2 series.A
	// 计算波浪最小minN
	if param.MinN > 0 {
		fminN := float64(param.MinN)
		for i := l - 3; i > -1; i-- {
			if wavs.Columns[2][i]-wavs.Columns[2][i+1] > fminN {
				continue
			}
			wavs.Columns[1][i], wavs.Columns[1][i+1] = -9, -9 // itype = -9
		}
		// 起始点minN检查
		i = l - 2
		if wavs.Columns[2][i]-wavs.Columns[2][l-1] <= fminN {
			// 右一点取消，起始点itype相反
			wavs.Columns[1][l-1] = -wavs.Columns[1][i]
			wavs.Columns[1][i] = -9 // itype = -9
		}

		wavs2 = series.GetA(waveFields)
		for i := 0; i < l; i++ {
			if wavs.Columns[1][i] == -9 {
				continue
			}
			wavs.RowAt(i, row)
			wavs2.AddRow0(wavs.Dates[i], row)
		}
		series.PutA(&wavs)
	} else {
		wavs2 = wavs
	}

	return wavs2
}
