package wave2

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../../app.conf")
}

func TestA(t *testing.T) {
	bar := bars.NewBar("sh600207", vars.PD1)
	bar.StartDay = 1190101
	bar.EndDay = 1201231
	bar.Load()

	param := DefaultWParam
	param.MinN = 0
	wavs := CalcWavsP(&bar, param)

	if wavs.Columns[1][0] != 0 && wavs.Columns[2][0] != 4 {
		series.Print(&wavs)
		t.Fatal()
	}

	if wavs.Columns[1][1] != 1 && wavs.Columns[2][1] != 3 {
		series.Print(&wavs)
		t.Fatal()
	}

	seqs := CalcSeqCol(&wavs, 0, true)
	wavs.AddColumn("seq", seqs)

	// if wavs.Dates[10] != 12002281500 && wavs.Columns[1][10] != -1 && wavs.Columns[3][10] != -4 {
	// 	series.Print(&wavs)
	// 	t.Fatal()
	// }
	// series.PutA(&wavs)
	//
	// param = DefaultWParam
	// param.MinN = 0
	// param.Percent = 11
	// wavs = CalcWavsP(&bar, param)
	// seqs = CalcSeqCol(&wavs, 0, true)
	// wavs.AddColumn("seq", seqs)
	// modes := SelectWModeOnBySeq(&wavs, seqs, 12005121500, 2)
	//
	// series.Print(&wavs)
	// PrintWModes(modes)
	//
	// series.PutA(&wavs)
}
