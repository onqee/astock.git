package wave2

import (
	"math"

	"gitee.com/onqee/astock/lib/data/series"
)

// CalcSeqCol 计算波浪关键点，并添加进wavs
// []string{price, itype, index}
// edge: 波浪高低点的边界，如果在边界内，则延续前一点的seq
// 边界范围：以第一次破坏趋势的点为准
func CalcSeqCol(wavs *series.A, edge float64, isPool bool) []float64 {
	l := wavs.Len()
	var col []float64
	if isPool {
		col = series.GetColumn(l)
	} else {
		col = make([]float64, l)
	}

	if l < 3 {
		return col
	}
	// 计算Seq
	var seqU, seqL int
	var valU0, valL0 float64 // edge默认比较的数值，以第一次变相为准
	if wavs.Columns[1][2] == 1 {
		valU0 = wavs.Columns[0][0]
		valL0 = wavs.Columns[0][1]
	} else {
		valU0 = wavs.Columns[0][1]
		valL0 = wavs.Columns[0][0]
	}
	// "price", "itype", "seq", "offset"
	for i := l - 3; i > -1; i-- {
		//wavs 时间是倒叙排列，i-- 循环为时间正序
		valR := wavs.Columns[0][i]   //大日期
		valL := wavs.Columns[0][i+2] //小日期,左侧日期
		itype := wavs.Columns[1][i]

		if itype == 1 { // U
			valU0 = wavs.Columns[0][i]
			if valR < valU0 && math.Abs(valR-valU0)/valU0 > edge { // 变相

			}

		} else if itype == -1 {

		}

	} // for

	return col
}
