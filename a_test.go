package astock

import (
	"fmt"
	"math"
	"path/filepath"
	"testing"

	"gitee.com/onqee/astock/compare"
	"gitee.com/onqee/astock/fmls/macd"
	"gitee.com/onqee/astock/fmls/tdd"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/fmlib"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"gitee.com/onqee/astock/runner"
	"gitee.com/onqee/astock/trader"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/log"
	_ "github.com/lib/pq"
)

var batch runner.BatchFmls

func init() {
	gno.Init("app.conf")
	log.App.SetLevel(log.LogNoticeStr)
	vars.IsSync = true
	vars.IsTest = true

	dget.DefaultStartDay = 1190101
	dget.DefaultEndDay = 1200701
	dget.DefaultStartDayM5 = 1190101
	dget.DefaultEndDayM5 = 1200701
}

func TestBatchFmls(t *testing.T) {
	prpareDat(vars.Code399001)
	batch = runner.NewBatchFmls()
	td := tdd.NewTDD()
	periods := []string{vars.PD1, vars.PM120}
	batch.AddItem(periods, td)
	md := macd.NewMacdy()
	md.LLT = 0
	batch.AddItem(periods, md)

	codes := []string{"sz000938", "sh600207", "sh600584"}
	// 生成数据
	errs := batch.Run(codes)
	if len(errs) > 0 {
		for err := range errs {
			fmt.Println(err)
		}
		t.Fatal()
	}
}

func TestRun(t *testing.T) {
	rule := runner.Rule{
		Periods: []string{vars.PD1, vars.PM120},
		FmlsRules: []runner.FmlRule{
			runner.FmlRule{Fml: macd.NewMacdy(), IsRequire: false},
			runner.FmlRule{Fml: tdd.NewTDD(), IsRequire: false},
		},
		PeriodsLogic:     fmlib.LogicScoreMax,
		FmlsLogic:        fmlib.LogicScoreMax,
		MainPeriodScopeN: 1,
	}

	code := vars.Code399001
	// 1：测试关键点03.05
	score := rule.ScoreOnP(code, 12003051500)
	if score[0] != 1.5 {
		t.Fatal(score)
	}
	series.PutRow(score)
	// 2：测试关键点03.11之后1天
	score = rule.ScoreOnP(code, 12003111500)
	if score[0]+score[1] != 0 {
		t.Fatal(score)
	}
	series.PutRow(score)
	// 2.1：测试关键点03.11之后1天，ScorpeN=2
	rule.MainPeriodScopeN = 2
	score = rule.ScoreOnP(code, 12003111500)
	if score[0]+score[1] != -1 {
		t.Fatal(score)
	}
	series.PutRow(score)
	// 3：测试12003041300
	rule.MainPeriodScopeN = 1
	score = rule.ScoreOnP(code, 12003041300)
	if score[0] != 1.5 || score[2] != 1 {
		t.Fatal(score)
	}
	series.PutRow(score)
	// 4：测试关键点03.11之后3天
	rule.PeriodsLogic = fmlib.LogicScoreSum
	rule.MainPeriodScopeN = 3
	score = rule.ScoreOnP(vars.Code399001, 12003111500)
	if score[0] != -2 || score[2] != -1 {
		t.Fatal(score)
	}
	series.PutRow(score)

	// 5: 测试Corr相似板块，安彩高科相似性0.83，会被忽略
	code = "sh600207"
	score = rule.ScoreOnP(code, 11907301500)
	if score[0] != -1.5 {
		t.Fatal(score)
	}
	// 6: 测试Corr相似板块，长电科技相似性
	code = "sh600584"
	score = rule.ScoreOnP(code, 11912191500)
	if score[0] != 1.5 {
		t.Fatal(score)
	}

	// 7: 测试scores 得分序列
	rule.PeriodsLogic = fmlib.LogicScoreSum
	rule.MainPeriodScopeN = 2
	code = vars.Code399001
	dat := rule.ScoresP(code, 0, math.MaxInt64)
	l := dat.Len()
	row := dat.RowAtP(1)
	if row[0] != -3 {
		t.Fatal(row)
	}
	series.PutRow(row)
	row = dat.RowAtP(l - 2)
	if row[2] != -2 {
		series.Print(dat)
		t.Fatal(row)
	}
	series.PutRow(row)
	// series.Print(dat)
	// b, _ := json.Marshal(rule)
	// file, _ := os.OpenFile("z:/a.conf", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.ModePerm)
	// file.Write(b)

	// 8: 测试空数据
	rule.PeriodsLogic = fmlib.LogicScoreSum
	rule.MainPeriodScopeN = 2
	code = "sh601686"
	dat = rule.ScoresP(code, 0, 0)
	series.Print(dat)
}

type sVe struct {
	Date  int64
	Value float64
}

func doTryWrite(arr []sVe, code, name, period string) {
	c := gno.GetConfig().GetConf("data")
	filename := filepath.Join(c.GetString("store_dir"), "test/fmls", period, name, code+".bin")
	// _, err := os.Stat(filename)
	// if os.IsNotExist(err) {
	dat := series.GetA(vars.FieldsVal1)
	row := dat.GetRowP()
	count := len(arr)
	for i := 0; i < count; i++ {
		row[0] = arr[i].Value
		dat.AddRow0(arr[i].Date, row)
	}
	series.WriteBin(&dat, filename)
	series.PutA(&dat)
	series.PutRow(row)
	// }
}

// macdy D1        -9        9        -5         5        -9
// macdy M120   -9        9          -5             5   -5
// tdd   D1        -9        9      -9              9  -9  -9            9						-9  -10
// tdd   M120     -9        9      -9                 9   -9-10                       -9  -10
// ----------------3.02---- 3.05------3.10------3.16------3.20----------4.09---------4.20----
func prpareDat(code string) {
	// 读取数据
	arr := []sVe{sVe{12003021500, -9}, sVe{12003051500, 9}, sVe{12003101500, -5}, sVe{12003161500, 5}, sVe{12003201500, -9},
		sVe{12004011500, -9}, sVe{12004081500, 9},
	}
	doTryWrite(arr, code, "macdy-3-llt6", vars.PD1)

	arr = []sVe{sVe{12002280930, -9}, sVe{12003041300, 9}, sVe{12003090930, -5}, sVe{12003171300, 5}, sVe{12003201300, -5},
		sVe{12004020900, -9}, sVe{12004091300, 9},
	}
	doTryWrite(arr, code, "macdy-3-llt6", vars.PM120)

	arr = []sVe{sVe{12003021500, -9}, sVe{12003051500, 9}, sVe{12003091500, -9}, sVe{12003171500, 9}, sVe{12003191500, -9}, sVe{12003201500, -11},
		sVe{12004200900, -9}, sVe{12004211300, -10},
	}
	doTryWrite(arr, code, "tdd-4", vars.PD1)

	arr = []sVe{sVe{12002280930, -9}, sVe{12003041300, 9}, sVe{12003060930, -9}, sVe{12003181300, 9}, sVe{12003200930, -9}, sVe{12003201300, -10},
		sVe{12004200900, -9}, sVe{12004211300, -10},
	}
	doTryWrite(arr, code, "tdd-4", vars.PM120)
}

func TestTrader(t *testing.T) {
	codes := []string{"sz000938", "sh600207", "sh600584", vars.Code000001, vars.Code399001, vars.Code399006}
	// 生成数据
	batch.Run(codes)

	code := "sh600207"
	bar := dget.Bar(code, vars.PD1)
	compare.StoreCorr(bar, codetool.CodesBlockTdx())

	rule := runner.Rule{
		Periods: []string{vars.PD1},
		FmlsRules: []runner.FmlRule{
			runner.FmlRule{Fml: macd.NewMacdy(), IsRequire: false},
			runner.FmlRule{Fml: tdd.NewTDD(), IsRequire: false},
		},
		PeriodsLogic:     fmlib.LogicScoreMax,
		FmlsLogic:        fmlib.LogicScoreMax,
		MainPeriodScopeN: 1,
	}

	codes = []string{"sz000938"}
	a := trader.Analyser{}
	acc := trader.NewAccount(0)
	acc.Params.AMaxBuyMoneyEvery = 0
	acc.Params.AMaxBuyVolEvery = 100
	a.SSPaddingDn = 0
	// a.RelatedDN = 2
	// a.RelatedMode = 1
	hss := a.ScoresToHodingss(acc, rule, codes, "z:/kt/test/trader01.csv")
	// 	trader.PrintHoldings(hss[0], true)
	p := hss[0].ClosedProfile()
	// if math.Abs(p.Profit-47529.39) > 0.01 {
	trader.PrintProfile(p)
	// 	t.Fatal(p.Profit)
	// }

	// codes = []string{"sz000938"}
	// a.SSPaddingDn = 2
	// a.RelatedDN = 2
	// a.RelatedMode = 0
	// hss = a.ScoresToHodingss(acc, rule, codes, "z:/kt/test/trader02.csv")
	// p = hss[0].ClosedProfile()
	// trader.PrintProfile(p)

}
