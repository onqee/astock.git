package trader

import (
	"math"
	"os"

	"gitee.com/onqee/astock/compare"
	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"gitee.com/onqee/astock/lib/tools/codetool"
	"gitee.com/onqee/astock/runner"
	"github.com/kere/gno/libs/util"
)

type Filter func(code string, date int64, stat int, score []float64) bool

// Analyser 分析
type Analyser struct {
	Filters      []Filter
	ActionF      func(row []float64) (stat int) `json:"-"`
	PointSelectF func(ids []int) int            `json:"-"`

	RelatedDN   int // 相关DN范围
	RelatedMode int // 0: Index && Blocks  1: index  2: Blocks

	CodesSortFunc func([]StatItem) // 多个买入、卖出点同时出现时的排序

	IsSkipST             bool
	TradableAmtInScopeOn [2]int // 流通市值[2]int{亿元开始， 亿元结束}
	CapitalAmtInScopeOn  [2]int // 总市值[2]int{亿元开始， 亿元结束}

	SSPaddingDn int //计算连续关键点，被允许的空隙
}

type StatItem struct {
	Code string
	Date int64
	Stat int
}

// ScoresToHodings
func (a *Analyser) ScoresToHodingss(acc *Account, rule runner.Rule, codes []string, csvName string) []Holdings {
	cpt := util.NewComputation(vars.ProcessNum)
	cpt.IsSync = vars.IsSync
	count := len(codes)

	actionFunc := DefaultActionFunc
	if a.ActionF != nil {
		actionFunc = a.ActionF
	}

	// arr 所有股票的得分点
	arr := make([]*series.A, count)
	cpt.Run(count, func(i int) {
		code := codes[i]
		scoreDat := rule.ScoresP(code, 0, math.MaxInt64)
		arr[i] = a.PointApplyP(scoreDat)
		series.PutA(scoreDat)
	})
	// arrMap indexs 和 其他板块指数的得分点数据
	arrMap := make(map[string]*series.A)

	row := series.GetRow(5)
	// 做数据清理
	defer func() {
		series.PutAs(arr)
		series.PutRow(row)
		for k, v := range arrMap {
			series.PutA(v)
			arrMap[k] = nil
		}
	}()

	var file *os.File
	var err error
	if csvName != "" {
		os.Remove(csvName)
		file, err = os.OpenFile(csvName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
		if err != nil {
			panic(err)
		}
		defer file.Close()
		// 写入CSV Head
		arr := util.GetItems(8)
		arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7] = "code", "date", "type", "price", "valumn", "money", "fee", "profit"
		err := util.WriteCsvRow(file, arr, vars.DefaultDecimal)
		if err != nil {
			panic(err)
		}
		util.PutItems(arr)
	}

	inScopeEmpty := [2]int{0, 0}
	codeStats := make([]StatItem, count)
	hss := make([]Holdings, count) // 每个股票的holdings
	closeI := util.StringsI(vars.FieldClose, vars.BarFields)
	an := len(a.Filters)
	barSZ := dget.Bar(vars.Code399001, vars.PD1)

	series.EveryApply(func(date int64, ids []int) {
		for i := 0; i < count; i++ {
			code := codes[i]
			index := ids[i]
			if index < 0 {
				codeStats[i] = StatItem{}
				continue
			}
			dat := arr[i]
			date := dat.Dates[index]
			// 1: Skip ST
			st := dget.ST(code)
			if a.IsSkipST && st.IsSTOn(date) {
				codeStats[i] = StatItem{}
				continue
			}
			// 2: Skip流通盘范围外的样本
			if a.TradableAmtInScopeOn != inScopeEmpty && !dget.IsTradableAmtInScopeOn(code, date, a.TradableAmtInScopeOn) {
				codeStats[i] = StatItem{}
				continue
			}
			// 3: Skip总市值范围外的样本
			if a.CapitalAmtInScopeOn != inScopeEmpty && !dget.IsCapitalAmtInScopeOn(code, date, a.CapitalAmtInScopeOn) {
				codeStats[i] = StatItem{}
				continue
			}

			dat.RowAt(index, row)
			stat := actionFunc(row)

			if an > 0 {
				// 执行过滤
				isOK := true
				for k := 0; k < an; k++ {
					if !a.Filters[k](code, date, stat, row) {
						isOK = false
						break
					}
				}
				if !isOK {
					continue
				}
			}
			if stat == vars.ActionNone {
				continue
			}

			// 相关计算：指数或板块
			if a.RelatedDN != 0 {
				isIndexOK := false
				isBlockOK := false
				// RelatedMode int // 0: Index && Blocks  1: index  2: Blocks
				if a.RelatedMode < 2 { // index
					pdat := getMapDat(arrMap, rule, a, codetool.WhatIndexIs(code))
					isIndexOK = relatedIt(pdat, barSZ, date, a.RelatedDN)
				}
				if a.RelatedMode == 0 || a.RelatedMode == 2 { // blocks
					corr := dget.GetBinDat(compare.BinCorrFileName(code, vars.PD1))
					v, _ := corr.ValueOn("code1", date)
					if v != 0 {
						codeBlock := codetool.ToCode(int(v))
						pdat := getMapDat(arrMap, rule, a, codeBlock)
						isBlockOK = relatedIt(pdat, barSZ, date, a.RelatedDN)
					}
				}
				switch a.RelatedMode {
				case 0:
					// indexs || blocks
					if !isIndexOK && !isBlockOK {
						continue
					}
				case 1:
					// indexs
					if !isIndexOK {
						continue
					}
				case 2:
					// blocks
					if !isBlockOK {
						continue
					}
				}
			}
			codeStats[i] = StatItem{code, date, stat}
		}

		if a.CodesSortFunc != nil {
			a.CodesSortFunc(codeStats)
		}

		for i := 0; i < count; i++ {
			item := codeStats[i]
			if item.Date == 0 || item.Stat == vars.ActionNone {
				continue
			}

			code := codes[i]
			hs := hss[i]
			h := &Holding{Code: code}
			n := len(hs)
			isAppend := false
			if n > 0 && !hs[n-1].IsClosed {
				h = &hs[n-1]
			} else {
				isAppend = true
			}

			date := item.Date
			// 关键点，出现后的一个小时，买入 or 卖出
			bar := dget.Bar(code, vars.PD1)
			// 检查是否因为最大持有时间等设置，提前完成
			if h.DoFinish(acc, bar, date) {
				if isAppend {
					hs = append(hs, *h)
					hss[i] = hs
				}
				continue
			}

			priceI, ityp := bar.Search(date)
			if ityp == series.IndexOverRangeLeft || ityp == series.IndexOverRangeRight {
				continue
			}
			// price := bar.ValueAt(vars.FieldClose, priceI)
			price := bar.Columns[closeI][priceI]

			if item.Stat == vars.ActionBuy {
				var vol int
				// 如果最大买入金额==0，则以最大买入数量为主
				// 如果两个限制都有，则以最小的为主
				if acc.Params.AMaxBuyMoneyEvery == 0 {
					vol = acc.Params.AMaxBuyVolEvery
				} else {
					vol = FixBuyVol(acc.Params.AMaxBuyMoneyEvery, price)
					if acc.Params.AMaxBuyVolEvery != 0 && vol > acc.Params.AMaxBuyVolEvery {
						vol = acc.Params.AMaxBuyVolEvery
					}
				}
				if !h.CheckBuy(acc, vol, price, date) {
					continue
				}

				h.Buy(acc, vol, price, date)
				if isAppend {
					hs = append(hs, *h)
					hss[i] = hs
				}
			} else {
				if h.Volumn == 0 {
					continue
				}
				h.Sell(acc, h.Volumn, price, date)
			}
		}

	}, arr...)

	for i := 0; i < count; i++ {
		l := len(hss[i])
		if l == 0 {
			continue
		}
		h := &hss[i][l-1]
		if !h.IsClosed {
			bar := dget.Bar(h.Code, vars.PD1)
			if !h.DoFinish(acc, bar, bar.LastDate()) {
				l := bar.Len()
				if l == 0 {
					panic(h.Code + " 长度等于0")
				}
				price := bar.Columns[closeI][l-1]
				h.Sell(acc, h.Volumn, price, bar.LastDate())
			}
		}
		writeHoldingsToCSV(file, hss[i])
	}

	return hss
}

// PointApplyP 多个得分密集出现，找到关键点
func (a *Analyser) PointApplyP(scoreDat *series.A) *series.A {
	n := scoreDat.Len()
	if n == 0 {
		return &series.EmptyA
	}
	row := scoreDat.GetRowP()
	defer series.PutRow(row)
	result := series.GetA(scoreDat.Fields)
	if a.SSPaddingDn < 2 {
		for i := 0; i < n; i++ {
			scoreDat.RowAt(i, row)
			result.AddRow(scoreDat.Dates[i], row)
		}
		return &result
	}

	bar := dget.Bar(vars.Code399001, vars.PD1).Range(datescore.DayKT(scoreDat.Dates[0]), datescore.DayTdx(scoreDat.LastDate()))
	count := bar.Len()

	ids := util.GetInts()
	defer util.PutInts(ids)
	paddingDN := a.SSPaddingDn
	if paddingDN == 0 {
		paddingDN = 1
	}
	var i0 int
	id := 0 // scoreDat 当前的索引

	pointSelectF := a.PointSelectF
	if pointSelectF == nil {
		pointSelectF = PointSelectFunc
	}

	var date int64
	// 区间,决定关键点函数
	f := func() {
		var index int
		if len(ids) == 0 {
			// fmt.Println(code, scoreDat.Len(), date)
			// os.Exit(0)
			return
		}
		index = pointSelectF(ids)
		scoreDat.RowAt(index, row)
		result.AddRow0(scoreDat.Dates[index], row)
	}

	for i := 0; i < count; i++ {
		date = bar.Dates[i]

		if i-i0 > paddingDN {
			// apply(date, ids)
			f()
			ids = ids[:0]
			ids = append(ids, id)
			i, _ = bar.Search(datescore.DayTdx(scoreDat.Dates[id]))
			i-- //因为for循环+1，所以这里-1
			i0 = i
			// 2: 重新定位i
			id++
			if id >= n {
				// apply(scoreDat.LastDate(), ids)
				f()
				// finish
				break
			}
			continue
		}

		if date/10000 == scoreDat.Dates[id]/10000 {
			i0 = i
			ids = append(ids, id)
			id++
			if id >= n {
				// apply(scoreDat.LastDate(), ids)
				f()
				// finish
				break
			}

			// 查看是否有当天的交易点
			// 如果有，则id往后顺延
			for k := 0; k < 10; k++ {
				if id+k >= n {
					// apply(scoreDat.LastDate(), ids)
					f()
					break
				}
				if scoreDat.Dates[id+k]/10000 == date/10000 {
					ids = append(ids, id+k)
				} else {
					id += k
					break
				}
			}
		}

	}

	return &result
}

func writeHoldingsToCSV(file *os.File, hs Holdings) error {
	arr := util.GetItems(8)
	defer util.PutItems(arr)

	// arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7] = "code", "date", "type", "price", "valumn", "money", "fee", "profit"
	// err := util.WriteCsvRow(file, arr, vars.DefaultDecimal)
	// if err != nil {
	// 	return err
	// }

	count := len(hs)
	for i := 0; i < count; i++ {
		h := hs[i]
		l := len(h.Actions)
		if l == 0 {
			continue
		}
		for k := 0; k < l; k++ {
			a := h.Actions[k]
			arr[0] = h.Code
			arr[1] = datescore.Score2DateTimeStr(a.Date)
			// arr[1] = a.Date
			if a.IType == vars.ActionBuy {
				arr[2] = vars.ActionBuy
			} else {
				arr[2] = vars.ActionSell
			}
			arr[3] = a.Price
			arr[4] = a.Volumn
			arr[5] = a.Money
			arr[6] = a.Fee

			// 最后一个sell
			if h.IsClosed && k == l-1 && a.IType == vars.ActionSell {
				arr[7] = h.Data.EndProfit
			} else {
				arr[7] = nil
			}

			err := util.WriteCsvRow(file, arr, 4)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func statItemsToDatP(items []StatItem, fields []string) series.A {
	dat := series.GetA(fields)
	row := dat.GetRowP()
	l := len(items)
	for i := 0; i < l; i++ {
		item := items[i]
		row[0] = float64(codetool.ToCodeI(item.Code))
		dat.AddRow0(item.Date, row)
	}
	series.PutRow(row)
	return dat
}

func relatedIt(pdat *series.A, barSZ *bars.Bar, date int64, dn int) bool {
	if pdat.Len() == 0 {
		return false
	}
	pindex, _ := pdat.Search(date)
	if pindex < 0 {
		return false
	}
	if series.LenBetween(pdat.Dates[pindex], date, barSZ.Dates) > dn {
		return false
	}

	return true
}

func getMapDat(arrMap map[string]*series.A, rule runner.Rule, a *Analyser, code string) *series.A {
	dat, isok := arrMap[code]
	if isok {
		return dat
	}
	scoreDat := rule.ScoresP(code, 0, math.MaxInt64)
	// dat = PointApplyP(scoreDat, 0, a.PointSelectF)
	arrMap[code] = scoreDat
	// series.PutA(scoreDat)
	return scoreDat
}
