package trader

import (
	"math"

	"github.com/kere/gno/libs/conf"
	"github.com/kere/gno/libs/util"
)

var (
	config conf.Conf
)

// 股票买卖的手续费分为4个部分：印花税、佣金、过户费以及交易所规费。
//https://baike.baidu.com/item/%E4%BA%A4%E6%98%93%E6%89%80%E8%A7%84%E8%B4%B9/2418096?fr=aladdin

// BuyFee 购买时的交易费用
func BuyFee(code string, amt float64, param *Params) float64 {
	fee := TradeFee(amt, param) + TransferFee(amt, param) // MarketFee(amt, param) 包含在手续费里
	return fee
}

// SellFee 卖出时的费用
func SellFee(code string, amt float64, param *Params) float64 {
	fee := TradeFee(amt, param) + TransferFee(amt, param) // MarketFee(amt, param)
	fee += StampTax(amt, param)
	return fee
}

// StampTax 印花税 印花税属于国家税务机关收取科目。在我国，各行各业都需要收税，金融市场也不 例外。印花税由证券公司代为收取，证券公司在你进行股票交易时扣去印花税，然后上交国家税务局。印花税收取的费率是：千分之一，单边收取，卖出股票收向卖出方收取。印花税费率不可调整，全国统一征收。
func StampTax(amt float64, param *Params) float64 {
	v := math.Abs(amt) * param.StampTax
	return util.Round(v, 2)
}

// TradeFee 佣金 佣金属于你的开户证券公司收取的经纪费用。根据国家的规定，券商佣金收费标准 最高不得超过千分之三，低于5元按五元收取，最低收取5元。可以说佣金是各家证券公司互相争夺客户的必杀技，大部分券商佣金费率在：千分之一到千分之三之间。有的甚至低至万分之二点五。佣金以每笔股票成交金额计算，不成交不收取，买卖双边征收。佣金越低越好，开户时可以券商谈判。
func TradeFee(amt float64, param *Params) float64 {
	fee := math.Abs(amt) * param.TradeFeeRate
	if fee < 5 {
		// fee = 5
		return 5
	}
	return util.Round(fee, 2)
}

// TransferFee 过户费，中国网财经7月30日讯 中国结算30日发布关于调整沪股通交易过户费收费标准的通知，自8月1日起，沪股通交易过户费收费标准由原“按成交股份面值的0.6‰元人民币计收(双向收取)”修改为“按成交金额0.02‰元人民币计收(双向收取)”
func TransferFee(amt float64, param *Params) float64 {
	fee := math.Abs(amt) * param.TransferFeeRate
	return util.Round(fee, 2)
}

// MarketFee 交易所经手费,包含在券商的手续费里
// 自2015年8月1号起，上海交易所A股规费/深圳交易所A股规费为：经手费0.00487% （双向收），证管费0.002% （双向收），合计为交易金额的0.00687%；
func MarketFee(amt float64, param *Params) float64 {
	return math.Abs(amt) * param.MarketFeeRate
}
