package trader

import (
	"encoding/json"
	"io"
	"os"

	"github.com/kere/gno"
)

const (
	// TdNormal 正常
	TdNormal = 0
	// TdFailed 失败
	TdFailed = -1
	// TdError 错误
	TdError = -2
	// TdSuccess 成功
	TdSuccess = 1
)

type Params struct {
	BuyFreezeDn  int // 买入冻结周期
	SellFreezeDn int // 卖出冻结周期

	BaseRate        float64 // 基准税率
	TradeFeeRate    float64 // 交易税
	TransferFeeRate float64 // 过户费
	MarketFeeRate   float64 // 交易所经手费
	StampTax        float64 // 印花税
	// Slippage            float64 // 交易滑点百分比
	AMaxBuyMoneyEvery float64 //最大单次购买金额限制, 如果最大买入金额==0，则以最大买入数量为主
	AMaxBuyVolEvery   int     //最大单次购买数量限制, 如果两个限制都有，则以最小的为主
	AMaxBuyNum        int     //最大购买次数限制
	AMaxHolding       float64 // 最大持有金额限制

	HMaxHoldingND1       int     // 最大持有的日线周期
	HMaxHoldingWinRatio  float64 // 最大持有周期内，获利了结比率
	HMaxHoldingLostRatio float64 // 最大持有周期内，亏损平仓比率
}

// LoadParams
func LoadParams(filename string) (Params, error) {
	p := Params{}
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return p, err
	}
	b, err := io.ReadAll(file)
	if err != nil {
		return p, err
	}
	err = json.Unmarshal(b, &p)
	return p, err
}

// DefaultParams
// AMaxBuyMoneyEvery 每次买入金额限制
// AMaxBuyNum 每次买入数量限制
// 如果最大买入金额==0，则以最大买入数量为主
// 如果两个限制都有，则以最小的为主
func DefaultParams() Params {
	c := gno.GetConfig().GetConf("trade")
	return Params{
		// 基准税率
		BaseRate:          c.DefaultFloat("base_rate", 0.03),
		AMaxBuyMoneyEvery: 50000,
		AMaxBuyNum:        1,
		AMaxHolding:       100000, //最大持有股票资产闲置

		// 交易滑点百分比
		// Slippage: c.DefaultFloat("slippage", 0.002),
		// StampTax 印花税
		StampTax: c.DefaultFloat("stamp_tax", 0.001),
		//交易费率
		TradeFeeRate: c.DefaultFloat("trade_fee_rate", 0.00025),
		// 过户费 2015年8月1日起 [1]  A股交易过户费由沪市按照成交面值0.3‰、深市按照成交金额0.0255‰向买卖双方投资者分别收取，统一调整为按照成交金额0.02‰向买卖双方投资者分别收取。交易过户费为中国结算收费，证券经营机构不予留存。
		TransferFeeRate: c.DefaultFloat("transfer_fee_rate", 0.00002),

		// MarketFeeRate 交易规费
		// 证管费：成交金额的0.002%双向收取
		// 证券交易经手费：A股，按成交金额的0.00487%双向收取；B股，按成交额0.00487%双向收取；基金，上海证券交易所按成交额双边收取0.0045%，深圳证券交易所按成交额0.00487%双向收取；权证，按成交额0.0045%双向收取。
		// A股项收费合计称为交易规费，合计收取成交金额的0.00687%，包含在券商交易佣金中。
		MarketFeeRate: c.DefaultFloat("market_fee_rate", 0.0000678),
	}
}

// FixBuyVol 规格化volume
func FixBuyVol(amt, price float64) int {
	vol := int(amt / price)
	return vol / 100 * 100
}
