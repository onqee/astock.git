package trader

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/lib/data/bars"
	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

var (
// errPriceLimit     = errors.New("涨跌停限制")
// errNotEnoughMoney = errors.New("没有足够的可用资金")
// errNotEnoughVol   = errors.New("没有足够的数量")
// errInFreezeDn     = errors.New("在冷冻时间内")
)

// Holding 持股
type Holding struct {
	Code    string
	Actions []Action
	Volumn  int `name:"量" inline:"tab"` // 量
	// 每次Action后，计算成本价
	CostPrice float64 `name:"成本价" ` // 成本价

	IsClosed bool

	Data struct {
		TotalBuy, TotalSell                            float64
		TotalFee                                       float64 `name:"TotalFee" inline:"tab"`
		BeginDate, LastDate, LastBuyDate, LastSellDate int64
		BuyNum, SellNum                                int
		EndProfit                                      float64 `name:"盈利" inline:"tab"` // 结束时盈利
		LastCost                                       float64 `name:"成本" inline:"tab"` // 用了多少钱
	}
}

func NewHolding(code string) Holding {
	h := Holding{
		Code: code,
		// Actions: getActions(),
	}
	return h
}

// // EndDo run after finish
// func (h *Holding) EndDo() {
// 	putActions(h.Actions)
// }

// CheckBuy by holdings
func (h *Holding) CheckBuy(acc *Account, vol int, price float64, date int64) bool {
	if vol == 0 {
		return false
	}

	if h.Data.BuyNum >= acc.Params.AMaxBuyNum {
		return false
	}

	m := float64(vol) * price
	fee := BuyFee(h.Code, m, &acc.Params)
	amt := m + fee
	if acc.initMoney > 0 && acc.Money < amt {
		return false
	}

	return true
}

// Buy by holdings
func (h *Holding) Buy(acc *Account, vol int, price float64, date int64) bool {
	m := float64(vol) * price
	fee := BuyFee(h.Code, m, &acc.Params)
	amt := m + fee
	if !acc.Use(h.Code, date, amt) {
		return false
	}

	// 计算成本价
	volumn := float64(h.Volumn)
	cprice := h.CostPrice
	h.CostPrice = (h.CostPrice*volumn + amt) / (volumn + float64(vol))
	h.Volumn += vol

	a := Action{IType: vars.ActionBuy, Date: date, Code: h.Code,
		Price: price, Money: amt, Fee: fee, Volumn: vol,
		CostPriceBefore: cprice, CostPriceAfter: h.CostPrice, HoldingVolAfter: h.Volumn}
	h.Actions = append(h.Actions, a)

	h.Data.LastCost = h.CostPrice * float64(h.Volumn) // 实际成本
	if h.Data.BeginDate == 0 {
		h.Data.BeginDate = date
	}
	// h.Data.BuyAmt += amt
	h.Data.LastBuyDate = date
	h.Data.LastDate = date
	h.Data.BuyNum++
	h.Data.TotalFee += fee
	h.Data.TotalBuy += amt

	return true
}

// CheckSell by holding
func (h *Holding) CheckSell(acc *Account, vol int, price float64, date int64) bool {
	if h.Volumn == 0 {
		return false
	}
	return true
}

// Sell by holding
// amt 为实际付出的资金
func (h *Holding) Sell(acc *Account, vol int, price float64, date int64) {
	if vol > h.Volumn {
		vol = h.Volumn
	}
	volumn := float64(h.Volumn)
	m := float64(vol) * price
	fee := SellFee(h.Code, m, &acc.Params)
	amt := m - fee
	acc.Put(h.Code, date, amt)

	cprice := h.CostPrice
	// 计算成本价
	if vol == h.Volumn {
		h.Data.LastCost = h.CostPrice * float64(h.Volumn)   // 实际成本
		h.Data.EndProfit = (price-h.CostPrice)*volumn - fee //结束时的盈利
		h.IsClosed = true
		h.CostPrice = 0
		h.Volumn = 0
	} else {
		h.CostPrice = (h.CostPrice*volumn - amt) / (volumn - float64(vol))
		h.Volumn -= vol
		h.Data.LastCost = h.CostPrice * float64(h.Volumn) // 实际成本
	}

	// 设置参数
	a := Action{IType: vars.ActionSell, Date: date, Code: h.Code,
		Price: price, Money: amt, Fee: fee, Volumn: vol,
		CostPriceBefore: cprice, CostPriceAfter: h.CostPrice, HoldingVolAfter: h.Volumn}
	h.Actions = append(h.Actions, a)

	h.Data.LastSellDate = date
	h.Data.LastDate = date
	h.Data.SellNum++
	h.Data.TotalFee += fee
	h.Data.TotalSell += amt
}

// DoFinish 是否有最大持有时间限制；最大涨跌幅
func (h *Holding) DoFinish(acc *Account, bar *bars.Bar, date int64) bool {
	if h.Volumn == 0 {
		return false
	}
	bi, _ := bar.Search(h.Data.BeginDate)
	index, _ := bar.Search(date)
	// a: 如果ND有值，则结束长度为i+ND
	nd := acc.Params.HMaxHoldingND1
	if nd == 0 && acc.Params.HMaxHoldingWinRatio == 0 && acc.Params.HMaxHoldingLostRatio == 0 {
		return false
	}

	count := index + 1
	if nd > 0 {
		count = bi + nd
		if count > index+1 {
			count = index + 1
		}
	}

	// 检查最大涨跌幅
	maxR := -math.MaxFloat32 // 最大盈利比率
	minR := math.MaxFloat32  // 最大亏损比率
	closeI := bar.FieldI(vars.FieldClose)

	for i := bi; i < count; i++ {
		price := bar.Columns[closeI][i]
		// c: 最大获利，卖出
		ratio := (price - h.CostPrice) / h.CostPrice
		if maxR < ratio {
			maxR = ratio
		}

		if acc.Params.HMaxHoldingWinRatio != 0 && maxR > acc.Params.HMaxHoldingWinRatio {
			h.Sell(acc, h.Volumn, price, bar.Dates[i])
			return true
		}

		// d: 最大亏损，卖出
		if minR > ratio {
			minR = ratio
		}

		if acc.Params.HMaxHoldingLostRatio != 0 && minR < -acc.Params.HMaxHoldingLostRatio {
			h.Sell(acc, h.Volumn, price, bar.Dates[i])
			return true
		}
	}

	// 超过最大持有周期
	if nd > 0 && bi+nd <= index {
		price := bar.Columns[closeI][bi+nd]
		h.Sell(acc, h.Volumn, price, bar.Dates[bi+nd])
		return true
	}

	return false
}

type HProfile struct {
	Profit      float64 `name:"利润"`
	ProfitRatio float64 `name:"收益率"`
	Cost        float64 `name:"持仓成本"`

	NumEven int `name:"保本次数"` //保本交易总次数
	NumLoss int `name:"亏损次数"` //亏损交易总次数
	NumWin  int `name:"获胜次数"` //盈利交易总次数
}

// ProfileOn 持股简报
// if !h.IsClosed and then profit on date
func (h *Holding) ProfileOn(date int64) HProfile {
	p := HProfile{}
	if h.IsClosed {
		p.Profit = h.Data.EndProfit
	} else {
		bar := dget.Bar(h.Code, vars.PD1)
		price, _ := bar.ValueOn(vars.FieldClose, date)
		p.Profit += (price - h.CostPrice) * float64(h.Volumn)
	}
	p.Cost = h.Data.LastCost
	p.ProfitRatio = p.Profit / p.Cost

	if p.ProfitRatio > 0.015 {
		p.NumWin++
	} else if p.ProfitRatio < -0.015 {
		p.NumLoss++
	} else {
		p.NumEven++
	}

	return p
}

// Print
func PrintHolding(h Holding, isPrintActions bool) {
	fmt.Println(util.PrintLine)
	fmt.Println("代码：", h.Code)
	if !h.IsClosed {
		util.PrintObj(h)
		fmt.Println(util.PrintDotted)
	}
	if isPrintActions {
		n := len(h.Actions)
		for i := 0; i < n; i++ {
			PrintAction(&h.Actions[i])
		}
	}

	util.PrintObj(h.Data)
	if h.IsClosed {
		fmt.Printf("OK: %s - %s	利润率：%.4f%%\n", datescore.Score2DateStr(h.Data.BeginDate), datescore.Score2DateStr(h.Data.LastDate), h.Data.EndProfit/h.Data.LastCost*100)
	} else {
		fmt.Printf("XX: %s - %s\n", datescore.Score2DateStr(h.Data.BeginDate), datescore.Score2DateStr(h.Data.LastDate))
	}
}
