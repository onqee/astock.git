package trader

import (
	"fmt"
	"time"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/runner"
	"github.com/kere/gno/libs/util"
)

func BatchAna(codes []string, acc *Account, rule runner.Rule, ana Analyser, dateA, dateB int64, csvName string) {
	now := time.Now()
	hhs := ana.ScoresToHodingss(acc, rule, codes, csvName)
	count := len(codes)
	profiles := make([]Profile, 0, count)
	for i := 0; i < count; i++ {
		code := codes[i]
		hs := hhs[i]

		if len(hs) > 0 {
			profiles = append(profiles, hs.ClosedProfile())
		}

		dget.ReleaseAll(code)
	}

	pp := CalcSumProfile(profiles)
	util.PrintObj(pp)

	fmt.Println("\nfinished:", time.Now().Sub(now))
	fmt.Println(util.PrintLine)
}
