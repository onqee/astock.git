package trader

import (
	"fmt"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

// Profile 评测简要
type Profile struct {
	StartDate int64
	EndDate   int64

	Fee    float64 `name:"交易费" inline:"tab2" suffix:"元"`
	Profit float64 `name:"收益" inline:"tab2" suffix:"元"`

	BuyNum   int `name:"买次数" inline:"tab2"`
	SellNum  int `name:"卖次数" inline:"tab2"`
	TradeNum int `name:"交易次数"`

	NumEven int `name:"保本次数" inline:"tab2"` //保本交易总次数
	NumLoss int `name:"亏损次数" inline:"tab2"` //亏损交易总次数
	NumWin  int `name:"获胜次数"`               //盈利交易总次数

	WinRatio     float64 `name:"胜率" inline:"tab2"`
	WinLossRatio float64 `name:"盈亏%" end:"break line"`

	ProfitRatio  float64 `name:"收益率" inline:"tab2"`
	MaxUsedMoney float64 `name:"最大支出"`
}

type SumProfile struct {
	Profile

	BetaHS300 float64 `name:"BetaHS300%" inline:"tab2"`
	BetaSH    float64 `name:"BetaSH%" inline:"tab2"`
	BetaSZ    float64 `name:"BetaSZ%"`
}

func CalcSumProfile(arr []Profile) SumProfile {
	count := len(arr)
	if count == 0 {
		return SumProfile{}
	}
	p := arr[0]
	var ratio float64
	for i := 1; i < count; i++ {
		p = p.Plus(arr[i])
		ratio += p.ProfitRatio
	}
	p.ProfitRatio = ratio / float64(count)
	return p.CalcBeta()
}

// ProfilePlus 对关闭的持仓做交易简报分析
func (p *Profile) Plus(pp Profile) Profile {
	ppp := Profile{
		Fee:    p.Fee + pp.Fee,
		Profit: p.Profit + pp.Profit,
		// Cost:     p.Cost + pp.Cost,
		BuyNum:   p.BuyNum + pp.BuyNum,
		SellNum:  p.SellNum + pp.SellNum,
		TradeNum: p.TradeNum + pp.TradeNum,
		NumEven:  p.NumEven + pp.NumEven,
		NumLoss:  p.NumLoss + pp.NumLoss,
		NumWin:   p.NumWin + pp.NumWin,
	}
	// ppp.ProfitRatio = ppp.Profit / ppp.Cost
	ppp.WinRatio = float64(ppp.NumWin) / float64(ppp.TradeNum)
	ppp.WinLossRatio = float64(ppp.NumWin) / float64(ppp.NumLoss)

	if p.StartDate < pp.StartDate {
		ppp.StartDate = p.StartDate
	} else {
		ppp.StartDate = pp.StartDate
	}
	if p.EndDate < pp.EndDate {
		ppp.EndDate = pp.EndDate
	} else {
		ppp.EndDate = p.EndDate
	}

	return ppp
}

// ClosedProfile 对关闭的持仓做交易简报分析
func (hs Holdings) ClosedProfile() Profile {
	p := Profile{}
	count := len(hs)
	if count == 0 {
		return p
	}
	var buyAmt, sellAmt float64
	for i := 0; i < count; i++ {
		item := hs[i]
		if p.StartDate == 0 && len(item.Actions) != 0 {
			p.StartDate = item.Actions[0].Date
		}
		p.EndDate = item.Data.LastDate

		if !item.IsClosed {
			continue
		}

		p.Fee += item.Data.TotalFee
		p.BuyNum += item.Data.BuyNum
		p.SellNum += item.Data.SellNum
		hp := item.ProfileOn(0)
		p.Profit += hp.Profit
		p.NumWin += hp.NumWin
		p.NumLoss += hp.NumLoss
		p.NumEven += hp.NumEven
		buyAmt += item.Data.TotalBuy
		sellAmt += item.Data.TotalSell
	}

	p.TradeNum = p.BuyNum + p.SellNum
	p.ProfitRatio = (sellAmt - buyAmt) / buyAmt
	p.WinRatio = float64(p.NumWin) / float64(count)

	return p
}

func PrintProfile(p Profile) {
	fmt.Println("Profile:", datescore.Score2DateStr(p.StartDate), "->", datescore.Score2DateStr(p.EndDate))
	util.PrintObj(p)
}

func (p *Profile) CalcBeta() SumProfile {
	pp := SumProfile{}
	if p.EndDate <= p.StartDate {
		return pp
	}

	bar := dget.Bar(vars.Code000300, vars.PD1)
	pA, _ := bar.ValueOn(vars.FieldClose, p.StartDate)
	pB, _ := bar.ValueOn(vars.FieldClose, p.EndDate)
	pp.BetaHS300 = (pB - pA) / pA

	bar = dget.Bar(vars.Code000001, vars.PD1)
	pA, _ = bar.ValueOn(vars.FieldClose, p.StartDate)
	pB, _ = bar.ValueOn(vars.FieldClose, p.EndDate)
	pp.BetaSH = (pB - pA) / pA

	bar = dget.Bar(vars.Code399001, vars.PD1)
	pA, _ = bar.ValueOn(vars.FieldClose, p.StartDate)
	pB, _ = bar.ValueOn(vars.FieldClose, p.EndDate)
	pp.BetaSZ = (pB - pA) / pA

	return pp
}
