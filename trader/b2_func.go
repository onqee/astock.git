package trader

import (
	"gitee.com/onqee/astock/lib/data/vars"
)

func PointSelectFunc(ids []int) int {
	return ids[len(ids)/2]
}

func DefaultActionFunc(row []float64) int {
	if row[0]+row[2] > 0.99 {
		return vars.ActionSell
	}
	if row[0]+row[2] < -0.99 {
		return vars.ActionBuy
	}
	return vars.ActionNone
}

func DefaultCodesSortFunc(items []StatItem) {
	// l := len(items)
	// codes := util.GetStrings(l)
	// bar := dget.Bar(vars.Code399001, period)
	// dat := statItemsToDatP(items, vars.FieldsVal2)
	// defer series.PutA(&dat)
	// for i := 0; i < l; i++ {
	// 	item := items[i]
	// 	dat.Columns[1][i] = dget.TurnoverRate(item.Code, item.Date)
	// }
	// series.ReverseA(&dat, vars.FieldVal2)
}
