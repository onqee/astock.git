package trader

import (
	"fmt"
	"sync"

	"gitee.com/onqee/astock/lib/data/series/datescore"
	"gitee.com/onqee/astock/lib/data/vars"
)

type Action struct {
	IType int
	Date  int64  // 交易时间
	Code  string // 股票代码

	Price  float64 // 交易价格
	Money  float64 // 交易金额
	Fee    float64 // 交易税费
	Volumn int     // 交易数量

	HoldingVolAfter int     // 交易后，持有的数量
	CostPriceAfter  float64 //交易后的成本价
	CostPriceBefore float64 //交易前的成本价
}

var actionsPool = sync.Pool{}

func getActions() []Action {
	v := actionsPool.Get()
	capN := 20
	if v == nil {
		return make([]Action, 0, capN)
	}
	row := v.([]Action)
	return row
}

// putActions to pool
func putActions(r []Action) {
	if cap(r) == 0 {
		return
	}
	actionsPool.Put(r[:0])
}

func PrintAction(a *Action) {
	str := "S"
	if a.IType == vars.ActionBuy {
		str = "B"
	}
	fmt.Printf("%s %s --\t价格%f * %d + 费%f = %.6f元\n", str, datescore.Score2DateStr(a.Date), a.Price, a.Volumn, a.Fee, a.Money)
	fmt.Printf("\t\t持仓%d * 成本价%f = %.6f\n", a.HoldingVolAfter, a.CostPriceAfter, float64(a.HoldingVolAfter)*a.CostPriceAfter)
	fmt.Printf("\t\t获利%.6f\n\n", (a.Price-a.CostPriceAfter)*float64(a.HoldingVolAfter))
}
