package trader

import (
	"fmt"
	"math"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno/libs/util"
)

// Holdings
type Holdings []Holding

// Print
func PrintHoldings(hs Holdings, isPrintActions bool) {
	count := len(hs)
	fmt.Println("总共持仓：", count)
	for i := 0; i < count; i++ {
		PrintHolding(hs[i], isPrintActions)
	}
}

// ProfitDatP 收益关键点
// 在目标数据下计算收益率
func (hs Holdings) ProfitDatP(targetCode string) *series.A {
	result := series.GetA(vars.FieldsVal2)

	count := len(hs)
	var dateA int64 = math.MaxInt64
	var dateB int64
	barIds := util.GetInts(count) // 每个bar的数据索引
	defer util.PutInts(barIds)
	finished0 := series.GetRow(count) //储存利润
	defer series.PutRow(finished0)
	finished1 := series.GetRow(count) //储存成本
	defer series.PutRow(finished1)

	for i := 0; i < count; i++ {
		item := hs[i]
		//找到bar的计算区间
		if dateA > item.Data.BeginDate {
			dateA = item.Data.BeginDate
		}
		if dateB < item.Data.LastDate {
			dateB = item.Data.LastDate
		}
		barIds[i] = -1
	}

	row := series.GetRow(2)
	defer series.PutRow(row)

	bar := dget.Bar(targetCode, vars.PD1).Range(dateA, dateB)
	l := bar.Len()
	closeI := bar.FieldI(vars.FieldClose)

	// 循环观察标准bar
	for i := 0; i < l; i++ {
		date := bar.Dates[i]
		//1: 循环每一个“持仓holding”
		for k := 0; k < count; k++ {
			item := hs[k]
			if date < item.Data.BeginDate { //忽略不再范围内的holding
				continue
			}

			stock := dget.Bar(item.Code, vars.PD1)
			//2: 持股holding已经结束
			if item.Volumn == 0 && date >= item.Data.LastDate {
				if finished0[k] == 0 || finished1[k] == 0 {
					// 储存：利润、成本
					finished0[k] = item.Data.EndProfit //利润
					finished1[k] = item.Data.LastCost  // 成本
				}
				row[0] += finished0[k]
				row[1] += finished1[k]
				continue
			}

			if barIds[k] < 0 { // 缓存id索引
				barIds[k], _ = stock.Search(date)
			}
			index := barIds[k]
			date0 := stock.Dates[index]
			//3: 股票停牌, 利润按最后获利计算
			if date != date0 {
				fmt.Printf("出错！目标股票%s可能停盘。当前计算日期%d != 股票日期%d\n", item.Code, date, date0)
				//按照上一次的数据
			}

			//4: 利润计算
			// price := stock.ValueAt(vars.FieldClose, index)
			price := stock.Columns[closeI][index]
			n := len(item.Actions)
			var action *Action
			for c := n - 1; c > -1; c-- { // 找到对应区间的action
				if date < item.Actions[c].Date {
					continue
				}
				action = &item.Actions[c]
				break
			}

			if action == nil { //出错，没有找到相应的Action
				fmt.Println("出错，没有找到相应的Action")
				continue
			}
			row[0] += (price - action.CostPriceAfter) * float64(action.HoldingVolAfter)
			row[1] += action.CostPriceAfter * float64(action.HoldingVolAfter)

			// 缓存的ids++
			barIds[k]++
		}

		result.AddRow0(date, row)
	}

	return &result
}
