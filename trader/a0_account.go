package trader

import "sync"

// Account 账户
type Account struct {
	Params       Params
	initMoney    float64
	Money        float64
	MaxUsedMoney float64
	locker       sync.Mutex
}

// NewAccount
func NewAccount(money float64) *Account {
	a := Account{initMoney: money, Money: money, MaxUsedMoney: money, Params: DefaultParams()}
	return &a
}

// Use
func (a *Account) Use(code string, date int64, money float64) bool {
	if a.initMoney > 0 && a.Money < money {
		return false
	}
	// a.locker.Lock()
	a.Money -= money
	// a.locker.Unlock()
	return true
}

// PutByC
func (a *Account) Put(code string, date int64, money float64) {
	// a.locker.Lock()
	a.Money += money
	// a.locker.Unlock()
}
