module gitee.com/onqee/astock/trader

go 1.16

replace (
	gitee.com/onqee/astock/compare => C:/Works/go/project/src/gitee.com/onqee/astock/compare
	gitee.com/onqee/astock/fmls => C:/Works/go/project/src/gitee.com/onqee/astock/fmls
	gitee.com/onqee/astock/fmls/macd => C:/Works/go/project/src/gitee.com/onqee/astock/fmls/macd
	gitee.com/onqee/astock/fmls/mval => C:/Works/go/project/src/gitee.com/onqee/astock/fmls/mval
	gitee.com/onqee/astock/fmls/tdd => C:/Works/go/project/src/gitee.com/onqee/astock/fmls/tdd
	gitee.com/onqee/astock/fmls/trendline => C:/Works/go/project/src/gitee.com/onqee/astock/fmls/trendline
	gitee.com/onqee/astock/fmls/wave => C:/Works/go/project/src/gitee.com/onqee/astock/fmls/wave

	gitee.com/onqee/astock/lib/data/bars => C:/Works/go/project/src/gitee.com/onqee/astock/lib/data/bars
	gitee.com/onqee/astock/lib/data/dget => C:/Works/go/project/src/gitee.com/onqee/astock/lib/data/dget
	gitee.com/onqee/astock/lib/data/series => C:/Works/go/project/src/gitee.com/onqee/astock/lib/data/series
	gitee.com/onqee/astock/lib/data/series/datescore => C:/Works/go/project/src/gitee.com/onqee/astock/lib/data/series/datescore
	gitee.com/onqee/astock/lib/data/vars => C:/Works/go/project/src/gitee.com/onqee/astock/lib/data/vars
	gitee.com/onqee/astock/lib/fmlib => C:/Works/go/project/src/gitee.com/onqee/astock/lib/fmlib

	gitee.com/onqee/astock/lib/mathlib => C:/Works/go/project/src/gitee.com/onqee/astock/lib/mathlib
	gitee.com/onqee/astock/lib/stocklib => C:/Works/go/project/src/gitee.com/onqee/astock/lib/stocklib
	gitee.com/onqee/astock/lib/stocklib/linear => C:/Works/go/project/src/gitee.com/onqee/astock/lib/stocklib/linear
	gitee.com/onqee/astock/lib/tools/codetool => C:/Works/go/project/src/gitee.com/onqee/astock/lib/tools/codetool
	gitee.com/onqee/astock/runner => C:/Works/go/project/src/gitee.com/onqee/astock/runner

	github.com/kere/gno => C:/Works/go/project/src/github.com/kere/gno
	github.com/kere/gno/db => C:/Works/go/project/src/github.com/kere/gno/db
	github.com/kere/gno/httpd => C:/Works/go/project/src/github.com/kere/gno/httpd
	github.com/kere/gno/libs/cache => C:/Works/go/project/src/github.com/kere/gno/libs/cache
	github.com/kere/gno/libs/conf => C:/Works/go/project/src/github.com/kere/gno/libs/conf
	github.com/kere/gno/libs/crypto => C:/Works/go/project/src/github.com/kere/gno/libs/crypto
	github.com/kere/gno/libs/i18n => C:/Works/go/project/src/github.com/kere/gno/libs/i18n
	github.com/kere/gno/libs/log => C:/Works/go/project/src/github.com/kere/gno/libs/log
	github.com/kere/gno/libs/myerr => C:/Works/go/project/src/github.com/kere/gno/libs/myerr
	github.com/kere/gno/libs/redis => C:/Works/go/project/src/github.com/kere/gno/libs/redis
	github.com/kere/gno/libs/util => C:/Works/go/project/src/github.com/kere/gno/libs/util
)

require (
	gitee.com/onqee/astock/compare v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/data/bars v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/data/dget v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/data/series v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/data/series/datescore v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/data/vars v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/lib/tools/codetool v0.0.0-00010101000000-000000000000
	gitee.com/onqee/astock/runner v0.0.0-00010101000000-000000000000
	github.com/kere/gno v0.0.0-00010101000000-000000000000
	github.com/kere/gno/libs/conf v0.0.0-00010101000000-000000000000
	github.com/kere/gno/libs/util v0.0.0-00010101000000-000000000000
	github.com/lib/pq v1.10.2
)
