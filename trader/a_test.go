package trader

import (
	"testing"

	"gitee.com/onqee/astock/lib/data/dget"
	"gitee.com/onqee/astock/lib/data/series"
	"gitee.com/onqee/astock/lib/data/vars"
	"github.com/kere/gno"
	"github.com/kere/gno/libs/util"
	_ "github.com/lib/pq"
)

func init() {
	gno.Init("../app.conf")
	dget.DefaultStartDay = 1190101
	dget.DefaultStartDayM5 = 1190101
	dget.DefaultEndDay = 1210501
	dget.DefaultEndDayM5 = 1210501
}

func TestHolding(t *testing.T) {
	acc := NewAccount(500000)
	// 测试样板在Excel里
	// 1: 构建SZ的持仓记录
	h1 := NewHolding(vars.Code399001)
	h1.Buy(acc, 2, 7626.24, 11901211500)
	h1.Buy(acc, 3, 9704.33, 11903111500)
	h1.Sell(acc, 1, 10132.34, 11904121500)

	if h1.Actions[1].CostPriceAfter != 8875.728 {
		PrintAction(&h1.Actions[1])
		t.Fatal()
	}
	if h1.Actions[2].CostPriceAfter != 8565.4075 {
		PrintAction(&h1.Actions[2])
		t.Fatal()
	}
	if h1.Data.LastCost != 34261.63 {
		t.Fatal()
	}
	// n := len(h1.Actions)
	// for i := 0; i < n; i++ {
	// 	PrintAction(&h1.Actions[i])
	// }
	// 2: 计算SZ的收益曲线，以000001作为参考
	hs := Holdings([]Holding{h1})
	dat := hs.ProfitDatP(vars.Code000001)
	row, _ := dat.RowOnP(11903291500)
	defer series.PutRow(row)
	if row[0]-5155.66 > 0.01 || row[1]-44378.64 > 0.01 {
		series.Print(dat)
		t.Fatal(row)
	}
	series.PutA(dat)

	// 3: 构建HS300的持仓记录
	h2 := NewHolding(vars.Code000300)
	h2.Buy(acc, 10, 3185.64, 11901231500)
	h2.Buy(acc, 12, 3749.71, 11903011500)
	h2.Sell(acc, 7, 4075.43, 11904091500)
	h2.Sell(acc, 15, 3889.28, 11904261500)

	if h2.Data.EndProfit-9883.22 > 0.01 || h2.Data.LastCost-48381.89 > 0.01 {
		t.Fatal(h2.Data.EndProfit, h2.Data.LastCost)
	}

	// 4: 计算HS300的收益曲线，以000001作为参考
	hs = Holdings([]Holding{h2})
	dat = hs.ProfitDatP(vars.Code000001)
	dat.RowOn(11903291500, row)
	if row[0]-8317.81 > 0.01 || row[1]-76873.67 > 0.01 {
		series.Print(dat)
		t.Fatal(row)
	}
	dat.RowOn(11904151500, row)
	if row[0]-11250.91 > 0.01 || row[1]-48381.89 > 0.01 {
		series.Print(dat)
		t.Fatal(row)
	}
	series.PutA(dat)
	p := hs.ClosedProfile() //对结束的持仓做交易分析
	if p.Profit-9883.22 > 0.01 {
		util.PrintObj(p)
		t.Fatal()
	}

	// 5: 计算合并的收益曲线，以000001作为参考
	hs = Holdings([]Holding{h1, h2})
	dat = hs.ProfitDatP(vars.Code000001)
	row, _ = dat.RowOnP(11903291500)
	if row[0]-13473.47 > 0.01 || row[1]-121252.31 > 0.01 {
		series.Print(dat)
		t.Fatal(row)
	}
}
